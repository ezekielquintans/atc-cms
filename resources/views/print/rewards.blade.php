
<!DOCTYPE html moznomarginboxes mozdisallowselectionprint>
<html>
<head>
	<title></title>
	<style type="text/css" media="print">
	    @page
	    {
	        size: auto;   /* auto is the initial value */
	        margin: 3mm;  /* this affects the margin in the printer settings */
	    }
	</style>
</head>
<body>
	@forelse($rewardsGiven as $row)
	<table style="width:298px;font-size:11px;font-family:verdana;">
		<thead>
			<th colspan="2" style="font-weight:bold;text-transform:uppercase;line-height:15px;">
				CONCIERGE - ALABANG TOWN CENTER<br/>
				UPPER GROUND LEVEL, AYALA ALABANG,<br/>
				MUNTINLUPA CITY<br/>
				ONE AYALA HOTLINE: 795-9595<br/>
			</th>
		</thead>
		<tbody>
			<tr>
				<td colspan="2"><hr/></td>
			</tr>
			<tr>
				<td colspan="2" style="text-align:center;text-transform:uppercase;"><b>{{$row->promo->name}}</b></td>
			</tr>
			<tr>
				<td colspan="2"><hr/></td>
			</tr>
			<tr>
				<td><b>DATE:</b></td>
				<td>{{date('Y-m-d H:i:s')}}</td>
			</tr>
			<tr>
				<td><b>TRANS. ID:</b></td>
				<td>{{strtoupper($token)}}</td>
			</tr>
			<tr>
				<td colspan="2"><hr/></td>
			</tr>
			<tr>
				<td colspan="2" style="text-align:center;"><b>CUSTOMER INFO</b></td>
			</tr>
			<tr>
				<td><b>NAME:</b></td>
				<td>{{strtoupper($row->user->last_name . ", " . $row->user->first_name . " " . $row->user->middle_name)}}</td>
			</tr>
			<tr>
				<td><b>PROMO:</b></td>
				<td>{{$row->promo->name}}</td>
			</tr>
			<tr>
				<td><b>VENUE:</b></td>
				<td>{{$row->rewards->venue}}</td>
			</tr>
			<tr>
				<td><b>DATE&TIME:</b></td>
				<td>{{$row->rewards->datetime->toDayDateTimeString()}}</td>
			</tr>
			<tr>
				<td><b>SERIAL NO:</b></td>
				<td>{{$row->serial_no}}</td>
			</tr>
			<tr>
				<td colspan="2"><hr/></td>
			</tr>
			<td colspan="2" style="text-align:center;font-weight:bold;line-height:15px;">
				<div style="height:100px;width: 100px;margin: auto;">
					<img src="{{asset($row->rewards->featured_img)}}" style="height:100%;width:100%;" />
				</div>
			</td>
			<tr>
				<td colspan="2" style="text-align:center;font-weight:bold;line-height:15px;">
					{{$row->rewards->details}} 
				</td>
			</tr>
			<tr>
				<td colspan="2" style="text-align:center;font-weight:bold;line-height:15px;">
					{{$row->promo->dti_permit}} 
				</td>
			</tr>
			<tr>
				<td colspan="2"><br/></td>
			</tr>
			<tr>
				<td colspan="2"><hr style="border-top: dotted 1px;" /></td>
			</tr>
			<tr>
				<td colspan="2"><br/></td>
			</tr>
		</tbody>
	</table>
	@empty
	@endforelse
	<script type="text/javascript">
		window.print();
	</script>
</body>
</html>
