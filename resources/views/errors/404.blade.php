<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" type="image/png" href="{{asset('public/landingv2/img/favicon-32x32.png')}}" sizes="32x32" />
    <link rel="icon" type="image/png" href="{{asset('public/landingv2/img/favicon-16x16.png')}}" sizes="16x16" />
    <!-- Place favicon.ico in the root directory -->
    <!-- text cdn -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Shadows+Into+Light" rel="stylesheet">
    <!--    <link rel="stylesheet" href="css/normalize.css">-->
    <link rel="stylesheet" href="{{asset('public/landingv2/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/landingv2/dist/Remodal-1.1.0/dist/remodal.css')}}">
    <link rel="stylesheet" href="{{asset('public/landingv2/dist/Remodal-1.1.0/dist/remodal-default-theme.css')}}">
    <link rel="stylesheet" href="{{asset('public/landingv2/bower_components/swiper/dist/css/swiper.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/landingv2/css/style.css')}}">
    <script src="{{asset('public/landingv2/js/vendor/modernizr-2.8.3.min.js')}}"></script>
</head>

<body class="">
    <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
    <!-- Add your site or application content here -->
    <section class="ec ec__view ec__view--bg remodal-bg">
        <!-- START : Type 1 Layout -->
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <img class="center-block ec__view__logo" src="{{asset('public/landingv2/img/main-logo.png')}}" alt="">
                    <div class="main-view m-0">
                        <div class="page-heading">
                            <!--<h3 class="text-center">Participants</h3>-->
                            <p>PAGE NOT FOUND</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END : Type 1 Layout -->
        <footer class="ec__footer ec__footer--mod">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center">
                            <p class="copyright">ONE AYALA HOTLINE: 795-9595</p>
                            <p class="copyright">Copyright © {{date('Y')}} Ayala Malls 360. All Rights Reserved</p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </section>
</body>
</html>