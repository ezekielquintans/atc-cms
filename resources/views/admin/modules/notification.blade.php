@if (count($errors) > 0)
    <div class="alert alert-danger">
        Whoops! There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (Session::has('message'))
    <div class="alert {{Session::get('messageClass')}}">
        {{Session::get('message')}}
    </div>
@endif
