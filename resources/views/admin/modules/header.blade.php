<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="req" content="{{ csrf_token() }}">

    <title>Concierge | Alabang Town Center</title>
{{--     <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet"> --}}
    <!-- Bootstrap -->
    <link href="{{asset("public/app/vendors/bootstrap/dist/css/bootstrap.min.css")}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{asset("public/app/vendors/font-awesome/css/font-awesome.min.css")}}" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{asset("public/app/vendors/iCheck/skins/flat/green.css")}}" rel="stylesheet">
    <!-- Switchery -->
    <link href="{{asset("public/app/vendors/switchery/dist/switchery.min.css")}}" rel="stylesheet">
    <!-- Datetimepicker -->
    <link href="{{asset("public/app/vendors/bootstrap-daterangepicker/daterangepicker.css")}}" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="{{asset("public/app/vendors/google-code-prettify/bin/prettify.min.css")}}"" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="{{asset("public/app/css/custom.css")}}" rel="stylesheet">
    <!-- Custom User Style -->
    <link rel="stylesheet" href="{{asset("public/app/css/style.css")}}">
    @yield('styles')
</head>

<body class="nav-md">
    <div class="container body">
        <div class="main_container">
        @include('admin.modules.sidebar')
        @include('admin.modules.topnav')
                    <!-- page content -->
                    <div class="right_col" role="main">
                        <div class="">