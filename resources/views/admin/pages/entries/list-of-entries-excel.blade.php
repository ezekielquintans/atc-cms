@if($download == true)
<table class="table table-striped">
    <tbody>
        <tr>
            <td><strong>COUPON NO.</strong></td>
            <td><strong>FULL NAME</strong></td>
            <td><strong>STORE</strong></td>
            <td><strong>AMOUNT</strong></td>
            <td><strong>RECEIPT NO.</strong></td>
            <td><strong>EMAIL ADDRESS</strong></td>
            <td><strong>CONTACT NO.</strong></td>
            <td><strong>CREATED DATE</strong></td>
        </tr>
        @forelse($entries as $key => $row)
            <tr>
                <td>{{$row->coupon_id}}</td>
                <td>{{$row->last_name . ", " . $row->first_name . " " . $row->middle_name}}</td>
                <td>{{$row->store_name}}</td>
                <td>{{$row->amount}}</td>
                <td>{{$row->receipt_no}}</td>
                <td>{{$row->email}}</td>
                <td>{{$row->contact}}</td>
                <td>{{date('M j, Y', strtotime($row->entry_created))}}</td>
            </tr>
        @empty
            <tr>
                <td colspan="7">No entries yet</td>
            </tr>
        @endforelse
    </tbody>
</table>
@endif