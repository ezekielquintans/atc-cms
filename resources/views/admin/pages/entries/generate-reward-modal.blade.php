 @if(Input::get('tk', '') != "" && count($rewardsGiven) > 0)
        <div id="modal" class="modal show bs-example-modal-sm modal-receipt-view" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><a href="{{URL::to('res/entries/create')}}"><span aria-hidden="true">×</span></a>
                        </button>
                        <img src="{{asset('public/app/img/success.png')}}" alt="" style="margin-bottom:15px;">
                        <h2 class="modal-title" id="myModalLabel2" style="font-size:24px;color:#39b54a">{{$rewardsGiven[0]->rewards->name}} Successful!</h2>
                    <div class="modal-body">
                        <div class="receipt-content" style="max-height:300px;">
                            <div class="row">
                                <div class="col-md-12">
                                    <img class="center-block receipt-logo" src="{{asset('public/app/img/receipt-logo.png')}}" style="margin: 0 auto;" alt="">
                                </div>
                            </div>
                            <form class="form-horizontal form-label-left">
                                <div class="form-group">
                                    <label class="control-label col-md-4" for="last-name">Name:</label>
                                    <div class="col-md-8">
                                        <h2>{{strtoupper($rewardsGiven[0]->user->last_name . ", " . $rewardsGiven[0]->user->first_name . " " . $rewardsGiven[0]->user->middle_name)}}</h2>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4" for="last-name">Venue:</label>
                                    <div class="col-md-8">
                                        <h2>{{$rewardsGiven[0]->rewards->venue ? $rewardsGiven[0]->rewards->venue : ""}}</h2>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4" for="last-name">Date & Time:</label>
                                    <div class="col-md-8">
                                        <h2>{{$rewardsGiven[0]->rewards->datetime->toDayDateTimeString()}}</h2>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4" for="last-name">No. of {{$rewardsGiven[0]->rewards->name}}:</label>
                                    <div class="col-md-8">
                                        <h2>(<span class="total">{{count($rewardsGiven)}}</span>) Entries</h2>
                                    </div>
                                </div>
                            </form>
                            <div class="row text-center">
                                @forelse($rewardsGiven as $list)
                                    <div style="display:inline;">
                                        <h1>{{$list->serial_no}}</h1>
                                    </div>
                                @empty
                                @endforelse
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <!--<img class="center-block receipt-logo" src="{{asset('public/landing/img/promo-logo-2.jpg')}}" alt="">-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <a href="{{URL::to('res/entries/create')}}"><button type="button" class="btn btn-orange btn-block btn-lg btn-print">NEW ENTRY</button></a>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                    <a href="{{URL::to('res/entries/print-rewards?tk=' . $token)}}" target="_blank"><button type="button" class="btn btn-green btn-block btn-lg btn-print">PRINT NOW!</button></a>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif