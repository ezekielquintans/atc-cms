@extends('admin.layouts.admin-layout')
@section('content')
    <div class="overlay">
        Loading member's information, please wait...
    </div>
    <div class="page-title">
        <div class="title_left">
            <h3>Add Entries</h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <form id="user-form" class="form-horizontal form-label-left" method="post" action="{{URL::to('res/entries')}}" enctype="multipart/form-data">
	    <!--USER INFORMATION-->
        <div class="row">
	        <div class="col-md-12 col-sm-12 col-xs-12">
	            <div class="x_panel">
	                <div class="x_title">
	                    <h2>Customer <small>Please fill in the required fields.</small></h2>
	                    <div class="clearfix"></div>
	                </div>
	                <div class="x_content">
	                    @if ($errors->any())
                            <div class="alert alert-danger">
                                <a href="{{URL::to(Request::url())}}" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <h5>Transaction failed due to following errors:</h5>
                                <ul style="margin-left: 20px;">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
	                	<div class="form-group">
		                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 col-sm-offset-3 col-xs-offset-12">
			                    <input type="text" class="form-control" placeholder="Search for..." id="search" autocomplete="off">
		                    </div>
	                	</div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fname">First Name </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="fname" name="first_name" class="form-control col-md-7 col-xs-12 customer-info" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fname">Middle Name </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="mname" name="middle_name" class="form-control col-md-7 col-xs-12 customer-info" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="lname">Last Name </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="lname" name="last_name" required="required" class="form-control col-md-7 col-xs-12 customer-info" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-sm-3 col-xs-12 control-label">Date of Birth</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" class="form-control datetime" name="dob" id="dob" value="" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email Address </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="email" id="email" name="email" class="form-control col-md-7 col-xs-12 customer-info">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Address </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="address" name="address" class="form-control col-md-7 col-xs-12 customer-info" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="contact">Contact No. </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" id="contact" name="contact" class="form-control col-md-7 col-xs-12 customer-info" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Customer ID:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="cusIDDisplay" class="form-control col-md-7 col-xs-12" type="text" readonly="readonly">
                                <input id="cusID" type="hidden" name="cusid">
                            </div>
                        </div>
                        <div class="form-group {{$errors->has('card_type') ? 'has-error' : ""}}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Card Type</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="userCard" type="hidden" name="cusCard">
                                @if($errors->has())
                                    <label>
                                    <input type="checkbox" class="card-type" name="card_type[bpi]" id="card-type-bpi" value="1" {{ old("card_type.bpi") ? "checked" : "" }} />
                                        BPI AMORE
                                     </label>
                                    <label style="margin-left: 20px;">
                                        <input type="checkbox" class="card-type" name="card_type[vip]" id="card-type-vip" value="1" {{ old("card_type.vip") ? "checked" : "" }} />
                                        VIPINOY
                                    </label>
                                @else
                                    <label>
                                        <input type="checkbox" class="card-type" name="card_type[bpi]" id="card-type-bpi" value="1" }} />
                                        BPI AMORE
                                    </label>
                                    <label style="margin-left: 20px;">
                                        <input type="checkbox" class="card-type" name="card_type[vip]" id="card-type-vip" value="1" }} />
                                        VIPINOY
                                    </label>
                                @endif
                                <span style="color:#a94442;font-style: italic;">{{$errors->has('card_type') ? $errors->first('card_type') : ""}}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="promo" class="control-label col-md-3 col-sm-3 col-xs-12">Select Promo:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="sel-promo" class="store form-control" name="promo" required>
                                    <option value="" selected> -- Select Promo-- </option>
                                    @forelse($promos as $promo)
                                        <option value="{{$promo->id}}"
                                            data-type="{{$promo->promo_type}}"
                                            data-rewards_id = "{{$promo->reward_id}}"
                                            data-rewards = "{{isset($promo->rewards) ? $promo->rewards->name : "Premium Item"}}"
                                            data-raffle_premise = "{{$promo->raffle_premise}}"        
                                            data-raffle_accumulation_type = "{{$promo->raffle_accumulation_type}}"        
                                            data-raffle_purchase_requirement = "{{$promo->raffle_purchase_requirement}}"        
                                            data-raffle_multiplier = "{{$promo->raffle_multiplier}}"        
                                            data-premium_premise = "{{$promo->premium_premise}}"        
                                            data-premium_accumulation_type = "{{$promo->premium_accumulation_type}}"        
                                            data-premium_purchase_requirement = "{{$promo->premium_purchase_requirement}}"        
                                            data-premium_ceiling = "{{$promo->premium_ceiling}}"        
                                            data-premium_multiplier = "{{$promo->premium_multiplier}}"        
                                            data-premium_event = "{{$promo->premium_isEvent == 1 ? "1" : "2"}}"        
                                        >{{$promo->name}}</option>
                                    @empty
                                    @endforelse
                                </select>
                            </div>
                        </div>
                        <input type="hidden" name="_token" value="{{ Session::token() }}">
	                </div>
	            </div>
	        </div>
	    </div>
        <!--ENTRY INFORMATION-->
	    <div class="row entry-info hidden">
	        <div class="col-md-12 col-sm-12 col-xs-12">
	            <div class="x_panel">
	                <div class="x_title">
	                    <h2></h2>
	                    <a class="btn btn-orange pull-right" href="#" id="add-receipt"><i class="fa fa-plus"></i> Add Receipt</a>
	                    <div class="clearfix"></div>
	                </div>
                    <div class="row modes-cont">
                        <div class="col-md-12">
                            <p>
                                <input type="radio" class="flat mode" id="modeS" value="S" name="mode" checked="" required />&nbsp Single &nbsp
                                <input type="radio" class="flat mode" id="modeA" value="A" name="mode" />&nbsp Accumulated
                            </p>
                        </div>
                    </div>
	                <div class="x_content" id="coupon-form-container">
                        <div class="row entry-row">
                            <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                <div class="form-group">
                                    <label class="control-label" >Store Name</label>
                                    <div class="">
                                        <select id="heard" class="store form-control" name="store[]" required>
											<option value="" selected> -- Select a Store -- </option>
                                            @forelse($storeListing as $store)
                                                <option data-category = "{{App\Stores::displayCurrentCategory($store)}}">{{$store}}</option>
                                            @empty
                                            @endforelse
										</select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                <div class="form-group">
                                    <label class="control-label" >Category</label>
                                    <input class="form-control category" type="text" readonly/>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                <div class="form-group">
                                    <label class="control-label">Invoice Number</label>
                                    <div class="">
                                        <input type="text" class="invoice-text form-control" required placeholder="Enter invoice number" name="invoice[]">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-12 col-xs-12 form-group amt-grp">
                                <div class="form-group">
                                    <label class="control-label">Amount</label>
                                    <div class="">
                                        <input type="number" class="form-control amount" required placeholder="Enter Amount" name="amount[]">
                                        <input type="hidden" class="form-control coupons" name="coupons[]">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1 col-sm-12 col-xs-12 form-group with-multiplier hidden">
                                <label class="control-label">Multiplier</label>
                                <div class="">
                                    <input type="checkbox" name="multiplier[]" class="multiplier form-control" disabled style="height:23px;width:23px;margin-left: 14px;">
                                </div>
                            </div>
	                    </div>
	                </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <input type="hidden" name="rows" id="norows" value="1">
                            <input type="hidden" name="total_coupons" id="noCoupons">
                        	<input type="hidden" name="total_tickets" id="noTickets">
                            <input type="hidden" name="promo_type" id="promo-type">
                            <input type="hidden" name="is_Event" id="is-event">
                            <input type="hidden" name="rewards_id" id="rewards-id" value="0">
                            <input type="hidden" id="if-mode-disabled" >
                            <button type="button" class="btn btn-success generate-coupon">Generate Coupons</button>
                        </div>
                        <div class="col-md-2 col-sm-12 col-xs-12 rewards-cont hidden">
                            <label class="reward-label" ></label>
                            <span id="rewardsText">0</span>
                        </div>
                        <div class="col-md-2 col-sm-12 col-xs-12 total-amount-cont">
							<label class="" >Total Amount:</label>
							<span id="totalAmount">0</span>
                        </div>
                        <div class="col-md-2 col-sm-12 col-xs-12 total-coupons-cont">
							<label class="" >Total Coupons:</label>
							<span id="totalCoupons">0</span>
                        </div>
                    </div>
	            </div>
	        </div>
            {{-- Insert Modal Here --}}
            @include('admin.pages.entries.reward-info-modal')
	    </div>
	</form>
    @include('admin.pages.entries.generate-coupon-modal')
    @include('admin.pages.entries.generate-reward-modal')
@stop

@section('scripts')
<script type="text/javascript" src="{{asset("public/app/js/chosen_v1.6.2/chosen.jquery.js")}}"></script>
<script type="text/javascript" src="{{asset("public/landing/js/entries.js")}}"></script>
@stop
