<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Alabang Town Center</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Place favicon.ico in the root directory -->
        <!-- text cdn -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Shadows+Into+Light" rel="stylesheet">
        <!--    <link rel="stylesheet" href="css/normalize.css">-->
        <link rel="stylesheet" href="{{asset('public/landingv2/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('public/landingv2/dist/Remodal-1.1.0/dist/remodal.css')}}">
        <link rel="stylesheet" href="{{asset('public/landingv2/dist/Remodal-1.1.0/dist/remodal-default-theme.css')}}">
        <link rel="stylesheet" href="{{asset('public/landingv2/css/style.css')}}">
        <script src="{{asset('public/landingv2/js/vendor/modernizr-2.8.3.min.js')}}"></script>
    </head>
    <body class="">
        <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <!-- Add your site or application content here -->
        <section class="ec ec__view ec__view--bg remodal-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="ec__view__wrpr">
                            <img class="center-block ec__view__logo" src="{{asset('public/landingv2/img/main-logo.png')}}" alt="">
                            <div class="row m-0">
                                <div class="col-md-6 p-0">
                                    <div class="ec__view__i">
                                        <!-- scrollable content -->
                                        <div class="info-content">
                                            <!-- Promo Logo -->
                                            <img class="center-block receipt-logo" src="{{asset($promo->image_url)}}" alt="" height="250" width="auto">
                                            <br/>
                                            <!-- Raffle title -->
                                            <div class="promo-title">
                                                <h1 class="giga" style="font-size: 23px;">One Loyal Shopper</h1>
                                                <h1 class="giga" style="font-size: 23px;">TERMS AND CONDITIONS</h1>
                                            </div>
                                                 <p>&nbsp;</p>
                                                 <p>I. &nbsp; &nbsp; The promo is from August 1 to&nbsp;<span data-term="goog_1627127993">October 31, 2017</span>&nbsp;</p>
                                                 <p>&nbsp;</p>
                                                 <p>II. &nbsp; &nbsp;The promo is open to all shoppers of Alabang Town Center.</p>
                                                 <p>&nbsp;</p>
                                                 <p>III. &nbsp; The shopper who accumulated at least P1000 worth purchase in any merchant store of Alabang Town Center must register and create a promo account and &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; completely fill-out the following details on the on-line registration data base: complete name, address, email address, contact number/s (office phone number, &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; house phone number or mobile phone number) and age.</p>
                                                 <p>&nbsp;</p>
                                                 <p>IV. One Loyal Shopper: Contest</p>
                                                 <p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>
                                                 <p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 1 &nbsp; &nbsp; &nbsp;Upon registration, concierge will input all details of the shopper to log amount of purchase.</p>
                                                 <p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>
                                                 <p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 2 &nbsp; &nbsp; &nbsp;The shopper may compete for the highest accumulated receipt which will be determined by the end of the promo. Receipt(s) must be registered in &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;one name only.</p>
                                                 <p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>
                                                 <p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 3 &nbsp; &nbsp; &nbsp;The shopper must present receipt at the concierge for validation, logging, and stamping.</p>
                                                 <p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>
                                                 <p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 4 &nbsp; &nbsp; &nbsp;The shopper will automatically receive a copy of the generated receipts indicating value of current and accumulated receipts.</p>
                                                 <p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>
                                                 <p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 5 &nbsp; &nbsp; &nbsp;A daily update of the highest accumulated spend will be posted at the concierge.&nbsp;</p>
                                                 <p>&nbsp;</p>
                                                 <p>&nbsp;V. &nbsp; &nbsp; Only receipts issued from&nbsp;August 1 to&nbsp;<span data-term="goog_1627127994">October 31, 2017</span>are eligible.&nbsp; Defaced, crumpled, tampered or receipts that are less than &frac34; its actual size will not be &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;honored.</p>
                                                 <p>&nbsp;</p>
                                                 <p>&nbsp;VI. &nbsp; &nbsp;Receipts may only be used once in either of two (2) methods of participation.</p>
                                                 <p>&nbsp;</p>
                                                 <p>&nbsp;</p>
                                                 <p>VII. &nbsp; &nbsp;The schedule of draw date, eligibility and prizes are as follows:</p>
                                                 <p>&nbsp;</p>
                                                 <table width="0">
                                                 <tbody>
                                                 <tr>
                                                 <td width="191">
                                                 <p>Determination of Winners</p>
                                                 </td>
                                                 <td width="218">
                                                 <p>Last Day of Submission of Entries (by<span data-term="goog_1627127995">11:59pm</span>)</p>
                                                 </td>
                                                 <td width="334">
                                                 <p>Prizes</p>
                                                 </td>
                                                 </tr>
                                                 <tr>
                                                 <td width="191">
                                                 <p><span data-term="goog_1627127996">November 3, 2017</span>&nbsp;(<span data-term="goog_1627127997">Monday</span>)</p>
                                                 <p>Raffle Draw</p>
                                                 <p>Receipt Assessment</p>
                                                 </td>
                                                 <td width="218">
                                                 <p><span data-term="goog_1627127998">October 31, 2017</span>&nbsp;(Tuesday)</p>
                                                 </td>
                                                 <td width="334">
                                                 <p>Grand Prize:</p>
                                                 <p>One (1) Chevrolet Trailblazer 4x2 DSL 2.5 L MT Switchblade Silver for One Lucky Winner (Raffle) &amp;</p>
                                                 <p>&nbsp;</p>
                                                 <p>One (1) Chevrolet Trailblazer 4x2 DSL 2.8 L AT Black Sapphire for One Loyal Shopper (Contest of highest accumulated receipts)</p>
                                                 <p>&nbsp;</p>
                                                 <p>&nbsp;</p>
                                                 </td>
                                                 </tr>
                                                 </tbody>
                                                 </table>
                                                 <p style="text-align: left;">&nbsp;</p>
                                                 <p style="text-align: left;">VIII. &nbsp; &nbsp;In case of a tie, winner shall be determined by whoever submitted the highest single / accumulated receipt amount based on timestamp.</p>
                                                 <p style="text-align: left;">&nbsp;</p>
                                                 <p style="text-align: left;">IX. &nbsp; &nbsp; &nbsp;The prizes are subject to the following conditions: &nbsp;</p>
                                                 <p style="text-align: left;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>
                                                 <p style="text-align: left;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;1. The winners shall shoulder the 20% tax for prizes exceeding P10,000.00 and other expenses that will be incurred in relation to the prize.</p>
                                                 <p style="text-align: left;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>
                                                 <p style="text-align: left;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;2. All&nbsp;prizes are transferable but not convertible to cash.</p>
                                                 <p style="text-align: left;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>
                                                 <p style="text-align: left;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;3. The car prize is exclusive of the costs of delivery, first year registration, and transfer of ownership.</p>
                                                 <p style="text-align: left;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>
                                                 <p style="text-align: left;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;4. Prizes not claimed after sixty (60) days from receipt of notification will be forfeited in favor of Alabang Town Center, Inc. with prior approval of DTI.</p>
                                                 <p style="text-align: left;">&nbsp;</p>
                                                 <p style="text-align: left;">&nbsp;X. &nbsp; &nbsp;Winners will be notified by email, telephone call and registered mail.&nbsp; The winners&rsquo; names will also be posted&nbsp;at the Alabang Town Center Concierge and &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; published at the Alabang Town Center Social Media accounts.</p>
                                                 <p style="text-align: left;">&nbsp;</p>
                                                 <p style="text-align: left;">&nbsp;XI. &nbsp; Winners may claim their prizes at the Administration Office of the Alabang Town Center.</p>
                                                 <p style="text-align: left;">&nbsp;</p>
                                                 <p style="text-align: left;">&nbsp;</p>
                                                 <p style="text-align: left;">XII. &nbsp; To claim the prizes, winners must present the notice sent by registered mail, the winning electronic raffle coupon, and any two (2) valid Government-issued &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;identification card with photo and signature.</p>
                                                 <p style="text-align: left;">&nbsp;</p>
                                                 <p style="text-align: left;">&nbsp;</p>
                                                 <p style="text-align: left;">XIII. &nbsp;Winners agree to the disclosure and publication of their names and other particulars as Alabang Town Center may deem appropriate and to participate in any &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;photo, publicity or other media arrangements made by Alabang Town Center to announce the winners to the general public.</p>
                                                 <p style="text-align: left;">&nbsp;</p>
                                                 <p style="text-align: left;">&nbsp;</p>
                                                 <p style="text-align: left;">XIV. &nbsp;Employees of Alabang Commercial Corporation, their merchants, maintenance and security agencies, and advertising and promotional agencies, and their &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; relatives up to the second degree of affinity or consanguinity, are disqualified from joining the promo.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 p-0">
                                    <div class="ec__view__iside text-center">
                                        <h4 class="text-italic showcase__title" style="text-align:center;">Submit your receipts with a minimum purchase of Php 1000 at the concierge and get a chance to win a Chevrolet Trailblazer!</h4>
                                        <div class="form__container">
                                            <h4>View your purchase details here</h4>
                                            <p>Check your email now for updates!</p>
                                            <form class="form-inline" method="post" action="{{URL::to('specialpromo/post-check-entries')}}">
                                                <div class="form-group">
                                                    <input type="email" required name="email" class="form-control" id="form_email" placeholder="Email Address"/>
                                                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                                                </div>
                                                <button type="submit" class="btn btn-purple btn-sm">SEND</button>
                                            </form>
                                        </div>
                                        <div class="helper__card">
                                            Be the shopper with the highest accumulated purchases at the date of the draw to win!
                                        </div>
                                        <p class="permit">{{$promo->dti_permit}}</p>
                                        <ul class="list-inline list-inline-white">
                                            <li>
                                                <a class="facebook" target="_blank" href="https://www.facebook.com/AlabangTownCenter/"><i class="icon-facebook"></i></a>
                                            </li>
                                            <li>
                                                <a class="twitter" target="_blank" href="https://twitter.com/alabangtowncntr?lang=en"><i class="icon-twitter"></i></a>
                                            </li>
                                            <li>
                                                <a class="instagram" target="_blank" href="https://www.instagram.com/alabangtowncenter/"><i class="icon-instagram"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="ec__footer ec__footer--mod">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="text-center">
                                <p class="copyright">ONE AYALA HOTLINE: 795-9595</p>
                                <p class="copyright">Copyright © 2016 Ayala Malls 360. All Rights Reserved</p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            @if(Session::has('msg_success'))
                <div class="remodal" data-remodal-id="modal">
                    <button data-remodal-action="close" class="remodal-close"></button>
                    <h1>Success</h1>
                    <p>
                        {{Session::get('msg_success')}}
                    </p>
                    <br>
                    <button data-remodal-action="confirm" class="remodal-confirm">OK</button>
                </div>
            @elseif(Session::has('msg_warning'))
                <div class="remodal" data-remodal-id="modal">
                    <button data-remodal-action="close" class="remodal-close"></button>
                    <h1>Warning!</h1>
                    <p>
                        {{Session::get('msg_warning')}}
                    </p>
                    <br>
                    <button data-remodal-action="confirm" class="remodal-cancel">OK</button>
                </div>
            @elseif(Session::has('msg_danger'))
                <div class="remodal" data-remodal-id="modal">
                    <button data-remodal-action="close" class="remodal-close"></button>
                    <h1>Oops!</h1>
                    <p>
                        {{Session::get('msg_danger')}}
                    </p>
                    <br>
                    <button data-remodal-action="confirm" class="remodal-cancel">OK</button>
                </div>
            @endif

        </section>
        <!-- end of content -->
        <script src="{{asset('public/landingv2/js/vendor/jquery-1.12.0.min.js')}}"></script>
        <script src="{{asset('public/landingv2/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('public/landingv2/js/plugins.js')}}"></script>
        <script src="{{asset('public/landingv2/js/main.js')}}"></script>
        <script src="{{asset('public/landing/dist/Remodal-1.1.0/dist/remodal.min.js')}}"></script>
        <script>
        $(document).ready(function(){
            // $('[data-remodal-id=modal]').remodal('');
            var inst = $.remodal.lookup[$('[data-remodal-id=modal]').data('remodal')];
            // открыть модальное окно
            if(inst)
            inst.open();
        });
        </script>
    </body>
</html>