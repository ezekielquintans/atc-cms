<!DOCTYPE html moznomarginboxes mozdisallowselectionprint>
<html>
<head>
	<title></title>
	<style type="text/css" media="print">
	    @page
	    {
	        size: auto;   /* auto is the initial value */
	        margin: 3mm;  /* this affects the margin in the printer settings */
	    }
	</style>
</head>
<body>
	<table style="width:298px;font-size:11px;font-family:verdana;">
		<thead>
			<th colspan="2" style="font-weight:bold;text-transform:uppercase;line-height:15px;">
				CONCIERGE - ALABANG TOWN CENTER<br/>
				UPPER GROUND LEVEL, AYALA ALABANG,<br/>
				MUNTINLUPA CITY<br/>
				ONE AYALA HOTLINE: 795-9595<br/>
			</th>
		</thead>
		<tbody>
			<tr>
				<td colspan="2"><hr/></td>
			</tr>
			<tr>
				<td colspan="2" style="text-align:center;text-transform:uppercase;"><b>One Lucky Winner, One Loyal Shopper</b></td>
			</tr>
			<tr>
				<td colspan="2"><hr/></td>
			</tr>
			<tr>
				<td><b>DATE</b></td>
				<td>{{date('Y-m-d H:i:s')}}</td>
			</tr>
			<tr>
				<td><b>TRANSACTION ID:</b></td>
				<td>{{strtoupper($token)}}</td>
			</tr>
			<tr>
				<td colspan="2"><hr/></td>
			</tr>
			<tr>
				<td colspan="2" style="text-align:center;"><b>CUSTOMER INFO</b></td>
			</tr>
			<tr>
				<td><b>CUSTOMER ID:</b></td>
				<td>{{str_pad($entry[0]->user_id, 8, '0', STR_PAD_LEFT)}}</td>
			</tr>
			<tr>
				<td><b>NAME:</b></td>
				<td>{{strtoupper($entry[0]->last_name . ", " . $entry[0]->first_name . " " . $entry[0]->middle_name)}}</td>
			</tr>
			<tr>
				<td><b>EMAIL:</b></td>
				<td>{{$entry[0]->email}}</td>
			</tr>
			<tr>
				<td><b>CONTACT #:</b></td>
				<td>{{$entry[0]->contact}}</td>
			</tr>
			<tr>
				<td><b>ADDRESS:</b></td>
				<td>{{$entry[0]->address}}</td>
			</tr>
			<tr>
				<td colspan="2"><hr/></td>
			</tr>
			<tr>
				<td colspan="2" style="text-align:center;"><b>RECEIPTS ADDED FOR THIS TRANSACTION</b></td>
			</tr>
			<tr>
				<td colspan="2" style="text-align:center;">{{$receiptCount}}<br/></td>
				<br />
			</tr>
			<tr>
				<td colspan="2" style="text-align:center;"><b>OVERALL PURCHASE</b></td>
			</tr>
			<tr>
				<td colspan="2" style="text-align:center;">PHP {{number_format($overAll,2,".",",")}}<br/></td>
			</tr>
			<tr>
				<td colspan="2"><hr/></td>
			</tr>
			<tr>
				<td colspan="2" style="text-align:center;font-weight:bold;line-height:15px;">
					For balance inquiries, visit www.alabangtowncenter.ph<br/>
					THANK YOU. PLEASE COME AGAIN.​<br/>
				</td>
			</tr>
		</tbody>
	</table>
	<script type="text/javascript">
		window.print();
	</script>
</body>
</html>
