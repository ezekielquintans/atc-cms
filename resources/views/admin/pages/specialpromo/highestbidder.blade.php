@extends('admin.layouts.admin-layout')
@section('content')
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Earn It Promo Highest Spender</h2>
                    <div class="pull-right">
                        <a href="{{Request::url() . '?download=true'}}">
                            <i class="fa fa-download fa-2x" id="btn-export" style="cursor:pointer;" title="Export"></i>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <h1 class="text-center hidden congrats-banner">Congratulations!</h1>
                            <div class="jumbotron">
                                <h1 class="jumbo text-center" id="myTargetElement"></h1>
                            </div>
                            <div class="row">

                                <h2 class="text-center">MEMBER DETAILS:</h2>
                                <form id="#" data-parsley-validate class="form-horizontal form-label-left">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label class="control-label text-left col-md-4 col-sm-4 col-xs-12" for="first-name">Full Name:
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <h2 id="winner-name"> -- </h2>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label class="control-label text-left col-md-4 col-sm-4 col-xs-12" for="first-name">Date of Birth:
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <h2 id="winner-dob"> -- </h2>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label class="control-label text-left col-md-4 col-sm-4 col-xs-12" for="first-name">Email Address:
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <h2 id="winner-email"> -- </h2>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label class="control-label text-left col-md-4 col-sm-4 col-xs-12" for="first-name">Contact No.:
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <h2 id="winner-contact"> -- </h2>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label class="control-label text-left col-md-2 col-sm-2 col-xs-12" for="first-name">Address:
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <h2 id="winner-address"> -- </h2>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </form>
                                <div class="ln_solid"></div>
                                <div class="clearfix"></div>
                                <div class="row">
                                    <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12">
                                        <a class="btn btn-orange btn-block btn-lg" href="#" id="get-highest-spender" data-spender="{{json_encode($spender)}}">Highest Spender</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('scripts')
    <script src="{{asset('public/app/dist/countUp.js-master/countUp.js-master/countUpOrig.js')}}"></script>
    <script src="{{asset('public/landing/js/specialpromodraw.js')}}"></script>
@stop