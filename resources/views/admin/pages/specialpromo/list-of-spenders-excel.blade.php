@if ($download == 'true')
<table class="table">
  <tbody id="tbl-listing">
    <tr>
        <td><strong>Name</strong></td>
        <td><strong>Total Amount</strong></td>
        <td><strong>Email Address</strong></td>
        <td><strong>Contact Number</strong></td>
        <td><strong>Address</strong></td>
    </tr>
    <?php  
        foreach ($user as $key => $u) {
            $u->total_amount = $total[$u->userid];
        }
        $spenders = collect($user);
        $spenders = $spenders->sortByDesc('total_amount');
    ?>
    @forelse($spenders->values() as $key => $u)
         <tr>
            <td>{{$u->name}}</td>
            <td class="text-right">PHP {{number_format($u->total_amount, 2, ".", ",")}}</td>
            <td class="text-center">{{$u->email}}</td>
            <td class="text-center">{{$u->contact}}</td>
            <td class="text-center">{{$u->address}}</td>
        </tr>
    @empty
        <tr><td colspan="5">No Entries Yet</td></tr>
    @endforelse
  </tbody>
</table>
@else
<table class="table table-striped" style="font-size: .7rem;">
    <tbody id="tbl-listing">
     <tr>
          <td>Name</td>
      </tr>
      <tr>
          <td>Total Amount</td>
      </tr>
      <tr>
          <td>Date of Birth</td>
      </tr>
      <tr>
          <td>Email Address</td>
      </tr>
      <tr>
          <td>Contact Number</td>
      </tr>
      <tr>
          <td>Address</td>
      </tr>
    </tbody>
</table>
@endif
