@if(Input::get('tk', '') != "" && count($entry) > 0)
        <div id="modal" class="modal show bs-example-modal-sm modal-receipt-view" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><a href="{{URL::to('res/specialpromo/create')}}"><span aria-hidden="true">×</span></a>
                        </button>
                        <img src="{{asset('public/app/img/success.png')}}" alt="" style="margin-bottom:15px;">
                        <h2 class="modal-title" id="myModalLabel2" style="font-size:24px;color:#39b54a">Receipt Submission Successful!</h2>
                        @if($errors->has())
                            <p class="alert alert-warning" style="color:#fff;font-size:20px;">
                                Something went wrong! Please check require fields.
                            </p>
                        @endif
                    </div>
                    <div class="modal-body">
                        <div class="receipt-content" style="max-height:300px;">
                            <div class="row">
                                <div class="col-md-12">
                                    <img class="center-block receipt-logo" src="{{asset('public/app/img/receipt-logo.png')}}" style="margin: 0 auto;" alt="">
                                </div>
                            </div>
                            <form class="form-horizontal form-label-left">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Date:</label>
                                    <div class="col-md-8">
                                        <h2>{{date('Y-m-d H:i:s')}}</h2>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4" >Name:</label>
                                    <div class="col-md-8">
                                        <h2>{{strtoupper($entry[0]->last_name . ", " . $entry[0]->first_name . " " . $entry[0]->middle_name)}}</h2>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4" >Email Address:</label>
                                    <div class="col-md-8">
                                        <h2>{{$entry[0]->email}}</h2>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4" >Contact Number:</label>
                                    <div class="col-md-8">
                                        <h2>{{$entry[0]->contact}}</h2>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4" >Overall purchase amount</label>
                                    <div class="col-md-8">
                                        @if($receiptCount <= 1)
                                            <h2>(<span class="total">{{$receiptCount}} receipt</span>) {{$entry[0]->total_amount}}</h2>
                                        @else
                                            <h2>(<span class="total">{{$receiptCount}} receipts</span>) {{$entry[0]->total_amount}}</h2>
                                        @endif
                                    </div>
                                </div>
                            </form>
                            {{-- <div class="row">
                                <div class="col-md-12">
                                    <img class="center-block receipt-logo" src="{{asset('public/landing/img/promo-logo-2.jpg')}}" alt="">
                                </div>
                            </div> --}}
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <a href="{{URL::to('res/specialpromo/create')}}"><button type="button" class="btn btn-orange btn-block btn-lg btn-print">NEW ENTRY</button></a>
                            </div>
                             <div class="col-md-6 col-sm-12 col-xs-12">
                                <a href="{{URL::to('res/specialpromo/print-entries?tk=' . $token)}}" target="_blank"><button type="button" class="btn btn-green btn-block btn-lg btn-print">PRINT NOW!</button></a>
                            </div> 
                        </div>

                    </div>
                </div>
            </div>
        </div>
    @endif