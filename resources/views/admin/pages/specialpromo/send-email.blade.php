
<!DOCTYPE html>
<html>
	<head>
		<title></title>
		<style type="text/css">
		</style>
	</head>
	<body style="width:500px;font-family: verdana;font-size:14px;">
		<div style="text-align: center;margin-bottom:25px;">
			<img src="http://www.alabangtowncenter.ph/assets/receipt-logo.png" style="float:left;">
			<div style="clear:both;"></div>
		</div>
		<div>
			<b>Hi {{$entry[0]->first_name}},</b><br/>
			<br/>
			@if($all == "")
				Congratulations! Your receipt was successfully added <b>PHP {{number_format($entry[0]->total_amount,2,'.',',')}} ({{$receiptCount}} {{$receiptCount <= 1 ? "receipt" : "receipts"}})</b> for the One Lucky Winner, One Loyal Shopper promo.
				You may find the details of your transaction below.<br/>
			@else
				You may find the details of all your transactions below.
			@endif
			<table cellpadding="5" style="margin-top:15px;width:100%;border-collapse: collapse;">
				@if($all == "")
					<tr>
						<td colspan="2"><hr style="border-style:dashed;border-color:#e1e1e1;"/></td>
					</tr>
					@if($token != "")
					<tr style="font-weight:bold;text-transform:uppercase;">
						<td style="border:none;padding:5px;width:200px;">Transaction ID:</td>
						<td style="border:none;padding:5px;">{{strtoupper($token)}}</td>
					</tr>
					@endif
					<tr>
						<td colspan="2"><hr style="border-style:dashed;border-color:#e1e1e1;"/></td>
					</tr>
				@endif
				<tr style="text-transform:uppercase;">
					<td style="width:200px;"><b>DATE:</b></td>
					<td>{{date('Y-m-d H:i:s')}}</td>
				</tr>
				<tr style="text-transform:uppercase;">
					<td><b>CUSTOMER ID:</b></td>
					<td>{{str_pad($entry[0]->user_id, 8, '0', STR_PAD_LEFT)}}</td>
				</tr>
				<tr style="text-transform:uppercase;">
					<td><b>NAME:</b></td>
					<td>{{strtoupper($entry[0]->last_name . ", " . $entry[0]->first_name . " " . $entry[0]->middle_name)}}</td>
				</tr>
				<tr>
					<td><b>EMAIL:</b></td>
					<td>{{$entry[0]->email}}</td>
				</tr>
				<tr style="text-transform:uppercase;">
					<td><b>CONTACT #:</b></td>
					<td>{{$entry[0]->contact}}</td>
				</tr>
				<tr style="text-transform:uppercase;">
					<td valign="top"><b>ADDRESS:</b></td>
					<td>{{$entry[0]->address}}</td>
				</tr>
				<tr>
					<td colspan="2"><hr style="border-style:dashed;border-color:#e1e1e1;"/></td>
				</tr>
				@if($all == "")
					<tr style="text-transform:uppercase;">
						<td colspan="2" style="text-align:center;"><b>RECEIPTS ADDED FOR THIS TRANSACTION</b></td>
					</tr>
					<tr>
						<td colspan="2" style="text-align:center;">{{$receiptCount}}<br/></td>
					</tr>
				@endif
			
				<tr style="text-transform:uppercase;">
					<td colspan="2" style="text-align:center;"><b>OVERALL PURCHASE</b></td>
				</tr>
				<tr>
					<td colspan="2" style="text-align:center;">PHP {{number_format($overAll,2,'.',',')}}<br/></td>
				</tr>
				<tr>
					<td colspan="2"><hr style="border-style:dashed;border-color:#e1e1e1;"/></td>
				</tr>
			</table>

			<div style="margin-top:15px;font-style:italic;font-size:12px;color:gray;">
				For inquiries, you may reach us at Concierge - Alabang Town Center, Upper Ground Level, Ayala Alabang,
				Muntinlupa City<br/>
				One Ayala Hotline: 795-9595
				<br/><br/>
				<!--DTI-FTEB Permit No. 11874, Series of 2016-->
			</div>
			<div style="margin-top:25px;">
				Regards,<br/>
				Concierge<br/>
			</div>
		</div>
	</body>
</html>
