@if ($download == 'true')
<table class="table">
  <tbody id="tbl-listing">
    <tr>
        <td><strong>Name</strong></td>
        <td><strong>Total Amount</strong></td>
        <td><strong>Email Address</strong></td>
        <td><strong>Contact Number</strong></td>
        <td><strong>Address</strong></td>
    </tr>
    @forelse($list as $key => $u)
         <tr>
            <td>{{$u->name}}</td>
            <td class="text-right">PHP {{$u->amount}}</td>
            <td class="text-center">{{$u->email}}</td>
            <td class="text-center">{{$u->contact}}</td>
            <td class="text-center">{{$u->address}}</td>
        </tr>
    @empty
        <tr><td colspan="5">No Entries Yet</td></tr>
    @endforelse
  </tbody>
</table>
@endif
