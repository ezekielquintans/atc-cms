@extends('admin.layouts.admin-layout')
@section('content')
    <div class="overlay">
        Loading member's information, please wait...
    </div>
    <div class="page-title">
        <div class="title_left">
            <h3>Earn It Promo</h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <form id="entry-form" class="form-horizontal form-label-left" method="post" action="{{URL::to('res/specialpromo')}}" enctype="multipart/form-data">
	    <!--USER INFORMATION-->
        <div class="row">
	        <div class="col-md-12 col-sm-12 col-xs-12">
	            <div class="x_panel">
	                <div class="x_title">
	                    <h2>Customer <small>Please fill in the required fields.</small></h2>
	                    <div class="clearfix"></div>
	                </div>
	                <div class="x_content">
	                    @if ($errors->any())
                            <div class="alert alert-danger">
                                <a href="{{URL::to(Request::url())}}" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <h5>Transaction failed due to following errors:</h5>
                                <ul style="margin-left: 20px;">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
	                	<div class="form-group">
		                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 col-sm-offset-3 col-xs-offset-12">
			                    <input type="text" class="form-control" placeholder="Search for..." id="search" autocomplete="off">
		                    </div>
	                	</div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fname">First Name </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="fname" name="first_name" class="form-control col-md-7 col-xs-12 customer-info" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fname">Middle Name </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="mname" name="middle_name" class="form-control col-md-7 col-xs-12 customer-info" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="lname">Last Name </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="lname" name="last_name" required="required" class="form-control col-md-7 col-xs-12 customer-info" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-sm-3 col-xs-12 control-label">Date of Birth</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" class="form-control datetime" name="dob" id="dob" value="" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email Address </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="email" id="email" name="email" class="form-control col-md-7 col-xs-12 customer-info">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Address </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="address" name="address" class="form-control col-md-7 col-xs-12 customer-info" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="contact">Contact No. </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" id="contact" name="contact" class="form-control col-md-7 col-xs-12 customer-info" required>
                            </div>
                        </div>
                        <div class="form-group {{$errors->has('card_type') ? 'has-error' : ""}}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Card Type</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                @if($errors->has())
                                    <label>
                                    <input type="checkbox" class="card-type" name="card_type[bpi]" id="card-type-bpi" value="1" {{ old("card_type.bpi") ? "checked" : "" }} />
                                        BPI AMORE
                                     </label>
                                    <label style="margin-left: 20px;">
                                        <input type="checkbox" class="card-type" name="card_type[vip]" id="card-type-vip" value="1" {{ old("card_type.vip") ? "checked" : "" }} />
                                        VIPINOY
                                    </label>
                                @else
                                    <label>
                                        <input type="checkbox" class="card-type" name="card_type[bpi]" id="card-type-bpi" value="1" }} />
                                        BPI AMORE
                                    </label>
                                    <label style="margin-left: 20px;">
                                        <input type="checkbox" class="card-type" name="card_type[vip]" id="card-type-vip" value="1" }} />
                                        VIPINOY
                                    </label>
                                @endif
                                <span style="color:#a94442;font-style: italic;">{{$errors->has('card_type') ? $errors->first('card_type') : ""}}</span>
                            </div>
                        </div>
                        <input id="cusID" type="hidden" name="cusid">
                        <input type="hidden" name="_token" value="{{ Session::token() }}">
                        <hr />
                        <div class="row">
                            <div class="table-responsive">
                                <table class="table table-striped jambo_table">
                                    <thead>
                                        <tr class="headings">
                                            <th class="column-title">Store Name</th>
                                            <th class="column-title">Category</th>
                                            <th class="column-title">Invoice Number</th>
                                            <th class="column-title">Amount</th>
                                            <th class="column-title no-link last"><span class="nobr">Action</span></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="entry-row">
                                            <td class="a-center ">
                                                <select id="heard" class="store form-control" name="store[] required">
                                                    <option value="" selected> -- Select a Store -- </option>
                                                    <option data-category = "OTHERS">OTHERS</option>
                                                    @forelse($storeListing as $store)
                                                        <option data-category = "{{App\Stores::displayCurrentCategory($store)}}">{{$store}}</option>
                                                    @empty
                                                    @endforelse
                                                </select>
                                            </td>
                                            <td class=" ">
                                                <input class="form-control category" type="text" readonly/>
                                            </td>
                                            <td class=" ">
                                                <input type="text" class="invoice-text form-control" placeholder="Enter invoice number" name="invoice[] required">
                                            </td>
                                            <td class=" ">
                                                <input type="number" class="form-control amount" placeholder="Enter Amount" name="amount[] required">
                                                <input type="hidden" class="form-control coupons" name="coupons[]">
                                            </td>
                                            <td class=" ">
                                                <button class="btn btn-default btn-delete-entry" title="Delete this entry">
                                                    <i class="fa fa-trash-o"></i>
                                                </button>
                                            </td>
                                        </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <!-- <div class="col-md-4 col-sm-12 col-xs-12 current-amount-cont">
                                <label class="" >Current Amount:</label>
                                <span id="currentAmount">0</span>
                            </div> -->
                            <div class="col-md-8 col-sm-12 col-xs-12 total-amount-cont">
                                <label class="" >Total Amount:</label>
                                <span id="totalAmount">0</span>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <input type="hidden" name="rows" id="norows" value="1">
                                <input type="hidden" name="promo" id="norows" value="{{isset($promo) ? $promo->id : 0}}">
                                <button type="button" class="btn btn-green submit-entries pull-right">Submit</button>
                                <button type="button" class="btn btn-orange pull-right" id="add-receipt"><i class="fa fa-plus"></i> Add Receipt</a>
                            </div>
                        </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</form>
    @include('admin.pages.specialpromo.generate-coupon-modal')
@stop

@section('scripts')
<script type="text/javascript" src="{{asset("public/app/js/chosen_v1.6.2/chosen.jquery.js")}}"></script>
<script type="text/javascript" src="{{asset("public/landing/js/urlConfig.js")}}"></script>
<script type="text/javascript" src="{{asset("public/landing/js/specialpromo.js")}}"></script>
@stop
