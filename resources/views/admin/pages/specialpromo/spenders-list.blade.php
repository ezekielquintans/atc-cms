@extends('admin.layouts.admin-layout')
@section('content')
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Earn It Promo
                    </h2>
                    <div class="pull-right">
                        <form>
                            <input type="hidden" name="download" value="true">
                            <button class="close" type="submit"><i class="fa fa-download"></i></button>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                     <div class="row winners-list">
                        <div class="col-md-12">
                            <div class="x_panel">
                                <div class="x_content" id="winners-table">
                                    <table class="table">
                                        <thead>
                                            <th>Name</th>
                                            <th class="text-center">Total Amount</th>
                                            <th class="text-center">Email Address</th>
                                            <th class="text-center">Contact Number</th>
                                        </thead>
                                        <tbody id="tbl-listing">
                                            <?php  
                                                foreach ($user as $key => $u) {
                                                    $u->total_amount = $total[$u->userid];
                                                }
                                                $spenders = collect($user);
                                                $spenders = $spenders->sortByDesc('total_amount');
                                            ?>
                                            @forelse($spenders->values() as $key => $u)
                                                 <tr>
                                                    <td>{{$u->name}}</td>
                                                    <td class="text-right">PHP {{number_format($u->total_amount, 2, ".", ",")}}</td>
                                                    <td class="text-center">{{$u->email}}</td>
                                                    <td class="text-center">{{$u->contact}}</td>
                                                </tr>
                                            @empty
                                                <tr><td colspan="5">No Entries Yet</td></tr>
                                            @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
@stop