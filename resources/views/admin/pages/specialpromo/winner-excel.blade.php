@if ($download == 'true')
<table id="winner" class="table table-hover" >
  <thead>
      <tr colspan="2">
        <td align="center">Earn It Winner</td>
      </tr>
  </thead>
  <tbody>
    <tr>
        <td align="left">Name</td>
        <td align="center">{{ $spender->name }}</td>
    </tr>
    <tr>
        <td align="left">Total Amount</td>
        <td align="center">PHP {{ $spender->total_amount }}</td>
    </tr>
    <tr>
        <td align="left">Date of Birth</td>
        <td align="center">{{ $spender->dob }}</td>
    </tr>
    <tr>
        <td align="left">Email Address</td>
        <td align="center">{{ $spender->email }}</td>
    </tr>
    <tr>
        <td align="left">Contact Number</td>
        <td align="center">{{ $spender->contact }}</td>
    </tr>
    <tr>
        <td align="left">Address</td>
        <td align="center">{{ $spender->address }}</td>
    </tr>
    <tr></tr>    
    <tr>
      <td align="center">____________________</td>
      <td align="center">____________________</td>
    </tr>    
    <tr>
      <td align="center">Promo Administrator</td>
      <td align="center">DTI Representative</td>
    </tr>    
  </tbody>
</table>
@else
<table class="table table-striped" style="font-size: .7rem;">
    <tbody id="tbl-listing">
     <tr>
          <td>Name</td>
      </tr>
      <tr>
          <td>Total Amount</td>
      </tr>
      <tr>
          <td>Date of Birth</td>
      </tr>
      <tr>
          <td>Email Address</td>
      </tr>
      <tr>
          <td>Contact Number</td>
      </tr>
      <tr>
          <td>Address</td>
      </tr>
    </tbody>
</table>
@endif
