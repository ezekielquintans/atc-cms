@extends('admin.layouts.admin-layout')
@section('content')
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Raffle Draw</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <h1 class="text-center hidden congrats-banner">Congratulations!</h1>
                            <div class="jumbotron">
                                <h1 class="jumbo text-center" id="myTargetElement">00000000</h1>
                            </div>
                            <div class="row">
                                <h2 class="text-center">MEMBER DETAILS:</h2>
                                    <form id="#" data-parsley-validate class="form-horizontal form-label-left">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="control-label text-left col-md-4 col-sm-4 col-xs-12" for="first-name">Member ID:
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <h2 id="winner-id"> -- </h2>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="control-label text-left col-md-4 col-sm-4 col-xs-12" for="first-name">Full Name:
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <h2 id="winner-name"> -- </h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="control-label text-left col-md-4 col-sm-4 col-xs-12" for="first-name">Coupon No:
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <h2 id="winner-coupon"> -- </h2>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="control-label text-left col-md-4 col-sm-4 col-xs-12" for="first-name">Email Address:
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <h2 id="winner-email"> -- </h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="control-label text-left col-md-4 col-sm-4 col-xs-12" for="first-name">Contact No.:
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <h2 id="winner-contact"> -- </h2>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="control-label text-left col-md-4 col-sm-4 col-xs-12" for="first-name">Date of Birth:
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <h2 id="winner-dob"> -- </h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="control-label text-left col-md-2 col-sm-2 col-xs-12" for="first-name">Address:
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <h2 id="winner-address"> -- </h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                <div class="ln_solid"></div>
                                <div class="clearfix"></div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <select class="form-control" name="promo" style="height:42px">
                                            <option value=""> -- Select a Promo -- </option>
                                            @forelse($promo as $row)
                                                <option value="{{$row->id}}">{{$row->name}}</option>
                                            @empty
                                            @endforelse
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <a class="btn btn-orange btn-block btn-lg" href="#" id="generate-winner">Pick a Winner</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row winners-list hidden">
        <div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1">
            <div class="x_panel">
                <div class="x_title">
                    <h2>List of Winners</h2>
                    <div class="pull-right">
                        <i class="fa fa-download fa-2x" id="btn-export" style="cursor:pointer;" title="Export to Excel"></i>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content" id="winners-table">
                    <table class="table table-striped">
                        <thead>
                            <th>#</th>
                            <th>Coupon No.</th>
                            <th>User ID</th>
                            <th>Last Name</th>
                            <th>First Name</th>
                            <th>Middle Name</th>
                            <th>Email Address</th>
                            <th>Contact Number</th>
                        </thead>
                        <tbody id="tbl-listing">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('scripts')
    <script src="{{asset('public/app/dist/countUp.js-master/countUp.js-master/countUp.js')}}"></script>
    <script src="{{asset('public/landing/js/raffle.js')}}"></script>
    <script type="text/javascript">
        $("select[name='promo'").change(function(event) {
            var selectedPromo = $(this).children(':selected').text();
            var promoId = $(this).val();
            if(selectedPromo == "Highest Bidder"){
                window.location.href = "highest-bidder?promo=" + promoId;
            }
        });
    </script>

@stop