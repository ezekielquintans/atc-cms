@extends('admin.layouts.admin-layout')

@section('content')
	<div class="clearfix"></div>
	<div class="row">
		@if($process == "ADD")
			<form class="form-horizontal" role="form" method="POST" action="{{ URL::to("res/rewards") }}" enctype="multipart/form-data" id="reward-form">
	    	@elseif($process == "EDIT")
			<form class="form-horizontal" role="form" method="POST" action="{{ URL::to("res/rewards/" .$rewardInfo->id) }}" enctype="multipart/form-data" id="reward-form">
			<input type="hidden" name="_method" value="PATCH">
		@endif
	    	<div class="col-lg-10 col-md-12 col-xs-12 col-lg-offset-1">
		        <div class="x_panel">
		        	<!-- Header Panel -->
		            <div class="x_title row">
							<div class="col-sm-6">
								<h3>Rewards Details</h3>
							</div>
							<div class="dropdown pull-right">
								<button type="button" btn-secondary class="btn dropdown-toggle" style="background-color: Transparent;color:#337ab7;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i class="fa fa-cog" aria-hidden="true"></i>	
								</button>
								<div class="dropdown-menu">
									<a class="dropdown-item btn" href="{{ URL::to("res/rewards") }}" style="color:inherit;">Go to Rewards List</a>
									<div class="dropdown-divider"></div>
									@if (strtoupper($process) == 'EDIT')
										<a class="dropdown-item btn" href="{{URL::to('res/rewards/create')}}" style="color:inherit;">Add a reward</a>
										<button type="button" class="dropdown-item btn btn-link btn-delete" data-href="{{ URL::to('res/rewards/destroy/'. $rewardInfo->id) }}" style="color:inherit;">Delete this reward</button>
									@endif
								</div>								
							</div>
		            </div>
					<!-- End of Header Panel -->

					<!-- Details Panel -->
		            <div class="x_content">
						@if($errors->has())
				            <div class="alert alert-error">
				                <a href="{{ URL::to(Request::url()) }}" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				                Please check the required fields.
				            </div>
						@elseif(Session::has('msg_success'))
							<div class="alert alert-success">
				                <a href="{{ URL::to(Request::url()) }}" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				                {{ Session::get('msg_success') }}
				            </div>
				        @endif
				        <div id="alerts"></div>

						<div class="form-group {{$errors->has('name') ? 'has-error' : ""}}">
							<label class="col-md-3 col-lg-3 col-sm-12 col-xs-12 control-label">Reward Name *</label>
							<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
								<input 	type="text" 
										class="form-control"  
										name="name" 
										value="{{ $errors->has() ? old('name') : (isset($rewardInfo->name) ? $rewardInfo->name : "") }}" 
										placeholder="..." 
								>
								<span style="color:#a94442;font-style: italic;">{{$errors->has('name') ? $errors->first('name') : ""}}</span>
							</div>
						</div>
						<div class="form-group {{$errors->has('rewards_count') ? 'has-error' : ""}}">
							<label class="col-md-3 col-lg-3 col-sm-12 col-xs-12 control-label">Stocks Available *</label>
							<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
								<input type="text" class="form-control" required name="rewards_count" value="{{ $errors->has() ? old('rewards_count') : (isset($rewardInfo->rewards_count) ? $rewardInfo->rewards_count : "") }}" placeholder="..." >
								<span style="color:#a94442;font-style: italic;">{{$errors->has('rewards_count') ? $errors->first('rewards_count') : ""}}</span>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 col-lg-3 col-sm-13 col-xs-13 control-label">Advance Settings</label>
							<div class="col-md-9 col-lg-9 col-sm-9 col-xs-9">								
								<label>
									<input type="checkbox" class="js-switch" id="adv-set-btn" name="advance_settings" {{$errors->has() && old('advance_settings') == "on" ? "checked" : (isset($rewardInfo->venue) && $rewardInfo->venue != "" ? "checked": "")}}>
								</label>
							</div>
						</div>
						<div class="advance-settings {{$errors->has() && old('advance_settings') == "on" ? "" : (isset($rewardInfo->venue) && $rewardInfo->venue != "" ? "": 'hidden')}}">
							<div class="form-group {{$errors->has('venue') ? 'has-error' : ""}}">
								<label class="col-md-3 col-lg-3 col-sm-12 col-xs-12 control-label">Venue *</label>
								<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
									<input type="text" class="form-control" name="venue" value="{{ $errors->has() ? old('venue') : (isset($rewardInfo->venue) ? $rewardInfo->venue : "") }}" placeholder="..." >
									<span style="color:#a94442;font-style: italic;">{{$errors->has('venue') ? $errors->first('venue') : ""}}</span>
								</div>
							</div>
							<div class="form-group {{$errors->has('datetime') ? 'has-error' : ""}}">
								<label class="col-md-3 col-lg-3 col-sm-12 col-xs-12 control-label">Date & Time *</label>
								<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
									@if($errors->has())
										<input type="text" class="form-control datetime" name="datetime" value="{{old('datetime')}}" placeholder="...">
									@else
										<input type="text" class="form-control datetime" name="datetime" value="{{date("Y-M-D h:i:s")}}" placeholder="...">
									@endif
									<span style="color:#a94442;font-style: italic;">{{$errors->has('datetime') ? $errors->first('datetime') : ""}}</span>
								</div>
							</div>
							<div class="form-group {{$errors->has('description') ? 'has-error' : ""}}">
								<label class="col-md-3 col-lg-3 col-sm-12 col-xs-12 control-label">Details</label>
								<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
					                  	<textarea id="details" class="form-control" name="details" value="{{isset($rewardInfo->details) ? $rewardInfo->details : ""}}"></textarea>
						                  <span id="char-count" style="font-style: italic;color:#666;">Characters left: 100</span>
						                  <span style="font-style: italic;">{{$errors->has('details') ? $errors->first('details') : ""}}</span>
								</div>
							</div>
							<div class="form-group {{$errors->has('featured_img') ? 'has-error' : ""}}">
								<label class="col-md-3 col-lg-3 col-sm-12 col-xs-12 control-label">Featured Image {{$process == "add" ? "*" : ""}}</label>
								<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
									<img id="create-cat-img" class="img-thumbnail {{$process == "EDIT"? "" : "hidden"}}" src="{{$process == "EDIT" ? asset(isset($rewardInfo->featured_img) ? $rewardInfo->featured_img : "") : ""}}" alt="Preview" width="100" height="100">
									<input type="file" class="form-control img-upload" name="featured_img" accept="image/*">
									<p class="help-block">Max size is 7MB. Format: .jpg or .png</p>
									<span style="color:#a94442;font-style: italic;">{{$errors->has('featured_img') ? $errors->first('featured_img') : ""}}</span>
								</div>
							</div>							
						</div>
					</div>
					<!-- End of Details Panel -->

					<!-- Footer Panel -->
		            <div class="x_footer">
				        <input type="hidden" name="_token" value="{{ Session::token() }}">
				        <button type="submit" class="btn btn-success pull-right">SUBMIT</button>
	
		            </div>
		            <!-- End of Footer Panel -->
		        </div>
		    </div>
        </form>
	</div>
@stop

@section("scripts")
	<script src="{{asset("public/landing/js/updatereward.js")}}"></script>
@stop