@extends('admin.layouts.admin-layout')

@section('content')
	<div class="clearfix"></div>
	<div class="row">
	    <div class="col-md-12 col-sm-12 col-xs-12">
	        <div class="x_panel">
	        @if(Session::has('msg_error'))
		        <div class="alert alert-error">
		            <a href="{{ URL::to(Request::url()) }}" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            {{ Session::get('msg_error') }}
		        </div>
			@elseif(Session::has('msg_success'))
				<div class="alert alert-success">
		            <a href="{{ URL::to(Request::url()) }}" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            {{ Session::get('msg_success') }}
		        </div>
		    @endif
	            <div class="x_title">
		            <form method="get">
		                <h2 class="col-md-3">List of Rewards</h2>
		                <div class="col-md-9">
		                    <div class="row">
		                        <div class="col-md-7">
		                            <div class="form-group">
		                                <input type="text" name="search" id="search" value="{{Input::get('search', '')}}" class="form-control" placeholder="Search for...">
		                            </div>
		                        </div>
		                        <div class="col-md-5">
		                            <div class="form-group text-right">
		                                <button class="btn btn-default" type="submit">Go!</button>
		                                <a href="{{ URL::to('res/rewards') }}" class="btn btn-default" >View All</a>
		                                <a href="{{ URL::to('res/rewards/create') }}"><button class="btn btn-default" type="button">Create a Reward</button></a>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		            </form>
	                <div class="clearfix"></div>
	            </div>
	            <div class="x_content">
	            	<table class="table table-striped">
						<thead>
							<tr>
								<th>#</th>												
								<th>Reward</th>
								<th>Promo</th>
								<th class="text-center">No. of Items</th>
								<th class="text-center"></th>
							</tr>
						</thead>
	                    <tbody>
                        	@if(!$rewardList->isEmpty())
                        		@foreach($rewardList as $key => $reward)

                        			<tr>
                        				<td width="1">{{ (Input::get('page', 1) - 1) * 10 + $key + 1 }}. </td>
                        				<td>{{ $reward->name }}</td>
                        				<td>{{ $reward->promo->name or '' }}</td>
                        				<td class="text-center">{{ $reward->rewards_count }}</td>
                        				<td class="text-center">{{$reward->created_at->toFormattedDateString()}}</td>
                        				<td align="right">
                        					<div class="btn-group">
                        						<button class="btn btn-default btn-link" data-href="{{URL::to('res/rewards/'.$reward->id .'/edit')}}" title="Edit this reward">
                        							<i class="fa fa-pencil"></i>
                        						</button>
                        						<button class="btn btn-default btn-link btn-delete" data-href="{{URL::to('res/rewards/destroy/'. $reward->id)}}" title="Delete this reward">
                        							<i class="fa fa-trash-o"></i>
                        						</button>
                                            </div>
                        				</td>
                        			</tr>
                        		@endforeach
                        	@else
                        		<tr>
                        			<td colspan="6">There are no promo yet.</td>
                        		</tr>
                        	@endif
	                    </tbody>
	            	</table>
	            </div>
	            <div class="x_footer">
	                {!! $rewardList->render() !!}
	            </div>
	        </div>
	    </div>
	</div>
@stop