@extends('admin.layouts.admin-layout')
@section('content')
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">

            <form method="get">
                <h2 class="col-md-6">List of Rewards</h2>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-4">
                            <select class="form-control" name="promo">
                                <option value=""> -- All Promos -- </option>
                                @forelse($promos as $row)
                                    <option value="{{$row->id}}" @if(Input::get('promo', '') == $row->id) selected @endif>{{$row->name}}</option>
                                @empty
                                @endforelse
                            </select>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" name="search" id="search" value="{{Input::get('search', '')}}" class="form-control" placeholder="Search for...">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <button class="btn btn-default" type="submit">Go!</button>
                                <button class="btn btn-default" onclick="location.href='entries'" type="button">View All</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                @if(Session::has('msg_success'))
                    <div class="alert alert-success">
                        <a href="{{URL::to(Request::url())}}" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {{Session::get('msg_success')}}
                    </div>
                @elseif(Session::has('msg_warning'))
                    <div class="alert alert-warning">
                        <a href="{{URL::to(Request::url())}}" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {{Session::get('msg_warning')}}
                    </div>
                @elseif(Session::has('msg_error'))
                    <div class="alert alert-danger">
                        {{Session::get('msg_error')}}
                        @if(!$errors->isEmpty())
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                            @endforeach
                        </ul>
                        @endif
                    </div>
                @endif
                <table class="table table-striped">
					<thead>
						<th>#</th>
						<th>SERIAL NO.</th>
                        <th>FULL NAME</th>
                        <th>EMAIL ADDRESS</th>
                        <th>CONTACT NO.</th>
                        <th>REWARD</th>
                        <th>PROMO</th>
                        <th>CREATED DATE</th>
						<th class="text-right">ACTION</th>
					</thead>
                    <tbody>
                        @forelse($rewards as $key => $row)
                            <tr>
                                <td width="1">{{ (Input::get('page', 1) - 1) * 20 + $key + 1 }}. </td>
                                <td>{{$row->serial_no}}</td>
                                <td>{{$row->last_name . ", " . $row->first_name . " " . $row->middle_name}}</td>
                                <td>{{$row->email}}</td>
                                <td>{{$row->contact}}</td>
                                <td>{{$row->reward_name}}</td>
                                <td>{{$row->promo_name}}</td>
                                <td>{{date('M j, Y', strtotime($row->entry_created))}}</td>
                                <td align="right" style="vertical-align: middle;">
                                    <div class="btn-group">
                                        <button class="btn btn-default btn-link btn-delete" data-href="{{URL::to('res/entries/destroy/'.$row->coupon_id)}}" title="Delete this user">
                                            <i class="fa fa-trash-o"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="7">No entries yet</td>
                            </tr>
                        @endforelse
                    </tbody>
            	</table>
            </div>
            <div class="x_footer">
                {!! $rewards->render() !!}
            </div>
        </div>
    </div>
</div>

@stop
@section('scripts')

@stop
