@extends('admin.layouts.admin-layout')
@section('content')
<link rel="stylesheet" href=" {{ asset('public/css/bootstrap-tokenfield.min.css') }}">

<div class="clearfix"></div>
<div class="row">
    @include('admin.modules.notification')
    <div class="col-md-8 col-xs-12 col-md-offset-2">
        <div class="x_panel">
            <div class="x_title">
                <h2 class="col-md-8 pull-left">Text Blast</h2>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <div class="col-md-12">

                    <!-- <form class="form-horizontal" id="selected-promo-form" action="{{ url('res/textblast') }}" method="get">
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="promo_id">Promo</label>
                            <div class="col-md-9">
                                <select class="form-control" name="selected_promo" id="promoSelect">
                                    <option value="0">NONE</option>
                                    @foreach($promos as $promo)
                                        @if(isset($selected_promo) && $promo->id == $selected_promo)
                                            <option value="{{ $promo->id }}" selected>{{ $promo->name }}</option>
                                        @else
                                            <option value="{{ $promo->id }}">{{ $promo->name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </form> -->

                    <form class="form-horizontal" id="mainForm" action="{{ url('res/textblast/send') }}" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="selected_promo" value="{{ $selected_promo }}">

                        <div class="form-group">
                            <label class="control-label col-md-3">Override Recipients</label>
                            <div class="col-md-9">
                                <input type="checkbox" id="overrideRecipientsSwitch" class="js-switch form-control">
                            </div>
                            <div id="recipientOverridePanel">
                                <div class="row">
                                    <div class="col-md-12 form-group" id="recipientsPanel">
                                        <textarea class="form-control" required id="recipients" name="recipients" style="resize: none;" placeholder="Input numbers here i.e. 9171234567" value=""></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group" id="filtersFormGroup">
                            <label class="control-label col-md-3">Filter Recipients</label>
                            <div class="col-md-9">
                                <input type="checkbox" id="filterRecipientsSwitch" class="js-switch form-control">

                                <div class="row">
                                    <div id="filtersPanel" style="display: none;">
                                        <div class="row">
                                            <div class="col-md-6 form-group">
                                                <label class="control-label" for="bpi_member">Amore Members</label>
                                                <input type="checkbox" class="flat" name="bpi_member" value="1">
                                            </div>
                                            <div class="col-md-6 form-group">
                                                <label class="control-label" for="vip_member">VIPinoy Members</label>
                                                <input type="checkbox" class="flat" name="vip_member" value="1">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="message" class="control-label">
                                Message (<span id="messageCharsLeft">160</span> / <span id="messageCount">1</span> <span id="#messageCountText">message</span>)
                            </label>
                            <textarea required id="message" name="message" rows="8" cols="80" class="form-control" style="resize: none;"></textarea>
                        </div>
                        <input type="submit" disabled id="sendTextBlast" value="Send" class="btn btn-default pull-right clearfix">
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#textBlastModal">Open Modal</button> -->

<style media="screen">
    .loader {
        border: 8px solid #f3f3f3; /* Light grey */
        border-top: 8px solid #39b54a; /* Blue */
        border-radius: 50%;
        width: 80px;
        height: 80px;
        animation: spin 2s linear infinite;
        margin: 0 auto;
    }

    .col-center {
        margin: 0 auto;
    }

    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
</style>
<!-- Modal -->
<div id="textBlastModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
    <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                <h4 class="modal-title">Sending Messages</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div id="sendingMessaagesDiv" class="col-md-12 text-center">
                        <div class="loader"></div>
                        <div id="loadingMsg">
                            <h4>Preparing recipients...</h4>
                        </div>
                        <div id="statusMsg" style="display: none;">
                            <h4>Sending <span id="sentSmsSpan">-</span>/<span class="recipientCount">-</span></h4>
                        </div>
                        <div class="col-md-6 col-md-offset-3">
                            <div class="col-md-6 text-center">
                                <span class="text-success">Sent: <span class="smsSuccess">0</span></span>
                            </div>
                            <div class="col-md-6 text-center">
                                <span class="text-danger">Failed: <span class="smsFailed">0</span></span>
                            </div>
                        </div>
                        <br>
                        <span class="text-info">Do not close this window while messages are still sending.</span>
                    </div>
                    <div id="messagesSentDiv" class="col-md-12 text-center" style="display: none;">
                        <h4><i class="fa fa-check text-success" style="font-size: 64px;"></i> </h4>
                        <span class="smsSuccess">0</span> messages were successfully sent out of <span class="recipientCount">0</span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="close-textblast-modal" data-dismiss="modal" style="display: none;">
                    Close
                </button>
            </div>
        </div>

    </div>
</div>
@stop

@section('scripts')
<script type="text/javascript" src="{{ asset('public/js/bootstrap-tokenfield.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/js/text-blast.js') }}"></script>
@stop
