@extends('admin.layouts.admin-layout')
@section('content')
		<div class="clearfix"></div>

		<div class="row">
            @include('admin.modules.notification')
		    <div class="col-md-8 col-xs-12 col-md-offset-2">
		        <div class="x_panel">
		            <div class="x_title">
						<h3>Edit User</h3>
		                <div class="clearfix"></div>
		            </div>
                    <form class="form-horizontal" action="{{ url('res/members/'.$user->id) }}" method="post">
                        <input type="hidden" name="_method" value="patch">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
			            <div class="x_content">
						@if(Session::has('msg_error'))
				            <div class="alert alert-error">
			                	<a href="{{URL::to(Request::url())}}" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				                {{Session::get('msg_error')}}
				            </div>
						@elseif(Session::has('msg_success'))
							<div class="alert alert-success">
		                		<a href="{{URL::to(Request::url())}}" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                		{{Session::get('msg_success')}}
			            	</div>
    				    @endif

						<div class="col-md-12">
							<div class="form-group">
								<label class="col-md-3 control-label">First Name</label>
								<div class="col-md-9">
									<input type="text" class="form-control" required name="first_name" value="{{ isset($user->first_name) ? $user->first_name : "" }}" placeholder="...">
									<input type="hidden" class="form-control" name="userid" value="{{ isset($user->id) ? $user->id : ""}}">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Middle Name</label>
								<div class="col-md-9">
									<input type="text" class="form-control" name="middle_name" value="{{ isset($user->middle_name) ? $user->middle_name : ""}}" placeholder="...">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Last Name</label>
								<div class="col-md-9">
									<input type="text" class="form-control" required name="last_name" value="{{ isset($user->last_name) ? $user->last_name : "" }}" placeholder="...">
								</div>
							</div>
							<hr/>
							<div class="form-group">
								<label class="col-md-3 control-label">Date of Birth</label>
								<div class="col-md-9">
									<input type="text" class="form-control datetime" name="dob" value="{{ isset($user->dob) ? $user->dob : date('YYYY-MM-DD') }}" placeholder="">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Email</label>
								<div class="col-md-9">
									<input type="email" class="form-control" required name="email" value="{{ isset($user->email) ? $user->email : "" }}" placeholder="">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Contact No.</label>
								<div class="col-md-9">
                                    <div class="input-group">
                                        <span class="input-group-addon">+63</span>
                                        <input type="number" class="form-control contact-input" required pattern="^([^0(+63)])+[\d]+" name="contact" value="{{ isset($user->contact) ? $user->contact : "" }}" placeholder="">
                                    </div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Address</label>
								<div class="col-md-9">
									<!-- <input type="text" class="form-control" required pattern="^.{4,},.{5,},.{5,}" placeholder="Unit # Street, Village/Barangay/District, City/Municipality" name="address"  value="{{isset($user->address) ? $user->address : ""}}"/> -->
									<input type="text" class="form-control" required  placeholder="Unit # Street, Village/Barangay/District, City/Municipality" name="address"  value="{{isset($user->address) ? $user->address : ""}}"/>
								</div>
							</div>
							<hr/>

							<div class="form-group {{$errors->has('card_type') ? 'has-error' : ""}}">
								<label class="col-md-3 col-lg-3 col-sm-12 col-xs-12 control-label">Card Type</label>
								<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12 control-label">
			                        @if($errors->has())
			                        	<label>
			                        	<input type="checkbox" class="js-switch card-type" name="card_type[bpi]" id="card-type-bpi" value="1" {{ old("card_type.bpi") ? "checked" : "" }} />
        		                        	BPI AMORE
                                         </label>
				                        <label style="margin-left: 20px;">
    				                        <input type="checkbox" class="js-switch card-type" name="card_type[vip]" id="card-type-vip" value="1" {{ old("card_type.vip") ? "checked" : "" }} />
    				                        VIPINOY
                                        </label>
			                        @else
			                        	<label>
                                            <input type="checkbox" class="js-switch card-type" name="card_type[bpi]" id="card-type-bpi" value="1" {{ $bpiCardPresent ? 'checked' : '' }} />
                        	                BPI AMORE
                                        </label>
				                        <label style="margin-left: 20px;">
                                            <input type="checkbox" class="js-switch card-type" name="card_type[vip]" id="card-type-vip" value="1" {{ $vipCardPresent > 0 ? 'checked' : '' }} />
    				                        VIPINOY
                                        </label>
			                        @endif
			                       	<span style="color:#a94442;font-style: italic;">{{$errors->has('card_type') ? $errors->first('card_type') : ""}}</span>
									</div>
								</div>
								<!--
                                <div class="{{ $bpiCardPresent ? '' : 'hidden' }}" id="bpi-settings-container">
									<div class="x_title">
										<h3>BPI AMORE</h3>
						            </div>
						            <div class="x_content">
						            	<div class="form-group {{$errors->has('beep_paywave') ? 'has-error' : ""}}">
						            		<label class="col-md-3 col-lg-3 col-sm-12 col-xs-12 control-label">BPI AMORE TYPE *</label>
						            		<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
                                                @if($bpiCardPresent)
												<label class="radio-inline">
											    	<input {{ $bpiCardPresent ? '' : 'disabled' }} type='radio' name='beep_paywave' value='beep' required {{ $bpiCard[0]['beep_paywave'] == 'beep' ? 'checked' : '' }}>BEEP
											    </label>
											    <label class='radio-inline'>
											    	<input {{ $bpiCardPresent ? '' : 'disabled' }} type='radio' name='beep_paywave' value='paywave' required {{ $bpiCard[0]['beep_paywave'] == 'paywave' ? 'checked' : '' }}>PAYWAVE
											    </label>
                                                @else
                                                <label class="radio-inline">
											    	<input disabled type='radio' name='beep_paywave' value='beep' required>BEEP
											    </label>
											    <label class='radio-inline'>
											    	<input disabled type='radio' name='beep_paywave' value='paywave' required>PAYWAVE
											    </label>
                                                @endif
                                                <br>
						            			<span style="color:#a94442;font-style: italic;">{{$errors->has('beep_paywave') ? $errors->first('beep_paywave') : ""}}</span>
						            		</div>
						            	</div>

						          		<div class="form-group {{$errors->has('new_renewal_bpi') ? 'has-error' : ""}}">
						            		<label class="col-md-3 col-lg-3 col-sm-12 col-xs-12 control-label">Card Status *</label>
						            		<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
                                                @if($bpiCardPresent)
                                                <label class="radio-inline">
											    	<input {{ $bpiCardPresent ? '' : 'disabled' }} type="radio" name="new_renewal_bpi" value="new" required {{ $bpiCard[0]['new_renewal'] == 'new' ? 'checked' : '' }}>New
											    </label>
											    <label class="radio-inline">
											    	<input {{ $bpiCardPresent ? '' : 'disabled' }} type="radio" name="new_renewal_bpi" value="renewal" required {{ $bpiCard[0]['new_renewal'] == 'renewal' ? 'checked' : '' }}>Renewal
											    </label>
                                                @else
                                                <label class="radio-inline">
											    	<input disabled type="radio" name="new_renewal_bpi" value="new" required>New
											    </label>
											    <label class="radio-inline">
											    	<input disabled type="radio" name="new_renewal_bpi" value="renewal" required>Renewal
											    </label>
                                                @endif
                                                <br>
						            			<span style="color:#a94442;font-style: italic;">{{$errors->has('new_renewal_bpi') ? $errors->first('new_renewal_bpi') : ""}}</span>
						            		</div>
						            	</div>

										<div class="form-group {{$errors->has('card_no_bpi') ? 'has-error' : ""}}">
											<label class="col-md-3 col-lg-3 col-sm-12 col-xs-12 control-label">Card No. *</label>
											<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
                                                @if($bpiCardPresent)
                                                <input type="text" class="form-control" name="card_no_bpi" required value="{{ $bpiCard[0]['card_no'] }}">
                                                @else
                                                <input disabled type="text" class="form-control" name="card_no_bpi" required value="{{ $errors->has() ? old('card_no_bpi') : (isset($user->card_no_bpi) ? $user->card_no_bpi : '') }}">
                                                @endif
												<span style="color:#a94442;font-style: italic;">{{$errors->has('card_no_bpi') ? $errors->first('card_no_bpi') : ""}}</span>
											</div>
										</div>

										<div class="form-group {{$errors->has('name_in_card') ? 'has-error' : ""}}">
											<label class="col-md-3 col-lg-3 col-sm-12 col-xs-12 control-label">Name In Card *</label>
											<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
                                                @if($bpiCardPresent)
                                                <input type="text" class="form-control" name="name_in_card" required value="{{ $bpiCard[0]['name_in_card'] }}">
                                                @else
                                                <input disabled type="text" class="form-control" name="name_in_card" required value="{{ $errors->has() ? old('name_in_card') : (isset($user->name_in_card) ? $user->name_in_card : '') }}">
                                                @endif
												<span style="color:#a94442;font-style: italic;">{{$errors->has('name_in_card') ? $errors->first('name_in_card') : ""}}</span>
											</div>
										</div>

										<div class="form-group {{$errors->has('card_expiry_bpi') ? 'has-error' : ""}}">
											<label class="col-md-3 col-lg-3 col-sm-12 col-xs-12 control-label">Card Expiry *</label>
											<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
                                                @if($bpiCardPresent)
                                                <input type="text" class="form-control datetime" name="card_expiry_bpi" required value="{{ $bpiCard[0]['card_expiry'] }}">
                                                @else
                                                <input disabled type="text" class="form-control datetime" name="card_expiry_bpi" required value="{{ $errors->has() ? old('card_expiry_bpi') : (isset($user->card_expiry_bpi) ? $user->card_expiry_bpi : date('YYYY-MM-DD')) }}">
                                                @endif
												<span style="color:#a94442;font-style: italic;">{{$errors->has('card_expiry_bpi') ? $errors->first('card_expiry_bpi') : ""}}</span>
											</div>
										</div>

										<div class="form-group {{$errors->has('source_of_income') ? 'has-error' : ""}}">
											<label class="col-md-3 col-lg-3 col-sm-12 col-xs-12 control-label">Source of Income</label>
											<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
                                                @if($bpiCardPresent)
                                                <input type="text" class="form-control" name="source_of_income" value="{{ $bpiCard[0]['source_of_income'] }}">
                                                @else
                                                <input disabled type="text" class="form-control" name="source_of_income" value="{{ $errors->has() ? old('source_of_income') : (isset($user->source_of_income) ? $user->source_of_income : '') }}">
                                                @endif
												<span style="color:#a94442;font-style: italic;">{{$errors->has('source_of_income') ? $errors->first('source_of_income') : ""}}</span>
											</div>
										</div>

										<div class="form-group {{$errors->has('birthday_bpi') ? 'has-error' : ""}}">
											<label class="col-md-3 col-lg-3 col-sm-12 col-xs-12 control-label">Birthday</label>
											<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
                                                @if($bpiCardPresent)
                                                <input type="text" class="form-control datetime" name="birthday_bpi" value="{{ $bpiCard[0]['birthday'] }}"  >
                                                @else
                                                <input disabled type="text" class="form-control datetime" name="birthday_bpi" value="{{ $errors->has() ? old('birthday_bpi') : (isset($user->birthday_bpi_bpi) ? $user->birthday_bpi_bpi : date('YYYY-MM-DD')) }}"  >
                                                @endif
												<span style="color:#a94442;font-style: italic;">{{$errors->has('birthday_bpi') ? $errors->first('birthday_bpi') : ""}}</span>
											</div>
										</div>

										<div class="form-group {{$errors->has('amount_bpi') ? 'has-error' : ""}}">
											<label class="col-md-3 col-lg-3 col-sm-12 col-xs-12 control-label">Amount</label>
											<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
                                                @if($bpiCardPresent)
                                                <input type="number" class="form-control" name="amount_bpi" value="{{ $bpiCard[0]['amount'] }}">
                                                @else
                                                <input disabled type="number" class="form-control" name="amount_bpi" value="{{ $errors->has() ? old('amount_bpi') : (isset($user->amount_bpi) ? $user->amount_bpi : '') }}">
                                                @endif
												<span style="color:#a94442;font-style: italic;">{{$errors->has('amount_bpi') ? $errors->first('amount_bpi') : ""}}</span>
											</div>
										</div>

										<div class="form-group {{$errors->has('ar_no_bpi') ? 'has-error' : ""}}">
											<label class="col-md-3 col-lg-3 col-sm-12 col-xs-12 control-label">A/R No.</label>
											<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
                                                @if($bpiCardPresent)
                                                <input type="text" class="form-control" name="ar_no_bpi" value="{{ $bpiCard[0]['ar_no'] }}">
                                                @else
                                                <input disabled type="text" class="form-control" name="ar_no_bpi" value="{{ $errors->has() ? old('ar_no_bpi') : (isset($user->ar_no_bpi) ? $user->ar_no_bpi : '') }}">
                                                @endif
												<span style="color:#a94442;font-style: italic;">{{$errors->has('ar_no_bpi') ? $errors->first('ar_no_bpi') : ""}}</span>
											</div>
										</div>

										<div class="form-group {{$errors->has('remarks_bpi') ? 'has-error' : ""}}">
											<label class="col-md-3 col-lg-3 col-sm-12 col-xs-12 control-label">Remarks</label>
											<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
                                                @if($bpiCardPresent)
                                                <input type="text" class="form-control" name="remarks_bpi" value="{{ $bpiCard[0]['remarks'] }}">
                                                @else
                                                <input disabled type="text" class="form-control" name="remarks_bpi" value="{{ $errors->has() ? old('remarks_bpi') : (isset($user->remarks_bpi) ? $user->remarks_bpi : '') }}">
                                                @endif
												<span style="color:#a94442;font-style: italic;">{{$errors->has('remarks_bpi') ? $errors->first('remarks_bpi') : ""}}</span>
											</div>
										</div>
						            </div>
					            </div>

                                <div class="{{ $vipCardPresent > 0 ? '' : 'hidden' }}" id="vip-settings-container">
									<div class="x_title">
										<h3>VIPINOY</h3>
						            </div>
						            <div class="x_content">
						            	<div class="form-group {{$errors->has('new_renewal_vip') ? 'has-error' : ""}}">
						            		<label class="col-md-3 col-lg-3 col-sm-12 col-xs-12 control-label">Card Status *</label>
						            		<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
                                                @if($vipCardPresent)
                                                <label class="radio-inline">
											    	<input {{ $vipCardPresent ? '' : 'disabled' }} type="radio" name="new_renewal_vip" required value="new" {{ $vipCard[0]['new_renewal'] == 'new' ? 'checked' : '' }}>New
											    </label>
											    <label class="radio-inline">
											    	<input {{ $vipCardPresent ? '' : 'disabled' }} type="radio" name="new_renewal_vip" required value="renewal" {{ $vipCard[0]['new_renewal'] == 'renewal' ? 'checked' : '' }}>Renewal
											    </label>
                                                @else
                                                <label class="radio-inline">
											    	<input disabled type="radio" name="new_renewal_vip" required value="new">New
											    </label>
											    <label class="radio-inline">
											    	<input disabled type="radio" name="new_renewal_vip" required value="renewal">Renewal
											    </label>
                                                @endif
                                                <br>
						            			<span style="color:#a94442;font-style: italic;">{{$errors->has('new_renewal_vip') ? $errors->first('new_renewal_vip') : ""}}</span>
						            		</div>
						            	</div>

										<div class="form-group {{ $errors->has('card_no_vip') ? 'has-error' : ""}}">
											<label class="col-md-3 col-lg-3 col-sm-12 col-xs-12 control-label">Card No. *</label>
											<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
                                                @if($vipCardPresent)
                                                <input type="text" class="form-control" name="card_no_vip" required value="{{ $vipCard[0]['card_no'] }}">
                                                @else
                                                <input disabled type="text" class="form-control" name="card_no_vip" required value="{{ $errors->has() ? old('card_no_vip') : (isset($user->card_no_vip) ? $user->card_no_vip : "") }}">
                                                @endif
												<span style="color:#a94442;font-style: italic;">{{ $errors->has('card_no_vip') ? $errors->first('card_no_vip') : ""}}</span>
											</div>
										</div>

										<div class="form-group {{$errors->has('name_of_ofw') ? 'has-error' : ""}}">
											<label class="col-md-3 col-lg-3 col-sm-12 col-xs-12 control-label">Name of OFW *</label>
											<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
                                                @if($vipCardPresent)
                                                <input type="text" class="form-control" name="name_of_ofw" required value="{{ $vipCard[0]['name_of_ofw'] }}">
                                                @else
                                                <input disabled type="text" class="form-control" name="name_of_ofw" required value="{{$errors->has() ? old('name_of_ofw') : (isset($user->name_of_ofw) ? $user->name_of_ofw : "") }}">
                                                @endif
												<span style="color:#a94442;font-style: italic;">{{ $errors->has('name_of_ofw') ? $errors->first('name_of_ofw') : ""}}</span>
											</div>
										</div>

										<div class="form-group {{ $errors->has('card_expiry_vip') ? 'has-error' : ""}}">
											<label class="col-md-3 col-lg-3 col-sm-12 col-xs-12 control-label">Card Expiry *</label>
											<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
                                                @if($vipCardPresent)
                                                <input type="text" class="form-control datetime" name="card_expiry_vip" required value="{{ $vipCard[0]['card_expiry'] }}">
                                                @else
                                                <input disabled type="text" class="form-control datetime" name="card_expiry_vip" required value="{{ $errors->has() ? old('card_expiry_vip') : (isset($user->card_expiry_vip) ? $user->card_expiry_vip : date('YYYY-MM-DD')) }}">
                                                @endif
												<span style="color:#a94442;font-style: italic;">{{ $errors->has('card_expiry_vip') ? $errors->first('card_expiry_vip') : ""}}</span>
											</div>
										</div>

										<div class="form-group {{ $errors->has('relationship') ? 'has-error' : ""}}">
											<label class="col-md-3 col-lg-3 col-sm-12 col-xs-12 control-label">Relationship *</label>
											<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
                                                @if($vipCardPresent)
                                                <input type="text" class="form-control" name="relationship" required value="{{ $vipCard[0]['relationship'] }}">
                                                @else
                                                <input disabled type="text" class="form-control" name="relationship" required value="{{ $errors->has() ? old('relationship') : (isset($user->relationship) ? $user->relationship : "") }}">
                                                @endif
												<span style="color:#a94442;font-style: italic;">{{ $errors->has('relationship') ? $errors->first('relationship') : ""}}</span>
											</div>
										</div>

										<div class="form-group {{ $errors->has('document_presented') ? 'has-error' : ""}}">
											<label class="col-md-3 col-lg-3 col-sm-12 col-xs-12 control-label">Document Presented</label>
											<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
                                                @if($vipCardPresent)
                                                <input type="text" class="form-control" name="document_presented" value="{{ $vipCard[0]['document_presented'] }}">
                                                @else
                                                <input disabled type="text" class="form-control" name="document_presented" value="{{ $errors->has() ? old('document_presented') : (isset($user->document_presented) ? $user->document_presented : "") }}">
                                                @endif
												<span style="color:#a94442;font-style: italic;">{{ $errors->has('document_presented') ? $errors->first('document_presented') : ""}}</span>
											</div>
										</div>

										<div class="form-group {{ $errors->has('document_no') ? 'has-error' : ""}}">
											<label class="col-md-3 col-lg-3 col-sm-12 col-xs-12 control-label">Document No.</label>
											<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
                                                @if($vipCardPresent)
                                                <input type="text" class="form-control" name="document_no" value="{{ $vipCard[0]['document_no'] }}">
                                                @else
                                                <input disabled type="text" class="form-control" name="document_no" value="{{ $errors->has() ? old('document_no') : (isset($user->document_no) ? $user->document_no : "") }}">
                                                @endif
												<span style="color:#a94442;font-style: italic;">{{ $errors->has('document_no') ? $errors->first('document_no') : ""}}</span>
											</div>
										</div>

										<div class="form-group {{ $errors->has('birthday_vip') ? 'has-error' : ""}}">
											<label class="col-md-3 col-lg-3 col-sm-12 col-xs-12 control-label">Birthday</label>
											<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
                                                @if($vipCardPresent)
                                                <input type="text" class="form-control datetime" name="birthday_vip" value="{{ $vipCard[0]['birthday'] }}">
                                                @else
                                                <input disabled type="text" class="form-control datetime" name="birthday_vip" value="{{ $errors->has() ? old('birthday_vip') : (isset($user->birthday_vip) ? $user->birthday_vip : date('YYYY-MM-DD')) }}">
                                                @endif
												<span style="color:#a94442;font-style: italic;">{{ $errors->has('birthday_vip') ? $errors->first('birthday_vip') : ""}}</span>
											</div>
										</div>

										<div class="form-group {{ $errors->has('amount_vip') ? 'has-error' : ""}}">
											<label class="col-md-3 col-lg-3 col-sm-12 col-xs-12 control-label">Amount</label>
											<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
                                                @if($vipCardPresent)
                                                <input type="number" class="form-control" name="amount_vip" value="{{ $vipCard[0]['amount'] }}">
                                                @else
                                                <input disabled type="number" class="form-control" name="amount_vip" value="{{ $errors->has() ? old('amount_vip') : (isset($user->amount_vip) ? $user->amount_vip : "") }}">
                                                @endif
												<span style="color:#a94442;font-style: italic;">{{ $errors->has('amount_vip') ? $errors->first('amount_vip') : ""}}</span>
											</div>
										</div>

										<div class="form-group {{ $errors->has('ar_no_vip') ? 'has-error' : ""}}">
											<label class="col-md-3 col-lg-3 col-sm-12 col-xs-12 control-label">A/R No.</label>
											<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
                                                @if($vipCardPresent)
                                                <input type="text" class="form-control" name="ar_no_vip" value="{{ $vipCard[0]['ar_no'] }}">
                                                @else
                                                <input disabled }} type="text" class="form-control" name="ar_no_vip" value="{{ $errors->has() ? old('ar_no_vip') : (isset($user->ar_no_vip) ? $user->ar_no_vip : "") }}">
                                                @endif
												<span style="color:#a94442;font-style: italic;">{{ $errors->has('ar_no_vip') ? $errors->first('ar_no_vip') : ""}}</span>
											</div>
										</div>

										<div class="form-group {{ $errors->has('remarks_vip') ? 'has-error' : ""}}">
											<label class="col-md-3 col-lg-3 col-sm-12 col-xs-12 control-label">Remarks</label>
											<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
                                                @if($vipCardPresent)
                                                <input type="text" class="form-control" name="remarks_vip" value="{{ $vipCard[0]['remarks'] }}">
                                                @else
                                                <input disabled type="text" class="form-control" name="remarks_vip" value="{{ $errors->has() ? old('remarks_vip') : (isset($user->remarks_vip) ? $user->remarks_vip : "") }}">
                                                @endif
												<span style="color:#a94442;font-style: italic;">{{ $errors->has('remarks_vip') ? $errors->first('remarks_vip') : ""}}</span>
											</div>
										</div>
						            </div>
						        </div>


								@if(!preg_match('/members/', URL::previous()))
									@if(Auth::user()->usertype == 3)
										<div class="form-group">
											<label class="col-md-3 control-label">Role</label>
											<div class="col-md-9">
												{!! Form::select('userType', $userTypes, Input::old('userType'), ['class' => 'form-control']) !!}
											</div>
										</div>
									@elseif(Auth::user()->usertype == 2)
										<input type="hidden" name="usertype" value="2">
									@endif
									<div class="form-group">
										<label class="col-md-3 control-label">Password</label>
										<div class="col-md-9">
											<input type="password" class="form-control" name="password" autocomplete="off">
										</div>
									</div>

									<div class="form-group">
										<label class="col-md-3 control-label">Confirm Password</label>
										<div class="col-md-9">
											<input type="password" class="form-control" name="password_confirmation" autocomplete="off">
										</div>
									</div>
								@endif
								-->
								<div class="form-group">
									<div class="col-md-9 col-md-offset-3">
										<button type="submit" class="btn btn-primary" style="color:#fff;">
											Update
										</button>
										@if(!preg_match('/edit\/user/', URL::previous()))
											<a href="{{ URL::to('res/members/') }}">
												<button type="button" class="btn btn-default btn-no-shadow">
													Cancel
												</button>
											</a>
										@else
											<a href="{{ URL::to('res/members/') }}">
												<button type="button" class="btn btn-default btn-no-shadow">
													Cancel
												</button>
											</a>
										@endif

									</div>
								</div>
							</div>
			            </div>
			            <div class="x_footer">
			            </div>
		            </form>
		        </div>
		    </div>
		</div>
@stop

@section('scripts')
<script type="text/javascript" src="{{ asset('public/js/members.js') }}"></script>
@stop
