@extends('admin.layouts.admin-layout')
@section('content')
<div class="clearfix"></div>
@if (count($errors) > 0)
    <div class="alert alert-danger">
        Whoops! There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (Session::has('message'))
    <div class="alert {{Session::get('messageClass')}}">
        {{Session::get('message')}}
    </div>
@endif
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">

            <form method="get">
                <h2 class="col-md-2 pull-left">List of members</h2>
                <div class="col-lg-8 col-md-5 pull-right">
                    <div class="input-group">
                        <input type="text" name="search" id="search" value="{{Input::get('search', '')}}" class="form-control with-shadow" placeholder="Search for...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">Go!</button>
                            <button class="btn btn-default" onclick="document.getElementById('search').value='';" type="submit">View All</button>
                            <a href="{{URL::to('res/members/create')}}" class="btn btn-primary">Register</a>
                        </span>
                    </div>
                </div>
                <div class="clearfix"></div>
            </form>

                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                @if(Session::has('msg_success'))
                    <div class="alert alert-success">
                        <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {{Session::get('msg_success')}}
                    </div>
                @elseif(Session::has('msg_error'))
                    <div class="alert alert-danger">
                        {{Session::get('msg_error')}}
                        @if(!$errors->isEmpty())
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                            @endforeach
                        </ul>
                        @endif
                    </div>
                @endif
                <table class="table table-striped">
                    <tbody>
                        @forelse($members as $key => $member)
                            <tr>
                                <td width="1%" style="vertical-align: middle;">{{ (Input::get('page', 1) - 1) * 20 + $key + 1 }}. </td>
                                <td width="1%">
                                    <img src="{{asset('public/app/img/user.png')}}" alt="" class="img-circle" width="70">
                                </td>
                                <td width="20%">
                                    {{ucwords($member->last_name . ", " . $member->first_name . " " . $member->middle_name)}}<br/>
                                    <small>
                                        {{$member->email}}<br/>
                                        {{$member->contact}}<br/>
                                        <i>Total Entries: {{ $member->entries()->count() }}</i>
                                    </small>
                                </td>
                                <td width="30%">
                                    {{$member->address}}<br/>
                                    <small>
                                        <i class="fa {{ in_array('bpi', $member->cards->lists('card_type')) ? 'fa-check text-success' : 'fa-remove text-danger' }}"></i>&nbsp;BPI Amore<br/>
                                        <i class="fa {{ in_array('vip', $member->cards->lists('card_type')) ? 'fa-check text-success' : 'fa-remove text-danger' }}"></i>&nbsp;VIPinoy<br/>
                                        <i>Created: {{date('M j, Y h:i:s A', strtotime($member->created_at))}}</i>
                                    </small>
                                </td>
                                <td width="49%" class="text-right">
                                    <div class="btn-group">
                                        <button class="btn btn-default btn-view-member-coupons" data-target="#view-modal" data-toggle="modal" data-memberid="{{$member->id}}" title="View coupons of this member">
                                            <i class="fa fa-eye"></i>
                                        </button>
                                        <button class="btn btn-default btn-link" data-href="members/{{$member->id}}/edit" title="Edit this member">
                                            <i class="fa fa-pencil"></i>
                                        </button>
                                       
                                    </div>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6">No members yet</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <div class="x_footer">
                {!! $members->render() !!}
            </div>
        </div>
    </div>
</div>
<div id="view-modal" class="modal fade bs-example-modal-sm modal-coupons-view" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel2">Coupons List</h4>
                </div>
                <div class="modal-body coupons-table" style="max-height: 700px;overflow-y: scroll;">
                    Loading entries...
                </div>
            </div>
        </div>
    </div>
@stop
@section('scripts')
<script>
    $(document).ready(function() {
        $(".btn-view-member-coupons").unbind().click(function(event) {
            var memberId = $(this).data('memberid');
            var url = 'members/getEntries/' + memberId;
            // var url = 'getEntriesOfUser/' + memberId;
            $(".coupons-table").html('Loading entries...');
            var table = '<table class="table">' +
                            '<thead>' +
                                '<tr>' +
                                    '<th></th>' +
                                    '<th>Coupon No.</th>' +
                                    '<th class="text-center">Store Name</th>' +
                                    '<th class="text-center">Receipt No.</th>' +
                                    '<th class="text-right">Amount</th>' +
                                '</tr>' +
                            '</thead>' +
                            '<tbody>';
            $.get(url, function(data) {
                if(data.length == 0){
                    table +=    '<tr><td colspan="5" align="center">No entries found.</td></tr>';
                }else{
                    $.each(data, function(index, val) {
                        table +=    '<tr>' +
                                        '<td>'+ (index + 1) +'</td>' +
                                        '<td>'+ val.unique_id +'</td>' +
                                        '<td class="text-center">'+ val.store_name +'</td>' +
                                        '<td class="text-center">'+ val.receipt_no +'</td>' +
                                        '<td class="text-right">₱ '+ $.number(val.amount, 2) +'</td>' +
                                    '</tr>';
                    });
                }
                table +=    '</tbody>' +
                        '</table>';
                $(".coupons-table").html(table);
            });
        });
    });
</script>
@stop
