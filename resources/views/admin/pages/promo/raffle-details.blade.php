@if($errors->has())
	<div class="{{old('promo_type') != "" && in_array("1", old('promo_type')) ? "" : "hidden"}}" id="raffle-settings-container">
@else
	<div class="{{$promo->promo_type == 3 || $promo->promo_type == 1  ? "" : "hidden"}}" id="raffle-settings-container">
@endif
	<div class="x_title">
		<h3>Raffle Settings</h3>
      </div>
      <div class="x_content">
      	<div class="form-group {{$errors->has('raffle_premise_type') ? 'has-error' : ""}}">
			<label class="col-md-3 col-lg-3 col-sm-12 col-xs-12 control-label">Premise *</label>
			<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12 control-label">
            <label>
            <input type="radio" class="flat raffle-premise-type" name="raffle_premise_type" id="raffle-premise-type-per-amount" value="1" {{$errors->has() && old('raffle_premise_type') == 1 ? "checked" : (isset($promo->raffle_premise) && $promo->raffle_premise == 1 ? "checked" : "")}}  {{$process == "view" || $promo->promo_type == 2 ? "disabled" : ""}}/>
            Per amount</label>
            <label style="margin-left: 20px;">
            <input type="radio" class="flat raffle-premise-type" name="raffle_premise_type" id="raffle-premise-type-per-minimum" value="2" {{$errors->has() && old('raffle_premise_type') == 2 ? "checked" : (isset($promo->raffle_premise) && $promo->raffle_premise == 2 ? "checked" : "")}}  {{$process == "view" || $promo->promo_type == 2 ? "disabled" : ""}}/>
            Per minimum amount</label>
            <span style="color:#a94442;font-style: italic;">{{$errors->has('raffle_premise_type') ? $errors->first('raffle_premise_type') : ""}}</span>
			</div>
		</div>
		<div class="form-group {{$errors->has('raffle_purchase_requirement') ? 'has-error' : ""}}">
			<label class="col-md-3 col-lg-3 col-sm-12 col-xs-12 control-label">Purchase Requirement *</label>
			<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
				<input type="number" class="form-control" name="raffle_purchase_requirement" value="{{$errors->has() ? old('raffle_purchase_requirement') : (isset($promo->raffle_purchase_requirement) ? $promo->raffle_purchase_requirement : "")}}" {{$process == "view" || $promo->promo_type == 2 ? "readonly" : ""}}>
				<span style="color:#a94442;font-style: italic;">{{$errors->has('raffle_purchase_requirement') ? $errors->first('raffle_purchase_requirement') : ""}}</span>
			</div>
		</div>
		<div class="form-group {{$errors->has('raffle_accumulation_type') ? 'has-error' : ""}}">
			<label class="col-md-3 col-lg-3 col-sm-12 col-xs-12 control-label">Receipt Accumulation *</label>
			<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12 control-label">
            @if($errors->has())
                  <label>
                  <input type="checkbox" class="flat raffle-accumulation-type" name="raffle_accumulation_type[]" id="raffle_accumulation-type-single" value="1" {{null == old('raffle_accumulation_type') ? "" : (in_array("1", old('raffle_accumulation_type')) ? "checked" : "")}}  {{$process == "view" || $promo->promo_type == 2 ? "disabled" : ""}}/>
                  Single</label>
                  <label style="margin-left: 20px;">
                  <input type="checkbox" class="flat raffle-accumulation-type" name="raffle_accumulation_type[]" id="raffle_accumulation-type-accumulated" value="2" {{null == old('raffle_accumulation_type') ? "" : (in_array("2", old('raffle_accumulation_type')) ? "checked" : "")}}  {{$process == "view" || $promo->promo_type == 2 ? "disabled" : ""}}/>
                  Accumulated</label>
           	@else
           		<label>
                  <input type="checkbox" class="flat raffle-accumulation-type" name="raffle_accumulation_type[]" id="raffle_accumulation-type-single" value="1" {{isset($promo->raffle_accumulation_type) && $promo->raffle_accumulation_type == 1 || $promo->raffle_accumulation_type == 3 ? "checked" : ""}}  {{$process == "view" || $promo->promo_type == 2 ? "disabled" : ""}}/>
                  Single</label>
                  <label style="margin-left: 20px;">
                  <input type="checkbox" class="flat raffle-accumulation-type" name="raffle_accumulation_type[]" id="raffle_accumulation-type-accumulated" value="2" {{isset($promo->raffle_accumulation_type) && $promo->raffle_accumulation_type == 2 || $promo->raffle_accumulation_type == 3 ? "checked" : ""}}  {{$process == "view" || $promo->promo_type == 2 ? "disabled" : ""}}/>
                  Accumulated</label>
           	@endif
           	<br />
           	<span style="color:#a94442;font-style: italic;font-weight:400;">{{$errors->has('raffle_accumulation_type') ? $errors->first('raffle_accumulation_type') : ""}}</span>
			</div>
		</div>
		<div class="form-group {{$errors->has('raffle_multiplier') ? 'has-error' : ""}}">
			<label class="col-md-3 col-lg-3 col-sm-13 col-xs-13 control-label">Multiplier</label>
			<div class="col-md-9 col-lg-9 col-sm-9 col-xs-9 control-label">
				<label>
				<input type="checkbox" class="flat raffle-multiplier" name="raffle_multiplier" value="1" {{$process == "view" ? "disabled" : ""}} {{$errors->has() && old('raffle_multiplier') == 1 ? "checked" : (isset($promo->raffle_multiplier) && $promo->raffle_multiplier == 1 ? "checked" : "")}}>
				X2</label>
				<span style="color:#a94442;font-style: italic;">{{$errors->has('raffle_multiplier') ? $errors->first('raffle_multiplier') : ""}}</span>
			</div>
		</div>
      </div>
</div>