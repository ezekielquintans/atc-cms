@extends('admin.layouts.admin-layout')
@section('content')
	<div class="clearfix"></div>
	<div class="row">
	    <div class="col-md-12 col-sm-12 col-xs-12">
	        <div class="x_panel">
	        @if(Session::has('msg_error'))
		        <div class="alert alert-error">
		            <a href="{{URL::to(Request::url())}}" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            {{Session::get('msg_error')}}
		        </div>
			@elseif(Session::has('msg_success'))
				<div class="alert alert-success">
		            <a href="{{URL::to(Request::url())}}" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            {{Session::get('msg_success')}}
		        </div>
		    @endif
	            <div class="x_title">
		            <form method="get">
		                <h2 class="col-md-3">List of Promos</h2>
		                <div class="col-md-9">
		                    <div class="row">
		                        <div class="col-md-7">
		                            <div class="form-group">
		                                <input type="text" name="search" id="search" value="{{Input::get('search', '')}}" class="form-control" placeholder="Search for...">
		                            </div>
		                        </div>
		                        <div class="col-md-5">
		                            <div class="form-group text-right">
		                                <button class="btn btn-default" type="submit">Go!</button>
		                                <button class="btn btn-default" onclick="document.getElementById('search').value='';" type="submit">View All</button>
		                                <a href="{{URL::to('res/promo/create')}}"><button class="btn btn-default" type="button">Create a Promo</button></a>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		            </form>
	                <div class="clearfix"></div>
	            </div>
	            <div class="x_content">
	            	<table class="table table-striped">
						<thead>
							<tr>
								<th></th>												
								<th>Promo</th>
								<th class="text-center">Status</th>
								<th class="text-center">Start</th>
								<th class="text-center">End</th>
								<th class="text-center"></th>
							</tr>
						</thead>
	                    <tbody>
                        	@if(!$promo->isEmpty())
                        		@foreach($promo as $key => $row)

                        			<tr>
                        				<td width="1">{{ (Input::get('page', 1) - 1) * 10 + $key + 1 }}. </td>
                        				<td>{{$row->name}}</td>
                        				<td class="text-center"><span class="label label-{{$row->date_ended->isPast() === true ? "danger" : "success"}}">{{$row->date_ended->isPast() === true  ?  "ENDED" : "ON-GOING"}}</span></td>
                        				<td class="text-center">{{$row->date_started->toFormattedDateString()}}</td>
                        				<td class="text-center">{{$row->date_ended->toFormattedDateString()}}</td>
                        				<td align="right">
                        					<div class="btn-group">
                        						<button class="btn btn-default btn-link" data-href="{{URL::to('res/promo/'.$row->id)}}" title="View this promo">
                        							<i class="fa fa-eye"></i>
                        						</button>
                        						<button class="btn btn-default btn-link" data-href="{{URL::to('res/promo/'.$row->id.'/edit')}}" title="Edit this promo">
                        							<i class="fa fa-pencil"></i>
                        						</button>
                        						<button class="btn btn-default btn-link btn-delete" data-href="{{URL::to('res/promo/destroy/'. $row->id)}}" title="Delete this promo">
                        							<i class="fa fa-trash-o"></i>
                        						</button>
                                            </div>
                        				</td>
                        			</tr>
                        		@endforeach
                        	@else
                        		<tr>
                        			<td colspan="6">There are no promo yet.</td>
                        		</tr>
                        	@endif
	                    </tbody>
	            	</table>
	            </div>
	            <div class="x_footer">
	                {!! $promo->render() !!}
	            </div>
	        </div>
	    </div>
	</div>

@stop