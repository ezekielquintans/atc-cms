@extends('admin.layouts.admin-layout')
@section('content')
		<div class="clearfix"></div>
		<div class="row">
			<form class="form-horizontal" role="form" method="POST" action="{{URL::to('res/promo')}}" enctype="multipart/form-data" id="promo-form">
		    		<div class="col-lg-10 col-md-12 col-xs-12 col-lg-offset-1">
			        <div class="x_panel">
			            <div class="x_title row">
								<div class="col-sm-6">
									<h3>Promo Details</h3>
								</div>
								<div class="dropdown pull-right">
									<button type="button" btn-secondary class="btn dropdown-toggle" style="background-color: Transparent;color:#337ab7;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<i class="fa fa-cog" aria-hidden="true"></i>
									</button>
									<div class="dropdown-menu">
										<a class="dropdown-item btn" href="{{URL::to("res/promo")}}" style="color:inherit;">Go to Promo List</a>
										<div class="dropdown-divider"></div>
										@if($process != "add")
											<a class="dropdown-item btn" href="{{URL::to('res/promo/create')}}" style="color:inherit;">Add a Promo</a>
											@if($process != "edit")
												<a class="dropdown-item btn" href="{{URL::to('res/promo/'.$promo->id.'/edit')}}" style="color:inherit;">Edit this promo</a>
											@endif
											<button type="button" class="dropdown-item btn btn-link btn-delete" data-href="{{URL::to('res/promo/destroy/'. $promo->id)}}" style="color:inherit;">Delete this promo</button>
										@endif
									</div>
								</div>
			            </div>

			            <div class="x_content">
						@if($errors->has())
					            <div class="alert alert-error">
					                <a href="{{ URL::to(Request::url()) }}" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					                Please check the required fields.
					            </div>
						@elseif(Session::has('msg_success'))
							<div class="alert alert-success">
				                		<a href="{{URL::to(Request::url())}}" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				                		{{Session::get('msg_success')}}
				            	</div>
					      @endif
					        <div id="alerts"></div>
							<div class="form-group {{$errors->has('name') ? 'has-error' : ""}}">
								<label class="col-md-3 col-lg-3 col-sm-12 col-xs-12 control-label">Promo Name *</label>
								<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
									<input 	type="text"
											class="form-control"
											name="name"
											value="{{ $errors->has() ? old('name') : (isset($promo->name) ? $promo->name : "") }}"
											placeholder="..." {{$process == "view" ? "readonly" : "required"}}
											required="" 
									>
									@if($process != "add" )
										<input type="hidden" class="form-control" name="promoid" value="{{ isset($promo->id) ? $promo->id : "" }}">
									@endif
									<span style="color:#a94442;font-style: italic;">{{$errors->has('name') ? $errors->first('name') : ""}}</span>
								</div>
							</div>
							<div class="form-group {{$errors->has('subtitle') ? 'has-error' : ""}}">
								<label class="col-md-3 col-lg-3 col-sm-12 col-xs-12 control-label">Subtitle</label>
								<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
									<input type="text" class="form-control"  name="subtitle" value="{{ $errors->has() ? old('subtitle') : (isset($promo->subtitle) ? $promo->subtitle : "") }}" placeholder="..." {{$process == "view" ? "readonly" : ""}}>
									<span style="color:#a94442;font-style: italic;">{{$errors->has('subtitle') ? $errors->first('subtitle') : ""}}</span>
								</div>
							</div>
							<div class="form-group {{$errors->has('helper') ? 'has-error' : ""}}">
								<label class="col-md-3 col-lg-3 col-sm-12 col-xs-12 control-label">Helper</label>
								<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
									<input type="text" class="form-control"  name="helper" value="{{ $errors->has() ? old('helper') : (isset($promo->helper) ? $promo->helper : "") }}" placeholder="..." {{$process == "view" ? "readonly" : "required"}} required="">
									<span style="color:#a94442;font-style: italic;">{{$errors->has('helper') ? $errors->first('helper') : ""}}</span>
								</div>
							</div>
							<div class="form-group {{$errors->has('description') ? 'has-error' : ""}}">
								<label class="col-md-3 col-lg-3 col-sm-12 col-xs-12 control-label">Description</label>
								<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
					                  	<textarea id="description" name="description">{{ $errors->has() ? old('description') : (isset($promo->description) ? $promo->description : "") }}</textarea>
					                  	<span style="color:#a94442;font-style: italic;">{{$errors->has('description') ? $errors->first('description') : ""}}</span>
								</div>
							</div>

							<div class="form-group {{$errors->has('dti_permit') ? 'has-error' : ""}}">
								<label class="col-md-3 col-lg-3 col-sm-12 col-xs-12 control-label">DTI Permit</label>
								<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
									<input type="text" class="form-control" name="dti_permit" value="{{ $errors->has() ? old('dti_permit') : (isset($promo->dti_permit) ? $promo->dti_permit : "") }}" placeholder="..." {{$process == "view" ? "readonly" : ""}}>
									<span style="color:#a94442;font-style: italic;">{{$errors->has('dti_permit') ? $errors->first('dti_permit') : ""}}</span>
								</div>
							</div>
							<div class="form-group {{$errors->has('duration') ? 'has-error' : ""}}">
								<label class="col-md-3 col-lg-3 col-sm-12 col-xs-12 control-label">Duration *</label>
								<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
									@if($errors->has())
										<input type="text" class="form-control duration" required name="duration" value="{{old('duration')}}" placeholder="..." {{$process == "view" ? "readonly" : ""}}>
									@else
										<input type="text" class="form-control duration" required name="duration" value="{{strtotime($promo->date_started) && strtotime($promo->date_ended) ? $promo->date_started->toDateString() . ' - ' . $promo->date_ended->toDateString() : "" }}" placeholder="..." {{$process == "view" ? "readonly" : ""}}>
									@endif
									<span style="color:#a94442;font-style: italic;">{{$errors->has('duration') ? $errors->first('duration') : ""}}</span>
								</div>
							</div>
							<div class="form-group {{$errors->has('featured_img') ? 'has-error' : ""}}">
								<label class="col-md-3 col-lg-3 col-sm-12 col-xs-12 control-label">Featured Image {{$process == "add" ? "*" : ""}}</label>
								<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
									<img id="create-cat-img" class="img-thumbnail {{$process == "view" || $process == "edit"? "" : "hidden"}}" src="{{$process == "view" || $process == "edit" ? asset(isset($promo->image_url) ? $promo->image_url : "") : ""}}" alt="Preview" width="100" height="100">
									@if($process == "add" || $process == "edit" )
										<input type="file" class="form-control img-upload" name="featured_img" {{$process == "view" ? "disabled" : ($process == "add" ? "required":"")}} accept="image/*">
										<p class="help-block">Max size is 7MB. Format: .jpg or .png</p>
									@endif
									<span style="color:#a94442;font-style: italic;">{{$errors->has('featured_img') ? $errors->first('featured_img') : ""}}</span>
								</div>
							</div>
							<div class="form-group {{$errors->has('promo_type') ? 'has-error' : ""}}">
								<label class="col-md-3 col-lg-3 col-sm-12 col-xs-12 control-label">Promo Type</label>
								<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12 control-label">
			                        @if($errors->has())
			                        	<label >
			                        	<input type="checkbox" class="js-switch promo-type" name="promo_type[]" id="promo-type-raffle" value="1" {{null == old('promo_type') ? "" : (in_array("1", old('promo_type')) ? "checked" : "")}}  {{$process == "view" ? "disabled" : ""}}/>
			                        	 Raffle</label>
				                        <label style="margin-left: 20px;">
				                        <input type="checkbox" class="js-switch promo-type" name="promo_type[]" id="promo-type-premium" value="2" {{null == old('promo_type') ? "" : (in_array("2", old('promo_type')) ? "checked" : "")}} {{$process == "view" ? "disabled" : ""}}/>
				                        Premium</label>
			                        @else
			                        	<label >
			                        	<input type="checkbox" class="js-switch promo-type" name="promo_type[]" id="promo-type-raffle" value="1" {{!isset($promo->promo_type) ? "" : ($promo->promo_type == 1 || $promo->promo_type == 3 ? "checked" : "")}}  {{$process == "view" ? "disabled" : ""}}/>
			                        	 Raffle</label>
				                        <label style="margin-left: 20px;">
				                        <input type="checkbox" class="js-switch promo-type" name="promo_type[]" id="promo-type-premium" value="2" {{!isset($promo->promo_type) ? "" : ($promo->promo_type == 2 || $promo->promo_type == 3 ? "checked" : "")}} {{$process == "view" ? "disabled" : ""}}/>
				                        Premium</label>
			                        @endif
			                       	<span style="color:#a94442;font-style: italic;">{{$errors->has('promo_type') ? $errors->first('promo_type') : ""}}</span>
								</div>
							</div>
						</div>
						@include('admin.pages.promo.raffle-details')
						@include('admin.pages.promo.premium-details')
			            
			            <div class="x_footer">
					        <input type="hidden" name="_token" value="{{ Session::token() }}">
					        <input type="hidden" id="input-process" name="process" value="{{$process}}">
					        <input type="hidden" name="raffle_on" id="raffle_on" value="{{!isset($promo->promo_type) ? "" : ($promo->promo_type == 1 || $promo->promo_type == 3 ? "1" : "")}}">
					        <input type="hidden" name="premium_on" id="premium_on" value="{{!isset($promo->promo_type) ? "" : ($promo->promo_type == 2 || $promo->promo_type == 3 ? "1" : "")}}">
					        <input type="hidden" name="promo_id" id="promo_id" value="{{isset($process) && $process == "edit" ? $promo->id : ""}}">
					        @if($process == "add" || $process == "edit")
					        	<button type="submit" class="btn btn-success pull-right">SUBMIT</button>
					        @endif
			            </div>
			        </div>
			    </div>
            	</form>
		</div>
@stop
@section("scripts")
<script src="{{asset("public/landing/js/tinymce/tinymce.min.js")}}"></script>
<script src="{{asset("public/landing/js/updatepromo.js")}}"></script>
@stop
