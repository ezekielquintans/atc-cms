@if($errors->has())
	<div class="{{old('promo_type') != "" && in_array("2", old('promo_type')) ? "" : "hidden"}}" id="premium-settings-container">
@else
	<div class="{{$promo->promo_type == 3 || $promo->promo_type == 2 ? "" : "hidden"}}" id="premium-settings-container">
@endif
      <div class="x_title">
			<h3>Premium Settings</h3>
      </div>
      <div class="x_content">
      	<div class="form-group {{$errors->has('premium_isEvent') ? 'has-error' : ""}}">
			<label class="col-md-3 col-lg-3 col-sm-13 col-xs-13 control-label">Event</label>
			<div class="col-md-9 col-lg-9 col-sm-9 col-xs-9 control-label">
				<label>
					<input type="checkbox" class="flat premium-isEvent" name="premium_isEvent" value="1" {{$process == "view" || $promo->promo_type == 1 ? "disabled" : ""}} {{$errors->has() && old('premium_isEvent') == 1 ? "checked" : (isset($promo->premium_isEvent) && $promo->premium_isEvent == 1 ? "checked" : "")}}>
				</label>
				<span style="color:#a94442;font-style: italic;">{{$errors->has('premium_isEvent') ? $errors->first('premium_isEvent') : ""}}</span>
			</div>
		</div>
		<div class="form-group {{$errors->has('reward_id') ? 'has-error' : ""}}">
			<label class="col-md-3 col-lg-3 col-sm-12 col-xs-12 control-label">Reward *</label>
			<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
				@if(!$rewards->isEmpty())
				<select class="form-control" name="reward_id" {{$process == "view" || $promo->promo_type == 1 ? "disabled" : ""}} >
					<option value="0" {{$promo->reward_id == "" || $promo->reward_id == 0? "selected" : ""}}>-Select Reward-</option>
					@if($promo->reward_id != 0)
					<option value="{{$promo->rewards->id}}" selected="">{{$promo->rewards->name}}</option>
					@endif
					@foreach($rewards as $reward)
					<option value="{{$reward->id}}" {{$reward->id == $promo->reward_id ? "selected" : ""}}>{{$reward->name}}</option>
					@endforeach
				</select>
				<span style="color:#a94442;font-style: italic;">{{$errors->has('premium_purchase_requirement') ? $errors->first('premium_purchase_requirement') : ""}}</span>
				@else
				@if($promo->reward_id != 0)
				<input type="hidden" name="reward_id" value="{{$promo->reward_id}}">
				@endif
				<div class="alert alert-warning alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
					<strong>Hi!</strong> It seems there's no available rewards. <a href="{{URL::to('res/rewards/create')}}">Click here</a> to make one!
				</div>
				@endif
			</div>
		</div>
      	<div class="form-group {{$errors->has('premium_premise_type') ? 'has-error' : ""}}">
			<label class="col-md-3 col-lg-3 col-sm-12 col-xs-12 control-label">Premise *</label>
			<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
            <label>
            <input type="radio" class="flat premium-premise-type" name="premium_premise_type" id="premium-premise-type-per-amount" value="1" {{$errors->has() && old('premium_premise_type') == 1 ? "checked" : (isset($promo->premium_premise) && $promo->premium_premise == 1 ? "checked" : "")}}  {{$process == "view" || $promo->promo_type == 1  ? "disabled" : ""}}/>
            Per amount</label>
            <label style="margin-left: 20px;">
            <input type="radio" class="flat premium-premise-type" name="premium_premise_type" id="premium-premise-type-per-minimum" value="2" {{$errors->has() && old('premium_premise_type') == 2 ? "checked" : (isset($promo->premium_premise) && $promo->premium_premise == 2 ? "checked" : "")}}  {{$process == "view" || $promo->promo_type == 1  ? "disabled" : ""}}/>
            Per minimum amount</label>
            <br />
            <span style="color:#a94442;font-style: italic;font-weight:400;">{{$errors->has('premium_premise_type') ? $errors->first('premium_premise_type') : ""}}</span>
			</div>
		</div>
		<div class="form-group {{$errors->has('premium_purchase_requirement') ? 'has-error' : ""}}">
			<label class="col-md-3 col-lg-3 col-sm-12 col-xs-12 control-label">Purchase Requirement *</label>
			<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
				<input type="number" class="form-control" name="premium_purchase_requirement" value="{{$errors->has() ? old('premium_purchase_requirement') : (isset($promo->premium_purchase_requirement) ? $promo->premium_purchase_requirement : "")}}" {{$process == "view" || $promo->promo_type == 1  ? "disabled" : ""}} >
				<span style="color:#a94442;font-style: italic;">{{$errors->has('premium_purchase_requirement') ? $errors->first('premium_purchase_requirement') : ""}}</span>
			</div>
		</div>
		<div class="form-group {{$errors->has('premium_accumulation_type') ? 'has-error' : ""}}">
			<label class="col-md-3 col-lg-3 col-sm-12 col-xs-12 control-label">Receipt Accumulation *</label>
			<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12 control-label">
				@if($errors->has())
                  <label>
                  <input type="radio" class="flat premium-accumulation-type" name="premium_accumulation_type[]" id="premium-accumulation-type-single" value="1" {{null == old('premium_accumulation_type') ? "" : (in_array("1", old('premium_accumulation_type')) ? "checked" : "")}}  {{$process == "view" || $promo->promo_type == 1 ? "disabled" : ""}}  />
                  Single</label>
                  <label style="margin-left: 20px;">
                  <input type="radio" class="flat premium-accumulation-type" name="premium_accumulation_type[]" id="premium-accumulation-type-accumulated" value="2" {{null == old('premium_accumulation_type') ? "" : (in_array("2", old('premium_accumulation_type')) ? "checked" : "")}}  {{$process == "view" || $promo->promo_type == 1 ? "disabled" : ""}} />
                  Accumulated</label>
              @else
              	<label>
                  <input type="radio" class="flat premium-accumulation-type" name="premium_accumulation_type[]" id="premium-accumulation-type-single" value="1" {{isset($promo->premium_accumulation_type) && $promo->premium_accumulation_type == 1 || $promo->premium_accumulation_type == 3 ? "checked" : ""}} {{$process == "view" || $promo->promo_type == 1 ? "disabled" : ""}} />
                  Single</label>
                  <label style="margin-left: 20px;">
                  <input type="radio" class="flat premium-accumulation-type" name="premium_accumulation_type[]" id="premium-accumulation-type-accumulated" value="2" {{isset($promo->premium_accumulation_type) && $promo->premium_accumulation_type == 2 || $promo->premium_accumulation_type == 3 ? "checked" : ""}}  {{$process == "view" || $promo->promo_type == 1 ? "disabled" : ""}} />
                  Accumulated</label>
              @endif
              <br />
              <span style="color:#a94442;font-style: italic;font-weight:400;">{{$errors->has('premium_accumulation_type') ? $errors->first('premium_accumulation_type') : ""}}</span>
			</div>
		</div>
		<div class="form-group {{$errors->has('premium_ceiling') ? 'has-error' : ""}}">
			<label class="col-md-3 col-lg-3 col-sm-12 col-xs-12 control-label">Ceiling (per transaction)</label>
			<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
				<input type="number" class="form-control" name="premium_ceiling" value="{{$errors->has() ? old('premium_ceiling') : (isset($promo->premium_ceiling) ? $promo->premium_ceiling : "1" )}}" {{$process == "view" || $promo->promo_type == 1  ? "disabled" : ""}} >
				<span style="color:#a94442;font-style: italic;">{{$errors->has('premium_ceiling') ? $errors->first('premium_ceiling') : ""}}</span>
			</div>
		</div>
		<div class="form-group {{$errors->has('premium_multiplier') ? 'has-error' : ""}}">
			<label class="col-md-3 col-lg-3 col-sm-13 col-xs-13 control-label">Multiplier</label>
			<div class="col-md-9 col-lg-9 col-sm-9 col-xs-9 control-label">
				<label>
				<input type="checkbox" class="js-switch premium-multiplier" name="premium_multiplier" value="1" {{$process == "view" || $promo->promo_type == 1  ? "disabled" : ""}} {{$errors->has() && old('premium_multiplier') == 1 ? "checked" : (isset($promo->premium_multiplier) && $promo->premium_multiplier == 1 ? "checked" : "")}}>
				X2</label>
				<span style="color:#a94442;font-style: italic;">{{$errors->has('premium_multiplier') ? $errors->first('premium_multiplier') : ""}}</span>
			</div>
		</div>
      </div>
</div>