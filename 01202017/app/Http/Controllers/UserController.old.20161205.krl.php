<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Input;
use Validator;
use Session;
use File;
use Auth;
class UserController extends Controller
{
    public function displayUsers(){
        $search = Input::get('search', '');
        if($search != ""){
            $users = User::where('status', '=', 1)
            ->where('usertype', '!=' , 1)
            ->orderBy('last_name', 'asc')
            ->where('first_name', 'LIKE', '%'.$search.'%')
            ->orWhere('middle_name', 'LIKE', '%'.$search.'%')
            ->orWhere('last_name', 'LIKE', '%'.$search.'%')
            ->paginate(10)
            ->setPath('users')
            ->appends(Input::except('page'));
        }else{
            $users = User::where('status', '=', 1)
            ->where('usertype', '!=' , 1)
            ->orderBy('last_name', 'asc')
            ->paginate(10)
            ->setPath('users')
            ->appends(Input::except('page'));
        }

        return view('admin.pages.user', compact('users'));
    }

    public function displayRegisterPage(){
        return view('admin.pages.register');
    }

    public function displayEditPage($username){
        $user = User::where('username','=',$username)->get();

        if(!$user->isEmpty()){
            return view('admin.pages.edit-user', compact('user'));
        }else{
            return view('admin.pages.404');
        }
    }

    public function processEditUser(Request $request){
        // dd($request->all());
        $rules = [
            'usertype'    => 'sometimes|numeric',
            'first_name'  => 'required|min:3|regex:/^[\pL\s\d]+$/u',
            'middle_name' => 'min:3|regex:/^[\pL\s]+$/u',
            'last_name'   => 'required|min:3|regex:/^[\pL\s\d]+$/u',
            'dob'         => 'required|date',
            'email'       => 'required|email',
            'contact'     => 'required|numeric',
            'address'     => 'required'
        ];

        if($request->get('password') != ""){
            $rules["password"] =  'required|confirmed|min:6';
        }

        if($request->get('username') != ""){
            $rules['username'] = 'required|max:255|unique:atc_users,username,NULL,id,status,1';
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }
        if($request->get('userid') != null){
            $user              = User::find($request->get('userid'));
        }else{
            $user = new User();
            $user->unique_id = strtoupper(uniqid());
            $user->status    = 1;
            $user->username  = $request->get('username');
        }
        $user->first_name  = $request->get('first_name');
        $user->middle_name = $request->get('middle_name');
        $user->last_name   = $request->get('last_name');
        $user->dob         = $request->get('dob');
        $user->email       = $request->get('email');
        $user->contact     = $request->get('contact');
        $user->address     = $request->get('address');
        $user->usertype    = $request->get('usertype');
        $user->updated_at  = date('Y-m-d h:i:s');

        if($request->get('password') != ""){
            $user->password = bcrypt($request->get('password'));
        }

        if($user->save()){
            if($request->get('userid') != null){
                Session::flash("message", "\"". $request->first_name . "\" has been updated.");
            }else{
                Session::flash("message", "\"". $request->first_name . "\" has been created.");
            }
            if (Auth::user()->usertype == 2) {
                Session::flash("message", "Your Profile has been updated.");
            }
            Session::flash("messageClass", "alert-success");

            if (Auth::user()->usertype == 3) {
                return Redirect::to(url('res/users'));
            }
            if (Auth::user()->usertype == 2) {
                return Redirect::back();
            }

        }else{
            Session::flash("message", "There was a problem with your request. Please try again later.");
            Session::flash("messageClass", "alert-danger");
            if($request->get('userid') != null){
                return Redirect::to('res/edit/user/'.$request->get('username'))->withInput();
            }else{
                return Redirect::to('res/register')->withInput();
            }
        }
    }

    public function processDeleteUser($username){
        $user = User::where('username', '=', $username)->first();
        if(count($user) == 1){
            $user->status = 0;
            $user->save();

            Session::flash("message", "\"". $username . "\" has been deleted.");
            Session::flash("messageClass", "alert-success");
        }else{
            Session::flash("message", "There was a problem with your request. Please try again later.");
            Session::flash("messageClass", "alert-danger");
        }
        return Redirect::back();
    }
}