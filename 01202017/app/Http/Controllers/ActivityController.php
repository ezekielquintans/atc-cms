<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Input;
use App\Activities;
use Auth;
use DB;
use Maatwebsite\Excel\Facades\Excel as Excel;

class ActivityController extends Controller{

	public static function logActivity($action, $userid = 0){
	    try {
	       $activity            = new Activities();
	       $activity->activity  = $action;
	       $activity->userid    = $userid;
	       $activity->ipaddress = $_SERVER['REMOTE_ADDR'];
	       $activity->save();
	    } catch (\Exception $e) {
	        return false;
	    }
	}

	public static function getLogActivity($limit = null){
	    try {
	        if($limit === null){
	            $activities = Activities::where('id', '!=', 0)->orderBy('id', 'desc')->get();
	        }else{
	            $activities = Activities::where('id', '!=', 0)->take($limit)->orderBy('id', 'desc')->get();
	        }
	        return $activities;
	    } catch (\Exception $e) {
	        return array();
	    }
	}

	protected function displayActivityLog(){
		$range = Input::get('date', '');
		$search = Input::get('search', '');
		if($search != '' || $range != ''){
			$activities = Activities::orderBy('created_at', 'desc');

			if($search != ''){
				$activities = $activities->where(function($query) use($search) {
								$query->where('activity', 'like', '%'.$search.'%')
								->orWhere('ipaddress', 'like', '%'.$search.'%');
							});
			}

			if($range != ''){
				$range      = $this->processDateRange($range);
				$activities = $activities->whereBetween('created_at', array($range['startDate'], $range['endDate']));
			}

			if(Input::get('export') == 0){
				$activities = $activities->paginate(10)
								->setPath('activity-log')
					            ->appends(Input::except('page'));
			}

		}else{
	    	$activities = Activities::orderBy('created_at', 'desc');
	    	if(Input::get('export') == 0){
	    		$activities = $activities->paginate(10);
	    	}

		}

		if(Input::get('export') == 1){
			$activities = $activities->get();
			Excel::create('ACTIVITY-LOG', function($excel) use($activities){
			    $excel->sheet('ACTIVITY-LOG', function($sheet) use($activities){
			        $sheet->setShowGridlines(false);
			        $sheet->loadView('admin.exports.activities', compact('activities'));
			    });
			})->export('xlsx');
			exit;
		}

	    return view('admin.pages.activities', compact('activities'));
	}

	public static function processDateRange($range, $startFormat = "Y-m-d H:i:s", $endFormat = "Y-m-d 23:59:00"){
	    if($range != ""){
	        $range = explode("-", $range);
	        if(!empty($range)){
	            $startDate = date($startFormat, strtotime(trim($range[0])));
	            $endDate   = date($endFormat, strtotime(trim($range[1])));
	        }
	    }else{
	        $startDate = date($startFormat, strtotime(date("Y-01-01 00:00:00")));
	        $endDate   = date($endFormat, strtotime(date('Y-m-d 23:59:00')));
	    }

	    return array(
	        "startDate" => $startDate,
	        "endDate"   => $endDate
	    );
	}

}