@extends('admin.layouts.admin-layout')
@section('content')
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">

            <form method="get">
                <h2 class="col-md-8">List of members</h2>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" name="search" id="search" value="{{Input::get('search', '')}}" class="form-control" placeholder="Search for...">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <button class="btn btn-default" type="submit">Go!</button>
                                <button class="btn btn-default" onclick="document.getElementById('search').value='';" type="submit">View All</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            	@if(Session::has('msg_success'))
                    <div class="alert alert-success">
                        <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {{Session::get('msg_success')}}
                    </div>
                @elseif(Session::has('msg_error'))
                    <div class="alert alert-danger">
                        {{Session::get('msg_error')}}
                        @if(!$errors->isEmpty())
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                            @endforeach
                        </ul>
                        @endif
                    </div>
                @endif
                <table class="table table-striped">
					<thead>
						<th>#</th>
                        <th>ID NO.</th>
                        <th>FULL NAME</th>
                        <th>EMAIL ADDRESS</th>
                        <th>ADDRESS</th>
                        <th>CONTACT NO.</th>
						<th>CREATED DATE</th>
                        <th></th>
					</thead>
                    <tbody>
                        @forelse($members as $key => $row)
                            <tr>
                                <td width="1">{{ (Input::get('page', 1) - 1) * 20 + $key + 1 }}. </td>
                                <td>{{str_pad($row->id, 8, '0', STR_PAD_LEFT)}}</td>
                                <td width="">{{$row->last_name . ", " . $row->first_name . " " . $row->middle_name}}</td>
                                <td width="10%">{{$row->email}}</td>
                                <td width="25%">{{$row->address}}</td>
                                <td width="">{{$row->contact}}</td>
                                <td width="">{{date('M j, Y', strtotime($row->entry_created))}}</td>
                                <td width="100px" align="right" style="vertical-align: middle;">
                                    <div class="btn-group">
                                        <button class="btn btn-default btn-view-member-coupons" data-memberid="{{$row->id}}" title="View coupons of this member">
                                            <i class="fa fa-eye"></i>
                                        </button>
                                        <button class="btn btn-default btn-link btn-delete" data-href="{{URL::to('res/members/delete/'.$row->id)}}" title="Delete this user">
                                            <i class="fa fa-trash-o"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6">No members yet</td>
                            </tr>
                        @endforelse
                    </tbody>
            	</table>
            </div>
            <div class="x_footer">
                {!! $members->render() !!}
            </div>
        </div>
    </div>
</div>
<div  class="modal fade bs-example-modal-sm modal-coupons-view" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h3 class="modal-title" id="myModalLabel2">Coupons List</h3>
                </div>
                <div class="modal-body coupons-table" style="max-height: 700px;overflow-y: scroll;">

                </div>
            </div>
        </div>
    </div>
@stop
@section('scripts')
<script>
    $(document).ready(function() {
        $(".btn-view-member-coupons").unbind().click(function(event) {
            var memberId = $(this).data('memberid');
            var url = 'getEntriesOfUser/' + memberId;
            $(".coupons-table").html('Loading entries, please wait...');
            $(".modal-coupons-view").on('show.bs.modal', function(event) {
                var table = '<table class="table">' +
                                '<thead>' +
                                    '<tr>' +
                                        '<th></th>' +
                                        '<th>Coupon No.</th>' +
                                        '<th class="text-center">Store Name</th>' +
                                        '<th class="text-center">Receipt No.</th>' +
                                        '<th class="text-right">Amount</th>' +
                                    '</tr>' +
                                '</thead>' +
                                '<tbody>';
                $.get(url, function(data) {
                    if(data.length == 0){
                        table +=    '<tr><td colspan="5">No entries found.</td></tr>';
                    }else{
                        $.each(data, function(index, val) {
                            table +=    '<tr>' +
                                            '<td>'+ (index + 1) +'</td>' +
                                            '<td>'+ val.unique_id +'</td>' +
                                            '<td class="text-center">'+ val.store_name +'</td>' +
                                            '<td class="text-center">'+ val.receipt_no +'</td>' +
                                            '<td class="text-right">'+ val.amount +'</td>' +
                                        '</tr>';
                        });
                    }
                    table +=    '</tbody>' +
                            '</table>';
                    $(".coupons-table").html(table);
                });

            }).modal('show');
        });
    });
</script>
@stop
