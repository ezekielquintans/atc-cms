<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Alabang Town Center</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Place favicon.ico in the root directory -->
        <!-- text cdn -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Shadows+Into+Light" rel="stylesheet">
        <!--    <link rel="stylesheet" href="css/normalize.css">-->
        <link rel="stylesheet" href="{{asset('public/landing/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('public/landing/dist/Remodal-1.1.0/dist/remodal.css')}}">
        <link rel="stylesheet" href="{{asset('public/landing/dist/Remodal-1.1.0/dist/remodal-default-theme.css')}}">
        <link rel="stylesheet" href="{{asset('public/landing/css/style.css')}}">
        <script src="{{asset('public/landing/js/vendor/modernizr-2.8.3.min.js')}}"></script>
    </head>
    <body class="">
        <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <!-- Add your site or application content here -->
        <section class="ec ec__view ec__view--bg remodal-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="ec__view__wrpr">
                            <img class="center-block ec__view__logo" src="{{asset('public/landing/img/main-logo.png')}}" alt="">
                            <div class="row m-0">
                                <div class="col-md-6 p-0">
                                    <div class="ec__view__i">
                                        <!-- scrollable content -->
                                        <div class="info-content">
                                            <!-- Promo Logo -->
                                            <img class="promo_brand" src="{{asset('public/landing/img/promo-logo.png')}}" alt="">
                                            <!-- Raffle title -->
                                            <div class="promo-title">
                                                <h1 class="giga">Raffle Promo</h1>
                                                <h1 class="giga">Terms and Conditions</h1>
                                            </div>
                                            <p class="text-center"><b>The promo runs from November 12, 2016 to January 15, 2017.</b></p>
                                            <p class="text-center">The promo is open to all shoppers of Alabang Town Center.</p>
                                            <h1 class="text-center">Raffle Draw Date:</h1>
                                            <p class="text-center">January 20, 2017, FRIDAY</p>
                                            <h1 class="text-center">Last Day of Submission of Entries:</h1>
                                            <p class="text-center">January 15, 2017, FRIDAY (11:59 p.m.)</p>
                                            <div class="division"></div>
                                            <h1 class="text-center">PRIZES</h1>
                                            <p><b>GRAND PRIZE:</b> One (1) Subaru Levorg</p>
                                            <p><b>MINOR PRIZES:</b> Seven (7) - 3 Days / 2 Nights Package in El Nido Cove Forest Room for two (2) persons trips for 2 to Cebu</p>
                                            <p>Inclusive of the following:</p>
                                            <ul>
                                                <li>Airfare for two (2) pax via ITI</li>
                                                <li>Breakfast and airport transfers</li>
                                            </ul>
                                            <div class="division"></div>
                                            <h1 class="text-center">Terms And Conditions</h1>
                                            <ol class="list-roman">
                                                <li>The shopper who accumulated at least P500 worth purchase in any merchant store of Alabang Town Center must register and create a promo account at the and completely fill-out the following details on the on-line registration data base: complete name, address, email address, contact number/s (office phone number, house phone number or mobile phone number) and age.</li>
                                                <li>Upon registration, a shopper is entitled to one (1) electronic raffle coupon for every P500 worth of single/accumulated receipts from shops of Alabang Town Center to be redeemed at the concierge after registration.
                                                    <ol class="list-alpha">
                                                        <li><i>For shoppers using a BPI-Amore Credit Card, they are entitled to two(2) electronic raffle coupons for every P500 worth of single/accumulated receipts from shops of Alabang Town Center. (Presentation of charge slip and BPI-Amore credit card is required).</i></li>
                                                    </ol>
                                                </li>
                                                <li>Only receipts issued from November 12 to January 15, 2017 are eligible.  Defaced, crumpled, tampered or receipts that are less than ¾ its actual size will not be honored.</li>
                                                <li>A shopper may only win once.  Non-winning entries for the Minor Raffle Draw will be eligible to participate in the Grand Raffle Draw.</li>
                                                <li>A shopper whose name is drawn more than once during the Minor and Grand Raffle Draw will get the prize with the higher value.
                                                    <ol class="list-alpha">
                                                        <li>The prizes are subject to the following conditions:</li>
                                                        <li>Except as otherwise specified herein, the taxes on all prizes will be paid by Alabang Commercial Corporation.</li>
                                                        <li>ITI will issue a travel certificate that entitles the winner to round trip airline tickets from Manila to El NIdo, Palawan.  Travel certificate shall indicate terms &amp; conditions, reservations procedures, and black-out periods.</li>
                                                        <li>El Nido Cove will issue gift certificates valid until December 31, 2017 that entitles winner to accommodations.  Travel certificate shall indicate terms &amp; conditions and reservations procedures.</li>
                                                        <li>Travel taxes and other travel-related expenses shall be for the account of the winner.</li>'
                                                        <li>The car prize is inclusive of the costs of delivery, first year registration, and transfer of ownership.</li>
                                                        <li>Prizes not claimed after sixty (60) days from date of notification will be forfeited in favor of Alabang Town Center, Inc. with prior approval of DTI.</li>
                                                    </ol>
                                                </li>
                                                <li>Winners will be notified by telephone call and registered mail.  The winners’ names will also be posted at the Alabang Town Center Concierge and published at the Alabang Town Center Social Media accounts.</li>
                                                <li>Winners may claim their prizes at the Administration Office of the Alabang Town Center.</li>
                                                <li>To claim the prizes, winners must present the notice sent by registered mail, the winning electronic raffle coupon, and any two (2) valid Government-issued identification card with photo and signature.</li>
                                                <li>Winners agree to the disclosure and publication of their names and other particulars as Alabang Town Center may deem appropriate and to participate in any photo, publicity or other media arrangements made by Alabang Town Center to announce the winners to the general public.</li>
                                                <li>Employees of Alabang Commercial Corporation, their merchants, maintenance and security agencies, and advertising and promotional agencies, and their relatives up to the second degree of affinity or consanguinity, are disqualified from joining the promo</li>
                                            </ol>
                                            <p class="text-center">DTI - FTEB Permit No. 11874, Series of 2016</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 p-0">
                                    <div class="ec__view__iside text-center">
                                        <h4 class="text-italic showcase__title">Shop Now and Get a chance to win a trip for two (2) to El Nido Palawan or drive a brand new “Subaru Levorg”</h4>
                                        <div class="form__container">
                                            <h4>View your coupons here!</h4>
                                            <p>Check your email now for updates!</p>
                                            <form class="form-inline" method="post" action="{{URL::to('post-check-entries')}}">
                                                <div class="form-group">
                                                    <input type="email" required name="email" class="form-control" id="form_email" placeholder="Email Address"/>
                                                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                                                </div>
                                                <button type="submit" class="btn btn-purple btn-sm">SEND</button>
                                            </form>
                                        </div>
                                        <div class="helper__card">
                                            <h1>One (1) raffle coupon <br> for every 500 php purchase </h1>
                                            <p class="small">Earn twice as much when you use your BPI Amore Card</p>
                                        </div>
                                        <p class="permit">DTI - FTEB Permit No. 11874, Series of 2016</p>
                                        <ul class="list-inline list-inline-white">
                                            <li>
                                                <a class="facebook" target="_blank" href="https://www.facebook.com/AlabangTownCenter/"><i class="icon-facebook"></i></a>
                                            </li>
                                            <li>
                                                <a class="twitter" target="_blank" href="https://twitter.com/alabangtowncntr?lang=en"><i class="icon-twitter"></i></a>
                                            </li>
                                            <li>
                                                <a class="instagram" target="_blank" href="https://www.instagram.com/alabangtowncenter/"><i class="icon-instagram"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="ec__footer ec__footer--mod">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="text-center">
                                <p class="copyright">ONE AYALA HOTLINE: 795-9595</p>
                                <p class="copyright">Copyright © 2016 Ayala Malls 360. All Rights Reserved</p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            @if(Session::has('msg_success'))
                <div class="remodal" data-remodal-id="modal">
                    <button data-remodal-action="close" class="remodal-close"></button>
                    <h1>Success</h1>
                    <p>
                        {{Session::get('msg_success')}}
                    </p>
                    <br>
                    <button data-remodal-action="confirm" class="remodal-confirm">OK</button>
                </div>
            @elseif(Session::has('msg_warning'))
                <div class="remodal" data-remodal-id="modal">
                    <button data-remodal-action="close" class="remodal-close"></button>
                    <h1>Warning!</h1>
                    <p>
                        {{Session::get('msg_warning')}}
                    </p>
                    <br>
                    <button data-remodal-action="confirm" class="remodal-cancel">OK</button>
                </div>
            @elseif(Session::has('msg_danger'))
                <div class="remodal" data-remodal-id="modal">
                    <button data-remodal-action="close" class="remodal-close"></button>
                    <h1>Oops!</h1>
                    <p>
                        {{Session::get('msg_danger')}}
                    </p>
                    <br>
                    <button data-remodal-action="confirm" class="remodal-cancel">OK</button>
                </div>
            @endif

        </section>
        <!-- end of content -->
        <script src="{{asset('public/landing/js/vendor/jquery-1.12.0.min.js')}}"></script>
        <script src="{{asset('public/landing/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('public/landing/js/plugins.js')}}"></script>
        <script src="{{asset('public/landing/js/main.js')}}"></script>
        <script src="{{asset('public/landing/dist/Remodal-1.1.0/dist/remodal.min.js')}}"></script>
        <script>
        $(document).ready(function(){
            // $('[data-remodal-id=modal]').remodal('');
            var inst = $.remodal.lookup[$('[data-remodal-id=modal]').data('remodal')];
            // открыть модальное окно
            inst.open();
        });
        </script>
    </body>
</html>