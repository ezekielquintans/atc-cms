@if(Session::has('msg_success'))
	<div class="alert alert-success">
		<a href="{{URL::to(Request::url())}}" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	    {{Session::get('msg_success')}}
	</div>
@elseif(Session::has('msg_warning'))
	<div class="alert alert-warning">
	    <a href="{{URL::to(Request::url())}}" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	    {{Session::get('msg_warning')}}
	</div>
@elseif(Session::has('msg_danger'))
	<div class="alert alert-danger">
	    <a href="{{URL::to(Request::url())}}" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	    {{Session::get('msg_danger')}}
	</div>
@endif
<form method="post" action="{{URL::to('post-check-entries')}}">
	<input type="email" required name="email"/>
	<input type="hidden" name="_token" value="{{ Session::token() }}">
	<input type="submit" name="submit" value="Check">
</form>