<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Alabang Town Center</title>

    <!-- Bootstrap -->
    <link href="{{asset('public/app/vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{asset('public/app/vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- Animate.css -->
    <link href="https://colorlib.com/polygon/gentelella/css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{asset('public/app/css/custom.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('public/app/css/style.css')}}">
</head>

<body class="login">
    <div>
        <a class="hiddenanchor" id="signup"></a>
        <a class="hiddenanchor" id="signin"></a>

        <div class="login_wrapper">
            <img src="{{asset('public/app/img/main-logo.png')}}" alt="">
            <div class="animate form login_form">
                <section class="login_content">
                    <h4>Sign in to start your session</h4>
                    <form method="POST" action="{{URL::to('res/login')}}">
                        <div>
                            <input type="hidden" name="_token" value="{{ Session::token() }}">
                            <input type="text" class="form-control atc-form" placeholder="Username" name="username" required value="{{$errors->has('username') ? old('username') : ''}}" />
                        </div>
                        <div>
                            <input type="password" class="form-control atc-form" placeholder="Password" name="password" autocomplete="new-password" required />
                        </div>
                        <div>
                            <button type="submit" class="btn submit btn-block btn-orange btn-md">Log in</button>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </section>
            </div>
             @if(Session::has('msg_error'))
                <div class="alert alert-danger">
                    {{Session::get('msg_error')}}
                </div>
            @endif
        </div>

    </div>
</body>

</html>