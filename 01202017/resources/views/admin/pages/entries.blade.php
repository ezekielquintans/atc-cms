@extends('admin.layouts.admin-layout')
@section('content')
    <div class="overlay">
        Loading member's information, please wait...
    </div>
    <div class="page-title">
        <div class="title_left">
            <h3>Add Entries</h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <form id="user-form" class="form-horizontal form-label-left" method="post" action="addEntries" enctype="multipart/form-data">
	    <!--USER INFORMATION-->
        <div class="row">
	        <div class="col-md-12 col-sm-12 col-xs-12">
	            <div class="x_panel">
	                <div class="x_title">
	                    <h2>Customer <small>Please fill in the required fields.</small></h2>
	                    <div class="clearfix"></div>
	                </div>
	                <div class="x_content">
	                    @if(Session::has('msg_success'))
    						<div class="alert alert-success">
    							<a href="{{URL::to(Request::url())}}" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                {{Session::get('msg_success')}}
    						</div>
                        @elseif(Session::has('msg_warning'))
                            <div class="alert alert-warning">
                                <a href="{{URL::to(Request::url())}}" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                {{Session::get('msg_warning')}}
                            </div>
						@elseif(Session::has('msg_error'))
							<div class="alert alert-danger">
								{{Session::get('msg_error')}}
								@if(!$errors->isEmpty())
								<ul>
									@foreach($errors->all() as $error)
									<li>{{$error}}</li>
									@endforeach
								</ul>
								@endif
							</div>
						@endif
	                	<div class="form-group">
		                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 col-sm-offset-3 col-xs-offset-12">
			                    <input type="text" class="form-control" placeholder="Search for..." id="search" autocomplete="off">
		                    </div>
	                	</div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">First Name </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="fname" name="first_name" class="form-control col-md-7 col-xs-12" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Last Name </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="lname" name="last_name" required="required" class="form-control col-md-7 col-xs-12" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Email Address </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="email" id="email" name="email" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Address </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="address" name="address" class="form-control col-md-7 col-xs-12" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Contact No. </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" id="contact" name="contact" class="form-control col-md-7 col-xs-12" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Customer ID:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="cusIDDisplay" class="form-control col-md-7 col-xs-12" type="text" readonly="readonly">
                                <input id="cusID" class="form-control col-md-7 col-xs-12" type="hidden" name="cusid" readonly="readonly">
                            </div>
                        </div>
                        <input type="hidden" name="_token" value="{{ Session::token() }}">
	                </div>
	            </div>
	        </div>
	    </div>
        <!--ENTRY INFORMATION-->
	    <div class="row">
	        <div class="col-md-12 col-sm-12 col-xs-12">
	            <div class="x_panel">
	                <div class="x_title">
	                    <h2>Entries<small>(1 Raffle coupon for every 500 Php purchase)</small></h2>
	                    <a class="btn btn-orange pull-right" href="#" id="add-receipt"><i class="fa fa-plus"></i> Add Receipt</a>
	                    <div class="clearfix"></div>
	                </div>
                    <div class="row">
                        <div class="col-md-12">
                            <p>
                                <input type="radio" class="flat mode" id="modeS" value="S" name="mode" checked="" required />&nbsp Single &nbsp
                                <input type="radio" class="flat mode" id="modeA" value="A" name="mode" />&nbsp Accumulated
                            </p>
                        </div>
                    </div>
	                <div class="x_content" id="coupon-form-container">
                        <div class="row entry-row">
                            <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                <div class="form-group">
                                    <label class="control-label" >Store Name</label>
                                    <div class="">
                                        <select id="heard" class="store form-control" name="store[]" required>
											<option value="" selected> -- Select a Store -- </option>
                                            @forelse($storeListing as $store)
                                                <option data-category = "{{App\Stores::displayCurrentCategory($store)}}">{{$store}}</option>
                                            @empty
                                            @endforelse
										</select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                <div class="form-group">
                                    <label class="control-label" >Category</label>
                                    <input class="form-control category" type="text" readonly/>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                <div class="form-group">
                                    <label class="control-label">Invoice Number</label>
                                    <div class="">
                                        <input type="text" class="invoice-text form-control" required placeholder="Enter invoice number" name="invoice[]">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-12 col-xs-12 form-group amt-grp">
                                <div class="form-group">
                                    <label class="control-label">Amount</label>
                                    <div class="">
                                        <input type="number" class="form-control amount" required placeholder="Enter Amount" name="amount[]">
                                        <input type="hidden" class="form-control coupons" name="coupons[]">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1 col-sm-12 col-xs-12 form-group with-multiplier">
                                <label class="control-label">Multiplier</label>
                                <div class="">
                                    <input type="checkbox" name="multiplier[]" class="multiplier form-control" disabled style="height:23px;width:23px;margin-left: 14px;">
                                </div>
                            </div>
	                    </div>
	                </div>
                    <div class="row">
                        <div class="col-md-8 col-sm-12 col-xs-12">
                            <input type="hidden" name="rows" id="norows" value="1">
                        	<input type="hidden" name="total_coupons" id="noCoupons">
                            <button type="button" id="submit-coupons" class="btn btn-success generate-coupon">Generate Coupons</button>
                        </div>

                        <div class="col-md-2 col-sm-12 col-xs-12">
							<label class="" >Total Amount:</label>
							<span id="totalAmount">0</span>
                        </div>
                        <div class="col-md-2 col-sm-12 col-xs-12">
							<label class="" >Total Coupons:</label>
							<span id="totalCoupons">0</span>
                        </div>
                    </div>
	            </div>
	        </div>
	    </div>
	</form>


    @if(Input::get('tk', '') != "" && count($entry) > 0)
        <div id="modal" class="modal show bs-example-modal-sm modal-receipt-view" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><a href="{{URL::to('res/entries/add')}}"><span aria-hidden="true">×</span></a>
                        </button>
                        <img src="{{asset('public/app/img/success.png')}}" alt="" style="margin-bottom:15px;">
                        <h2 class="modal-title" id="myModalLabel2" style="font-size:24px;color:#39b54a">Coupon Generation Successful!</h2>
                        @if(Session::has('msg_warning'))
                            <p class="alert alert-warning" style="color:#fff;font-size:20px;">
                                {{Session::get('msg_warning')}}
                            </p>
                        @endif
                    </div>
                    <div class="modal-body">
                        <div class="receipt-content" style="max-height:300px;">
                            <form class="form-horizontal form-label-left">
                                <div class="form-group">
                                    <label class="control-label col-md-4" for="first-name">Date:</label>
                                    <div class="col-md-8">
                                        <h2>{{date('Y-m-d H:i:s')}}</h2>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4" for="last-name">Name:</label>
                                    <div class="col-md-8">
                                        <h2>{{strtoupper($entry[0]->last_name . ", " . $entry[0]->first_name . " " . $entry[0]->middle_name)}}</h2>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4" for="last-name">Email Address:</label>
                                    <div class="col-md-8">
                                        <h2>{{$entry[0]->email}}</h2>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4" for="last-name">Contact Number:</label>
                                    <div class="col-md-8">
                                        <h2>{{$entry[0]->contact}}</h2>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4" for="last-name">No. of Entries:</label>
                                    <div class="col-md-8">
                                        <h2>(<span class="total">{{count($entry)}}</span>) Entries</h2>
                                    </div>
                                </div>
                            </form>
                            <div class="row text-center">
                                @forelse($entry as $list)
                                    <div style="display:inline;">
                                        <h1>{{$list->coupon_id}}</h1>
                                    </div>
                                @empty
                                @endforelse
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <img class="center-block receipt-logo" src="{{asset('public/app/img/receipt-logo.png')}}" alt="">
                                </div>
                                <div class="col-md-6">
                                    <img class="center-block receipt-logo" src="{{asset('public/app/img/receipt-logo-2.png')}}" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <a href="{{URL::to('res/entries/add')}}"><button type="button" class="btn btn-orange btn-block btn-lg btn-print">NEW ENTRY</button></a>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <a href="{{URL::to('res/entries/print-entries?tk=' . $token)}}" target="_blank"><button type="button" class="btn btn-green btn-block btn-lg btn-print">PRINT NOW!</button></a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    @endif
@stop

@section('styles')
    <link rel="stylesheet" href="{{asset("public/app/js/chosen_v1.6.2/chosen.css")}}">
@stop

@section('scripts')
<script type="text/javascript" src="{{asset("public/app/js/chosen_v1.6.2/chosen.jquery.js")}}"></script>
<script>
jQuery(document).ready(function($) {
    // $('#heard').chosen();
    $('.overlay').css('height', $(document).height() + 'px');
    /*User */
    var users = {
    	@forelse($users as $user)
    		"{{$user->id}}" : "{{$user->last_name.', '.$user->first_name.'  ('.ucfirst($user->address).')'}}" ,
    	@empty
    	@endforelse
    };
    var usersArray = $.map(users, function(value, key) {
		return {
			value: value,
			data: key
		};
	});
    var NoResultsLabel = "No Results";

    $('#search').autocomplete({
            lookup: usersArray,
            showNoSuggestionNotice:true,
          	minChars: 3,
          	onSelect: function (suggestion) {
                $('.overlay').show();
                $("body").css("overflow", "hidden");
		        $.get('{{URL::to('res/getUserInfo')}}',{userid : suggestion.data}, function(data) {
                    $('.overlay').hide();
                    $("body").css("overflow", "auto");

		        	$("#fname").val(data.first_name);
                    $("#lname").val(data.last_name);
                    $("#email").val(data.email);
                    $("#address").val(data.address);
		        	$("#contact").val(data.contact);
                    $("#cusID").val(data.id);
		        	$("#cusIDDisplay").val(data.display_id);
		        });
		    }
    });
    /*Settings*/
    $("input[type=radio][name=mode]").on('ifChecked', function(event) {

        if(event.currentTarget.value == "A"){
            $(".multiplier").each(function() {
               $(this).attr({
                   "disabled"   : true,
                   'checked'    : false
               });;
            });
        }else if(event.currentTarget.value == "S"){
            $(".multiplier").each(function() {
              $(this).removeAttr('disabled').removeAttr('checked');
            });
           }
        $(".amount").first().trigger('keyup');
    });

    $(document).on('click','.multiplier' ,function(event) {
        $(".amount").first().trigger('keyup');
    });

    /*Receipts based on settings*/
    $(document).on('keyup', '.amount', function(event) {
    	var amount = $(this).val();
        var coupons = 0;
        var totalAmount = 0;
        var totalCoupons = 0;
        var additionalCoupons = 0;
        var mode = $("input[name=mode]:checked").val();
        var multiplier = $(this).parentsUntil('.row').siblings('.with-multiplier').find('.multiplier');
    	if(Math.floor(amount) == amount && $.isNumeric(amount) && amount != 0){
    		coupons = Math.floor(amount/500);
            $(this).siblings('.coupons').val(coupons);
            $('.amount').each(function() {
                totalAmount += parseInt($(this).val());
            });
            if (mode == "S") {
                $('.coupons').each(function() {
                    totalCoupons += parseInt($(this).val());
                });
            } else if(mode == "A"){
                totalCoupons =  Math.floor(totalAmount/500);
            }
            $(".multiplier").each(function() {
                if ($(this).is(':checked')) {
                    var stockCoupon = $(this).parentsUntil('.row').siblings('.amt-grp').find('.coupons');
                    additionalCoupons += parseInt(stockCoupon.val());
                }
            });
            totalCoupons += additionalCoupons;
            if(totalCoupons >= 1){
                if(mode == "S"){
                    $(multiplier).removeAttr('disabled');
                }
                $("#noCoupons").val(totalCoupons);
                $("#totalCoupons").text(totalCoupons);
		    	$("#totalAmount").text(totalAmount);
    		}else{
                $("#noCoupons").val('0');
                $("#totalCoupons").text('0');
                $("#totalAmount").text('0');
            }
    	}
    });
    /*Configure on enter key event*/



    /*Add receipt*/
    var ctr = 1;
    $("#add-receipt").click(function(event){
    	event.preventDefault();
        ctr++;
        var newRow = $("#coupon-form-container")
        .children('.row')
        .first()
        .clone(true)
        .find(".multiplier").attr({
            'checked'   : false,
            'disabled'  : true
        }).end()
        .find("input:text").val("").end()
        .find('select').attr('id', 'store-'+ctr).end()
        .find(':input[type="number"]').val("").end()
    	newRow.appendTo('#coupon-form-container');
    	var noOfRows = parseInt($("#norows").val());;
    	noOfRows += 1;
    	$("#norows").val(noOfRows);
        // $('#store-'+ctr).chosen();
    });

    $('#submit-coupons').click(function(){
        var valid = true;
        $('input,textarea,select').filter('[required]:visible').each(function(){
            if($(this).val() == ""){
                valid = false;
                $(this).css('border','2px solid red');
            }else{
                $(this).css('border','1px solid #ccc');
            }
        });

        var requestCount = $('input.invoice-text').length;
        var ctr = 0;
        if(valid === true){
            $('.overlay').html("Validating entry, please wait...").show();
            $("body").css("overflow", "hidden");
        }

        $('input.invoice-text').each(function() {
            var input      = $(this);
            var invoice    = $(this).val();
            var store      = $(this).closest('div.row').find('select.store').val();

            if(valid === true){
                $.get('../validate-invoice?store='+store+'&invoice='+invoice, function(data){
                    if(data == "false"){
                        valid = false;
                        input.css('border','2px solid orange');
                        input.closest('div.form-group').find('label.control-label').html('Invoice Number<small style="color:red"> *Already used.</small>');
                    }else{
                        input.css('border','2px solid green');
                        input.closest('div.form-group').find('label.control-label').html('Invoice Number');
                    }
                    ctr++;
                    if(ctr == requestCount){
                        $("body").css("overflow", "auto");
                        $('.overlay').html("Loading member's information, please wait...").hide();
                        if(valid === true){
                            $("body").css("overflow", "hidden");
                            $('.overlay').html("Adding entry, please wait...").show();
                            $('#user-form').submit();
                        }
                    }
                });
            }
        });
    });

    $(document).on('change', '.store', function(){
        var category = $('option:selected', this).data('category');
        $(this).closest('div.entry-row').find('input.category').val(category);
    });
});
</script>
@stop
