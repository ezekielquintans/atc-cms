@extends('admin.layouts.admin-layout')
@section('content')
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Raffle Draw</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <h1 class="text-center hidden congrats-banner">Congratulations!</h1>
                            <div class="jumbotron">
                                <h1 class="jumbo text-center" id="myTargetElement">00000000</h1>
                            </div>
                            <div class="row">
                                <h2 class="text-center">MEMBER DETAILS:</h2>
                                    <form id="#" data-parsley-validate class="form-horizontal form-label-left">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="control-label text-left col-md-4 col-sm-4 col-xs-12" for="first-name">Member ID:
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <h2 id="winner-id"> -- </h2>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="control-label text-left col-md-4 col-sm-4 col-xs-12" for="first-name">Full Name:
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <h2 id="winner-name"> -- </h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="control-label text-left col-md-4 col-sm-4 col-xs-12" for="first-name">Coupon No:
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <h2 id="winner-coupon"> -- </h2>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="control-label text-left col-md-4 col-sm-4 col-xs-12" for="first-name">Email Address:
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <h2 id="winner-email"> -- </h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="control-label text-left col-md-4 col-sm-4 col-xs-12" for="first-name">Contact No.:
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <h2 id="winner-contact"> -- </h2>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="control-label text-left col-md-4 col-sm-4 col-xs-12" for="first-name">Date of Birth:
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <h2 id="winner-dob"> -- </h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="control-label text-left col-md-2 col-sm-2 col-xs-12" for="first-name">Address:
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <h2 id="winner-address"> -- </h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                <div class="ln_solid"></div>
                                <div class="clearfix"></div>
                                <a class="btn btn-orange btn-block btn-lg" href="#" id="generate-winner">Pick a Winner</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row winners-list hidden">
        <div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1">
            <div class="x_panel">
                <div class="x_title">
                    <h2>List of Winners</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-striped">
                        <thead>
                            <th>#</th>
                            <th>Coupon No.</th>
                            <th>User ID</th>
                            <th>Last Name</th>
                            <th>First Name</th>
                            <th>Middle Name</th>
                            <th>Email Address</th>
                            <th>Contact Number</th>
                        </thead>
                        <tbody id="tbl-listing">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('scripts')
    <script src="{{asset('public/app/dist/countUp.js-master/countUp.js-master/countUp.js')}}"></script>
    <script type="text/javascript">
        var ctr = 1;
        $('#generate-winner').click(function(){
            var winner = {};
            var button = $(this);
            button.attr('disabled', 'disabled');
            $('#myTargetElement').html('Loading...');
            $.post('kashing',{"_token": $('meta[name="req"]').attr('content') }, function(data){
                var winner = JSON.parse(data);
                var options = {
                    useEasing : false,
                    useGrouping : false,
                    separator : '',
                    decimal : '',
                    prefix : '',
                    suffix : '',
                    coupon: winner.coupon_no
                };
                var demo = new CountUp("myTargetElement", 0, winner.entry_id, 0, 1.5, options);
                demo.reset();
                demo.start(function(){
                    $('#winner-name').html(winner.first_name + ' ' +winner.middle_name+ ' ' + winner.last_name);
                    $('#winner-id').html(winner.custom_id);
                    $('#winner-coupon').html(winner.coupon_no);
                    $('#winner-email').html(winner.email);
                    $('#winner-contact').html(winner.contact);
                    $('#winner-address').html(winner.address);
                    $('#winner-dob').html(winner.dob);

                    var html = "";
                        html += "<tr>";
                        html += "<td class='ordinal'>"+ctr+"</td>";
                        html += "<td>"+winner.coupon_no+"</td>";
                        html += "<td>"+winner.custom_id+"</td>";
                        html += "<td>"+winner.last_name+"</td>";
                        html += "<td>"+winner.first_name+"</td>";
                        html += "<td>"+winner.middle_name+"</td>";
                        html += "<td>"+winner.email+"</td>";
                        html += "<td>"+winner.contact+"</td>";
                        html += "</tr>";

                    $('#tbl-listing').append(html);
                    $('.winners-list').removeClass('hidden');
                    // ordinal();
                    button.removeAttr('disabled');
                    ctr++;
                });
            });
        });

        function ordinal(){
            $(".ordinal").text(function (i, t) {
                i++;
                var str = i.toString().slice(-1),
                    ord = '';
                switch (str) {
                    case '1':
                        ord = 'st';
                        break;
                    case '2':
                        ord = 'nd';
                        break;
                    case '3':
                        ord = 'rd';
                        break;
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                    case '0':
                        ord = 'th';
                        break;
                }
                return i + ord;
            });
        }
    </script>
@stop