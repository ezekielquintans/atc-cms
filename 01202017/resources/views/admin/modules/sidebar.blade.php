<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="{{URL::to('res/members')}}" class="site_title"><img src="{{asset("public/app/img/main-logo.png")}}" alt=""></a>
        </div>

        <div class="clearfix"></div>

        <!-- menu profile quick info -->
        <div class="profile">
            <div class="profile_pic">
                <img src="{{asset("public/app/img/user.png")}}" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
                @if(Auth::user()->isAdmin())
                    <span>Admin,</span>
                @elseif(Auth::user()->isConcierge())
                    <span>Concierge,</span>
                @endif
                <h2>{{Auth::user()->first_name == "" ? Auth::user()->username : Auth::user()->first_name}}</h2>
            </div>
        </div>
        <!-- /menu profile quick info -->

        <br />

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>Menu</h3>
                <ul class="nav side-menu">

                    <li class="{{Request::is('res/members') ? 'active' : ''}}"><a href="{{URL::to('res/members')}}"><i class="fa fa-users fa-fw"></i> Members</a></li>
                    <li ><a href="#"><i class="fa fa-list fa-fw"></i> Entries<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li class="{{Request::is('res/entries') ? 'current-page' : ''}}"><a href="{{URL::to("res/entries")}}">List of Entries</a></li>
                            <li class="{{Request::is('res/entries/add') == "add" ? 'current-page' : ''}}"><a href="{{URL::to("res/entries/add")}}">Add Entry</a></li>
                        </ul>
                    </li>
                    @if(Auth::user()->isAdmin())
                        <li class="{{Request::is('res/raffle-draw') ? 'active' : ''}}"><a href="{{URL::to('res/raffle-draw')}}"><i class="fa fa-trophy fa-fw"></i> Raffle Draw</a></li>
                        <li class="{{Request::is('res/users') ? 'active' : ''}}"><a href="{{URL::to('res/users')}}"><i class="fa fa-user fa-fw"></i> Users</a></li>
                    @endif
                </ul>
            </div>

        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <!--
        <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
        </div>
-->
        <!-- /menu footer buttons -->
    </div>
</div>