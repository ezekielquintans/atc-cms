<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserCardRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
        if (auth()->user()->isConcierge() || auth()->user()->isAdmin()) {
            return true;
        } else {
            return false;
        }
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        $request = Request::all();

        $validationRules = [
            'first_name'    => 'required',
            'last_name'     => 'required',
            'dob'           => 'required',
            'email'         => 'required|email|unique:atc_users,email,'.$this->userid.',id,status,1',
            'contact'       => 'required|numeric',
            'address'       => 'required',
        ];
        /*
        if (isset($request['card_type'])) {
            if (isset($request['card_type']['bpi'])) {
                $validationRules['new_renewal_bpi']     = 'required';
                $validationRules['card_no_bpi']         = 'required';
                $validationRules['card_expiry_bpi']     = 'required';
                // $validationRules['birthday_bpi']        = 'required';
                // $validationRules['amount_bpi']          = 'required';
                // $validationRules['ar_no_bpi']           = 'required';
                $validationRules['name_in_card']        = 'required';
                $validationRules['beep_paywave']        = 'required';
                // $validationRules['source_of_income']    = 'required';
            }
            if (isset($request['card_type']['vip'])) {
                $validationRules['new_renewal_vip']     = 'required';
                $validationRules['card_no_vip']         = 'required';
                $validationRules['card_expiry_vip']     = 'required';
                // $validationRules['birthday_vip']        = 'required';
                // $validationRules['amount_vip']          = 'required';
                // $validationRules['ar_no_vip']           = 'required';
                // $validationRules['document_presented']  = 'required';
                // $validationRules['document_no']         = 'required';
                $validationRules['name_of_ofw']         = 'required';
                $validationRules['relationship']        = 'required';
            }
        }
        */
		return $validationRules;
	}

    // public function messages()
    // {
    //     return [
    //         'new_renewal_bpi.required'     => 'Application type is required',
    //         'new_renewal_vip.required'     => 'Application type is required',
    //         'name_in_card.required'        => 'Name of the card owner is required',
    //         'name_of_ofw.required'         => 'Name of the OFW is required',
    //         'relationship.required'        => 'Relationship with the OFW is required',
    //         'beep_paywave.required'        => 'Card type is required',
    //         'document_presented.required'  => 'Document presented is required',
    //         'document_no.required'         => 'Document number is required',
    //         'source_of_income.required'    => 'Source of income is required',
    //         'birthday.required'            => 'Birthdate is required',
    //         'card_no.required'             => 'Card number is required',
    //         'card_expiry.required'         => 'Card expiry date is required',
    //         'amount.required'              => 'Amount is required',
    //         'ar_no.required'               => 'AR number is required',
    //         'remarks.required'             => 'Remarks are required',
    //     ];
    // }

}
