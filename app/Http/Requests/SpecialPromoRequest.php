<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class SpecialPromoRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		if(auth()->user()->isConcierge() || auth()->user()->isAdmin()){
			return true;
		}
		return false;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'email'         => 'required|email|unique:atc_users,email,'.$this->cusid.',id,status,1',
			'address'       => 'required',
			'contact'       => 'required|numeric|digits_between:7,11',
			'cusid'         => 'numeric',
			'mode'          => 'in:S,A',
		];
	}

	public function messages(){
		return [
			'required'          => 'The :attribute is required',
			'required_if'       => 'The :attribute is required',
			'total_coupons.min' => 'Minimum amount must be 500 or more to generate a coupon',
			'alpha_dash'        => 'The :attribute must contain letters only',
			'email'             => ':attribute must be valid',
			'numeric'           => ':attribute must be numeric values only',
			'digits_between'    => ':attribute is invalid due to unknown format. Must be a telephone number or mobile number only',
			'in'                => ':attribute must ne one of the following types: :values',
			'integer'           => ':attribute must be an integer value',
		];
	}

}
