<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Session;
class PromoRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		if(auth()->user()->isAdmin()){
			return true;
		}
		return false;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'process'         			=> 'required|in:add,edit',
			'name'         				=> 'required|max:255',
			'subtitle'       				=> 'max:255',
			'dti_permit'        			=> 'max:100',
			'promo_type'         			=> 'required|array',
			'raffle_on'         			=> 'integer',
			'premium_on'         			=> 'integer',
			'featured_img'           		=> 'required_if:process,add|mimes:jpeg,svg,png,jpg',
			'raffle_premise_type' 			=> 'required_if:raffle_on,1|integer|in:1,2',
			'raffle_purchase_requirement' 	=> 'required_if:raffle_on,1|numeric',
			'raffle_accumulation_type' 		=> 'required_if:raffle_on,1|array',
			'raffle_multiplier' 			=> 'integer|in:0,1',
			'premium_premise_type' 			=> 'required_if:premium_on,1|integer|in:1,2',
			'premium_purchase_requirement' 	=> 'required_if:premium_on,1|numeric',
			'premium_accumulation_type' 		=> 'required_if:premium_on,1|array',
			'premium_ceiling' 			=> 'required_if:premium_on,1|integer|min:1|max:5',
			'reward_id' 				=> 'required_if:premium_on,1|integer',
			'premium_multiplier' 			=> 'integer|in:0,1',
			'premium_isEvent' 			=> 'integer|in:0,1',
		];
	}
	/**
	 * Get the error messages for the defined validation rules.
	 *
	 * @return array
	 */
	public function messages()
	{
	    $messages =  [
	        'required'          	=> 'This field is required',
			'required_if'       	=> 'This field is required',
			'in' 				=> 'Invalid value',
			'array' 			=> 'Invalid value',
			'integer' 			=> 'Invalid value',
			'mimes'        		=> 'Images must be in jpeg/jpg or png',
			'numeric'             	=> 'Must be a numeric value.',
			'name.max'          	=> 'Max. length for this field is 255',
			'subtitle.max'      	=> 'Max. length for this field is 255',
			'digits_between'    	=> ':attribute is invalid due to unknown format. Must be a telephone number or mobile number only',
			'in'                	=> ':attribute must ne one of the following types: :values',
			'integer'           	=> ':attribute must be an integer value',
	    ];
	    
	    return $messages;
	}

}
