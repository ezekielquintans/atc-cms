<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Session;

class RewardsRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		if(auth()->user()->isAdmin()){
			return true;
		}
		return false;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name'         			=> 'required|max:150',
			'rewards_count'         	=> 'required',
			'venue'           		=> 'required_if:advance_settings,on|max:150',
			'datetime'				=> 'required_if:advance_settings,on|date',
			'featured_img'           	=> 'mimes:jpeg,svg,png,jpg',
			'details'           		=> 'max:250',
		];
	}

	public function messages() {
		$messages =  [
	        	'required'          	=> 'This field is required',
			'required_if'       	=> 'This field is required',
			'name.max'          	=> 'Max. length for this field is 150',
			'venue.max'          	=> 'Max. length for this field is 150',
			'details.max'          	=> 'Max. length for this field is 250',
			'mimes'        		=> 'Images must be in jpeg/jpg or png',
	    	];
	    // Session::flash('msg_error', "Please check the required fields!");
	    return $messages;
	}

}
