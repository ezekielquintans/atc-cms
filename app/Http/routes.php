<?php

/*USER LEVEL*/
Route::get('email', 'EntryController@sendEmail');
Route::get('check-entries', 'EntryController@checkEntries');
Route::post('post-check-entries', 'EntryController@postCheckEntries');
Route::post('specialpromo/post-check-entries', 'SpecialPromoController@postCheckEntries');
Route::get('/', 'GuestController@index');
Route::get('promos/{id}/{name}', 'GuestController@show');
Route::get('specialpromo/{id}/{name}', 'SpecialPromoController@show');
Route::get('generate', 'EntryController@generateUniqueId');
/*ADMIN LEVEL*/
Route::group(['prefix'=> 'res'],function(){
	/*****ACCESS*****/
	Route::get('/', 'Auth\AuthController@displayLogin');
	Route::get('login', 'Auth\AuthController@displayLogin');
	Route::post('login', 'Auth\AuthController@processLogin');

	/********CONCEIRGE********/
	Route::group(['middleware' => 'concierge'],function(){
		/*****ACCESS*****/
		Route::get('logout', 'Auth\AuthController@processLogout');

		/*****USER*****/
		Route::resource('cards', 'UserCardController');
		Route::get('users/getInfo/', ['uses' => 'UserController@getUserInfo']);
		Route::get('tickets-claimed', 'AdminController@getTicketsEarned');
		Route::get('user/edit/{username}', 'UserController@displayEditPage');
		Route::post('user/edit/process-edit-user', 'UserController@processEditUser');

        	/*****MEMBERS*****/
		Route::resource('members', 'MemberController');
		Route::get('members/destroy/{id}', ['uses' => 'MemberController@destroy']);
		Route::get('members/getEntries/{id}', ['uses' => 'EntryController@showAjax']);
		Route::get('search/autocomplete', 'MemberController@autocomplete');
		Route::get('getUserInfo', 'MemberController@getUserInfo');
		Route::get('getEntriesOfUser/{id}', 'MemberController@getEntriesOfMember');

		
        	/*****ENTRIES*****/
		Route::get('entries/destroy/{id}', 'EntryController@destroy');
		Route::get('entries/print-entries', 'EntryController@printEntries');
		Route::get('entries/print-rewards', 'EntryController@printRewards');
		Route::get('validate-invoice', 'EntryController@validateInvoice');
		Route::get('validate-reward', 'EntryController@validateRewards');
		Route::resource('entries', 'EntryController');

		/***** REWARDS GIVEN*****/
		Route::get('rewardsgiven/validate-transaction', 'RewardGivenController@validateTransaction');
		Route::resource('rewardsgiven','RewardGivenController');

		
		/******SPECIAL PROMO*****/
		Route::resource('specialpromo/print-entries', 'SpecialPromoController@printEntries');
		Route::resource('specialpromo', 'SpecialPromoController');

	});

	/********ADMIN********/
	Route::group(['middleware' => 'admin'],function(){
		/*RAFFLE AND WINNER*/
		Route::get('raffle-draw', 'AdminController@displayRaffleDraw');
		Route::post('kashing', 'AdminController@generateWinner');
		/*Temp route*/
		Route::get('highest-spender', 'SpecialPromoController@showHighestBidder');

		/*****USERS*****/
		Route::get('users', 'UserController@displayUsers');
		Route::get('register', 'UserController@displayRegisterPage');
		Route::get('user/delete/{username}', 'UserController@processDeleteUser');
		Route::post('process-register', 'UserController@processEditUser');

		/*****PROMO*****/
		Route::resource('promo','PromoController');
		Route::get('promo/destroy/{id}', 'PromoController@destroy');


		/***** REWARDS *****/
		Route::resource('rewards','RewardController');
		Route::get('rewards/destroy/{id}', 'RewardController@destroy');
		Route::get('rewards/validate-transaction', 'RewardController@validateTransaction');
		


		/***** TEXTBLAST *****/
        Route::get('textblast', ['uses' => 'TextBlastController@index']);
        Route::get('textblast/getCredentials', ['uses' => 'TextBlastController@getCredentials']);
        Route::get('textblast/prepare', ['uses' => 'TextBlastController@getRecipients']);
        Route::post('textblast/send', ['uses' => 'TextBlastController@send']);
        Route::post('textblast/saveActivity', ['uses' => 'TextBlastController@saveActivityEntry']);

	});
});
