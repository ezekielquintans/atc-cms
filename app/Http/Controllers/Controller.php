<?php namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Session;

abstract class Controller extends BaseController {

	use DispatchesCommands, ValidatesRequests;

	protected function returnSuccess($message, $path = ""){
	    Session::flash('msg_success', $message);

	    if($path != ""){
	        return redirect()->to($path);
	    }

	    return redirect()->back();
	}

	protected function returnFailed($message = ""){
	    if ($message == "") {
	        $message = "Something went wrong with your transaction. Please try again.";
	    }

	    Session::flash('msg_error',$message);

	    return redirect()->back()->withInput();
	}

}
