<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\Http\Controllers\ActivityController;
use Illuminate\Http\Request;
use Auth;
use Session;

class AuthController extends Controller {

	use AuthenticatesAndRegistersUsers;

	public function displayLogin(){
		return view('admin.pages.login');
	}

	public function processLogin(Request $request){
		$this->validate($request, [
            'username' 		=> 'required',
            'password' 		=> 'required'
		]);
		$credentials = $request->only('username', 'password');
		$credentials['status'] = 1;
		if(Auth::attempt($credentials, false)){
			ActivityController::logActivity(Auth::user()->username . ' has logged in.', Auth::user()->id);
			return redirect()->to(url('res/members'));
		}else{
			Session::flash('msg_error', "These credentials do not match our records.");
				return redirect()->back()
				->withInput($request->only('username'))
				->withErrors([
				'username' => "These credentials does not match our records.",
			]);
		}
    	}

    	public function processLogout(){
	    	ActivityController::logActivity(Auth::user()->username . ' has logged out.', Auth::user()->id);
	  	Session::flush();
	  	Auth::logout();
	  	return redirect()->to('res/login');
    	}

}
