<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\User;
use App\Entry;
use Auth;
use Response;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Input;
use Laracasts\Classes\UserCardProcess;
use Session;
use Validator;
class UserController extends Controller
{
    public function displayUsers(){
        $search = Input::get('search', '');
        if($search != ""){
            $users = User::where('status', '=', 1)
            ->where('usertype', '!=' , 1)
            ->orderBy('last_name', 'asc')
            ->where('first_name', 'LIKE', '%'.$search.'%')
            ->orWhere('middle_name', 'LIKE', '%'.$search.'%')
            ->orWhere('last_name', 'LIKE', '%'.$search.'%')
            ->paginate(10)
            ->setPath('users')
            ->appends(Input::except('page'));
        }else{
            $users = User::where('status', '=', 1)
            ->where('usertype', '!=' , 1)
            ->orderBy('last_name', 'asc')
            ->paginate(10)
            ->setPath('users')
            ->appends(Input::except('page'));
        }

        return view('admin.pages.user', compact('users'));
    }


    public function displayRegisterPage(){
        return view('admin.pages.register');
    }

    public function displayEditPage($username){
        $user = User::where('username','=',$username)->first();

        if(count($user) > 0){
            if(((Auth::user()->usertype == 2 && $user->usertype == 2) && ($user->id != Auth::user()->id)) ||
                (Auth::user()->usertype == 2 && $user->usertype == 3)
                ){
                $user = array();
            }
        }else{
            $user = array();
        }
        return view('admin.pages.edit-user', compact('user'));
    }

    public function processEditUser(Request $request){
        $rules = [
            'usertype'    => 'sometimes|numeric',
            'first_name'  => 'required|min:3|regex:/^[\pL\s\d.]+$/u',
            'middle_name' => 'min:3|regex:/^[\pL\s]+$/u',
            'last_name'   => 'required|min:3|regex:/^[\pL\s\d.]+$/u',
            'email'       => 'required|email',
            'contact'     => 'required|numeric',
            'address'     => 'required'
        ];

        if($request->get('password') != ""){
            $rules["password"] =  'required|confirmed|min:6';
        }

        if($request->get('username') != ""){
            $rules['username'] = 'required|max:255|unique:atc_users,username,NULL,id,status,1';
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }
        if($request->get('userid') != null){
            $user              = User::find($request->get('userid'));
        }else{
            $user = new User();
            $user->unique_id = strtoupper(uniqid());
            $user->status    = 1;
            $user->username  = $request->get('username');
        }
        $user->first_name  = $request->get('first_name');
        $user->middle_name = $request->get('middle_name');
        $user->last_name   = $request->get('last_name');
        $user->dob         = $request->get('dob');
        $user->email       = $request->get('email');
        $user->contact     = $request->get('contact');
        $user->address     = $request->get('address');
        if(Auth::user()->usertype == 3){
            $user->usertype    = $request->get('usertype');
        }
        $user->updated_at  = date('Y-m-d h:i:s');

        if($request->get('password') != ""){
            $user->password = bcrypt($request->get('password'));
        }

        if($user->save()){
            if($request->get('userid') != null){
                Session::flash("message", "\"". $request->first_name . "\" has been updated.");
            }else{
                Session::flash("message", "\"". $request->first_name . "\" has been created.");
            }
            if (Auth::user()->usertype == 2) {
                Session::flash("message", $request->first_name . "'s profile has been updated.");
            }
            Session::flash("messageClass", "alert-success");

            if (Auth::user()->usertype == 3) {
                return Redirect::to(url('res/users'));
            }
            if (Auth::user()->usertype == 2) {
                return Redirect::to('res/members');
            }

        }else{
            Session::flash("message", "There was a problem with your request. Please try again later.");
            Session::flash("messageClass", "alert-danger");
            if($request->get('userid') != null){
                if(Auth::user()->usertype == 3){
                    return Redirect::to('res/user/edit/'.$request->get('username'))->withInput();
                }else{
                    return Redirect::to('res/members');
                }
            }else{
                return Redirect::to('res/register')->withInput();
            }
        }
    }
    public function displayUpdateMembers($process,$id = null){
        $user = [];
        if(!is_null($id)){
            $user = User::find($id);
        }
        return view('admin.pages.update-member', compact('user'));
    }
    public function processUpdateMembers($process,$id = null){
        Session::flash("msg_error","Sorry, This feature is currently under construction");
        return redirect()->back();
    }

    public function processDeleteUser($username){
        $user = User::where('username', '=', $username)->first();
        if(count($user) == 1) {
            $user->status = 0;
            $user->save();

            Session::flash("message", "\"". $username . "\" has been deleted.");
            Session::flash("messageClass", "alert-success");
        }else{
            Session::flash("message", "There was a problem with your request. Please try again later.");
            Session::flash("messageClass", "alert-danger");
        }
        return Redirect::back();
    }

    public function index()
    {
        $members = User::where('status', 1)
            ->where('usertype', 1)
            ->orderBy('last_name', 'asc')
            ->paginate(20)
            ->setPath('test')
            ->appends(Input::except('page'));

        return view('admin.pages.members', ['members' => $members]);
    }

    public function create()
    {
        return view('admin.pages.add-member', [
            'userTypes' => User::getUserTypes(),
        ]);
    }

    public function store()
    {

    }

    public function show()
    {

    }

    public function getUserInfo()
    {
        $userID = Input::get('userid');
		$user = User::find($userID);
		if(count($user) > 0){
			$user->display_id = str_pad(Input::get('userid'), 8, '0', STR_PAD_LEFT);
			$rewardsClaimed = RewardGiven:: where('userid', $user->id)
				->whereRaw('Date(created_at) = CURDATE()')
				->count();
			$user->rewardsClaimedToday = $rewardsClaimed;
		}
		return Response::json($user);
    }

    public function edit($id)
    {
        $user = User::find($id);
        
        return view('admin.pages.update-member', ['user' => $user]);
    }

    public function update()
    {

    }

    public function destroy($id)
    {
        $user = User::find($id)->setStatus(0);

        return redirect()->back();
    }

}
