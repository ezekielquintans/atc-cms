<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\RewardGiven;
use App\Promo;
use Response;
use Input;

class RewardGivenController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$promos = Promo::where('status', 1)
				->where('date_started', '<=' , date('Y-m-d H:i:s'))
				->where('date_ended', '>=', date('Y-m-d H:i:s'))
				->whereIn('promo_type',[2,3])
				->get();

		$search  = Input::get('search', '');
		$promoId = Input::get('promo', '');
		$rewards = RewardGiven::selectRaw('atc_promos.name as promo_name,atc_rewards.name as reward_name,atc_rewards_given.serial_no as serial_no, atc_rewards_given.userid, atc_users.first_name, atc_users.last_name, atc_users.middle_name, atc_users.email, atc_users.address,atc_users.dob,atc_users.contact, atc_rewards_given.created_at as entry_created')
					->leftJoin('atc_users','atc_rewards_given.userid', '=', 'atc_users.id')
					->leftJoin('atc_promos','atc_rewards_given.promo_id', '=', 'atc_promos.id')
					->leftJoin('atc_rewards','atc_rewards_given.rewards_id', '=', 'atc_rewards.id')
					->where('atc_users.usertype', 1);

		if($search != ''){
			$rewards = $rewards->where(function($q) use ($search){
				$q->where('atc_users.first_name', 'LIKE', '%' . $search . '%')
				->orWhere('atc_users.last_name', 'LIKE', '%' . $search . '%')
				->orWhere('atc_users.middle_name', 'LIKE', '%' . $search . '%')
				->orWhere('atc_users.email', 'LIKE', '%' . $search . '%')
				->orWhere('atc_rewards_given.id', '=', ltrim($search, '0'))
				->orWhere('atc_users.id', '=', ltrim($search, '0'));
			});
		}
		if($promoId != '' && is_numeric($promoId)){
			$rewards = $rewards->where('atc_rewards_given.promo_id', $promoId);
		}
		$rewards = $rewards->orderBy('atc_rewards_given.id','desc')
			->paginate(20)
			->setPath('rewardsgiven')
			->appends(Input::except('page'));
		// dd($rewards);
    	return view('admin.pages.rewards.view-rewardsgiven', compact('rewards','promos'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function validateTransaction(Request $request){
		if($request->ajax()){
			$rewardsClaimed = RewardGiven:: where('userid', $request->input('cId',''))
				->where('rewards_id', $request->input('rId',''))
                ->whereRaw('Date(created_at) = CURDATE()')
                ->count();
            return Response::json($rewardsClaimed);
		}
	}

}
