<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\ActivityController;
use Illuminate\Http\Request;
use Input;
use Response;
use Session;
use Redirect;
use Auth;
use URL;
use Carbon\Carbon;
use Validator;
use App\User;
use App\Coupons;
use App\Entry;
use App\Stores;
use App\Promo;
use App\RewardGiven;
use App\Reward;
use DB;


class AdminController extends Controller {

	public function displayRaffleDraw(){
		$promo = Promo::where('status', 1)
				// ->whereIn('promo_type',[1,3])
				->where('date_started', '<=' , date('Y-m-d'))
				->where('date_ended', '>=', date('Y-m-d'))
				->get(['id','name']);
		return view('admin.pages.raffle', compact('promo'));
	}

	public function generateWinner(Request $request){
		if(!is_numeric($request->promo_id)){
			return json_encode(array());
		}

		$winner =  Entry::selectRaw('atc_users.unique_id as user_unique, atc_entries.unique_id as coupon_no, atc_entries.id as entry_id, atc_entries.user_id, atc_users.first_name, atc_users.last_name, atc_users.middle_name, atc_users.email, atc_users.address,atc_users.dob,atc_users.contact, atc_entries.unique_id as entrie_unique')
					->leftJoin('atc_users','atc_entries.user_id', '=', 'atc_users.id')
					->where('atc_entries.status', 1)
					->where('atc_entries.promo_id', $request->promo_id)
					->where('atc_users.usertype', 1)
					->orderByRaw('RAND()')
					->first();
		if(!is_null($winner)){
			$winner = $winner->toArray();
		// ActivityController::logActivity($winner->first_name.' '.$winner->last_name.' has won the raffle draw', Auth::user()->id);
			$winner['entry_id'] = str_pad($winner["entry_id"], 7, '0', STR_PAD_LEFT);
			$winner['custom_id'] = str_pad($winner["user_id"], 8, '0', STR_PAD_LEFT);
		}
		return json_encode($winner);
	}

	public function updateWinner($userid){
		$entry = Entry::where('user_id', $userid)->first();
		$entry->status = 2;
		$entry->save();
	}


	

	

}
