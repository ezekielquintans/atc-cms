<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\UserCardRequest;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\User;
use App\Entry;
use App\RewardGiven;
use App\UserCard;
use Input;
use Response;
use Session;
use Auth;
use DB;
use Carbon\Carbon;
use App\Services\TextBlast;
use App\Activities;

class MemberController extends Controller {
    public function index(Request $request)
    {
        $user = Auth::user();
        $searchQuery = $request->search;
        $searchResult;

        $members = User::with('cards')
            ->where('status', 1)
            // ->where('usertype', $user->isAdmin() ? '<=' : '<', $user->userType());
            ->where('usertype', 1);

        if(!empty($searchQuery) && $searchQuery != null) {
            $searchResult = User::searchUser($members, $searchQuery);

            if ($searchResult->count() == 0) {
                $members = $searchResult;
            }
        }

        $members = $members->orderBy('created_at', 'desc')
            ->paginate(20)
            ->setPath('members')
            ->appends(Input::except('page'));

        return view('admin.pages.members.members', ['members' => $members]);
    }

    public function create()
    {
        return view('admin.pages.members.add-member', [
            'userTypes' => User::getUserTypes(),
        ]);
    }

    public function store(Request $request)
    {
        $v = $this->validate($request, [
                'email'         => 'required|email|unique:atc_users,email',
        ]);

        User::addMemeber($request);
        Activities::newUser("$request->first_name $request->last_name");

        return redirect()->back();
    }

    public function show($id)
    {

    }

    public function edit($id)
    {
        $user = User::find($id);
        $bpiCard = $user->cards()->bpi()->get();
        $vipCard = $user->cards()->vip()->get();

        return view('admin.pages.members.update-member', [
            'user' => $user,
            'cards' => $user->cards(),
            'bpiCard' => isset($bpiCard) ? $bpiCard : null,
            'bpiCardPresent' => empty($bpiCard[0]) ? false : true,
            'vipCard' => isset($vipCard) ? $vipCard : null,
            'vipCardPresent' => empty($vipCard[0]) ? false : true,
        ]);
    }

    public function update(UserCardRequest $request)
    {
        $user = User::find($request->userid);
        $user->updateUser($request);
        Activities::updateUser($request->userid);

        return redirect()->back();
    }

    public function destroy($id)
    {
        $user = User::find($id)->setStatus(0);
        Activities::destroyedUser($id);

        return redirect()->back();
    }
    public function autocomplete(Request $request){
        if($request->ajax()){
            $searchQuery = explode(" ",Input::get('query'));

            $results = [];
            $users = [];
            $usersQuery = User::query();

            foreach ($searchQuery as $key => $search) {               
                $usersQuery->where('status',1)
                    ->where('usertype',1)
                    ->where(function($q) use ($search){
                        $q->Where('first_name', 'LIKE', '%'.$search.'%')
                        ->orWhere('middle_name', 'LIKE', '%'.$search.'%')
                        ->orWhere('last_name', 'LIKE', '%'.$search.'%')
                        ->orWhere('email', 'LIKE', '%'.$search.'%');
                });
            }
            $users = $usersQuery->orderBy('first_name')->get();
            foreach ($users as $key => $user) {
                $results[] = [
                    "value" => $user->last_name.', '.$user->first_name.' '.$user->middle_name.'  ('.ucfirst($user->address).')',
                    "data"  => $user->id
                ];
            }
            $suggestions['suggestions'] = $results;
            return Response::json($suggestions);
        }
    }
    public function getUserInfo(Request $request){
        if($request->ajax()){
            $userID = Input::get('userid');
            $user = User::find($userID);
            if(count($user) > 0){
                $user->display_id = str_pad(Input::get('userid'), 8, '0', STR_PAD_LEFT);
                $rewardsClaimed = RewardGiven:: where('userid', $user->id)
                    ->whereRaw('Date(created_at) = CURDATE()')
                    ->count();
                $user->rewardsClaimedToday = $rewardsClaimed;
                $userCards = UserCard::where('user_id',$userID)
                ->get();
                $cards = [];
                foreach($userCards as $uc){
                    if($uc->card_type == "bpi"){
                        $cards[] = "bpi";
                    }else if($uc->card_type == "vip"){
                        $cards[] = "vip";
                    }
                }
                // $user->userCards = implode(", ",$cards);
                $user->userCards = $cards;
            }
            return Response::json($user);
        }
    }
    public function getEntriesOfMember($id){
        if(isset($id) && $id != ""){
            $entries = Entry::where('user_id',$id)
                ->where('status',1)
                ->get();
            return Response::json($entries);
        }
        exit();
    }
}
