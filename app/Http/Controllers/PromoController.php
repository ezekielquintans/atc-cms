<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\PromoRequest;
use App\Promo;
use App\Reward;
use Input;
use Session;
use Auth;
use Redirect;


class PromoController extends Controller {

	public function __construct()
	{
		$this->promo = new Promo();
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$search  =  Input::get('search', '');
		$promo = Promo::where('status','<>',0);
		if($search != ''){
			$promo = $promo->where(function($query) use($search){
				$query->where('atc_promos.name', 'LIKE', '%' . $search . '%')
					->orWhere('atc_promos.id', '=', ltrim($search, '0'));
			});
		}
		$promo = $promo->orderBy('date_ended','desc')
			->with('rewards')
			->paginate(10)
            ->setPath('promo')
            ->appends(Input::except('page'));
		return view('admin.pages.promo.promo',compact('promo'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$rewards = Reward::where('promo_id',0)
		->where('status',1)
		->get();
		$process = "add";
		$promo = new Promo;
		return view("admin.pages.promo.update-promo",compact('promo','process','rewards'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(PromoRequest $request)
	{
		$duration = explode(" - ",$request->duration);
		$promo = new Promo;
		$process = "Added";
		if($request->promo_id != "" && $request->process == "edit"){
			$promo = Promo::find($request->promo_id);
			$process = "Edited";
		}
		$promo->name 	= $request->name;
		$promo->subtitle 	= $request->subtitle;
		$promo->helper 	= $request->input('helper','');
		$promo->description 	= $request->input('description','');
		if($request->hasFile('featured_img')){
		    $destination = 'public/landing/img';
		    $featured_img = $request->file('featured_img');
		    $filename_img = uniqid('promo_').'.'.$featured_img->getClientOriginalExtension();
		    $upload_featured_img = $featured_img->move($destination, $filename_img);
		    $promo->image_url      = $destination.'/'.$filename_img;
		}
		$promo->helper 	= $request->input('helper','');
		$promo->dti_permit 	= $request->dti_permit;
		// $promo->reward_id = $request->reward
		$promo->promo_type = array_sum($request->promo_type);
		if($request->raffle_on == 1){
		  	$promo->raffle_premise = $request->raffle_premise_type;
		  	$promo->raffle_accumulation_type = array_sum($request->raffle_accumulation_type);
		  	$promo->raffle_purchase_requirement = $request->raffle_purchase_requirement;
		  	$promo->raffle_multiplier = $request->input("raffle_multiplier",0);
		  	if($request->premium_on != 1){
		  		$promo->premium_premise = 0;
		  		$promo->premium_accumulation_type = 0;
			  	$promo->premium_purchase_requirement = 0;
			  	$promo->premium_ceiling = 0;
			  	$promo->premium_multiplier = 0;
			  	$promo->premium_isEvent  = 0;	
		  	}
		}
		if($request->premium_on == 1){
			$promo->premium_premise = $request->premium_premise_type;
		  	$promo->premium_accumulation_type = array_sum($request->premium_accumulation_type);
		  	$promo->premium_purchase_requirement = $request->premium_purchase_requirement;
		  	$promo->premium_ceiling = $request->premium_ceiling;
		  	$promo->premium_multiplier = $request->input("premium_multiplier",0);
		  	$promo->premium_isEvent  = $request->input("premium_isEvent",0);
		  	/*Reset previous reward*/
		  	if($request->reward_id != ""){
			  	$prevReward = Reward::find($promo->reward_id);
				if(isset($prevReward->promo_id) && $prevReward->promo_id != $request->reward_id){
					$prevReward->promo_id  = 0;
					$prevReward->save();
				}
		  	}
		  	$promo->reward_id  = $request->input("reward_id",0);
		}
			$promo->date_started	= strtotime($duration[0]);
			$promo->date_ended	= strtotime($duration[1]);
			if($request->raffle_on != 1){
				$promo->raffle_premise = 0;
			  	$promo->raffle_accumulation_type = 0;
			  	$promo->raffle_purchase_requirement = 0;
			  	$promo->raffle_multiplier = 0;
			}

		$promo->save();
		if($request->has('reward_id') && $request->premium_on == 1){	
			$reward = Reward::find($request->reward_id);
			$reward->promo_id = $promo->id;
			$reward->save();
		}
		Session::flash('msg_success', 'Promo '. lcfirst($process).' successfully!');
		ActivityController::logActivity(Auth::user()->username . ' '. $process .' a promo. ' .$promo->id, Auth::user()->id);
		if($process == "Added"){
		return Redirect::to('res/promo/'.$promo->id);
		}
		return Redirect::back()->withInput();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id = null)
	{
		$rewards = Reward::where('promo_id',0)
		->where('status',1)
		->get();
		$process = "view";
		if(!is_null($id)){
			$promo = Promo::find($id);
		}
		return view("admin.pages.promo.update-promo",compact('promo','process','rewards'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id = null)
	{
		if(!is_null($id)){
			$promo = Promo::where('id',$id)
			->with('rewards')
			->first();
			if($promo->reward_id != 0){
				$rewards = Reward::where('promo_id','<>',$promo->reward_id)
				->where('promo_id',0)
				->where('status',1)
				->get();
			}else{
				$rewards = Reward::where('promo_id',0)
				->where('status',1)
				->get();
			}
			$process = "edit";
			return view("admin.pages.promo.update-promo",compact('promo','process','rewards'));
		}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		return $this->store();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$promo = Promo::find($id);
		$promo->status = 0;
		$promo->save();
		ActivityController::logActivity($promo->name . '  was deleted', Auth::user()->id);
		Session::flash('msg_success', 'Deleted successfully!.');
		return Redirect::to('res/promo');
	}

}
