<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\EntryRequest;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Entry;
use App\User;
use App\Promo;
use App\Stores;
use App\RewardGiven;
use App\Reward;
use Response;
use Input;
use Auth;
use Session;
use Redirect;
use Validator;
use Excel;


class EntryController extends Controller {

	public function index()
	{
		$promos = Promo::where('status', 1)
				->where('date_started', '<=' , date('Y-m-d H:i:s'))
				->where('date_ended', '>=', date('Y-m-d H:i:s'))
				->whereIn('promo_type',[1,3])
				->get();

		$search  = Input::get('search', '');
		$promoId = Input::get('promo', '');
		$entries =  Entry::selectRaw('atc_entries.unique_id as coupon_id, atc_entries.user_id, atc_users.first_name, atc_users.last_name, atc_users.middle_name, atc_users.email, atc_users.address,atc_users.dob,atc_users.contact, atc_entries.created_at as entry_created,atc_entries.amount,atc_entries.store_name,atc_entries.receipt_no')
					->leftJoin('atc_users','atc_entries.user_id', '=', 'atc_users.id')
					->where('atc_entries.status', 1)
					->where('atc_users.usertype', 1);

		if($search != ''){
			$entries = $entries->where(function($query) use($search){
				$query->where('atc_users.first_name', 'LIKE', '%' . $search . '%')
				->orWhere('atc_users.last_name', 'LIKE', '%' . $search . '%')
				->orWhere('atc_users.middle_name', 'LIKE', '%' . $search . '%')
				->orWhere('atc_users.email', 'LIKE', '%' . $search . '%')
				->orWhere('atc_entries.id', '=', ltrim($search, '0'))
				->orWhere('atc_users.id', '=', ltrim($search, '0'));
				// ->orWhere('atc_users.middle_name', 'LIKE', '%' . $search . '%');
			});
		}

		if($promoId != '' && is_numeric($promoId)){
			$entries = $entries->where('promo_id', $promoId);
		}

		$entries = $entries->where('atc_entries.status',"=",1)
			->orderBy('atc_entries.id','desc')
			->paginate(20)
                  ->setPath('entries')
                  ->appends(Input::except('page'));

        $download = Input::get('download',false);
		if($download)
		Excel::create('List of Entries', function($excel) use($entries, $download){
			$excel->sheet('List of Entries', function($sheet) use($entries, $download){
			$sheet->setShowGridlines(true);
			$sheet->loadView('admin.pages.entries.list-of-entries-excel', compact('entries', 'download'));
			});
		})->export('xlsx');
        return view('admin.pages.entries.view-entries', compact('entries','promos'));
	}

	public function create()
	{
		$users = User::where('status',1)
			->where('usertype',1)
			->get();

		$token    = Input::get('tk', '');
		if($token != ""){
			$entry 	  =  Entry::selectRaw('atc_entries.token,atc_entries.unique_id as coupon_id, atc_entries.user_id, atc_users.first_name, atc_users.last_name, atc_users.middle_name, atc_users.email, atc_users.address,atc_users.dob,atc_users.contact')
						->leftJoin('atc_users','atc_entries.user_id', '=', 'atc_users.id')
						->where('atc_entries.status', 1)
						->where('atc_entries.token', $token)
						->where('atc_users.usertype', 1)
						->orderBy('atc_entries.created_at', 'desc')
						->get();
		}else{
			$entry = array();
		}
		$isEvent = Input::get('event','');
		if($isEvent == true){
			$rewardsGiven = RewardGiven::	with('rewards','promo')
				->with(['user' => function ($query){
					$query->where('usertype',1);
				}])
				->where('batch_id',$token)
				->orderBy('created_at','desc')
				->get();

		}else{
			$rewardsGiven = array();
		}

		$stores       = new Stores();
		$storeListing = $stores->getStores();

		$promos = Promo::where('status', 1)
				->with('rewards')
				->where('date_started', '<=' , date('Y-m-d H:i:s'))
				->where('date_ended', '>=', date('Y-m-d H:i:s'))
				->get();
		return view('admin.pages.entries.entries',compact('users', 'entry', 'token', 'storeListing','promos','rewardsGiven'));
	}

	public function store(EntryRequest $request)
	{
		// dd($request->all());
		if(strtok($request->server('HTTP_REFERER'),'?') !== url('res/entries/create')){
			exit();
		}
        	$stores       = new Stores();
        	$storeListing = $stores->getStores();

		$couponIds = array();
		$customerId   = $request->cusid;
		$noOfRows     = $request->rows;
		$store        = $request->store;
		$promo        = $request->promo;
		$invoice      = $request->invoice;
		$amount       = $request->amount;
		$coupons      = $request->coupons;
		$rewards      = $request->tickets;
		$multiplier   = $request->multiplier;
		$fullname	  = $request->first_name.' '.$request->last_name;
		$token 		  = $this->generateUniqueId(uniqid(), 13);
			/*Register User if not yet existed*/
		/*Register User if not yet existed*/
		if($customerId == ""){
			$checkExist = User::where('first_name',$request->first_name)
							->where('last_name',$request->last_name)
							->where('contact',$request->contact);
			if($checkExist->exists()){
				if($checkExist->first()->status == 0){
					$existingUser = User::find($checkExist->first()->id);
					$existingUser->status = 1;
					$existingUser->save();
				}
				$customerId = $checkExist->first()->id;
			}else{
				$user = User::addMemeber($request);
				$customerId = $user->id;
			}
		}else{
			$user = User::find($customerId);
        	$user->updateUser($request);

		}
		
			/*Insert Coupons*/
			// dd($request->all());
		if ($request->is_Event != 1 && $request->promo_type != 2) {
			$tempTotal = 0;
			for ($i=0; $i < $noOfRows ; $i++) {
				$noCoupons = 0;
				if(isset($multiplier[$i]) && $multiplier[$i] == "on" || $request->cusCard == true){
					$noCoupons = $coupons[$i] * 2;
				}else{
					$noCoupons = $coupons[$i];
				}

				if($request->get('mode') == "A" || $request->cusCard == true){
					$tempTotal = $tempTotal + $noCoupons;
					if($i == $noOfRows - 1){
						if($tempTotal != $request->get('total_coupons')){
							$temp = $request->get('total_coupons') - $tempTotal;
							$noCoupons = $noCoupons + $temp;
						}
					}
				}
				for ($c=0; $c < $noCoupons ; $c++) {
					$categoryFetched = $stores->getCurrentCategory($store[$i]);
					if($categoryFetched === null || $categoryFetched == ""){
						$categoryFetched = "-";
					}

					$entry = new Entry;
					$entry->user_id 		= $customerId;
					$entry->promo_id 		= $promo;
					$entry->category 		= $categoryFetched;
					$entry->store_name 	= $store[$i];
					$entry->receipt_no 	= $invoice[$i];
					$entry->amount 		= $amount[$i];
					$entry->status 		= 1;
					$entry->token 		= $token;
					$entry->ipaddress		= $_SERVER['REMOTE_ADDR'];
					$entry->browser		= $_SERVER['HTTP_USER_AGENT'];
					$entry->save();

					$entry->unique_id = $this->generateUniqueId($entry->id);
					$entry->save();
					$couponIds[] = $entry->unique_id;
				}
			}
		}
		/*Insert moview Tickets*/
		$total_rewards = $request->total_tickets;
		$rewardsGivenIds = [];
		if ($request->promo_type != 1 && isset($total_rewards)) {
			for ($t=0; $t < $total_rewards; $t++) {
				$reward = new RewardGiven;
				$reward->userid 		= $customerId;
				$reward->promo_id 	= $promo;
				$reward->rewards_id 	= $request->rewards_id;
				if($request->is_Event == 1){
					$reward->serial_no=strtoupper(uniqid());
				}else{
					$reward->serial_no= $rewards[$t];
				}
				$reward->batch_id 	= $token;
				$reward->ipaddress 	= $_SERVER['REMOTE_ADDR'];
				$reward->save();
				$rewardsGivenIds[] = $reward->serial_no;
			}
		}
		if($request->is_Event != 1){
			$forPrint = array(
				"customerId" => $customerId,
				"noOfRows"	 => $noOfRows,
				"store"	 => $store,
				"invoice"	 => $invoice,
				"couponIds"	 => $couponIds,
				"token"	 => $token
			);
			Session::forget('for-print');
			Session::put('for-print', $forPrint);

			$emailSent = $this->sendEmail("", $token);
			$emailSentInt = 0;
			
			if($emailSent === true){
				$emailSentInt = 1;
				Session::flash('msg_success', 'Entries inserted successfully!');
			}else{
				Session::flash('msg_warning', 'Entries inserted successfully, but there was a problem sending an email to the user.');
			}
			$entrySent = Entry::where('token', $token)->update(['email_sent'=>$emailSentInt]);
			if($request->promo_type == 1 || $request->promo_type == 3){
				ActivityController::logActivity(Auth::user()->username . ' generated '.count($couponIds).' coupons for customer no. '.$customerId.', ', Auth::user()->id);
			} 
			if($request->promo_type == 2 || $request->promo_type == 3 && isset($total_rewards)){
				ActivityController::logActivity(Auth::user()->username . ' given '.$total_rewards.' of reward_id '.$request->rewards_id . ' for customer no. '.$customerId.', ', Auth::user()->id);
			}

        		return Redirect::to('res/entries/create?success=true&tk=' . $token);
		}else{
			$generateTickets = array(
				"customerId" => $customerId,
				"noOfRows"	 => $noOfRows,
				"store"	 => $store,
				"invoice"	 => $invoice,
				"rewardIds"	 => $rewardsGivenIds       ,
				"token"	 => $token
			);
			Session::forget('generateTickets');
			Session::put('generateTickets', $generateTickets);
			$emailSent = $this->sendRewardsInfo("", $token);
			if($emailSent === true){
				Session::flash('msg_success', 'Entries inserted successfully!');
			}else{
				Session::flash('msg_warning', 'Entries inserted successfully, but there was a problem sending an email to the user.');
			}
			if(isset($total_rewards)){
				ActivityController::logActivity(Auth::user()->username . ' generated '.count($total_rewards).' of reward_id'.$request->rewards_id.' for customer no. '.$customerId.', ', Auth::user()->id);
        		}
        		return Redirect::to('res/entries/create?success=true&tk=' . $token . '&event=true');
		}		
	}

	public function show($id)
	{
		//
	}

	public function showAjax($userId)
	{
        $entries = Entry::with('user')
            ->where('user_id', $userId)
            ->where('status', 1)
            ->get();

        return Response::json($entries);
	}

	public function edit($id)
	{
		//
	}

	public function update($id)
	{
		//
	}

	public function destroy($id)
	{
		$entry = Entry::where('unique_id', $id)
			->where('status',1)
			->first();
		if(count($entry) == 0){
			Session::flash('msg_warning', 'Failed to delete coupon.');
			return Redirect::back();
		}
		$entry->status = 0;
		$entry->save();
		ActivityController::logActivity('Coupon no. '.$entry->id . '  was deleted', Auth::user()->id);
		Session::flash('msg_success', 'Deleted successfully!.');
		return Redirect::back();
	}
	public function printEntries(){
		$token    = Input::get('tk', '');
		$entry 	  =  Entry::selectRaw('atc_entries.promo_id,atc_entries.token,atc_entries.unique_id as coupon_id, atc_entries.user_id, atc_users.first_name, atc_users.last_name, atc_users.middle_name, atc_users.email, atc_users.address,atc_users.dob,atc_users.contact')
					->leftJoin('atc_users','atc_entries.user_id', '=', 'atc_users.id')
					->where('atc_entries.status', 1)
					->where('atc_entries.token', $token)
					->where('atc_users.usertype', 1)
					->get();

		$totalCount =  Entry::where('user_id', $entry[0]->user_id)
						->where('status', 1)
						->where('promo_id', $entry[0]->promo_id)
						->get()
						->count();

		$customer   = $entry->first()->first_name.' '.$entry->first()->last_name;

		$promoInfo  = Promo::where('id', $entry[0]->promo_id)->first();


		$rewardsGiven = RewardGiven::selectRaw('atc_rewards.name as reward, atc_rewards_given.*')
						->where('batch_id', $token)
						->leftJoin('atc_rewards', 'atc_rewards_given.rewards_id', '=', 'atc_rewards.id')
						->get();


		ActivityController::logActivity(Auth::user()->username . ' printed coupons for '. $customer, Auth::user()->id);
		
		return view('print.entries', compact('entry','token','totalCount', 'rewardsGiven', 'promoInfo'));
	}
	public function printRewards(){
		
		$token    = Input::get('tk', '');
		$rewardsGiven = RewardGiven::with('rewards','promo')
			->with(['user' => function ($query){
				$query->where('usertype',1);
			}])
			->where('batch_id',$token)
			->orderBy('created_at','desc')
			->get();
		// dd($rewardsGiven);
		$customer   = $rewardsGiven[0]->user->first_name.' '.$rewardsGiven[0]->user->last_name;
		ActivityController::logActivity(Auth::user()->username . ' printed '. $rewardsGiven[0]->rewards->name .' for '. $customer, Auth::user()->id);
		return view('print.rewards', compact('token','rewardsGiven'));
	}
	public function validateInvoice(Request $request, $store = "", $invoice = ""){
		if(Input::get('store','') != ""){
			$store = Input::get('store');
		}

		if(Input::get('invoice','') != ""){
			$invoice = Input::get('invoice');
		}

		$entry = Entry::where('store_name', $store)
				->where('receipt_no', $invoice)
				->where('status', 1)
				->first();

		if($request->ajax()){
			if(count($entry) > 0){
				echo "false";
				exit;
			}
			echo "true";
			exit;
		}else{
			if(count($entry) > 0){
				return false;
			}
			return true;
		}
	}

	public function validateRewards(Request $request, $serialno = "", $promoid = ""){
		if(Input::get('serialno','') != ""){
			$serial = Input::get('serialno');
		}

		if(Input::get('promoid','') != ""){
			$promo = Input::get('promoid');
		}
		$rewards = RewardGiven::where('serial_no', $serial)
				->where('promo_id', $promo)
				->first();
		if($request->ajax()){
			if(count($rewards) > 0){
				echo "false";
				exit;
			}
			echo "true";
			exit;
		}else{
			if(count($rewards) > 0){
				return false;
			}
			return true;
		}
	}

	public function sendEmail($userid = "", $token = "", $promoId = ""){
		$entry 	  =  Entry::selectRaw('atc_entries.promo_id, atc_entries.token,atc_entries.unique_id as coupon_id, atc_entries.user_id, atc_users.first_name, atc_users.last_name, atc_users.middle_name, atc_users.email, atc_users.address,atc_users.dob,atc_users.contact')
					->leftJoin('atc_users','atc_entries.user_id', '=', 'atc_users.id')
					->where('atc_entries.status', 1)
					->where('atc_users.usertype', 1);

		if($userid != ""){
			$entry = $entry->where('atc_entries.user_id', $userid);
			$all = "all";
		}

		if($token != ""){
			$entry = $entry->where('atc_entries.token', $token);
			$all = "";
		}

		if($promoId != ""){
			$entry = $entry->where('promo_id', $promoId);
		}

		$entry = $entry->orderBy('atc_entries.id', 'desc')->get();

		if(count($entry) == 0){
			return false;
		}

		$totalCount =  Entry::where('user_id', $entry[0]->user_id)->where('status', 1);
		if($promoId != "" || $entry[0]->promo_id != ""){
			if($promoId == ""){
				$promoId = $entry[0]->promo_id;
			}
			$totalCount = $totalCount->where('promo_id', $promoId);
		}
		$totalCount = $totalCount->get()->count();

		$promoInfo    = array();
		$rewardsGiven = array();
		$promoInfo    = Promo::where('id', $entry[0]->promo_id)->first();
		if($token != ""){

			$rewardsGiven = RewardGiven::selectRaw('atc_rewards.name as reward, atc_rewards_given.*')
							->where('batch_id', $token)
							->leftJoin('atc_rewards', 'atc_rewards_given.rewards_id', '=', 'atc_rewards.id')
							->get();
		}

		try{
			\Mail::send(['html'=>'admin.email.send-coupons'], ['rewardsGiven'=> $rewardsGiven, 'promoInfo'=> $promoInfo,'totalCount' => $totalCount, 'entry' => $entry, 'token' => $token, 'all' => $all], function ($m) use ($entry, $totalCount) {
			    $m->from('info@alabangtowncenter.ph', 'Concierge - Alabang Town Center');
			    $m->to($entry[0]->email, "Concierge")->subject('[Concierge] - Coupon Details');
			});
			if(count(\Mail::failures()) > 0){
				return false;
			}
			return true;
		}catch(\Exception $e){
			return false;
		}
	}

	public function sendRewardsInfo($userid = "", $token = "", $promoId = ""){
		$rewardsGiven = [];
		$rewardsGiven = RewardGiven::selectRaw('atc_rewards.name as reward, atc_rewards_given.*,atc_users.*')
				->leftJoin('atc_rewards', 'atc_rewards_given.rewards_id', '=', 'atc_rewards.id')
				->leftJoin('atc_users','atc_rewards_given.userid', '=', 'atc_users.id');

		if($userid != ""){
			$rewardsGiven = $rewardsGiven->where('atc_rewards_given.user_id', $userid);
			$all = "all";
		}

		if($token != ""){
			$rewardsGiven = $rewardsGiven->where('atc_rewards_given.batch_id', $token);
			$all = "";
		}

		if($promoId != ""){
			$rewardsGiven = $rewardsGiven->where('atc_rewards_given.promo_id', $promoId);
		}

		$rewardsGiven = $rewardsGiven
			->orderBy('atc_rewards_given.id', 'desc')
			->get();
		if(count($rewardsGiven) == 0){
			return false;
		}

		$totalCount =  RewardGiven::with(['promo' => function($query){
				$query->where('date_started', '<=' , date('Y-m-d H:i:s'))
				->where('date_ended', '>=', date('Y-m-d H:i:s'));
			}])
			->where('atc_rewards_given.userid', $rewardsGiven[0]->userid);
		if($promoId != "" || $rewardsGiven[0]->promo_id != ""){
			if($promoId == ""){
				$promoId = $rewardsGiven[0]->promo_id;
			}
			$totalCount = $totalCount->where('promo_id', $promoId);
		}
		$totalCount = $totalCount->get()->count();

		$promoInfo    = array();
		$promoInfo    = Promo::where('id', $rewardsGiven[0]->promo_id)->first();
		try{
			\Mail::send(['html'=>'admin.email.send-rewards-info'], ['rewardsGiven'=> $rewardsGiven, 'promoInfo'=> $promoInfo,'totalCount' => $totalCount, 'token' => $token, 'all' => $all], function ($m) use ($rewardsGiven, $totalCount) {
			    $m->from('info@alabangtowncenter.ph', 'Concierge - Alabang Town Center');
			    $m->to($rewardsGiven[0]->email, "Concierge")->subject('[Concierge] - Reward Details');
			});
			if(count(\Mail::failures()) > 0){
				return false;
			}
			return true;
		}catch(\Exception $e){
			return false;
		}

	}
	private function generateUniqueId($id = 1, $length = 8){
	    	$unique = $id . uniqid() . strtotime(date('Y-m-d H:i:s:')) . "Htech2k17!";
	    	$unique = md5($unique);
	    	$unique = hash('sha256', $unique);
	    	return  strtoupper(substr($unique, 0, $length));
	}

	public function checkEntries(){
		return view('admin.pages.entries.check-entries');
	}

	public function postCheckEntries(Request $request){
		$rules = [
		    'email'       => 'required|email'
		];

		$validator = Validator::make($request->all(), $rules);

		if ($validator->fails()) {
		    $this->throwValidationException(
		        $request, $validator
		    );
		}

		$user = User::where('email', Input::get('email'))->where('status', 1)->first();
		if(count($user) == 0){
			Session::flash('msg_danger', "Sorry, we can't find an account with the given email address, ". Input::get('email') .". Please try again with a different email address.");
		}else{
			$emailSent = $this->sendEmail($user->id, "", $request->promo);
			if($emailSent === true){
				Session::flash('msg_success', 'The details of your entries were sent to your email address. Please check your email.');
			}else{
				Session::flash('msg_warning', 'We\'ve found your account but there was a problem sending an email at this moment. Please try again later.');
			}
		}

		return Redirect::back();
	}
}
