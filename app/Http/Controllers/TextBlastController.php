<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Services\TextBlast;
use App\Promo;
use App\User;
use App\Activities;
use Session;
use DB;

class TextBlastController extends Controller {
    protected $textBlaster;

    public function __construct()
    {
        $this->textBlaster = new TextBlast();
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
        $promos = Promo::all();

		return view('admin.pages.text-blast', [
            'promos' => $promos,
            'selected_promo' => $request->selected_promo,
        ]);
	}

    public function getRecipients(Request $request)
    {
        $recipients = User::where('status', 1)
            ->with('cards');
        $consolidated_list = DB::table('consolidated_masterlist');
        

        // Get members with BPI Card
        if ($request->bpi_member && $request->vip_member == false) {
            $recipients = $recipients->whereHas('cards', function($query) {
                $query->where('card_type', 'bpi');
            });
            
            $consolidated_list = $consolidated_list->where('amore','<>','');
        } else if ($request->bpi_member == false && $request->vip_member) {
            $recipients = $recipients->whereHas('cards', function($query) {
                $query->where('card_type', 'vip');
            });
            
            $consolidated_list = $consolidated_list->where('vipinoy','<>','');
        } else if($request->bpi_member && $request->vip_member) {
            $recipients = $recipients->has('cards');
            
            $consolidated_list = $consolidated_list->where('vipinoy','<>','')
            	->orWhere('amore','<>','');
        } 

       	$recipients = $recipients->where('contact','!=','00000000000')
            ->whereRaw('length(contact) = 11')
            ->whereRaw("contact REGEXP '^[0-9]+$'")
            ->lists('contact');
        $consolidated_list = $consolidated_list->where('mobile','!=','00000000000')
            ->whereRaw('length(mobile) = 11')
            ->whereRaw("mobile REGEXP '^[0-9]+$'")
        ->whereNotIn('mobile',$recipients)->lists('mobile');
        $all_recipients = array_merge($recipients,$consolidated_list);

        if ($request->ajax()) {        	
            return json_encode($all_recipients);
            // return json_encode($recipients);
        } else {
            return $recipients = $recipients->get();
        }
    }

    public function getCredentials()
    {
        $this->textBlaster->getCredentials();
        return json_encode($this->textBlaster->credentials);
    }

    public function saveActivityEntry(Request $request)
    {
        return json_encode(Activities::textBlastSent($request->message));
    }

    public function send(Request $request)
    {
        if ($request->ajax()) {
            return json_encode($this->textBlaster->sendSmsAjax($request->sessionId, trim($request->recipient), $request->message));
        } else {
            try {
                $recipients = $this->getRecipients($request);

                foreach($recipients as $recipient) {
                    $this->textBlaster->sendSms(trim($recipient->contact), $request->message);
                    Activities::textBlastSent($request->message, $recipient->contact);
                }
                // Activities::textBlastSent($message, $responsse->success);

                Session::flash('message', 'Messages sent to ' . count($recipients) . ' recipients!');
                Session::flash("messageClass", "alert-success");
            } catch(Exception $err) {
                Session::flash("message", "There was a problem with your request. Please try again later.");
                Session::flash("messageClass", "alert-danger");
            }

            return redirect()->back();
        }
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($promoId)
	{
        $promos = Promo::all();

		return view('admin.pages.text-blast', [
            'promos' => $promos,
            'selected_promo' => $promoId,
        ]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
