<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\SpecialPromoRequest;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Entry;
use App\User;
use App\Promo;
use App\Stores;
use App\RewardGiven;
use App\Reward;
use Response;
use Input;
use Auth;
use Session;
use Redirect;
use Validator;
use DB;
use Excel;

class SpecialPromoController extends Controller {

	public function index()
	{
		$promo = Promo::where('name', 'LIKE', '%Earn It%')
				->where('status', 1)
				->where('date_started', '<=' , date('Y-m-d'))
				->where('date_ended', '>=', date('Y-m-d'))->first();
		$promo_id = isset($promo->id) ? $promo->id : 0;
		$list = DB::select("SELECT
								u.id as userid,
								CONCAT(first_name , ' ' , u.middle_name,' ' , u.last_name) as name,
							    u.address, u.email,u.contact,
							    amount,
							    a2.created_at
							FROM
							    atc_entries a2
							LEFT JOIN
								atc_users u ON a2.user_id = u.id
							WHERE a2.status = 1
							AND a2.promo_id = " . $promo_id
							." AND u.usertype = 1
							ORDER BY name asc");
		/*
			(SELECT
							            COUNT(unique_id)
							        FROM
							            atc_entries a1
							        WHERE
							            a1.receipt_no = a2.receipt_no
							            AND a1.status = 1
							            AND a1.promo_id = " . $promo_id . ") AS entry_count,
		 */
		$user = [];
		$total = [];
		$coupons = [];
		// dd($list);
		foreach($list as $item){
			$user[$item->userid] = $item;
			if(!isset($total[$item->userid])) $total[$item->userid] = 0;
			$total[$item->userid] = $total[$item->userid] + $item->amount;
			if(!isset($coupons[$item->userid])) $coupons[$item->userid] = 0;
			// $coupons[$item->userid] = $coupons[$item->userid] + $item->entry_count;
			$coupons[$item->userid] = 0;
		}
		$download = Input::get('download',false);
		if($download)
		Excel::create('List of Spenders', function($excel) use($list, $user, $total, $coupons, $download){
			$excel->sheet('List of Spenders - Processed', function($sheet) use($list, $user, $total, $coupons, $download){
			$sheet->setShowGridlines(true);
			$sheet->loadView('admin.pages.specialpromo.list-of-spenders-excel', compact('list', 'user', 'total', 'coupons','promo', 'download'));
			});
			$excel->sheet('List of Spenders - Expanded', function($sheet) use($list, $user, $total, $coupons, $download){
			$sheet->setShowGridlines(true);
			$sheet->loadView('admin.pages.specialpromo.list-of-spenders-excel-full', compact('list', 'user', 'total', 'coupons','promo', 'download'));
			});
		})->export('xlsx');
		return view('admin.pages.specialpromo.spenders-list', compact('list', 'user', 'total', 'coupons','promo'));
	}

	public function create()
	{
		$users = User::where('status',1)
			->where('usertype',1)
			->get();

		$token    = Input::get('tk', '');
		if($token != ""){
			$entry 	  =  Entry::selectRaw('atc_entries.token,SUM(atc_entries.amount) as total_amount,atc_entries.user_id, atc_users.first_name, atc_users.last_name, atc_users.middle_name, atc_users.email, atc_users.address,atc_users.dob,atc_users.contact')
						->leftJoin('atc_users','atc_entries.user_id', '=', 'atc_users.id')
						->where('atc_entries.status', 1)
						->where('atc_entries.token', $token)
						->where('atc_users.usertype', 1)
						->orderBy('atc_entries.created_at', 'desc')
						->get();
			$receiptCount = Entry::where('status',1)
				->where('token',$token)
				->count();

		}else{
			$receiptCount = 0;
			$entry = array();
		}
		$stores       = new Stores();
		$storeListing = $stores->getStores();
		$promo = Promo::where('name', 'LIKE', '%Earn It%')
				->where('status', 1)
				->where('date_started', '<=' , date('Y-m-d'))
				->where('date_ended', '>=', date('Y-m-d'))->first();

		return view('admin.pages.specialpromo.entries',compact('users', 'entry', 'token', 'promo','storeListing','receiptCount'));
	}
	public function show($id){
		$promo = Promo::find(47) != null ? Promo::find(47) : new Promo;
		return view('admin.pages.specialpromo.landing',compact('promo'));
	}
	public function store(SpecialPromoRequest $request)
	{
		if(strtok($request->server('HTTP_REFERER'),'?') !== url('res/specialpromo/create')){
			exit();
		}
        	$stores       = new Stores();
        	$storeListing = $stores->getStores();

		$couponIds = array();
		$customerId   = $request->cusid;
		$noOfRows     = $request->rows;
		$store        = $request->store;
		$promo        = $request->promo;
		$invoice      = $request->invoice;
		$amount       = $request->amount;
		$fullname	  = $request->first_name.' '.$request->last_name;
		$token 		  = $this->generateUniqueId(uniqid(), 13);
		$customerInfo = $request->only('first_name','middle_name','last_name','dob','email','address','contact','card_type','cusid');
		/*Register User if not yet existed*/
		if($customerId == ""){
			$checkExist = User::where('first_name',$request->first_name)
							->where('last_name',$request->last_name)
							->where('contact',$request->contact);
			if($checkExist->exists()){
				if($checkExist->first()->status == 0){
					$existingUser = User::find($checkExist->first()->id);
					$existingUser->status = 1;
					$existingUser->save();
				}
				$customerId = $checkExist->first()->id;
			}else{
				$user = User::addMemeber($request);
				$customerId = $user->id;
			}
		}else{
			$user = User::find($customerId);
        	$user->updateUser($request);

		}

		
		/*Insert Entries*/
		$tempTotal = 0;
		for ($r=0; $r < $noOfRows ; $r++) {
			$categoryFetched = isset($store[$r]) ? $stores->getCurrentCategory($store[$r]) : "";
			if($categoryFetched === null || $categoryFetched == ""){
				$categoryFetched = "-";
			}

			$entry = new Entry;
			$entry->user_id 	= $customerId;
			$entry->promo_id 	= $promo;
			$entry->category 	= $categoryFetched;
			$entry->store_name 	= isset($store[$r]) ? $store[$r] : '';
			$entry->receipt_no 	= isset($invoice[$r]) ? $invoice[$r] : '';
			$entry->amount 		= isset($amount[$r]) ? $amount[$r] : '';
			$entry->status 		= 1;
			$entry->token 		= $token;
			$entry->ipaddress	= $_SERVER['REMOTE_ADDR'];
			$entry->browser		= $_SERVER['HTTP_USER_AGENT'];
			$entry->save();
			$entry->unique_id = $this->generateUniqueId($entry->id);
			$entry->save();
			$couponIds[] = $entry->unique_id;
		}
		

		$forPrint = array(
			"customerId" => $customerId,
			"noOfRows"	 => $noOfRows,
			"store"	 => $store,
			"invoice"	 => $invoice,
			"couponIds"	 => $couponIds,
			"token"	 => $token
		);
		Session::forget('for-print');
		Session::put('for-print', $forPrint);

		$emailSent = $this->emailSent("", $token,$customerId);
		$emailSentInt = 0;
		
		if($emailSent === true){
			$emailSentInt = 1;
			Session::flash('msg_success', 'Entries inserted successfully!');
		}else{
			Session::flash('msg_warning', 'Entries inserted successfully, but there was a problem sending an email to the user.');
		}
		$entrySent = Entry::where('token', $token)->update(['email_sent'=>$emailSentInt]);
		
		ActivityController::logActivity(Auth::user()->username . ' submitted a total amount of  '. array_sum($request->amount) .' for customer no. '.$customerId.', ', Auth::user()->id); 

    	return Redirect::to('res/specialpromo/create?success=true&tk=' . $token);
	}

	public function showAjax($userId)
	{
        $entries = Entry::with('user')
            ->where('user_id', $userId)
            ->where('status', 1)
            ->get();

        return Response::json($entries);
	}

	public function edit($id)
	{
		//
	}

	public function update($id)
	{
		//
	}

	public function destroy($id)
	{
		
	}
	public function printEntries(){
		$token    = Input::get('tk', '');
		$entry 	  =  Entry::selectRaw('atc_entries.promo_id,atc_entries.token,SUM(atc_entries.amount) as total_amount,atc_entries.user_id, atc_users.first_name, atc_users.last_name, atc_users.middle_name, atc_users.email, atc_users.address,atc_users.dob,atc_users.contact')
						->leftJoin('atc_users','atc_entries.user_id', '=', 'atc_users.id')
						->where('atc_entries.token', $token)
						->where('atc_entries.status', 1)
						->where('atc_users.usertype', 1)
						->get();
		$promoInfo  = Promo::where('id', $entry[0]->promo_id)
				->where('status', 1)
				->where('date_started', '<=' , date('Y-m-d'))
				->where('date_ended', '>=', date('Y-m-d'))->first();

		
		$receiptCount = Entry::where('status',1)
				->where('atc_entries.token', $token)
				->count();
		$overAll = 0;

		$customer   = $entry->first()->first_name.' '.$entry->first()->last_name;
		$amount = Entry::where('status',1)
			->where('promo_id', $entry[0]->promo_id)
			->where('user_id',$entry[0]->user_id)
			->get();
		$overAll = $amount->sum('amount');


		// dd($entry,$receiptCount,$overAll);
		ActivityController::logActivity(Auth::user()->username . ' printed coupons for '. $customer, Auth::user()->id);
		
		return view('admin.pages.specialpromo.rewards', compact('entry','token','overAll', 'receiptCount'));
	}


	public function emailSent($userid = "", $token = "",$cusId = ""){
		$entry 	  =  Entry::selectRaw('atc_entries.token,atc_entries.token,SUM(atc_entries.amount) as total_amount,atc_entries.user_id, atc_users.first_name, atc_users.last_name, atc_users.middle_name, atc_users.email, atc_users.address,atc_users.dob,atc_users.contact')
						->leftJoin('atc_users','atc_entries.user_id', '=', 'atc_users.id')
						->where('atc_entries.status', 1)
						->where('atc_users.usertype', 1);
		$receiptCount = 0;
		$overAll = 0;
		$list = new Entry;

		if($userid != ""){
			$list = Entry::where('status',1)
				->where('user_id',$userid)
				->where('promo_id',0)
				->where('status',1);
			$receiptCount = $list->count();
			$list = $list->get();
			$overAll = $list->sum('amount');
			$all = "all";
			$entry = $entry->where('atc_entries.user_id', $userid);
		}

		if($token != ""){
			$entry = $entry->where('atc_entries.token', $token);
			$receiptCount = Entry::where('status',1)
				->where('token',$token)
				->count();
			$all = "";
		}

		if($cusId != ""){
			$amount = Entry::where('status',1)
				->where('promo_id', 0)
				->where('user_id',$cusId)
				->get();
			$overAll = $amount->sum('amount');
		}

		$entry = $entry->orderBy('atc_entries.id', 'desc')->get();
		if(count($entry) == 0){
			return false;
		}
		try{
			\Mail::send(['html'=>'admin.pages.specialpromo.send-email'], ['receiptCount' => $receiptCount, 'entry' => $entry, 'token' => $token,'all' => $all,'overAll' => $overAll], function ($m) use ($entry, $receiptCount) {
			    $m->from('info@alabangtowncenter.ph', 'Concierge - Alabang Town Center');
			    $m->to($entry[0]->email, "Concierge")->subject('[Concierge] - Entry Details');
			});
			if(count(\Mail::failures()) > 0){
				return false;
			}
			return true;
		}catch(\Exception $e){
			dd($e);
			return false;
		}
	}

	public function showHighestBidder()
	{
		$promo = Promo::where('name', 'LIKE', '%Earn It%')
				->where('status', 1)
				->where('date_started', '<=' , date('Y-m-d'))
				->where('date_ended', '>=', date('Y-m-d'))->first();
		$promo_id = isset($promo->id) ? $promo->id : 0;
		$list = DB::select("SELECT
								u.id as userid,
								CONCAT(first_name , ' ' , u.middle_name,' ' , u.last_name) as name,
							    u.dob,u.address, u.email,u.contact,
							    amount,
							    a2.created_at
							FROM
							    atc_entries a2
							LEFT JOIN
								atc_users u ON a2.user_id = u.id
							WHERE a2.status = 1
							AND a2.promo_id = " . $promo_id
							." AND u.usertype = 1
							ORDER BY created_at desc");
		/*
			 (SELECT
							            COUNT(unique_id)
							        FROM
							            atc_entries a1
							        WHERE
							            a1.receipt_no = a2.receipt_no
							            AND a1.status = 1) AS entry_count,
		 */
		$user = [];
		$total = [];
		$coupons = [];
		foreach($list as $item){
			$user[$item->userid] = $item;
			if(!isset($total[$item->userid])) $total[$item->userid] = 0;
			$total[$item->userid] = $total[$item->userid] + $item->amount;
			if(!isset($coupons[$item->userid])) $coupons[$item->userid] = 0;
			// $coupons[$item->userid] = $coupons[$item->userid] + $item->entry_count;
			$coupons[$item->userid] = 0;
		}
		$spender = [];
        if(!empty($user)){
            $highestAmnt = 0;
            foreach ($user as $key => $u) {
                if($total[$u->userid] >= $highestAmnt){
                    $highestAmnt = $total[$u->userid];
                    $spender = $u;
                    $spender->total_amount = $total[$spender->userid];   
                }    
            }   
        }
        $download = Input::get('download',false);
        if($download){
        	// dd($spender);
        	Excel::create('Earn It Winner!', function($excel) use($spender, $download){
				$excel->sheet('List of Winners', function($sheet) use($spender, $download){
				$sheet->setShowGridlines(true);
				$sheet->cells('A1', function($cells) {
					$cells->setFont(array(
	                    'family'     => 'Calibri',
	                    'size'       => '19',
	                    'bold'       =>  true
	                  ));
				});
				$sheet->cells('B1:B7', function($cells) {
					$cells->setFont(array(
	                    'bold'       =>  true
	                  ));
				});
            	$sheet->mergeCells('A1:B1');
							$sheet->loadView('admin.pages.specialpromo.winner-excel', compact('spender', 'download'));
				});
				})->export('xlsx');
        }
		return view('admin.pages.specialpromo.highestbidder', compact('list', 'user', 'total', 'coupons','promo','spender'));
	}

	public function postCheckEntries(Request $request){
		$rules = [
		    'email'       => 'required|email'
		];

		$validator = Validator::make($request->all(), $rules);

		if ($validator->fails()) {
		    $this->throwValidationException(
		        $request, $validator
		    );
		}

		$user = User::where('email', Input::get('email'))->where('status', 1)->first();
		if(count($user) == 0){
			Session::flash('msg_danger', "Sorry, we can't find an account with the given email address, ". Input::get('email') .". Please try again with a different email address.");
		}else{
			$emailSent = $this->emailSent($user->id);
			if($emailSent === true){
				Session::flash('msg_success', 'The details of your entries were sent to your email address. Please check your email.');
			}else{
				Session::flash('msg_warning', 'We\'ve found your account but there was a problem sending an email at this moment. Please try again later.');
			}
		}

		return Redirect::back(); 
	}

	private function generateUniqueId($id = 1, $length = 8){
	    	$unique = $id . uniqid() . strtotime(date('Y-m-d H:i:s:')) . "Htech2k17!";
	    	$unique = md5($unique);
	    	$unique = hash('sha256', $unique);
	    	return  strtoupper(substr($unique, 0, $length));
	}

}

