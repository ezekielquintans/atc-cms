<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\RewardsRequest;
use App\Reward;
use Input;
use Session;
use Redirect;

class RewardController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$rewardList = Reward::with('promo');

	    	if (Input::get('search', '') != '') {
	    		$rewardList = $rewardList->where('name', 'LIKE', '%' . Input::get('search') . '%');
	    	}

	    	$rewardList = $rewardList->where('status', '<>', 0)
				->orderBy('created_at','desc')
				->paginate(10)
	            	->setPath('rewards')
	            	->appends(Input::except('page'));

	    	return view('admin.pages.rewards.rewards-table', compact('rewardList'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$process = "ADD";
	    	return view('admin.pages.rewards.update-rewards', compact('process'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(RewardsRequest $request)
	{
		$status = Reward::updateOrCreate(['id' => 0], $request->except('advance_settings','_token','featured_img'));
		if($request->hasFile('featured_img')){
		    $destination 			= 'public/landing/rewards_img';
		    $featured_img 			= $request->file('featured_img');
		    $filename_img 			= uniqid('reward_').'.'.$featured_img->getClientOriginalExtension();
		    $upload_featured_img 	= $featured_img->move($destination, $filename_img);
		    $status->featured_img	= $destination.'/'.$filename_img;
		    $status->save();
		}
		Session::flash('msg_success', 'Reward has been successfully created.');
		return Redirect::to('res/rewards');
		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$process = "EDIT";
		$rewardInfo = Reward::find($id);
	    	return view('admin.pages.rewards.update-rewards', compact('process','rewardInfo'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(RewardsRequest $request,$id)
	{
		if(Reward::updateOrCreate(['id' => $id], $request->except('advance_settings','_token'))){
			Session::flash('msg_success', 'Reward has been successfully updated.');
			return Redirect::to('res/rewards');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$rewardInfo = Reward::find($id);
		$rewardInfo->status = 0;
		$rewardInfo->save();
		ActivityController::logActivity($rewardInfo->name . '  was deleted', auth()->user()->id);
		Session::flash('msg_success', 'Deleted successfully!.');
		return Redirect::to('res/rewards');
	}
}
