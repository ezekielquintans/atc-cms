<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Auth;
use Session;
use DateTime;
use App\UserCard;
use App\Activities;
use DB;
class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'atc_users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password','usertype','address','dob','contact','status'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

    const USER_TYPE_ADMIN = 3;
    const USER_TYPE_CONCIERGE = 2;
    const USER_TYPE_MEMBER = 1;

	public function isAdmin()
    {
       return $this->usertype == 3;
    }

    public function isConcierge()
    {
       return $this->usertype == 3 ||  $this->usertype == 2;
    }

    public function userType()
    {
        return $this->usertype;
    }

    public function entries()
    {
        return $this->hasMany('App\Entry');
    }

    public function cards()
    {
        return $this->hasMany('App\UserCard');
    }

    public function rewards()
    {
        return $this->hasMany('App\RewardGiven', 'userid', 'id');
    }

    public static function getUserTypes()
    {
        return [
            self::USER_TYPE_ADMIN => 'Admin',
            self::USER_TYPE_CONCIERGE => 'Concierge',
            self::USER_TYPE_MEMBER => 'Member',
        ];
    }

    /*
    * @param int $status This would be the new status to set for the user
    */
    public function setStatus($status)
    {
        try {
            $this->status = 0;
            $this->save();

            Session::flash("message", "$this->last_name, $this->first_name has been deleted.");
            Session::flash("messageClass", "alert-success");
        } catch(Exception $err) {
            Session::flash("message", "There was a problem with your request. Please try again later.");
            Session::flash("messageClass", "alert-danger");
        }
    }

    public function addUser($request) {

    }

    public static function addMemeber($request) {
        try {
            $username  = strtolower($request->first_name . "." . $request->last_name);
            $unique_id = strtoupper(uniqid());
            User::insert([
                'username'     => $username,
                'first_name'   => $request->first_name,
                'last_name'    => $request->last_name,
                'middle_name'  => $request->middle_name,
                'unique_id'    => $unique_id,
                'usertype'     => isset($request->userType) ? $request->userType : User::USER_TYPE_MEMBER,
                'dob'          => $request->dob,
                'address'      => $request->address,
                'contact'      => $request->contact,
                'email'        => $request->email,
                'status'       => 1,
                'password'     => bcrypt($username)
            ]);
            $user = User::firstOrNew(['unique_id' => $unique_id],['email' => $request->email]);
            
            $cards = $user->cards()->get();

            if (isset($request['card_type'])) {
                foreach($request->card_type as $cardType => $card) {
                    UserCard::addOrUpdate($user, $cardType, $request);
                }
            }

            Session::flash("message", "Member was successfully added!");
            Session::flash("messageClass", "alert-success");
            return $user;
        } catch(Exception $err) {
            Session::flash("message", "There was a problem with your request. Please try again later.");
            Session::flash("messageClass", "alert-danger");
        }
    }

    public function updateUser($request)
    {
        try {
            $this->first_name   = $request->first_name;
            $this->middle_name  = $request->middle_name;
            $this->last_name    = $request->last_name;
            $this->dob          = $request->dob;
            $this->email        = $request->email;
            $this->contact      = $request->contact;
            $this->address      = $request->address;

            $this->save();

            $cards = $this->cards()->get();

            if (isset($request['card_type'])) {
                foreach($request->card_type as $cardType => $card) {
                    UserCard::addOrUpdate($this, $cardType, $request);
                }

                foreach($cards as $card) {
                    if(in_array($card->card_type, array_keys($request['card_type'])) === false) {
                        $card->delete();
                    }
                }
            }

            if(empty($cards)) {
                foreach($cards as $card) {
                    $card->delete();
                }
            }

            Session::flash("message", "Member's information was successfully updated!");
            Session::flash("messageClass", "alert-success");
        } catch(Exception $err) {
            Session::flash("message", "There was a problem with your request. Please try again later.");
            Session::flash("messageClass", "alert-danger");
        }
    }

    public static function searchUser($userQuery, $searchQuery)
    {
        $bpiMatch;
        $vipMatch;
        $createdMatch;
        $createdMonthMatch;
        preg_match('(^amore$)', strtolower($searchQuery), $bpiMatch, 0);
        preg_match('(^vipinoy$)', strtolower($searchQuery), $vipMatch, 0);
        preg_match('(^created:)', strtolower($searchQuery), $createdMatch, 0);
        preg_match('(^created month:)',  strtolower($searchQuery), $createdMonthMatch, 0);

        $colonIndex = strpos($searchQuery, ':') + 1;

        if(empty($bpiMatch) == false) {
            $userQuery = $userQuery->whereHas('cards', function($query) {
                $query->where('card_type', 'bpi');
            });
        } else if(empty($vipMatch) == false) {
            $userQuery = $userQuery->whereHas('cards', function($query) {
                $query->where('card_type', 'vip');
            });
        } else if(empty($createdMatch) == false) {
            $trimmedSearch = substr($searchQuery, $colonIndex);
            if ($trimmedSearch != false) {
                if(strtotime($trimmedSearch) === false) {
                    return User::where('id', 0); // Return empty collection due to error of input
                }

                $generatedDate = new DateTime($trimmedSearch);
                $generatedDate = $generatedDate->format('Y-m-d');

                $userQuery = $userQuery->where(function($query) use($generatedDate) {
                    $query->where('created_at', 'LIKE', "%$generatedDate%");
                });
            }
        } else if(empty($createdMonthMatch) == false) {
            $trimmedSearch = substr($searchQuery, $colonIndex);
            if ($trimmedSearch != false) {
                if(strtotime($trimmedSearch) === false) {
                    return User::where('id', 0); // Return empty collection due to error of input
                }

                $generatedMonth = new DateTime($trimmedSearch);
                $generatedMonth = $generatedMonth->format('-m-');

                $userQuery = $userQuery->where(function($query) use($generatedMonth) {
                    $query->where('created_at', 'LIKE', "%$generatedMonth%");
                });
            }
        } else {
            $sq = explode(" ",$searchQuery);
            foreach ($sq as $search) {
                 $userQuery->where(function($query) use($search) {
                        $query->where('first_name', 'LIKE', "%$search%")
                        ->orWhere('middle_name', 'LIKE', "%$search%")
                        ->orWhere('last_name', 'LIKE', "%$search%")
                        ->orWhere('email', 'LIKE', "%$search%")
                        ->orWhere('id', 'LIKE', '%'.ltrim($search).'%');
                    });
            }
        }

        return $userQuery;
    }
}
