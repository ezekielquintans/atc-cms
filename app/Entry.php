<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Entry extends Model {

	protected $table = 'atc_entries';
	protected $fillable = ['entry_id','user_id','category','store_name','receipt_no','amount'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
