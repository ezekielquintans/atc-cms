<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserCard extends Model {
    use SoftDeletes;
	protected $table = "atc_users_card";
	protected $guarded = ["id"];
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'user_id',
        'beep_paywave',
        'new_renewal',
        'card_title',
        'card_type',
        'source_of_income',
        'name_of_ofw',
        'name_in_card',
        'relationship',
        'document_presented',
        'document_no',
        'birthday',
        'card_no',
        'card_expiry',
        'amount',
        'ar_no',
        'remarks',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function scopeBpi($query)
    {
        return $query->where('card_type', 'bpi');
    }

    public function scopeVip($query)
    {
        return $query->where('card_type', 'vip');
    }

    public static function addOrUpdate(User $user, $cardType, $request)
    {
        $data = [
            'card_type'         => $cardType,
            'new_renewal'       => $request["new_renewal_$cardType"],
            'card_no'           => $request["card_no_$cardType"],
            'card_expiry'       => $request["card_expiry_$cardType"],
            'card_title'        => isset($request["card_title_$cardType"]) ? $request["card_expiry_$cardType"] : null,
            'birthday'          => $request["birthday_$cardType"],
            'amount'            => $request["amount_$cardType"],
            'ar_no'             => $request["ar_no_$cardType" ],
            'remarks'           => $request["remarks_$cardType"],
        ];

        if($cardType == 'bpi') {
            $data['name_in_card']       = isset($request['name_in_card'])? $request['name_in_card'] : null;
            $data['beep_paywave']       = isset($request['beep_paywave'])? $request['beep_paywave'] : null;
            $data['source_of_income']   = isset($request['source_of_income'])? $request['source_of_income'] : null;
        } else if($cardType == 'vip')
        {
            $data['name_of_ofw']        = isset($request['name_of_ofw'])? $request['name_of_ofw'] : null;
            $data['relationship']       = isset($request['relationship'])? $request['relationship'] : null;
            $data['document_presented'] = isset($request['document_presented'])? $request['document_presented'] : null;
            $data['document_no']        = isset($request['document_no'])? $request['document_no'] : null;
        }
        $card = UserCard::updateOrCreate(['user_id' => $user->id, 'card_type' => $cardType], $data);

        $user->cards()->save($card);
    }
}
