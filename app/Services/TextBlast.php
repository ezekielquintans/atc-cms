<?php namespace App\Services;

use Session;
use stdClass;
use SoapClient;
use App\Activities;

class TextBlast
{
    public $credentials;
    public $client;

    const ACCT_PIXSEL = 0;
    const ACCT_HTECH = 1;
    // Merchant Credentials
    const WEBSERVICE_URL = 'http://pixsell.com.ph/pxws/Service.asmx?wsdl';

    // Sender Details
    const AREA_CODE = '63';

    // PixSell Users
    private $pixSellAccounts = [];
    private $accountSenderNames = [];
    private $accountUsed = self::ACCT_HTECH;

    public function __construct()
    {
        $this->client = new SoapClient('http://pixsell.com.ph/pxws/Service.asmx?wsdl');

        $this->pixSellAccounts[] = new PxClientAuthentication('7',     '247',      'MPORTAL',  'TEST',     '12345');
        $this->accountSenderNames[] = 'Pixsell';
       
        $this->pixSellAccounts[] = new PxClientAuthentication('121',   '40271',    'HTECH',    'ADX116',   'B436RZ');
        $this->accountSenderNames[] = 'ATC';
        
        // $this->pixSellAccounts[] = new PxClientAuthentication('144',   '46160',    'HTECH1',    'HJSD88',   'SMA0ZX');
        // $this->accountSenderNames[] = 'ATC';
    }
    public function __deconstruct()
    {
        $this->client = null;
       
        $this->pixSellAccounts[] = null;
        $this->accountSenderNames[] = null;
    }
    public function getCredentials()
    {
        $response = $this->send("PxClientAuthentication", $this->pixSellAccounts[$this->accountUsed]);
        $this->credentials = $response->PxClientAuthenticationResult;
        return $response->PxClientAuthenticationResult;
    }

    public function send($command, $data)
    {
        return $this->client->__soapCall($command, ['parameters' => $data]);
    }

    private function getFiles($url)
    {
        return file_get_contents($url);
    }

    public function sendSms($mobileNumber, $message)
    {
        // dd($mobileNumber);
        try{
            $status;
            for ($m=0; $m < count($message); $m++) { 
                $msg = $message[$m];

                if(count($message) > 1){
                    $msg = "(" . ($m+1) . "/" . count($message) . ") " . $message[$m];
                }
                $mobileNumber = $this->fixMobileNumber($mobileNumber);
                $merchantRefNo = substr("HTECH" . hash('sha256', uniqid()), 0, 50);
                if (strlen($msg) <= 160) {
                    $data                = new PxSendSMS();
                    $data->sessionID     = $this->credentials->session_Id;
                    $data->merchantRefNo = (string)$merchantRefNo;
                    $data->sendername    = $this->accountSenderNames[$this->accountUsed];
                    $data->areaCode      = self::AREA_CODE;
                    $data->mobileNumber  = $mobileNumber;
                    $data->smsMessage    = $msg;
                    $data->isFlash       = 'FALSE';

                    $response          = new stdClass();
                    $response->data    = $this->send("PxSendSMS", $data)->PxSendSMSResult;
                    $response->success = $response->data->statusCode == 20 ? true : false;

                    $status = $response;
                } else {
                    return "Message exceeds 160 character limit.";
                }
            }
            return $status;
        } catch( \Exception $e){
            return "Failed to send message";
        }
    }

    public function sendSmsAjax($session_Id, $mobileNumber, $message)
    {
        try{
            $status;
            for ($m = 0; $m < count($message); $m++) { 
                $msg = $message[$m];

                if(count($message) > 1) {
                    $msg = "(" . ($m+1) . "/" . count($message) . ") " . $message[$m];
                }

                $mobileNumber = $this->fixMobileNumber($mobileNumber);
                // dd($mobileNumber,$msg);
                $merchantRefNo = substr(hash('sha256', uniqid()), 0, 50);

                if (strlen($msg) <= 160) {
                    $data = new PxSendSMS();
                    $data->sessionID     = $session_Id;
                    $data->merchantRefNo = (string)$merchantRefNo;
                    $data->sendername    = $this->accountSenderNames[$this->accountUsed];
                    $data->areaCode      = self::AREA_CODE;
                    $data->mobileNumber  = $mobileNumber;
                    $data->smsMessage    = $msg;
                    $data->isFlash       = 'FALSE';

                    $response          = new stdClass();
                    $response->data    = $this->send("PxSendSMS", $data)->PxSendSMSResult;
                    $response->success = $response->data->statusCode == 20 ? true : false;
                    Activities::textBlastSent($msg, $mobileNumber);

                    $status = $response;
                } else {
                    return "Message exceeds 160 character limit.";
                }
            }

            return $status;
        } catch( \Exception $e){
            return "Failed to send message";
        }
    }

    private function fixMobileNumber($mobileNumber) {
        $patterns = [
            '/^(\+63)/',
            '/^(63)/',
            '/^(0)/',
        ];
        $replacements = ['', '', '', ''];

        return preg_replace($patterns, $replacements, $mobileNumber);
    }

    public function verifySmsRequestStatus($merchantRefNo)
    {
        $data = new stdClass();
        $data->sessionID = $this->getCredentials()->session_Id;

        $this->client->__soapCall("PxSendSMS", ['parameters' => $data]);
    }
}

class PxClientAuthentication {
    public $merchantID;
    public $membersID;
    public $merchantUserName;
    public $merchantPassword;
    public $ePin;

    public function __construct($merchId = null, $membersId = null, $username = null, $password = null, $ePin = null)
    {
        $this->merchantID = $merchId;
        $this->membersID = $membersId;
        $this->merchantUserName = $username;
        $this->merchantPassword = $password;
        $this->ePin = $ePin;
    }
    public function __deconstruct($merchId = null, $membersId = null, $username = null, $password = null, $ePin = null)
    {
        $this->merchantID = null;
        $this->membersID = null;
        $this->merchantUserName = null;
        $this->merchantPassword = null;
        $this->ePin = null;
    }
}

class PxSendSMS {
    public $sessionID;
    public $merchantRefNo;
    public $sendername;
    public $areaCode;
    public $mobileNumber;
    public $smsMessage;
    public $isFlash;
}
