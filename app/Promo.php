<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests\PromoRequest;

class Promo extends Model {

	protected $table = 'atc_promos';
	protected $fillable = ['name','subtitle',' description',' image_url',' helper',' dti_permit',' receipt_condition',' purchase_requirement',' multiplier',' ceiling',' type',' date_started',' date_ended',' status'];
	protected $dates = ['date_started','date_ended','created_at','updated_at'];
	protected $primaryKey = 'id';

	public function rewards(){
		return $this->hasOne('App\Reward');
	}
}


