<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class RewardGiven extends Model {

	protected $table = 'atc_rewards_given';
	protected $fillable = ['userid','promo_id','rewards_id','serial_no','ipaddress'];

	public function rewards(){
		return $this->belongsTo('App\Reward','rewards_id');
	}

	public function user(){
		return $this->hasOne('App\User','id','userid');
	}

	public function promo(){
		return $this->hasOne('App\Promo','id','promo_id');
	}

}
