<?php 

namespace Laracasts\Classes;

use App\UserCard;


class UserCardProcess {

	private $userID;
	private $userCardData = [];
	private $userData = [];

	public function setUserID($userID) {
		$this->userID = $userID;

		return $this;
	}

	public function setUserData($requestData) {
		$this->userData = $requestData;

		return $this;
	}

	public function editUserCard() {
		foreach ($this->userData['card_type'] as $cardTypeName => $value) {
			$this->setUserCardData($cardTypeName);

			$filter = [
				'user_id'   => $this->userID,
				'card_type' => $cardTypeName
			];

			UserCard::updateOrCreate($filter, $this->userCardData);

			/* Clear Data per Iteration */
			$this->userCardData = [];
		}
	}

	private function setUserCardData($cardTypeName) {
		switch (strtoupper($cardTypeName)) {
			case 'BPI':
				$this->userCardData['user_id']          = $this->userID;
				$this->userCardData['card_title']       = 'BPI AMORE';
				$this->userCardData['card_type']        = $cardTypeName;
				// $this->userCardData['beep_paywave']     = $this->userData['beep_paywave'];
				// $this->userCardData['new_renewal']      = $this->userData['new_renewal'];
				$this->userCardData['name_in_card']     = $this->userData['name_in_card'];
				$this->userCardData['source_of_income'] = $this->userData['source_of_income'];
				$this->userCardData['birthday']         = $this->userData['birthday'];
				$this->userCardData['card_no']          = $this->userData['card_no'];
				$this->userCardData['card_expiry']      = $this->userData['card_expiry'];
				$this->userCardData['amount']           = $this->userData['amount'];
				$this->userCardData['ar_no']            = $this->userData['ar_no'];
				$this->userCardData['remarks']          = $this->userData['remarks'];

				break;
			
			case 'VIP':
				$this->userCardData['user_id']            = $this->userID;
				$this->userCardData['card_title']         = 'VIPINOY';
				$this->userCardData['card_type']          = $cardTypeName;
				$this->userCardData['new_renewal']        = $this->userData['new_renewal'];
				$this->userCardData['name_of_ofw']        = $this->userData['name_of_ofw'];
				$this->userCardData['relationship']       = $this->userData['relationship'];
				$this->userCardData['document_presented'] = $this->userData['document_presented'];
				$this->userCardData['document_no']        = $this->userData['document_no'];
				$this->userCardData['birthday']           = $this->userData['birthday'];
				$this->userCardData['card_no']            = $this->userData['card_no'];
				$this->userCardData['card_expiry']        = $this->userData['card_expiry'];
				$this->userCardData['amount']             = $this->userData['amount'];
				$this->userCardData['ar_no']              = $this->userData['ar_no'];
				$this->userCardData['remarks']            = $this->userData['remarks'];

				break;

			default:
				# code...
				break;
		}
	}
}