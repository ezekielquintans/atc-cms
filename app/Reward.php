<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Reward extends Model {

	protected $table = 'atc_rewards';
	protected $fillable = ['name', 'promo_id', 'rewards_count','venue','details','datetime'];
	protected $dates = ['datetime'];
	protected $primaryKey = 'id';

	public function promo()
    {
		return $this->belongsTo('App\Promo', 'promo_id');
	}
	public function reward(){
		return $this->hasMany('App\RewardGiven','rewards_id');
	}
	

    public function rewardsGiven()
    {
        return $this->hasMany('App\RewardGiven', 'reward_id', 'id');
    }

}
