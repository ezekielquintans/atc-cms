<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Activities extends Model
{
    protected $table = 'atc_activities';
    protected $fillable = ['activity', 'userid', 'ipaddress', 'contact_number'];

    public static function newUser($userName)
    {
        $userId = auth()->user()->id;
        $username = auth()->user()->username;

        try {
            self::create([
                'activity' => "$username added a new user: $userName.",
                'userid' => $userId,
                'ipaddress' => $_SERVER['REMOTE_ADDR'],
            ]);
        } catch (Exception $e) {
            return false;
        }
    }

    public static function updateUser($updatedUserId)
    {
        $userId = auth()->user()->id;
        $username = auth()->user()->username;

        try {
            self::create([
                'activity' => "$username updated the information of user id: $updatedUserId",
                'userid' => $userId,
                'ipaddress' => $_SERVER['REMOTE_ADDR'],
            ]);
        } catch (Exception $e) {
            return false;
        }
    }

    public static function destroyedUser($destroyedUserId)
    {
        $userId = auth()->user()->id;
        $username = auth()->user()->username;

        try {
            self::create([
                'activity' => "$username deleted user id: $destroyedUserId",
                'userid' => $userId,
                'ipaddress' => $_SERVER['REMOTE_ADDR'],
            ]);
        } catch (Exception $e) {
            return false;
        }
    }

    public static function textBlastSent($message, $contact = "")
    {
        $userId   = auth()->user()->id;
        $username = auth()->user()->username;

        try {
            self::create([
                'activity'       => "$username sent a textblast with the message: $message",
                'userid'         => $userId,
                'contact_number' => $contact,
                'ipaddress'      => $_SERVER['REMOTE_ADDR'],
            ]);
        } catch (Exception $e) {
            return false;
        }
    }
}
