<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Promo extends Model {

	protected $table = 'atc_promos';
	protected $fillable = ['promo_id','promo_name'];


}
