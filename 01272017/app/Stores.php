<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Stores extends Model {

	public $categories        = array();
	public $stores            = array();
	public $specificCategories = array();

	public function __construct(){
		/**Categories**/
			$this->categories["RESTAURANTS"][] = "Abe";
			$this->categories["RESTAURANTS"][] = "Balot Balot";
			$this->categories["RESTAURANTS"][] = "Bonchon";
			$this->categories["RESTAURANTS"][] = "Bulgogi Brothers";
			$this->categories["RESTAURANTS"][] = "Cabalen";
			$this->categories["RESTAURANTS"][] = "Chatime";
			$this->categories["RESTAURANTS"][] = "Crisostomo";
			$this->categories["RESTAURANTS"][] = "Elephant Express";
			$this->categories["RESTAURANTS"][] = "El Presidente of Binondo";
			$this->categories["RESTAURANTS"][] = "Gerry’s Grill";
			$this->categories["RESTAURANTS"][] = "Giligan’s";
			$this->categories["RESTAURANTS"][] = "Gloria Maris";
			$this->categories["RESTAURANTS"][] = "Gongcha";
			$this->categories["RESTAURANTS"][] = "Hanamaruken";
			$this->categories["RESTAURANTS"][] = "Healthy Shabu-Shabu";
			$this->categories["RESTAURANTS"][] = "Ikkoryu Fukuoka Ramen";
			$this->categories["RESTAURANTS"][] = "Jipan";
			$this->categories["RESTAURANTS"][] = "John & Yoko";
			$this->categories["RESTAURANTS"][] = "Kenjitei";
			$this->categories["RESTAURANTS"][] = "Kim N’ Chi";
			$this->categories["RESTAURANTS"][] = "Luk Yuen";
			$this->categories["RESTAURANTS"][] = "Mary Grace Café";
			$this->categories["RESTAURANTS"][] = "Max’s Restaurant";
			$this->categories["RESTAURANTS"][] = "Mongolian Quickstop";
			$this->categories["RESTAURANTS"][] = "Nanbantei";
			$this->categories["RESTAURANTS"][] = "Northpark";
			$this->categories["RESTAURANTS"][] = "Pancake House";
			$this->categories["RESTAURANTS"][] = "Papermoon";
			$this->categories["RESTAURANTS"][] = "Pepper Lunch";
			$this->categories["RESTAURANTS"][] = "PhoHoa";
			$this->categories["RESTAURANTS"][] = "Razon’s";
			$this->categories["RESTAURANTS"][] = "Recipes by Café Metro";
			$this->categories["RESTAURANTS"][] = "Rock & Seoul";
			$this->categories["RESTAURANTS"][] = "Santouka";
			$this->categories["RESTAURANTS"][] = "Seafood Island";
			$this->categories["RESTAURANTS"][] = "Serenitea";
			$this->categories["RESTAURANTS"][] = "Shilin";
			$this->categories["RESTAURANTS"][] = "Shou";
			$this->categories["RESTAURANTS"][] = "Soban";
			$this->categories["RESTAURANTS"][] = "Soi";
			$this->categories["RESTAURANTS"][] = "Ted’s Lapaz Batchoy";
			$this->categories["RESTAURANTS"][] = "Tempura";
			$this->categories["RESTAURANTS"][] = "The Boiling Seafood";
			$this->categories["RESTAURANTS"][] = "Wee Nam Kee";
			$this->categories["RESTAURANTS"][] = "Yabu";
			$this->categories["RESTAURANTS"][] = "Yakimix";

		/**End categories**/

		/**Assignment of stores**/
			$this->stores[] = "Abe";
			$this->stores[] = "Balot Balot";
			$this->stores[] = "Bonchon";
			$this->stores[] = "Bulgogi Brothers";
			$this->stores[] = "Cabalen";
			$this->stores[] = "Chatime";
			$this->stores[] = "Crisostomo";
			$this->stores[] = "Elephant Express";
			$this->stores[] = "El Presidente of Binondo";
			$this->stores[] = "Gerry’s Grill";
			$this->stores[] = "Giligan’s";
			$this->stores[] = "Gloria Maris";
			$this->stores[] = "Gongcha";
			$this->stores[] = "Hanamaruken";
			$this->stores[] = "Healthy Shabu-Shabu";
			$this->stores[] = "Ikkoryu Fukuoka Ramen";
			$this->stores[] = "Jipan";
			$this->stores[] = "John & Yoko";
			$this->stores[] = "Kenjitei";
			$this->stores[] = "Kim N’ Chi";
			$this->stores[] = "Luk Yuen";
			$this->stores[] = "Mary Grace Café";
			$this->stores[] = "Max’s Restaurant";
			$this->stores[] = "Mongolian Quickstop";
			$this->stores[] = "Nanbantei";
			$this->stores[] = "Northpark";
			$this->stores[] = "Pancake House";
			$this->stores[] = "Papermoon";
			$this->stores[] = "Pepper Lunch";
			$this->stores[] = "PhoHoa";
			$this->stores[] = "Razon’s";
			$this->stores[] = "Recipes by Café Metro";
			$this->stores[] = "Rock & Seoul";
			$this->stores[] = "Santouka";
			$this->stores[] = "Seafood Island";
			$this->stores[] = "Serenitea";
			$this->stores[] = "Shilin";
			$this->stores[] = "Shou";
			$this->stores[] = "Soban";
			$this->stores[] = "Soi";
			$this->stores[] = "Ted’s Lapaz Batchoy";
			$this->stores[] = "Tempura";
			$this->stores[] = "The Boiling Seafood";
			$this->stores[] = "Wee Nam Kee";
			$this->stores[] = "Yabu";
			$this->stores[] = "Yakimix";
		/**End stores**/

		/**Specific categories**/
			$this->specificCategories["Abe"]              = "RESTAURANTS";
			$this->specificCategories["Balot Balot"]              = "RESTAURANTS";
			$this->specificCategories["Bonchon"]                  = "RESTAURANTS";
			$this->specificCategories["Bulgogi Brothers"]         = "RESTAURANTS";
			$this->specificCategories["Cabalen"]                  = "RESTAURANTS";
			$this->specificCategories["Chatime"]                  = "RESTAURANTS";
			$this->specificCategories["Crisostomo"]               = "RESTAURANTS";
			$this->specificCategories["Elephant Express"]         = "RESTAURANTS";
			$this->specificCategories["El Presidente of Binondo"] = "RESTAURANTS";
			$this->specificCategories["Gerry’s Grill"]            = "RESTAURANTS";
			$this->specificCategories["Giligan’s"]                = "RESTAURANTS";
			$this->specificCategories["Gloria Maris"]             = "RESTAURANTS";
			$this->specificCategories["Gongcha"]                  = "RESTAURANTS";
			$this->specificCategories["Hanamaruken"]              = "RESTAURANTS";
			$this->specificCategories["Healthy Shabu-Shabu"]      = "RESTAURANTS";
			$this->specificCategories["Ikkoryu Fukuoka Ramen"]    = "RESTAURANTS";
			$this->specificCategories["Jipan"]                    = "RESTAURANTS";
			$this->specificCategories["John & Yoko"]              = "RESTAURANTS";
			$this->specificCategories["Kenjitei"]                 = "RESTAURANTS";
			$this->specificCategories["Kim N’ Chi"]               = "RESTAURANTS";
			$this->specificCategories["Luk Yuen"]                 = "RESTAURANTS";
			$this->specificCategories["Mary Grace Café"]          = "RESTAURANTS";
			$this->specificCategories["Max’s Restaurant"]         = "RESTAURANTS";
			$this->specificCategories["Mongolian Quickstop"]      = "RESTAURANTS";
			$this->specificCategories["Nanbantei"]                = "RESTAURANTS";
			$this->specificCategories["Northpark"]                = "RESTAURANTS";
			$this->specificCategories["Pancake House"]            = "RESTAURANTS";
			$this->specificCategories["Papermoon"]                = "RESTAURANTS";
			$this->specificCategories["Pepper Lunch"]             = "RESTAURANTS";
			$this->specificCategories["PhoHoa"]                   = "RESTAURANTS";
			$this->specificCategories["Razon’s"]                  = "RESTAURANTS";
			$this->specificCategories["Recipes by Café Metro"]    = "RESTAURANTS";
			$this->specificCategories["Rock & Seoul"]             = "RESTAURANTS";
			$this->specificCategories["Santouka"]                 = "RESTAURANTS";
			$this->specificCategories["Seafood Island"]           = "RESTAURANTS";
			$this->specificCategories["Serenitea"]                = "RESTAURANTS";
			$this->specificCategories["Shilin"]                   = "RESTAURANTS";
			$this->specificCategories["Shou"]                     = "RESTAURANTS";
			$this->specificCategories["Soban"]                    = "RESTAURANTS";
			$this->specificCategories["Soi"]                      = "RESTAURANTS";
			$this->specificCategories["Ted’s Lapaz Batchoy"]      = "RESTAURANTS";
			$this->specificCategories["Tempura"]                  = "RESTAURANTS";
			$this->specificCategories["The Boiling Seafood"]      = "RESTAURANTS";
			$this->specificCategories["Wee Nam Kee"]              = "RESTAURANTS";
			$this->specificCategories["Yabu"]                     = "RESTAURANTS";
			$this->specificCategories["Yakimix"]                  = "RESTAURANTS";
		/**End specific categories**/
	}

	public function getCategories(){
		return $this->categories;
	}

	public function getStores(){
		return $this->stores;
	}

	public function getCurrentCategory($search = ""){
		// foreach($this->categories as $category => $storeListing){
		// 	foreach($storeListing as $store){
		// 		if($search == $store){
		// 			return $category;
		// 		}
		// 	}
		// }
		if(isset($this->specificCategories[$search])){
			return $this->specificCategories[$search];
		}
		return "-";
	}

	public static function displayCurrentCategory($search = ""){
		$categories = new Stores;
		return $categories->getCurrentCategory($search);
	}

	public function __destruct(){
		$this->stores = null;
		$this->categories = null;
	}

}
