<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

        $this->call('UserSeeder');
		$this->call('CouponSeeder');
	}

}

class CouponSeeder extends Seeder{
    private $noOfCoupons = 9999;
    private $categories  = ["food","fashion","retail","hnb","electronics","furniture","stationary"];
    private $stores      = ["starbucks","bench","mango","guess","chilis","mcdonalds","burgerking"];
    public function run(){
        for($x = 1; $x <= $this->noOfCoupons; $x++ ){

            DB::table('atc_entries')->insert([
                'user_id'    => rand(1,80),
                'category'   => $this->categories[rand(0, count($this->categories) - 1)],
                'store_name' => $this->stores[rand(0, count($this->stores) - 1)],
                'receipt_no' => str_pad(rand(7000000,99999999), 8, '0', STR_PAD_LEFT),
                'token'      => strtoupper(uniqid()),
                'unique_id'  => strtoupper(uniqid()),
                'amount'     => rand(500,4000),
                'ipaddress'  => '127.0.0.1',
                'status'     => 1
            ]);
        }
    }
}

class UserSeeder extends Seeder{
	private $noOfUsers  = 80;
	private $firstNames = ["Sophia","Emma","Olivia","Isabella","Ava","Lily","Zoe","Chloe","Mia","Madison","Emily","Ella","Madelyn","Abigail","Aubrey","Addison","Avery","Layla","Hailey","Amelia","Hannah","Charlotte","Kaitlyn","Harper","Kaylee","Sophie","Mackenzie","Peyton","Riley","Grace","Brooklyn","Sarah","Aaliyah","Anna","Arianna","Ellie","Natalie","Isabelle","Lillian","Evelyn","Elizabeth","Lyla","Lucy","Claire","Makayla","Kylie","Audrey","Maya","Leah","Gabriella","Annabelle","Savannah","Nora","Reagan","Scarlett","Samantha","Alyssa","Allison","Elena","Stella","Alexis","Victoria","Aria","Molly","Maria","Bailey","Sydney","Bella","Mila","Taylor","Kayla","Eva","Jasmine","Gianna","Alexandra","Julia","Eliana","Kennedy","Brianna","Ruby","Lauren","Alice","Violet","Kendall","Morgan","Caroline","Piper","Brooke","Elise","Alexa","Sienna","Reese","Clara","Paige","Kate","Nevaeh","Sadie","Quinn","Isla","Eleanor","Aiden","Jackson","Ethan","Liam","Mason","Noah","Lucas","Jacob","Jayden","Jack","Logan","Ryan","Caleb","Benjamin","William","Michael","Alexander","Elijah","Matthew","Dylan","James","Owen","Connor","Brayden","Carter","Landon","Joshua","Luke","Daniel","Gabriel","Nicholas","Nathan","Oliver","Henry","Andrew","Gavin","Cameron","Eli","Max","Isaac","Evan","Samuel","Grayson","Tyler","Zachary","Wyatt","Joseph","Charlie","Hunter","David","Anthony","Christian","Colton","Thomas","Dominic","Austin","John","Sebastian","Cooper","Levi","Parker","Isaiah","Chase","Blake","Aaron","Alex","Adam","Tristan","Julian","Jonathan","Christopher","Jace","Nolan","Miles","Jordan","Carson","Colin","Ian","Riley","Xavier","Hudson","Adrian","Cole","Brody","Leo","Jake","Bentley","Sean","Jeremiah","Asher","Nathaniel","Micah","Jason","Ryder","Declan","Hayden","Brandon","Easton","Lincoln","Harrison"];
	private $lastNames  = ["Smith","Jones","Taylor","Williams","Brown","Davies","Evans","Wilson","Thomas","Roberts","Johnson","Lewis","Walker","Robinson","Wood","Thompson","White","Watson","Jackson","Wright","Green","Harris","Cooper","King","Lee","Martin","Clarke","James","Morgan","Hughes","Edwards","Hill","Moore","Clark","Harrison","Scott","Young","Morris","Hall","Ward","Turner","Carter","Phillips","Mitchell","Patel","Adams","Campbell","Anderson","Allen","Cook","Bailey","Parker","Miller","Davis","Murphy","Price","Bell","Baker","Griffiths","Kelly","Simpson","Marshall","Collins","Bennett","Cox","Richardson","Fox","Gray","Rose","Chapman","Hunt","Robertson","Shaw","Reynolds","Lloyd","Ellis","Richards","Russell","Wilkinson","Khan","Graham","Stewart","Reid","Murray","Powell","Palmer","Holmes","Rogers","Stevens","Walsh","Hunter","Thomson","Matthews","Ross","Owen","Mason","Knight","Kennedy","Butler","Saunders","Cole","Pearce","Dean","Foster","Harvey","Hudson","Gibson","Mills","Berry","Barnes","Pearson","Kaur","Booth","Dixon","Grant","Gordon","Lane","Harper","Ali","Hart","Mcdonald","Brooks","Ryan","Carr","Macdonald","Hamilton","Johnston","West","Gill","Dawson","Armstrong","Gardner","Stone","Andrews","Williamson","Barker","George","Fisher","Cunningham","Watts","Webb","Lawrence","Bradley","Jenkins","Wells","Chambers","Spencer","Poole","Atkinson"];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        /**
         * Create concierge
         */
        DB::table('atc_users')->insert([
            'username'    => "atc.concierge",
            'first_name'  => "concierge",
            'last_name'   => "",
            'middle_name' => "",
            'unique_id'   => strtoupper(uniqid()),
            'usertype'    => 2,
            'dob'         => '',
            'address'     => '',
            'contact'     => '',
            'email'       => 'atc.concierge@alabangtowncenter.ph',
            'status'      => 1,
            'password'    => bcrypt('secret')
        ]);

        /**
         * Create admin
         */
        DB::table('atc_users')->insert([
            'username'    => "atc.admin",
            'first_name'  => "admin",
            'last_name'   => "",
            'middle_name' => "",
            'unique_id'   => strtoupper(uniqid()),
            'usertype'    => 3,
            'dob'         => '',
            'address'     => '',
            'contact'     => '',
            'email'       => 'atc.admin@alabangtowncenter.ph',
            'status'      => 1,
            'password'    => bcrypt('secret')
        ]);

        for($x = 1; $x <= $this->noOfUsers; $x++ ){
            $firstName = $this->firstNames[rand(0, count($this->firstNames) - 1)];
            $lastName  = $this->lastNames[rand(0, count($this->lastNames) - 1)];
            $middleName  = $this->lastNames[rand(0, count($this->lastNames) - 1)];
            $email     = strtolower($firstName . "." . $lastName ."@sampleemail.com");
            $username  = strtolower($firstName . "." . $lastName);
            DB::table('atc_users')->insert([
                'username'    => $username,
                'first_name'  => $firstName,
                'last_name'   => $lastName,
                'middle_name' => $middleName,
                'unique_id'   => strtoupper(uniqid()),
                'usertype'    => 1,
                'dob'         => '1992-04-04',
                'address'     => 'Philippines',
                'contact'     => '7723151',
                'email'       => $email,
                'status'      => 1,
                'password'    => bcrypt('secret')
            ]);
        }
    }
}
