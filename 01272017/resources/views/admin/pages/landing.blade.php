<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Alabang Town Center</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Place favicon.ico in the root directory -->
        <!-- text cdn -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Shadows+Into+Light" rel="stylesheet">
        <!--    <link rel="stylesheet" href="css/normalize.css">-->
        <link rel="stylesheet" href="{{asset('public/landing/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('public/landing/dist/Remodal-1.1.0/dist/remodal.css')}}">
        <link rel="stylesheet" href="{{asset('public/landing/dist/Remodal-1.1.0/dist/remodal-default-theme.css')}}">
        <link rel="stylesheet" href="{{asset('public/landing/css/style.css')}}">
        <script src="{{asset('public/landing/js/vendor/modernizr-2.8.3.min.js')}}"></script>
    </head>
    <body class="">
        <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <!-- Add your site or application content here -->
        <section class="ec ec__view ec__view--bg remodal-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="ec__view__wrpr">
                            <img class="center-block ec__view__logo" src="{{asset('public/landing/img/main-logo.png')}}" alt="">
                            <div class="row m-0">
                                <div class="col-md-6 p-0">
                                    <div class="ec__view__i">
                                        <!-- scrollable content -->
                                        <div class="info-content">
                                            <!-- Promo Logo -->
                                            <img class="center-block receipt-logo" src="{{asset('public/landing/img/promo-logo-2.jpg')}}" alt="" height="250" width="auto">
                                            <br/>
                                            {{-- <img class="promo_brand" src="{{asset('public/landing/img/promo-logo.png')}}" alt=""> --}}
                                            <!-- Raffle title -->
                                            <div class="promo-title">
                                                <h1 class="giga" style="font-size: 23px;">CELEBRATE & FEAST! LUCK IS ON YOUR PLATE</h1>
                                                <h1 class="giga" style="font-size: 23px;">TERMS AND CONDITIONS</h1>
                                            </div>
                                            <ol>
                                            	<li>
                                            		<p>The promo is from January 23-February 12, 2017.</p>
                                            		<p>The promo is open to Alabang Town Center diners in the following participating Asian Food establishments:</p>
                                            ​​		<ul style="margin-top:-35px;">
                                            			<li>Abe</li>
                                            			<li>Balot Balot</li>
                                            			<li>Bonchon</li>
                                            			<li>Bulgogi Brothers</li>
                                            			<li>Cabalen</li>
                                            			<li>Chatime</li>
                                            			<li>Crisostomo</li>
                                            			<li>Elephant Express</li>
                                            			<li>El Presidente of Binondo</li>
                                            			<li>Gerry’s Grill</li>
                                            			<li>Giligan’s</li>
                                            			<li>Gloria Maris</li>
                                            			<li>Gongcha</li>
                                            			<li>Hanamaruken</li>
                                            			<li>Healthy Shabu-Shabu</li>
                                            			<li>Ikkoryu Fukuoka Ramen</li>
                                            			<li>Jipan</li>
                                            			<li>John & Yoko</li>
                                            			<li>Kenjitei</li>
                                            			<li>Kim N’ Chi</li>
                                            			<li>Luk Yuen</li>
                                            			<li>Mary Grace Café</li>
                                            			<li>Max’s Restaurant</li>
                                            			<li>Mongolian Quickstop</li>
                                            			<li>Nanbantei</li>
                                            			<li>Northpark</li>
                                            			<li>Pancake House</li>
                                            			<li>Papermoon</li>
                                            			<li>Pepper Lunch</li>
                                            			<li>PhoHoa</li>
                                            			<li>Razon’s</li>
                                            			<li>Recipes by Café Metro</li>
                                            			<li>Rock & Seoul</li>
                                            			<li>Santouka</li>
                                            			<li>Seafood Island</li>
                                            			<li>Serenitea</li>
                                            			<li>Shilin</li>
                                            			<li>Shou</li>
                                            			<li>Soban</li>
                                            			<li>Soi</li>
                                            			<li>Ted’s Lapaz Batchoy</li>
                                            			<li>Tempura</li>
                                            			<li>The Boiling Seafood</li>
                                            			<li>Wee Nam Kee</li>
                                            			<li>Yabu</li>
                                            			<li>Yakimix</li>
                                            		</ul>
                                            	</li>
                                            	<li>
                                            		Diner is entitled to one (1) electronic raffle coupon for every P500 worth of single/accumulated receipts from participating restaurants of Alabang Town Center to be redeemed at the concierge.
                                            	</li>
                                            	<li>
                                            		Diner must register by providing the following details: complete name, address, email address, contact number/s (office phone number, house phone number or mobile phone number) and age.
                                            	</li>
                                            	<li>
                                            		Upon registration, concierge will input all the details of the diner to generate number of raffle coupon/s.
                                            	</li>
                                            	<li>
                                            		Diner will receive a copy of the generated receipts indicating number of coupons redeemed per transaction and total number of coupons to date.
                                            	</li>
                                            	<li>
                                            		Only receipts issued from January 23-February 12, 2017 are eligible.  Defaced, crumpled, tampered or receipts that are less than ¾ its actual size will not be honored.
                                            	</li>
                                            	<li>
                                            		There is a total of five (5) Swatch Rocking Rooster watches for the promo.  The schedule of draw dates, eligibility and prizes are as follows:
                                            		<table class="table" style="font-size:10px;">
                                            			<tr class="text-center">
                                            				<td>Draw Date<br/>(Time:  4:00pm)</td>
                                            				<td>Last Day of Submission of Entries (by 9:00pm)</td>
                                            				<td>Prizes</td>
                                            			</tr>
                                            			<tr class="text-center">
                                            				<td>February 13, 2017 (Friday)</td>
                                            				<td>February 12, 2017 (Thursday)</td>
                                            				<td>Five (5) winners of a Swatch Rocking Rooster Watch</td>
                                            			</tr>
                                            		</table>
                                            		*** A DTI representative will witness the raffle draw.
                                            	</li>
                                            	<li>
                                            		A diner may only win once during the entire promo.
                                            	</li>
                                            	<li>
                                            		The prizes are subject to the following conditions:
                                            		<ul>
                                            			<li>
                                            				All prizes are transferable but not convertible to cash.
                                            			</li>
                                            			<li>
                                            				Prizes not claimed after sixty (60) days from date of notification will be forfeited in favor of Alabang Commercial Corporation with prior approval of DTI.
                                            			</li>
                                            		</ul>
                                            	</li>
                                            	<li>
                                            		Winners will be notified by telephone call and e-Mail.  The winners’ names will also be posted at the Alabang Town Center’s official Facebook and Twitter accounts
                                            	</li>
                                            	<li>
                                            		Winners may claim their prizes at the Alabang Town Center Administration Office from Monday to Friday from 10am-6pm.
                                            	</li>
                                            	<li>
                                            		To claim the prizes, winners must present the notice sent by e-mail, and any two (2) valid Government-issued identification card with photo and signature.
                                            	</li>
                                            	<li>
                                            		Employees of Alabang Commercial Corporation, Gift Gate Inc., their merchants, maintenance and security agencies, and advertising and promotional agencies, and their relatives up to the second degree of affinity or consanguinity, are disqualified from joining the promo.
                                            	</li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 p-0">
                                    <div class="ec__view__iside text-center">
                                        <h4 class="text-italic showcase__title" style="text-align:center;">Dine now and Get a chance to win a<br/> "Swatch Rocking Rooster Watch"</h4>
                                        <div class="form__container">
                                            <h4>View your coupons here!</h4>
                                            <p>Check your email now for updates!</p>
                                            <form class="form-inline" method="post" action="{{URL::to('post-check-entries')}}">
                                                <div class="form-group">
                                                    <input type="email" required name="email" class="form-control" id="form_email" placeholder="Email Address"/>
                                                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                                                </div>
                                                <button type="submit" class="btn btn-purple btn-sm">SEND</button>
                                            </form>
                                        </div>
                                        <div class="helper__card">
                                            <h1>One (1) raffle coupon <br> for every 500 php purchase </h1>
                                            {{-- <p class="small">Earn twice as much when you use your BPI Amore Card</p> --}}
                                        </div>
                                        <p class="permit">DTI-FTEB Permit No. 0154, Series of 2017</p>
                                        <ul class="list-inline list-inline-white">
                                            <li>
                                                <a class="facebook" target="_blank" href="https://www.facebook.com/AlabangTownCenter/"><i class="icon-facebook"></i></a>
                                            </li>
                                            <li>
                                                <a class="twitter" target="_blank" href="https://twitter.com/alabangtowncntr?lang=en"><i class="icon-twitter"></i></a>
                                            </li>
                                            <li>
                                                <a class="instagram" target="_blank" href="https://www.instagram.com/alabangtowncenter/"><i class="icon-instagram"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="ec__footer ec__footer--mod">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="text-center">
                                <p class="copyright">ONE AYALA HOTLINE: 795-9595</p>
                                <p class="copyright">Copyright © 2016 Ayala Malls 360. All Rights Reserved</p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            @if(Session::has('msg_success'))
                <div class="remodal" data-remodal-id="modal">
                    <button data-remodal-action="close" class="remodal-close"></button>
                    <h1>Success</h1>
                    <p>
                        {{Session::get('msg_success')}}
                    </p>
                    <br>
                    <button data-remodal-action="confirm" class="remodal-confirm">OK</button>
                </div>
            @elseif(Session::has('msg_warning'))
                <div class="remodal" data-remodal-id="modal">
                    <button data-remodal-action="close" class="remodal-close"></button>
                    <h1>Warning!</h1>
                    <p>
                        {{Session::get('msg_warning')}}
                    </p>
                    <br>
                    <button data-remodal-action="confirm" class="remodal-cancel">OK</button>
                </div>
            @elseif(Session::has('msg_danger'))
                <div class="remodal" data-remodal-id="modal">
                    <button data-remodal-action="close" class="remodal-close"></button>
                    <h1>Oops!</h1>
                    <p>
                        {{Session::get('msg_danger')}}
                    </p>
                    <br>
                    <button data-remodal-action="confirm" class="remodal-cancel">OK</button>
                </div>
            @endif

        </section>
        <!-- end of content -->
        <script src="{{asset('public/landing/js/vendor/jquery-1.12.0.min.js')}}"></script>
        <script src="{{asset('public/landing/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('public/landing/js/plugins.js')}}"></script>
        <script src="{{asset('public/landing/js/main.js')}}"></script>
        <script src="{{asset('public/landing/dist/Remodal-1.1.0/dist/remodal.min.js')}}"></script>
        <script>
        $(document).ready(function(){
            // $('[data-remodal-id=modal]').remodal('');
            var inst = $.remodal.lookup[$('[data-remodal-id=modal]').data('remodal')];
            // открыть модальное окно
            inst.open();
        });
        </script>
    </body>
</html>