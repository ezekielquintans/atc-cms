</div>
</div>
<!-- /page content -->

<!-- footer content -->
<footer>
    <div class="pull-right">
       Copyright {{date('Y')}}
    </div>
    <div class="clearfix"></div>
</footer>
<!-- /footer content -->
</div>
</div>

<!-- jQuery -->
<script src="{{asset("public/app/vendors/jquery/dist/jquery.min.js")}}"></script>
<!-- Bootstrap -->
<script src="{{asset("public/app/vendors/bootstrap/dist/js/bootstrap.min.js")}}"></script>
<!-- jQuery autocomplete -->
<script src="{{asset("public/app/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js")}}"></script>
<!-- Custom Theme Scripts -->
<script src="{{asset("public/app/js/custom.min.js")}}"></script>
<!-- iCheck -->
<script src="{{asset("public/app/vendors/iCheck/icheck.min.js")}}"></script>
<!-- bootstrap-daterangepicker -->
    <script>
      $(document).ready(function() {
        $('.btn-link').click(function() {
            if($(this).hasClass('btn-delete')){
                if(!confirm('Do you really want to delete this?')){
                    return false;
                }
            }
            location.href=$(this).data('href');
        });
      });
    </script>
    <!-- Custom Scripts -->
    @yield("scripts")
</body>

</html>