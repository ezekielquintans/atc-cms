<?php

/*USER LEVEL*/


Route::get('email', 'AdminController@sendEmail');
Route::get('check-entries', 'AdminController@checkEntries');
Route::post('post-check-entries', 'AdminController@postCheckEntries');
Route::get('/', 'AdminController@displayLandingPage');
Route::get('promos/{id}/{name}', 'AdminController@displayPromos');

Route::get('generate', 'AdminController@generateUniqueId');
/*ADMIN LEVEL*/
Route::group(['prefix'=> 'res'],function(){
	/*****ACCESS*****/
	Route::get('/', 'AdminController@displayLogin');
	Route::get('login', 'AdminController@displayLogin');
	Route::post('login', 'AdminController@processLogin');

	/********Concierge********/
	Route::group(['middleware' => 'concierge'],function(){
		/*****ACCESS*****/
		Route::get('logout', 'AdminController@processLogout');
		/*****USER*****/
		Route::get('members', 'AdminController@getMembers');
		Route::get('members/delete/{id}', 'AdminController@deleteMember');
		Route::get('getUserInfo', 'AdminController@getUserInfo');
		Route::get('tickets-claimed', 'AdminController@getTicketsEarned');
		Route::get('getEntriesOfUser/{id}', 'AdminController@getEntriesOfUser');
		Route::get('edit/user/{username}', 'UserController@displayEditPage');
		Route::post('edit/user/process-edit-user', 'UserController@processEditUser');

		Route::get('entries', 'AdminController@getEntries');
		Route::get('entries/delete/{id}', 'AdminController@deleteEntry');
		Route::get('entries/add', 'AdminController@displayUpdateEntries');
		Route::post('entries/addEntries', 'AdminController@addEntries');
		Route::get('entries/print-entries', 'AdminController@printEntries');
		Route::get('search/autocomplete', 'AdminController@autocomplete');
		Route::get('validate-invoice', 'AdminController@validateInvoice');
		Route::get('validate-reward', 'AdminController@validateRewards');
	});

	/********Admin********/
	Route::group(['middleware' => 'admin'],function(){
		/*Raffle and winner*/
		Route::get('raffle-draw', 'AdminController@displayRaffleDraw');
		Route::post('kashing', 'AdminController@generateWinner');
		/*****Users*****/
		Route::get('users', 'UserController@displayUsers');
		Route::get('register', 'UserController@displayRegisterPage');
		Route::get('delete/user/{username}', 'UserController@processDeleteUser');
		Route::post('process-register', 'UserController@processEditUser');
	});
});
