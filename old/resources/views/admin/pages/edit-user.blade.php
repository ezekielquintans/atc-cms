@extends('admin.layouts.admin-layout')
@section('content')
		<div class="clearfix"></div>
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				Whoops! There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif

		@if (Session::has('message'))
			<div class="alert {{Session::get('messageClass')}}">
				{{Session::get('message')}}
			</div>
		@endif
		<div class="row">
		    <div class="col-md-7 col-xs-12">
		        <div class="x_panel">
		            <div class="x_title">
		            	@if(!empty($user))
							@if(Auth::user()->usertype == 3)
								<h3>Edit User</h3>
							@elseif(Auth::user()->usertype == 2)
								<h3>Edit Profile</h3>
							@endif
						@else
							<h3>Oops!</h3>
						@endif
		                <div class="clearfix"></div>
		            </div>
		            <form class="form-horizontal" role="form" method="POST" action="{{URL::to('res/edit/user/process-edit-user')}}" enctype="multipart/form-data">
			            <div class="x_content">
			            	@if(!empty($user))
							<div class="col-md-12">
								<div class="form-group">
									<label class="col-md-3 control-label">First Name</label>
									<div class="col-md-9">
										<input type="text" class="form-control" required name="first_name" value="{{ $user->first_name }}" placeholder="...">
										<input type="hidden" class="form-control" name="userid" value="{{ $user->id }}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Middle Name</label>
									<div class="col-md-9">
										<input type="text" class="form-control" name="middle_name" value="{{ $user->middle_name }}" placeholder="...">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Last Name</label>
									<div class="col-md-9">
										<input type="text" class="form-control" required name="last_name" value="{{ $user->last_name }}" placeholder="...">
									</div>
								</div>
								<hr/>
								<div class="form-group">
									<label class="col-md-3 control-label">Date of Birth</label>
									<div class="col-md-9">
										<input type="date" class="form-control" name="dob" value="{{ $user->dob }}" placeholder="">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Email</label>
									<div class="col-md-9">
										<input type="email" class="form-control" required name="email" value="{{ $user->email }}" placeholder="">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Contact No.</label>
									<div class="col-md-9">
										<input type="number" class="form-control" required name="contact" value="{{ $user->contact }}" placeholder="">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Address</label>
									<div class="col-md-9">
										<input type="text" class="form-control" required name="address" value="{{$user->address}}"/>
									</div>
								</div>
								<hr/>


								@if(!preg_match('/members/', URL::previous()))
									@if(Auth::user()->usertype == 3)
										<div class="form-group">
											<label class="col-md-3 control-label">Role</label>
											<div class="col-md-9">
												<select name='usertype' class="form-control">
													<option value="3"  {{ $user->usertype == 3 ? 'selected' : '' }}>Admin</option>
													<option value="2"  {{ $user->usertype == 2 ? 'selected' : '' }}>Concierge</option>
													<option value="1"  {{ $user->usertype == 1 ? 'selected' : '' }}>Member</option>
												</select>
											</div>
										</div>
									@elseif(Auth::user()->usertype == 2)
										<input type="hidden" name="usertype" value="2">
									@endif
									<div class="form-group">
										<label class="col-md-3 control-label">Password</label>
										<div class="col-md-9">
											<input type="password" class="form-control" name="password" autocomplete="off">
										</div>
									</div>

									<div class="form-group">
										<label class="col-md-3 control-label">Confirm Password</label>
										<div class="col-md-9">
											<input type="password" class="form-control" name="password_confirmation" autocomplete="off">
										</div>
									</div>
								@endif
								<div class="form-group">
									<div class="col-md-9 col-md-offset-3">
										<button type="submit" class="btn btn-primary">
											Update
										</button>
										@if(!preg_match('/edit\/user/', URL::previous()))
											<a href="{{URL::previous()}}">
												<button type="button" class="btn btn-default btn-no-shadow">
													Cancel
												</button>
											</a>
										@else
											<a href="{{URL::to('res/members')}}">
												<button type="button" class="btn btn-default btn-no-shadow">
													Cancel
												</button>
											</a>
										@endif

									</div>
								</div>
							</div>
							@else
								Sorry, we can't find the user that we are trying to edit.
							@endif
			            </div>
			            <div class="x_footer">
			            </div>
			            <input type="hidden" name="_token" value="{{ Session::token() }}">
		            </form>
		        </div>
		    </div>
		</div>
@stop