<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Alabang Town Center</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Place favicon.ico in the root directory -->
        <!-- text cdn -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Shadows+Into+Light" rel="stylesheet">
        <!--    <link rel="stylesheet" href="css/normalize.css">-->
        <link rel="stylesheet" href="{{asset('public/landing/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('public/landing/dist/Remodal-1.1.0/dist/remodal.css')}}">
        <link rel="stylesheet" href="{{asset('public/landing/dist/Remodal-1.1.0/dist/remodal-default-theme.css')}}">
        <link rel="stylesheet" href="{{asset('public/landing/css/style.css')}}">
        <script src="{{asset('public/landing/js/vendor/modernizr-2.8.3.min.js')}}"></script>
    </head>
    <body class="">
        <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <!-- Add your site or application content here -->
        <section class="ec ec__view ec__view--bg remodal-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="ec__view__wrpr">
                            <img class="center-block ec__view__logo" src="{{asset('public/landing/img/main-logo.png')}}" alt="">
                            <div class="row m-0">
                                <div class="col-md-6 p-0">
                                    <div class="ec__view__i">
                                        <!-- scrollable content -->
                                        <div class="info-content">
                                            <!-- Promo Logo -->
                                            <img class="center-block receipt-logo" src="{{asset($promo->image_url)}}" alt="" height="250" width="auto">
                                            <br/>
                                            <!-- Raffle title -->
                                            <div class="promo-title">
                                                <h1 class="giga" style="font-size: 23px;">{{$promo->name}}</h1>
                                                <h1 class="giga" style="font-size: 23px;">TERMS AND CONDITIONS</h1>
                                            </div>
                                            {!!$promo->description!!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 p-0">
                                    <div class="ec__view__iside text-center">
                                        <h4 class="text-italic showcase__title" style="text-align:center;">{!!$promo->subtitle!!}</h4>
                                        <div class="form__container">
                                            <h4>View your coupons here!</h4>
                                            <p>Check your email now for updates!</p>
                                            <form class="form-inline" method="post" action="{{URL::to('post-check-entries')}}">
                                                <div class="form-group">
                                                    <input type="email" required name="email" class="form-control" id="form_email" placeholder="Email Address"/>
                                                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                                                    <input type="hidden" name="promo" value="{{ $promo->id }}">
                                                </div>
                                                <button type="submit" class="btn btn-purple btn-sm">SEND</button>
                                            </form>
                                        </div>
                                        <div class="helper__card">
                                            {!!$promo->helper!!}
                                        </div>
                                        <p class="permit">{{$promo->dti_permit}}</p>
                                        <ul class="list-inline list-inline-white">
                                            <li>
                                                <a class="facebook" target="_blank" href="https://www.facebook.com/AlabangTownCenter/"><i class="icon-facebook"></i></a>
                                            </li>
                                            <li>
                                                <a class="twitter" target="_blank" href="https://twitter.com/alabangtowncntr?lang=en"><i class="icon-twitter"></i></a>
                                            </li>
                                            <li>
                                                <a class="instagram" target="_blank" href="https://www.instagram.com/alabangtowncenter/"><i class="icon-instagram"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="ec__footer ec__footer--mod">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="text-center">
                                <p class="copyright">ONE AYALA HOTLINE: 795-9595</p>
                                <p class="copyright">Copyright © 2016 Ayala Malls 360. All Rights Reserved</p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            @if(Session::has('msg_success'))
                <div class="remodal" data-remodal-id="modal">
                    <button data-remodal-action="close" class="remodal-close"></button>
                    <h1>Success</h1>
                    <p>
                        {{Session::get('msg_success')}}
                    </p>
                    <br>
                    <button data-remodal-action="confirm" class="remodal-confirm">OK</button>
                </div>
            @elseif(Session::has('msg_warning'))
                <div class="remodal" data-remodal-id="modal">
                    <button data-remodal-action="close" class="remodal-close"></button>
                    <h1>Warning!</h1>
                    <p>
                        {{Session::get('msg_warning')}}
                    </p>
                    <br>
                    <button data-remodal-action="confirm" class="remodal-cancel">OK</button>
                </div>
            @elseif(Session::has('msg_danger'))
                <div class="remodal" data-remodal-id="modal">
                    <button data-remodal-action="close" class="remodal-close"></button>
                    <h1>Oops!</h1>
                    <p>
                        {{Session::get('msg_danger')}}
                    </p>
                    <br>
                    <button data-remodal-action="confirm" class="remodal-cancel">OK</button>
                </div>
            @endif

        </section>
        <!-- end of content -->
        <script src="{{asset('public/landing/js/vendor/jquery-1.12.0.min.js')}}"></script>
        <script src="{{asset('public/landing/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('public/landing/js/plugins.js')}}"></script>
        <script src="{{asset('public/landing/js/main.js')}}"></script>
        <script src="{{asset('public/landing/dist/Remodal-1.1.0/dist/remodal.min.js')}}"></script>
        <script>
        $(document).ready(function(){
            // $('[data-remodal-id=modal]').remodal('');
            var inst = $.remodal.lookup[$('[data-remodal-id=modal]').data('remodal')];
            // открыть модальное окно
            inst.open();
        });
        </script>
    </body>
</html>