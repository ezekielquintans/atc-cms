@extends('admin.layouts.admin-layout')
@section('content')
	<div class="clearfix"></div>
	@if (count($errors) > 0)
		<div class="alert alert-danger">
			Whoops! There were some problems with your input.<br><br>
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

	@if (Session::has('message'))
		<div class="alert {{Session::get('messageClass')}}">
			{{Session::get('message')}}
		</div>
	@endif
	<div class="row">
	    <div class="col-md-12 col-sm-12 col-xs-12">
	        <div class="x_panel">
	            <div class="x_title">
		            <form method="get">
		                <h2 class="col-md-3">List of Users</h2>
		                <div class="col-md-9">
		                    <div class="row">
		                        <div class="col-md-7">
		                            <div class="form-group">
		                                <input type="text" name="search" id="search" value="{{Input::get('search', '')}}" class="form-control" placeholder="Search for...">
		                            </div>
		                        </div>
		                        <div class="col-md-5">
		                            <div class="form-group text-right">
		                                <button class="btn btn-default" type="submit">Go!</button>
		                                <button class="btn btn-default" onclick="document.getElementById('search').value='';" type="submit">View All</button>
		                                <a href="{{URL::to('res/register')}}"><button class="btn btn-default" type="button">Create a User</button></a>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		            </form>
	                <div class="clearfix"></div>
	            </div>
	            <div class="x_content">
	            	<table class="table table-striped">
						<thead>
							<tr>
								<th></th>
								<th>Display Name</th>
								<th>Username</th>
								<th class="text-center"></th>
							</tr>
						</thead>
	                    <tbody>
                        	@if(!$users->isEmpty())
                        		@foreach($users as $key => $row)
                        			<tr>
                        				<td width="1">{{ (Input::get('page', 1) - 1) * 10 + $key + 1 }}. </td>
                        				<td>{{$row->first_name}}</td>
                        				<td>{{$row->username}}</td>
                        				<td align="right">
                        					<div class="btn-group">
                        						<button class="btn btn-default btn-link" data-href="edit/user/{{$row->username}}" title="Edit this user">
                        							<i class="fa fa-pencil"></i>
                        						</button>
                        						@if(Auth::user()->id != $row->id)
                        						<button class="btn btn-default btn-link btn-delete" data-href="delete/user/{{$row->username}}" title="Delete this user">
                        							<i class="fa fa-trash-o"></i>
                        						</button>
                        						@endif
                                            </div>
                        				</td>
                        			</tr>
                        		@endforeach
                        	@else
                        		<tr>
                        			<td colspan="6">There are no users yet.</td>
                        		</tr>
                        	@endif
	                    </tbody>
	            	</table>
	            </div>
	            <div class="x_footer">
	                {!! $users->render() !!}
	            </div>
	        </div>
	    </div>
	</div>

@stop