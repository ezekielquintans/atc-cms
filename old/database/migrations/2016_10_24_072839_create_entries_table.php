<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('atc_entries', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->integer('promo_id');
			$table->string('category', 150);
			$table->string('store_name', 150);
			$table->string('receipt_no', 30);
			$table->string('token', 60);
			$table->string('unique_id', 60);
			$table->decimal('amount', 10, 2);
			$table->string('ipaddress', 20);
			$table->longText('browser');
			$table->tinyInteger('status');
			$table->tinyInteger('email_sent')->defaul(0);
			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->default('0000-00-00 00:00:00');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('atc_entries');
	}

}
