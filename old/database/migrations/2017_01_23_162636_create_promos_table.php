<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('atc_promos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 255);
			$table->text('subtitle');
			$table->text('description');
			$table->string('image_url', 255);
			$table->text('helper');
			$table->string('dti_permit', 255);
			$table->decimal('amt_per_coupon', 10, 2);
			$table->timestamp('date_started')->default('0000-00-00 00:00:00');
			$table->timestamp('date_ended')->default('0000-00-00 00:00:00');
			$table->tinyInteger('status')->default(1);
			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->default('0000-00-00 00:00:00');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('atc_promos');
	}

}
