<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRewardsGivenTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('atc_rewards_given', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('userid');
			$table->integer('promo_id');
			$table->integer('rewards_id');
			$table->string('serial_no');
			$table->string('ipaddress', 20);
			$table->string('batch_id', 20);
			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->default('0000-00-00 00:00:00');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('atc_rewards_given');
	}

}
