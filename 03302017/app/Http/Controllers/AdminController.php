<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\ActivityController;
use Illuminate\Http\Request;
use Input;
use Response;
use Session;
use Redirect;
use Auth;
use URL;
use Validator;
use App\User;
use App\Coupons;
use App\Entry;
use App\Stores;
use App\Promo;
use App\RewardGiven;


class AdminController extends Controller {

	public function displayLandingPage(){
		$promos = Promo::where('status', 1)
				->where('date_started', '<=' , date('Y-m-d H:i:s'))
				->where('date_ended', '>=', date('Y-m-d H:i:s'))
				->get();
		return view('admin.pages.main', compact('promos'));
	}

	public function displayPromos($id, $name){
		if(is_numeric($id)){
			$promo = Promo::where('id', $id)->first();
			if(count($promo) != 0){
				return view('admin.pages.landing', compact('promo'));
			}else{
				return redirect('/');
			}
		}else{
			return redirect('/');
		}
	}

	public function displayLogin(){
		return view('admin.pages.login');
	}

	public function displayUpdateEntries(){
		$users = User::where('status',1)
			->where('usertype',1)
			->get();

		$token    = Input::get('tk', '');
		if($token != ""){
			$entry 	  =  Entry::selectRaw('atc_entries.token,atc_entries.unique_id as coupon_id, atc_entries.user_id, atc_users.first_name, atc_users.last_name, atc_users.middle_name, atc_users.email, atc_users.address,atc_users.dob,atc_users.contact')
						->leftJoin('atc_users','atc_entries.user_id', '=', 'atc_users.id')
						->where('atc_entries.status', 1)
						->where('atc_entries.token', $token)
						->where('atc_users.usertype', 1)
						->orderBy('atc_entries.created_at', 'desc')
						->get();
		}else{
			$entry = array();
		}

		$stores       = new Stores();
		$storeListing = $stores->getStores();

		$promos = Promo::where('status', 1)
				->where('date_started', '<=' , date('Y-m-d H:i:s'))
				->where('date_ended', '>=', date('Y-m-d H:i:s'))
				->get();

		return view('admin.pages.entries',compact('users', 'entry', 'token', 'storeListing','promos'));
	}

	public function getUserInfo(){
		$userID = Input::get('userid');
		$user = User::find($userID);
		if(count($user) > 0){
			$user->display_id = str_pad(Input::get('userid'), 8, '0', STR_PAD_LEFT);
			$ticketsClaimed = RewardGiven:: where('userid', $user->id)
				->whereRaw('Date(created_at) = CURDATE()')
				->count();
			$ticketsClaimed = $ticketsClaimed >= 2 ? 'claimed' : $ticketsClaimed;
			$user->ticketsClaimedToday = $ticketsClaimed;
		}
		return Response::json($user);
	}

	public function validateInvoice(Request $request, $store = "", $invoice = ""){
		if(Input::get('store','') != ""){
			$store = Input::get('store');
		}

		if(Input::get('invoice','') != ""){
			$invoice = Input::get('invoice');
		}

		$entry = Entry::where('store_name', $store)
				->where('receipt_no', $invoice)
				->where('status', 1)
				->first();

		if($request->ajax()){
			if(count($entry) > 0){
				echo "false";
				exit;
			}
			echo "true";
			exit;
		}else{
			if(count($entry) > 0){
				return false;
			}
			return true;
		}
	}

	public function validateRewards(Request $request, $serialno = "", $promoid = ""){
		if(Input::get('serialno','') != ""){
			$serial = Input::get('serialno');
		}

		if(Input::get('promoid','') != ""){
			$promo = Input::get('promoid');
		}
		$rewards = RewardGiven::where('serial_no', $serial)
				->where('promo_id', $promo)
				->first();
		if($request->ajax()){
			if(count($rewards) > 0){
				echo "false";
				exit;
			}
			echo "true";
			exit;
		}else{
			if(count($rewards) > 0){
				return false;
			}
			return true;
		}
	}

	public function addEntries(Request $request){
		// var_dump(isset($request->total_tickets));
		// dd($request->all());
		if(strtok($request->server('HTTP_REFERER'),'?') !== url('res/entries/add')){
			exit();
		}
		$validator = Validator::make($request->all(), [
			'email'         => 'required|email',
			'address'       => 'required',
			'contact'       => 'required|numeric|digits_between:7,11',
			'promo'       	=> 'required',
			'cusid'         => 'numeric',
			'mode'          => 'in:S,A',
			'row'           => 'integer',
			'total_coupons' => 'required|min:1',
			// 'total_tickets' => 'required_if:promo,2',
        ],[
			'required'          => 'The :attribute is required',
			'required_if'       => 'The :attribute is required',
			'total_coupons.min' => 'Minimum amount must be 500 or more to generate a coupon',
			'alpha_dash'        => 'The :attribute must contain letters only',
			'email'             => ':attribute must be valid',
			'numeric'           => ':attribute must be numeric values only',
			'digits_between'    => ':attribute is invalid due to unknown format. Must be a telephone number or mobile number only',
			'in'                => ':attribute must ne one of the following types: :values',
			'integer'           => ':attribute must be an integer value',
        ]);
        if($validator->fails()) {
            Session::flash('msg_error', "Ooops. something went wrong!");
            return Redirect::back()->withInput()->withErrors($validator);
        } else{
        	$stores       = new Stores();
        	$storeListing = $stores->getStores();

			$couponIds = array();
			$customerId   = $request->cusid;
			$noOfRows     = $request->rows;
			$store        = $request->store;
			$promo        = $request->promo;
			$invoice      = $request->invoice;
			$amount       = $request->amount;
			$coupons      = $request->coupons;
			$tickets      = $request->tickets;
			$multiplier   = $request->multiplier;
			$fullname	  = $request->first_name.' '.$request->last_name;
			$token 		  = $this->generateUniqueId(uniqid(), 13);
			/*Register User if not yet existed*/
			if($customerId == ""){
				$checkExist = User::where('first_name',$request->first_name)
								->where('last_name',$request->last_name)
								->where('contact',$request->contact);
				if($checkExist->exists()){
					if($checkExist->first()->status == 0){
						$existingUser = User::find($checkExist->first()->id);
						$existingUser->status = 1;
						$existingUser->save();
					}
					$customerId = $checkExist->first()->id;
				}else{
					$user = new User;
					$user->first_name 	=  $request->first_name;
					$user->last_name 	=  $request->last_name;
					$user->email 		=  $request->email;
					$user->username     =  $request->email;
					$user->address 		=  $request->address;
					$user->contact 		=  $request->contact;
					$user->usertype 	=  1;
					$user->status 		=  1;
					$user->unique_id	= $this->generateUniqueId(uniqid());
					$user->save();
					$customerId = $user->id;
				}
			}
			/*Insert Coupons*/
			$tempTotal = 0;
			for ($i=0; $i < $noOfRows ; $i++) {
				$noCoupons = 0;
				if(isset($multiplier[$i]) && $multiplier[$i] == "on"){
					$noCoupons = $coupons[$i] * 2;
				}else{
					$noCoupons = $coupons[$i];
				}

				if($request->get('mode') == "A"){
					$tempTotal = $tempTotal + $noCoupons;
					if($i == $noOfRows - 1){
						if($tempTotal != $request->get('total_coupons')){
							$temp = $request->get('total_coupons') - $tempTotal;
							$noCoupons = $noCoupons + $temp;
						}
					}
				}
				for ($c=0; $c < $noCoupons ; $c++) {
					$categoryFetched = $stores->getCurrentCategory($store[$i]);
					if($categoryFetched === null || $categoryFetched == ""){
						$categoryFetched = "-";
					}

					$entry = new Entry;
					$entry->user_id 	= $customerId;
					$entry->promo_id 	= $promo;
					$entry->category 	= $categoryFetched;
					$entry->store_name 	= $store[$i];
					$entry->receipt_no 	= $invoice[$i];
					$entry->amount 		= $amount[$i];
					$entry->status 		= 1;
					$entry->token 		= $token;
					$entry->ipaddress	= $_SERVER['REMOTE_ADDR'];
					$entry->browser		= $_SERVER['HTTP_USER_AGENT'];
					$entry->save();

					$entry->unique_id = $this->generateUniqueId($entry->id);
					$entry->save();
					$couponIds[] = $entry->unique_id;
				}
			}
			/*Insert moview Tickets*/
			$total_tickets = $request->total_tickets;
			if ($promo == 2 && isset($total_tickets)) {
				for ($t=0; $t < $total_tickets; $t++) {
					$reward = new RewardGiven;
					$reward->userid 	= $customerId;
					$reward->promo_id 	= $promo;
					$reward->rewards_id = 1;
					$reward->serial_no 	= $tickets[$t];
					$reward->batch_id 	= $token;
					$reward->ipaddress 	= $_SERVER['REMOTE_ADDR'];
					$reward->save();
					#check in front if tickets already claimed everyday
				}
			}
		}

		$forPrint = array(
			"customerId" => $customerId,
			"noOfRows"	 => $noOfRows,
			"store"		 => $store,
			"invoice"	 => $invoice,
			"couponIds"	 => $couponIds,
			"token"		 => $token
		);
		Session::forget('for-print');
		Session::put('for-print', $forPrint);

		$emailSent = $this->sendEmail("", $token);
		$emailSentInt = 0;
		if($emailSent === true){
			$emailSentInt = 1;
			Session::flash('msg_success', 'Entries inserted successfully!');
		}else{
			Session::flash('msg_warning', 'Entries inserted successfully, but there was a problem sending an email to the user.');
		}
		$entrySent = Entry::where('token', $token)->update(['email_sent'=>$emailSentInt]);
		ActivityController::logActivity(Auth::user()->username . ' generated '.count($couponIds).' coupons for customer no. '.$customerId.', ', Auth::user()->id);
        return Redirect::to('res/entries/add?success=true&tk=' . $token);
	}

	public function sendEmail($userid = "", $token = "", $promoId = ""){
		$entry 	  =  Entry::selectRaw('atc_entries.promo_id, atc_entries.token,atc_entries.unique_id as coupon_id, atc_entries.user_id, atc_users.first_name, atc_users.last_name, atc_users.middle_name, atc_users.email, atc_users.address,atc_users.dob,atc_users.contact')
					->leftJoin('atc_users','atc_entries.user_id', '=', 'atc_users.id')
					->where('atc_entries.status', 1)
					->where('atc_users.usertype', 1);

		if($userid != ""){
			$entry = $entry->where('atc_entries.user_id', $userid);
			$all = "all";
		}

		if($token != ""){
			$entry = $entry->where('atc_entries.token', $token);
			$all = "";
		}

		if($promoId != ""){
			$entry = $entry->where('promo_id', $promoId);
		}

		$entry = $entry->orderBy('atc_entries.id', 'desc')->get();

		if(count($entry) == 0){
			return false;
		}

		$totalCount =  Entry::where('user_id', $entry[0]->user_id)->where('status', 1);
		if($promoId != "" || $entry[0]->promo_id != ""){
			if($promoId == ""){
				$promoId = $entry[0]->promo_id;
			}
			$totalCount = $totalCount->where('promo_id', $promoId);
		}
		$totalCount = $totalCount->get()->count();

		$promoInfo    = array();
		$rewardsGiven = array();
		$promoInfo    = Promo::where('id', $entry[0]->promo_id)->first();
		if($token != ""){

			$rewardsGiven = RewardGiven::selectRaw('atc_rewards.name as reward, atc_rewards_given.*')
							->where('batch_id', $token)
							->leftJoin('atc_rewards', 'atc_rewards_given.rewards_id', '=', 'atc_rewards.id')
							->get();
		}

		try{
			\Mail::send(['html'=>'admin.email.send-coupons'], ['rewardsGiven'=> $rewardsGiven, 'promoInfo'=> $promoInfo,'totalCount' => $totalCount, 'entry' => $entry, 'token' => $token, 'all' => $all], function ($m) use ($entry, $totalCount) {
			    $m->from('info@alabangtowncenter.ph', 'Concierge - Alabang Town Center');
			    $m->to($entry[0]->email, "Concierge")->subject('[Concierge] - Coupon Details');
			});
			if(count(\Mail::failures()) > 0){
				return false;
			}
			return true;
		}catch(\Exception $e){
			return false;
		}
	}

	public function displayRaffleDraw(){
		$promo = Promo::where('status', 1)
				->where('date_started', '<=' , date('Y-m-d H:i:s'))
				->where('date_ended', '>=', date('Y-m-d H:i:s'))
				->get(['id','name']);
		return view('admin.pages.raffle', compact('promo'));
	}

	public function processLogin(Request $request){
		$this->validate($request, [
            'username' 		=> 'required',
            'password' 		=> 'required'
        ]);
        $credentials = $request->only('username', 'password');
        $credentials['status'] = 1;
        if(Auth::attempt($credentials, false)){
        	ActivityController::logActivity(Auth::user()->username . ' has logged in.', Auth::user()->id);
            return redirect()->to(url('res/members'));
        }else{
           Session::flash('msg_error', "These credentials do not match our records.");
           return redirect()->back()
            ->withInput($request->only('username'))
            ->withErrors([
                'username' => "These credentials does not match our records.",
            ]);
        }
    }

    public function processLogout(){
    	ActivityController::logActivity(Auth::user()->username . ' has logged out.', Auth::user()->id);
        Session::flush();
        Auth::logout();
        return redirect()->to('res/login');
    }

	public function printEntries(){
		$token    = Input::get('tk', '');
		$entry 	  =  Entry::selectRaw('atc_entries.promo_id,atc_entries.token,atc_entries.unique_id as coupon_id, atc_entries.user_id, atc_users.first_name, atc_users.last_name, atc_users.middle_name, atc_users.email, atc_users.address,atc_users.dob,atc_users.contact')
					->leftJoin('atc_users','atc_entries.user_id', '=', 'atc_users.id')
					->where('atc_entries.status', 1)
					->where('atc_entries.token', $token)
					->where('atc_users.usertype', 1)
					->get();

		$totalCount =  Entry::where('user_id', $entry[0]->user_id)
						->where('status', 1)
						->where('promo_id', $entry[0]->promo_id)
						->get()
						->count();

		$customer   = $entry->first()->first_name.' '.$entry->first()->last_name;

		$promoInfo  = Promo::where('id', $entry[0]->promo_id)->first();


		$rewardsGiven = RewardGiven::selectRaw('atc_rewards.name as reward, atc_rewards_given.*')
						->where('batch_id', $token)
						->leftJoin('atc_rewards', 'atc_rewards_given.rewards_id', '=', 'atc_rewards.id')
						->get();


		ActivityController::logActivity(Auth::user()->username . ' printed coupons for '. $customer, Auth::user()->id);
		return view('print.entries', compact('entry','token','totalCount', 'rewardsGiven', 'promoInfo'));
	}

	public function generateWinner(Request $request){
		if(!is_numeric($request->promo_id)){
			return json_encode(array());
		}

		$winner =  Entry::selectRaw('atc_users.unique_id as user_unique, atc_entries.unique_id as coupon_no, atc_entries.id as entry_id, atc_entries.user_id, atc_users.first_name, atc_users.last_name, atc_users.middle_name, atc_users.email, atc_users.address,atc_users.dob,atc_users.contact, atc_entries.unique_id as entrie_unique')
					->leftJoin('atc_users','atc_entries.user_id', '=', 'atc_users.id')
					->where('atc_entries.status', 1)
					->where('atc_entries.promo_id', $request->promo_id)
					->where('atc_users.usertype', 1)
					->orderByRaw('RAND()')
					->first()
					->toArray();
		// ActivityController::logActivity($winner->first_name.' '.$winner->last_name.' has won the raffle draw', Auth::user()->id);
		$winner['entry_id'] = str_pad($winner["entry_id"], 7, '0', STR_PAD_LEFT);
		$winner['custom_id'] = str_pad($winner["user_id"], 8, '0', STR_PAD_LEFT);
		return json_encode($winner);
	}

	public function updateWinner($userid){
		$entry = Entry::where('user_id', $userid)->first();
		$entry->status = 2;
		$entry->save();
	}

	public function getEntries(){
		$promos = Promo::where('status', 1)
				->where('date_started', '<=' , date('Y-m-d H:i:s'))
				->where('date_ended', '>=', date('Y-m-d H:i:s'))
				->get();

		$search  = Input::get('search', '');
		$promoId = Input::get('promo', '');
		$entries =  Entry::selectRaw('atc_entries.unique_id as coupon_id, atc_entries.user_id, atc_users.first_name, atc_users.last_name, atc_users.middle_name, atc_users.email, atc_users.address,atc_users.dob,atc_users.contact, atc_entries.created_at as entry_created')
					->leftJoin('atc_users','atc_entries.user_id', '=', 'atc_users.id')
					->where('atc_entries.status', 1)
					->where('atc_users.usertype', 1);

		if($search != ''){
			$entries = $entries->where(function($query) use($search){
				$query->where('atc_users.first_name', 'LIKE', '%' . $search . '%')
				->orWhere('atc_users.last_name', 'LIKE', '%' . $search . '%')
				->orWhere('atc_users.middle_name', 'LIKE', '%' . $search . '%')
				->orWhere('atc_users.email', 'LIKE', '%' . $search . '%')
				->orWhere('atc_entries.id', '=', ltrim($search, '0'))
				->orWhere('atc_users.id', '=', ltrim($search, '0'));
				// ->orWhere('atc_users.middle_name', 'LIKE', '%' . $search . '%');
			});
		}

		if($promoId != '' && is_numeric($promoId)){
			$entries = $entries->where('promo_id', $promoId);
		}

		$entries = $entries->orderBy('atc_entries.id','desc')
					->paginate(20)
                    ->setPath('entries')
                    ->appends(Input::except('page'));

        return view('admin.pages.view-entries', compact('entries','promos'));
	}

	public function getMembers(){
		$search  =  Input::get('search', '');
		$members =  User::selectRaw('(SELECT COUNT(id) FROM atc_entries WHERE atc_entries.user_id = atc_users.id) as entry_count, atc_users.username, atc_users.id, atc_users.first_name, atc_users.last_name, atc_users.middle_name, atc_users.email, atc_users.address,atc_users.dob,atc_users.contact, atc_users.created_at as entry_created')
					->where('atc_users.usertype', 1)
					->where('atc_users.status', 1);

		if($search != ''){
			$members = $members->where(function($query) use($search){
				$query->where('atc_users.first_name', 'LIKE', '%' . $search . '%')
				->orWhere('atc_users.last_name', 'LIKE', '%' . $search . '%')
				->orWhere('atc_users.middle_name', 'LIKE', '%' . $search . '%')
				->orWhere('atc_users.email', 'LIKE', '%' . $search . '%')
				->orWhere('atc_users.id', '=', ltrim($search, '0'));
			});
		}

		$members = $members->orderBy('atc_users.last_name','asc')
					->orderBy('atc_users.last_name','asc')
					->paginate(20)
                    ->setPath('members')
                    ->appends(Input::except('page'));

        return view('admin.pages.members', compact('members'));
	}

	public function checkEntries(){
		return view('admin.pages.check-entries');
	}

	public function postCheckEntries(Request $request){
		$rules = [
		    'email'       => 'required|email'
		];

		$validator = Validator::make($request->all(), $rules);

		if ($validator->fails()) {
		    $this->throwValidationException(
		        $request, $validator
		    );
		}

		$user = User::where('email', Input::get('email'))->where('status', 1)->first();
		if(count($user) == 0){
			Session::flash('msg_danger', "Sorry, we can't find an account with the given email address, ". Input::get('email') .". Please try again with a different email address.");
		}else{
			$emailSent = $this->sendEmail($user->id, "", $request->promo);
			if($emailSent === true){
				Session::flash('msg_success', 'The details of your entries were sent to your email address. Please check your email.');
			}else{
				Session::flash('msg_warning', 'We\'ve found your account but there was a problem sending an email at this moment. Please try again later.');
			}
		}

        return Redirect::back();
	}

	public function deleteEntry($id){
        $entry = Entry::where('unique_id', $id)->first();
        if(count($entry) == 0){
        	Session::flash('msg_warning', 'Failed to delete coupon.');
        	return Redirect::back();
        }
        $entry->status = 0;
        $entry->save();
        ActivityController::logActivity('Coupon no. '.$entry->id . '  was deleted', Auth::user()->id);
        Session::flash('msg_success', 'Deleted successfully!.');
        return Redirect::back();
    }

    public function deleteMember($id){
        $user = User::find($id);
        $user->status = 0;
        $user->save();
        ActivityController::logActivity($user->last_name . ', ' . $user->first_name . ' ' . $user->middle_name . '(' . $user->id .')' . '  was deleted', Auth::user()->id);
        Session::flash('msg_success', 'Deleted successfully!.');
        return Redirect::back();
    }

    public function generateUniqueId($id = 1, $length = 8){
    	$unique = $id . uniqid() . strtotime(date('Y-m-d H:i:s:')) . "Htech2k17!";
    	$unique = md5($unique);
    	$unique = hash('sha256', $unique);
    	return  strtoupper(substr($unique, 0, $length));
    }

    public function getEntriesOfUser($id){
    	if(isset($id) && $id != ""){
    		$entries = Entry::where('user_id',$id)
    			->where('status',1)
    			->get();
    		return Response::json($entries);
    	}
    	exit();
    }
}
