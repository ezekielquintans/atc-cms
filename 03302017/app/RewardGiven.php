<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class RewardGiven extends Model {

	protected $table = 'atc_rewards_given';
	protected $fillable = ['userid','promo_id','rewards_id','serial_no','ipaddress'];

}
