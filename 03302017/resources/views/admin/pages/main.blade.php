<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" type="image/png" href="{{asset('public/landingv2/img/favicon-32x32.png')}}" sizes="32x32" />
    <link rel="icon" type="image/png" href="{{asset('public/landingv2/img/favicon-16x16.png')}}" sizes="16x16" />
    <!-- Place favicon.ico in the root directory -->
    <!-- text cdn -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Shadows+Into+Light" rel="stylesheet">
    <!--    <link rel="stylesheet" href="css/normalize.css">-->
    <link rel="stylesheet" href="{{asset('public/landingv2/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/landingv2/dist/Remodal-1.1.0/dist/remodal.css')}}">
    <link rel="stylesheet" href="{{asset('public/landingv2/dist/Remodal-1.1.0/dist/remodal-default-theme.css')}}">
    <link rel="stylesheet" href="{{asset('public/landingv2/bower_components/swiper/dist/css/swiper.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/landingv2/css/style.css')}}">
    <script src="{{asset('public/landingv2/js/vendor/modernizr-2.8.3.min.js')}}"></script>
</head>

<body class="">
    <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
    <!-- Add your site or application content here -->
    <section class="ec ec__view ec__view--bg remodal-bg">
        <!-- START : Type 1 Layout -->
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <img class="center-block ec__view__logo" src="{{asset('public/landingv2/img/main-logo.png')}}" alt="">
                    <div class="main-view m-0">
                        <div class="page-heading">
                            <!--<h3 class="text-center">Participants</h3>-->
                            <p>Click any of these promo to view</p>
                        </div>
                        <div class="row">
                            @if(count($promos) <= 2)
                                @forelse($promos as $row)
                                    <div class="col-md-6 @if(count($promos) == 1) col-md-offset-3 @endif">
                                        <a href="{{URL::to('promos/' . $row->id . '/' . str_slug($row->name))}}">
                                            <div class="card-block card-promo card-type-1">
                                                <div class="card-image">
                                                    <img src="{{asset($row->image_url)}}" alt="">
                                                    <span class="card-title">{{$row->name}}</span>
                                                    <div class="highlight-overlay"></div>
                                                </div>
                                                <div class="card-content">
                                                    <p>Promo runs from {{date('F j, Y', strtotime($row->date_started))}} to {{date('F j, Y', strtotime($row->date_ended))}}</p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                @empty
                                @endforelse
                            @elseif(count($promos) > 2)
                                <div class="swiper-container">
                                    <div class="swiper-wrapper">
                                        @forelse($promos as $row)
                                            <div class="swiper-slide div-link">
                                                <a href="{{URL::to('promos/' . $row->id . '/' . str_slug($row->name))}}">
                                                    <div class="card-block card-promo">
                                                        <div class="card-image">
                                                            <img src="{{asset($row->image_url)}}" alt="">
                                                            <span class="card-title">{{$row->name}}</span>
                                                            <div class="highlight-overlay"></div>
                                                        </div>
                                                        <div class="card-content">
                                                            <p>Promo runs from {{date('F j, Y', strtotime($row->date_started))}} to {{date('F j, Y', strtotime($row->date_ended))}}</p>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>

                                        @empty
                                        @endforelse
                                    </div>
                                    <div class="swiper-pagination"></div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END : Type 1 Layout -->
        <footer class="ec__footer ec__footer--mod">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center">
                            <p class="copyright">ONE AYALA HOTLINE: 795-9595</p>
                            <p class="copyright">Copyright © {{date('Y')}} Ayala Malls 360. All Rights Reserved</p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </section>
    <!-- end of content -->
    <script src="{{asset('public/landingv2/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('public/landingv2/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('public/landingv2/dist/Remodal-1.1.0/dist/remodal.min.js')}}"></script>
    <script src="{{asset('public/landingv2/bower_components/swiper/dist/js/swiper.jquery.min.js')}}"></script>
    <script src="{{asset('public/landingv2/bower_components/swiper/dist/js/swiper.min.js')}}"></script>

    <script src="{{asset('public/landingv2/js/plugins.js')}}"></script>
    <script src="{{asset('public/landingv2/js/main.js')}}"></script>
    <script>
        $(".div-link").click(function () {
            window.location = $(this).find("a").attr("href");
            return false;
        });
    </script>
    <script>
        var swiper = new Swiper('.swiper-container', {
            pagination: '.swiper-pagination',
            slidesPerView: 4,
            direction: 'horizontal',
            centeredSlides: true,
            paginationClickable: true,
            //            scrollbarHide: true,
            //            grabCursor: true,
            spaceBetween: 30,
            breakpoints: {
                1024: {
                    slidesPerView: 3,
                    spaceBetween: 40
                },
                768: {
                    slidesPerView: 2,
                    spaceBetween: 30
                },
                640: {
                    slidesPerView: 1,
                    spaceBetween: 20
                },
                320: {
                    slidesPerView: 1,
                    spaceBetween: 20
                }
            }
        });
    </script>

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
        //            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        //            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        //            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        //            e.src='https://www.google-analytics.com/analytics.js';
        //            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
        //            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
    </script>
</body>

</html>