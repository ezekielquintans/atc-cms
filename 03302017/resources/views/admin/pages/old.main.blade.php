<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Alabang Town Center</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Place favicon.ico in the root directory -->
        <!-- text cdn -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Shadows+Into+Light" rel="stylesheet">
        <!--    <link rel="stylesheet" href="css/normalize.css">-->
        <link rel="stylesheet" href="{{asset('public/landing/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('public/landing/dist/Remodal-1.1.0/dist/remodal.css')}}">
        <link rel="stylesheet" href="{{asset('public/landing/dist/Remodal-1.1.0/dist/remodal-default-theme.css')}}">
        <link rel="stylesheet" href="{{asset('public/landing/css/style.css')}}">
        <script src="{{asset('public/landing/js/vendor/modernizr-2.8.3.min.js')}}"></script>
    </head>
    <body class="">
        <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <!-- Add your site or application content here -->
        <section class="ec ec__view ec__view--bg remodal-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="ec__view__wrpr">
                            <img class="center-block ec__view__logo" src="{{asset('public/landing/img/main-logo.png')}}" alt="">
                            <div class="row m-0">
                                <div class="col-md-6 p-0">
                                    <div class="ec__view__iside text-center">
                                        <a href="promos/2/food-holiday">
                                            <img src="{{asset('public/landing/img/promo-logo-3.jpg')}}" class="img-responsive" style="height: 100%; width:auto; margin:0 auto;">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6 p-0">
                                    <div class="ec__view__i text-center">
                                        <a href="promos/1/celebrate-and-feast">
                                            <img src="{{asset('public/landing/img/promo-logo-2.jpg')}}" class="img-responsive" style="height: 100%; width:auto; margin:0 auto;">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="ec__footer ec__footer--mod">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="text-center">
                                <p class="copyright">ONE AYALA HOTLINE: 795-9595</p>
                                <p class="copyright">Copyright © 2016 Ayala Malls 360. All Rights Reserved</p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>


        </section>
        <!-- end of content -->
        <script src="{{asset('public/landing/js/vendor/jquery-1.12.0.min.js')}}"></script>
        <script src="{{asset('public/landing/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    </body>
</html>