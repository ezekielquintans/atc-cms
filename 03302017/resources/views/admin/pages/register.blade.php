@extends('admin.layouts.admin-layout')
@section('content')
	<div class="clearfix"></div>
	@if (count($errors) > 0)
		<div class="alert alert-danger">
			Whoops! There were some problems with your input.<br><br>
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

	@if (Session::has('message'))
		<div class="alert {{Session::get('messageClass')}}">
			{{Session::get('message')}}
		</div>
	@endif
	<div class="row">
	    <div class="col-md-7 col-xs-12">
	        <div class="x_panel">
	            <div class="x_title">
					<h3>Create User</h3>
	                <div class="clearfix"></div>
	            </div>
	            <form class="form-horizontal" role="form" method="POST" action="process-register" enctype="multipart/form-data">
		            <div class="x_content">
						<div class="col-md-12">
							<div class="form-group">
								<label class="col-md-3 control-label">First Name</label>
								<div class="col-md-9">
									<input type="text" class="form-control" required name="first_name" value="{{old('first_name')}}" placeholder="...">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Middle Name</label>
								<div class="col-md-9">
									<input type="text" class="form-control" name="middle_name" value="{{old('middle_name')}}" placeholder="...">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Last Name</label>
								<div class="col-md-9">
									<input type="text" class="form-control" required name="last_name" value="{{old('last_name')}}" placeholder="...">
								</div>
							</div>
							<hr/>
							<div class="form-group">
								<label class="col-md-3 control-label">Date of Birth</label>
								<div class="col-md-9">
									<input type="date" class="form-control" required name="dob" value="{{old('dob')}}" placeholder="">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Email</label>
								<div class="col-md-9">
									<input type="email" class="form-control" required name="email" value="{{old('email')}}" placeholder="">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Contact No.</label>
								<div class="col-md-9">
									<input type="number" class="form-control" required name="contact" value="{{old('contact')}}" placeholder="">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Address</label>
								<div class="col-md-9">
									<textarea class="form-control" required name="address">{{old('address')}}</textarea>
								</div>
							</div>
							<hr/>
							<div class="form-group">
								<label class="col-md-3 control-label">Role</label>
								<div class="col-md-9">
									<select name='usertype' class="form-control">
										<option value="1" @if(old('usertype') == '1') selected @endif>Member</option>
										<option value="2" @if(old('usertype') == '2') selected @endif>Concierge</option>
										<option value="3" @if(old('usertype') == '3') selected @endif>Admin</option>

									</select>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-3 control-label">Username</label>
								<div class="col-md-9">
									<input type="text" class="form-control" name="username" autocomplete="off" value="{{old('username')}}">
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-3 control-label">Password</label>
								<div class="col-md-9">
									<input type="password" class="form-control" name="password" autocomplete="off">
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-3 control-label">Confirm Password</label>
								<div class="col-md-9">
									<input type="password" class="form-control" name="password_confirmation" autocomplete="off">
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-9 col-md-offset-3">
									<button type="submit" class="btn btn-primary">
										Save
									</button>
									<a href="{{URL::to('res/users')}}">
										<button type="button" class="btn btn-default btn-no-shadow">
											Cancel
										</button>
									</a>
								</div>
							</div>
						</div>
		            </div>
		            <div class="x_footer">
		            </div>
		            <input type="hidden" name="_token" value="{{ Session::token() }}">
	            </form>
	        </div>
	    </div>
	</div>
@stop