<!-- top navigation -->
                <div class="top_nav">
                    <div class="nav_menu">
                        <nav class="" role="navigation">
                            <div class="nav toggle">
                                <a id="menu_toggle">&#9776;</a>
                            </div>

                            <ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <img src="{{asset("public/app/img/user.png")}}" alt="">{{Auth::user()->first_name == "" ? Auth::user()->username : Auth::user()->first_name}}
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                                        @if(Auth::user()->usertype == 2)
                                            <li><a href="{{URL::to("res/edit/user/". Auth::user()->username)}}"><i class="fa fa-user pull-right"></i> Edit Profile</a></li>
                                        @endif
                                        <li><a href="{{URL::to("res/logout")}}"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <!-- /top navigation -->