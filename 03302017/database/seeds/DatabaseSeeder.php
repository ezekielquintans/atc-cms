<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

        $this->call('UserSeeder');
        $this->call('CouponSeeder');
		$this->call('PromoSeeder');
        $this->call('RewardsSeeder');
	}

}

class CouponSeeder extends Seeder{
    private $noOfCoupons = 9999;
    private $categories  = ["food","fashion","retail","hnb","electronics","furniture","stationary"];
    private $stores      = ["starbucks","bench","mango","guess","chilis","mcdonalds","burgerking"];
    public function run(){
        for($x = 1; $x <= $this->noOfCoupons; $x++ ){

            DB::table('atc_entries')->insert([
                'user_id'    => rand(1,80),
                'category'   => $this->categories[rand(0, count($this->categories) - 1)],
                'store_name' => $this->stores[rand(0, count($this->stores) - 1)],
                'promo_id'   => rand(1,2),
                'receipt_no' => str_pad(rand(7000000,99999999), 8, '0', STR_PAD_LEFT),
                'token'      => strtoupper(substr(uniqid(),0, 8)),
                'unique_id'  => strtoupper(substr(uniqid(),0, 8)),
                'amount'     => rand(500,4000),
                'ipaddress'  => '127.0.0.1',
                'status'     => 1
            ]);
        }
    }
}

class UserSeeder extends Seeder{
	private $noOfUsers  = 80;
	private $firstNames = ["Sophia","Emma","Olivia","Isabella","Ava","Lily","Zoe","Chloe","Mia","Madison","Emily","Ella","Madelyn","Abigail","Aubrey","Addison","Avery","Layla","Hailey","Amelia","Hannah","Charlotte","Kaitlyn","Harper","Kaylee","Sophie","Mackenzie","Peyton","Riley","Grace","Brooklyn","Sarah","Aaliyah","Anna","Arianna","Ellie","Natalie","Isabelle","Lillian","Evelyn","Elizabeth","Lyla","Lucy","Claire","Makayla","Kylie","Audrey","Maya","Leah","Gabriella","Annabelle","Savannah","Nora","Reagan","Scarlett","Samantha","Alyssa","Allison","Elena","Stella","Alexis","Victoria","Aria","Molly","Maria","Bailey","Sydney","Bella","Mila","Taylor","Kayla","Eva","Jasmine","Gianna","Alexandra","Julia","Eliana","Kennedy","Brianna","Ruby","Lauren","Alice","Violet","Kendall","Morgan","Caroline","Piper","Brooke","Elise","Alexa","Sienna","Reese","Clara","Paige","Kate","Nevaeh","Sadie","Quinn","Isla","Eleanor","Aiden","Jackson","Ethan","Liam","Mason","Noah","Lucas","Jacob","Jayden","Jack","Logan","Ryan","Caleb","Benjamin","William","Michael","Alexander","Elijah","Matthew","Dylan","James","Owen","Connor","Brayden","Carter","Landon","Joshua","Luke","Daniel","Gabriel","Nicholas","Nathan","Oliver","Henry","Andrew","Gavin","Cameron","Eli","Max","Isaac","Evan","Samuel","Grayson","Tyler","Zachary","Wyatt","Joseph","Charlie","Hunter","David","Anthony","Christian","Colton","Thomas","Dominic","Austin","John","Sebastian","Cooper","Levi","Parker","Isaiah","Chase","Blake","Aaron","Alex","Adam","Tristan","Julian","Jonathan","Christopher","Jace","Nolan","Miles","Jordan","Carson","Colin","Ian","Riley","Xavier","Hudson","Adrian","Cole","Brody","Leo","Jake","Bentley","Sean","Jeremiah","Asher","Nathaniel","Micah","Jason","Ryder","Declan","Hayden","Brandon","Easton","Lincoln","Harrison"];
	private $lastNames  = ["Smith","Jones","Taylor","Williams","Brown","Davies","Evans","Wilson","Thomas","Roberts","Johnson","Lewis","Walker","Robinson","Wood","Thompson","White","Watson","Jackson","Wright","Green","Harris","Cooper","King","Lee","Martin","Clarke","James","Morgan","Hughes","Edwards","Hill","Moore","Clark","Harrison","Scott","Young","Morris","Hall","Ward","Turner","Carter","Phillips","Mitchell","Patel","Adams","Campbell","Anderson","Allen","Cook","Bailey","Parker","Miller","Davis","Murphy","Price","Bell","Baker","Griffiths","Kelly","Simpson","Marshall","Collins","Bennett","Cox","Richardson","Fox","Gray","Rose","Chapman","Hunt","Robertson","Shaw","Reynolds","Lloyd","Ellis","Richards","Russell","Wilkinson","Khan","Graham","Stewart","Reid","Murray","Powell","Palmer","Holmes","Rogers","Stevens","Walsh","Hunter","Thomson","Matthews","Ross","Owen","Mason","Knight","Kennedy","Butler","Saunders","Cole","Pearce","Dean","Foster","Harvey","Hudson","Gibson","Mills","Berry","Barnes","Pearson","Kaur","Booth","Dixon","Grant","Gordon","Lane","Harper","Ali","Hart","Mcdonald","Brooks","Ryan","Carr","Macdonald","Hamilton","Johnston","West","Gill","Dawson","Armstrong","Gardner","Stone","Andrews","Williamson","Barker","George","Fisher","Cunningham","Watts","Webb","Lawrence","Bradley","Jenkins","Wells","Chambers","Spencer","Poole","Atkinson"];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        /**
         * Create concierge
         */
        DB::table('atc_users')->insert([
            'username'    => "atc.concierge",
            'first_name'  => "concierge",
            'last_name'   => "",
            'middle_name' => "",
            'unique_id'   => strtoupper(uniqid()),
            'usertype'    => 2,
            'dob'         => '',
            'address'     => '',
            'contact'     => '',
            'email'       => 'atc.concierge@alabangtowncenter.ph',
            'status'      => 1,
            'password'    => bcrypt('secret')
        ]);

        /**
         * Create admin
         */
        DB::table('atc_users')->insert([
            'username'    => "atc.admin",
            'first_name'  => "admin",
            'last_name'   => "",
            'middle_name' => "",
            'unique_id'   => strtoupper(uniqid()),
            'usertype'    => 3,
            'dob'         => '',
            'address'     => '',
            'contact'     => '',
            'email'       => 'atc.admin@alabangtowncenter.ph',
            'status'      => 1,
            'password'    => bcrypt('secret')
        ]);

        for($x = 1; $x <= $this->noOfUsers; $x++ ){
            $firstName = $this->firstNames[rand(0, count($this->firstNames) - 1)];
            $lastName  = $this->lastNames[rand(0, count($this->lastNames) - 1)];
            $middleName  = $this->lastNames[rand(0, count($this->lastNames) - 1)];
            // $email     = strtolower($firstName . "." . $lastName ."@sampleemail.com");
            $email = "leikxile@gmail.com";
            $username  = strtolower($firstName . "." . $lastName);
            DB::table('atc_users')->insert([
                'username'    => $username,
                'first_name'  => $firstName,
                'last_name'   => $lastName,
                'middle_name' => $middleName,
                'unique_id'   => strtoupper(uniqid()),
                'usertype'    => 1,
                'dob'         => '1992-04-04',
                'address'     => 'Philippines',
                'contact'     => '7723151',
                'email'       => $email,
                'status'      => 1,
                'password'    => bcrypt('secret')
            ]);
        }
    }
}

class PromoSeeder extends Seeder{
    public function run()
    {
        /**
         * Create Promos
         */
        DB::table('atc_promos')->insert([
            "name"        => "CELEBRATE & FEAST! LUCK IS ON YOUR PLATE",
            "subtitle"    => 'Dine now and Get a chance to win a <br/> "Swatch Rocking Rooster Watch"',
            "description" => '<ol>
                                                <li>
                                                    <p>The promo is from January 23-February 12, 2017.</p>
                                                    <p>The promo is open to Alabang Town Center diners in the following participating Asian Food establishments:</p>
                                            ​​      <ul>
                                                        <li>Abe</li>
                                                        <li>Balot Balot</li>
                                                        <li>Bonchon</li>
                                                        <li>Bulgogi Brothers</li>
                                                        <li>Cabalen</li>
                                                        <li>Chatime</li>
                                                        <li>Crisostomo</li>
                                                        <li>Elephant Express</li>
                                                        <li>El Presidente of Binondo</li>
                                                        <li>Gerry’s Grill</li>
                                                        <li>Giligan’s</li>
                                                        <li>Gloria Maris</li>
                                                        <li>Gongcha</li>
                                                        <li>Hanamaruken</li>
                                                        <li>Healthy Shabu-Shabu</li>
                                                        <li>Ikkoryu Fukuoka Ramen</li>
                                                        <li>Jipan</li>
                                                        <li>John & Yoko</li>
                                                        <li>Kenjitei</li>
                                                        <li>Kim N’ Chi</li>
                                                        <li>Luk Yuen</li>
                                                        <li>Mary Grace Café</li>
                                                        <li>Max’s Restaurant</li>
                                                        <li>Mongolian Quickstop</li>
                                                        <li>Nanbantei</li>
                                                        <li>Northpark</li>
                                                        <li>Pancake House</li>
                                                        <li>Papermoon</li>
                                                        <li>Pepper Lunch</li>
                                                        <li>PhoHoa</li>
                                                        <li>Razon’s</li>
                                                        <li>Recipes by Café Metro</li>
                                                        <li>Rock & Seoul</li>
                                                        <li>Santouka</li>
                                                        <li>Seafood Island</li>
                                                        <li>Serenitea</li>
                                                        <li>Shilin</li>
                                                        <li>Shou</li>
                                                        <li>Soban</li>
                                                        <li>Soi</li>
                                                        <li>Ted’s Lapaz Batchoy</li>
                                                        <li>Tempura</li>
                                                        <li>The Boiling Seafood</li>
                                                        <li>Wee Nam Kee</li>
                                                        <li>Yabu</li>
                                                        <li>Yakimix</li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    Diner is entitled to one (1) electronic raffle coupon for every P500 worth of single/accumulated receipts from participating restaurants of Alabang Town Center to be redeemed at the concierge.
                                                </li>
                                                <li>
                                                    Diner must register by providing the following details: complete name, address, email address, contact number/s (office phone number, house phone number or mobile phone number) and age.
                                                </li>
                                                <li>
                                                    Upon registration, concierge will input all the details of the diner to generate number of raffle coupon/s.
                                                </li>
                                                <li>
                                                    Diner will receive a copy of the generated receipts indicating number of coupons redeemed per transaction and total number of coupons to date.
                                                </li>
                                                <li>
                                                    Only receipts issued from January 23-February 12, 2017 are eligible.  Defaced, crumpled, tampered or receipts that are less than ¾ its actual size will not be honored.
                                                </li>
                                                <li>
                                                    There is a total of five (5) Swatch Rocking Rooster watches for the promo.  The schedule of draw dates, eligibility and prizes are as follows:
                                                    <table class="table" style="font-size:10px;">
                                                        <tr class="text-center">
                                                            <td>Draw Date<br/>(Time:  4:00pm)</td>
                                                            <td>Last Day of Submission of Entries (by 9:00pm)</td>
                                                            <td>Prizes</td>
                                                        </tr>
                                                        <tr class="text-center">
                                                            <td>February 13, 2017 (Friday)</td>
                                                            <td>February 12, 2017 (Thursday)</td>
                                                            <td>Five (5) winners of a Swatch Rocking Rooster Watch</td>
                                                        </tr>
                                                    </table>
                                                    *** A DTI representative will witness the raffle draw.
                                                </li>
                                                <li>
                                                    A diner may only win once during the entire promo.
                                                </li>
                                                <li>
                                                    The prizes are subject to the following conditions:
                                                    <ul>
                                                        <li>
                                                            All prizes are transferable but not convertible to cash.
                                                        </li>
                                                        <li>
                                                            Prizes not claimed after sixty (60) days from date of notification will be forfeited in favor of Alabang Commercial Corporation with prior approval of DTI.
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    Winners will be notified by telephone call and e-Mail.  The winners’ names will also be posted at the Alabang Town Center’s official Facebook and Twitter accounts
                                                </li>
                                                <li>
                                                    Winners may claim their prizes at the Alabang Town Center Administration Office from Monday to Friday from 10am-6pm.
                                                </li>
                                                <li>
                                                    To claim the prizes, winners must present the notice sent by e-mail, and any two (2) valid Government-issued identification card with photo and signature.
                                                </li>
                                                <li>
                                                    Employees of Alabang Commercial Corporation, Gift Gate Inc., their merchants, maintenance and security agencies, and advertising and promotional agencies, and their relatives up to the second degree of affinity or consanguinity, are disqualified from joining the promo.
                                                </li>
                                            </ol>',
            "helper"         => "<h1>One (1) raffle coupon <br> for every 500 php purchase </h1>",
            "dti_permit"     => "DTI - FTEB Permit No. 0154, Series of 2017",
            "image_url"      => "public/landing/img/promo-logo-2.jpg",
            "amt_per_coupon" => 500.00,
            "date_started"   => "2017-01-01 00:00:00",
            "date_ended"     => "2017-12-30 23:59:00"
        ]);

        DB::table('atc_promos')->insert([
            "name"           => "FOOD HOLIDAY PROMO 2017",
            "subtitle"       => '',
            "description"    => "",
            "helper"         => "<h1>One (1) raffle coupon <br> for every 1000 php receipt </h1>",
            "dti_permit"     => "DTI - FTEB Permit No. 0154, Series of 2017",
            "image_url"      => "public/landing/img/promo-logo-3.jpg",
            "amt_per_coupon" => 1000.00,
            "date_started"   => "2017-01-01 00:00:00",
            "date_ended"     => "2017-12-30 23:59:00"
        ]);
    }
}

class RewardsSeeder extends Seeder{
    public function run()
    {
        /**
         * Create rewards
         */
        DB::table('atc_rewards')->insert([
            "promo_id" => 2,
            "name"     => "Cinema Tickets"
        ]);
    }
}
