<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRewardsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('atc_rewards', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 150);
			$table->integer('promo_id');
			$table->integer('rewards_count');
			$table->string('venue',150);
			$table->string('featured_img',150);
			$table->datetime('datetime');
			$table->text('details');
			$table->integer('status')->default(1);
			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->default('0000-00-00 00:00:00');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('atc_rewards');
	}

}
