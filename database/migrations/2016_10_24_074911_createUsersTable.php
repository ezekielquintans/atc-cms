<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('atc_users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('unique_id',60);
			$table->string('first_name',32);
			$table->string('last_name',32);
			$table->string('middle_name',32);
			$table->string('username',60);
			$table->string('remember_token');
			$table->string('email',40);
			$table->string('password',70);
			$table->longText('address');
			$table->date('dob');
			$table->string('contact');
			$table->tinyInteger('usertype');
			$table->tinyInteger('status');
			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->default('0000-00-00 00:00:00');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('atc_users');
	}

}
