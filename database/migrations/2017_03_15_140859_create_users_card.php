<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersCard extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('atc_users_card', function ($table) {
			$table->increments('id');
			$table->integer('user_id');
			$table->string('beep_paywave', 10)->nullable();
			$table->string('new_renewal', 10)->nullable();
			$table->string('card_title')->nullable();
			$table->string('card_type')->nullable();
			$table->string('source_of_income')->nullable();
			$table->string('name_of_ofw')->nullable();
			$table->string('name_in_card')->nullable();
			$table->string('relationship')->nullable();
			$table->string('document_presented')->nullable();
			$table->string('document_no')->nullable();
			$table->string('birthday')->nullable();
			$table->string('card_no')->nullable();
			$table->string('card_expiry')->nullable();
			$table->string('amount')->nullable();
			$table->string('ar_no')->nullable();
			$table->string('remarks')->nullable();
            $table->timestamps();
            $table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('atc_users_card');
	}

}
