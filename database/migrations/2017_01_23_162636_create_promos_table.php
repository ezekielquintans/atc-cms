<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('atc_promos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 255);
			$table->text('subtitle');
			$table->text('description');
			$table->string('image_url', 255);
			$table->text('helper');
			$table->string('dti_permit', 255);
			$table->integer('reward_id');
			$table->tinyInteger('promo_type');
			$table->tinyInteger('raffle_premise');
			$table->tinyInteger('raffle_accumulation_type');
			$table->integer('raffle_purchase_requirement');
			$table->tinyInteger('raffle_multiplier');
			$table->tinyInteger('premium_premise');
			$table->tinyInteger('premium_accumulation_type');
			$table->integer('premium_purchase_requirement');
			$table->tinyInteger('premium_ceiling');
			$table->tinyInteger('premium_multiplier');			
			$table->tinyInteger('premium_isEvent');			
			$table->date('date_started');
			$table->date('date_ended');
			$table->tinyInteger('status')->default(1);
			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->default('0000-00-00 00:00:00');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('atc_promos');
	}

}
