function showBpiForm(event) {
    if($('#card-type-bpi')[0].checked == true) {
        $("#bpi-settings-container").removeClass('hidden');
        $("#bpi-settings-container input").each(function(index, obj){
            $(obj).attr('disabled', false);
        });
    } else {
        $("#bpi-settings-container")
          .addClass('hidden')
          .find('input[type=radio]')
          .iCheck('uncheck');
        $("#bpi-settings-container")
          .find(':input[type=number]')
          .val("");
        $("#bpi-settings-container input").each(function(index, obj){
            $(obj).attr('disabled', true);
        });
        $(".right_col").css('min-height', 'auto');
    }
};

function showVipForm(event) {
    if($('#card-type-vip')[0].checked == true) {
        $("#vip-settings-container").removeClass('hidden');
        $("#vip-settings-container input").each(function(index, obj){
            $(obj).attr('disabled', false);
        });
    } else {
        $("#vip-settings-container")
          .addClass('hidden')
          .find('input[type=radio]')
          .iCheck('uncheck');
        $("#vip-settings-container")
          .find(':input[name="vip_purchase_requirement"]')
          .val("");
        $("#vip-settings-container")
          .find(':input[name="vip_ceiling"]')
          .val("1");
        $("#vip-settings-container input").each(function(index, obj){
            $(obj).attr('disabled', true);
        });
        $(".right_col").css('min-height', 'auto');
    }
};

$(document).ready(function() {
    $(".datetime").daterangepicker({
        singleDatePicker: true,
        "timePicker24Hour": true,
        timePickerIncrement: 5,
        showDropdowns: true,
        minDate: '1920-01-01',
        // startDate: moment(),
        buttonClasses: ['btn-sm-fix','btn-sm'],
        locale: { format: "YYYY-MM-DD" },
    });

    /*Promo type switches*/
    $('#card-type-bpi').click(function() {
        showBpiForm();
        $("html, body").animate({ scrollTop: $(document).height() }, 700);
    });
    $('#card-type-vip').click(function() {
        showVipForm();
        $("html, body").animate({ scrollTop: $(document).height() }, 700);
    });

    if($('#card-type-bpi')[0].checked == true) {
        showBpiForm();
    }
    if($('#card-type-vip')[0].checked == true) {
        showVipForm();
    }

    var checkContactInput = function(e) {
        var input = e.target;
        var inputLength = input.value.length;
        var allowedCharsArray = [106, 107, 109, 110, 187, 189];

        if ($.inArray(e.keyCode, allowedCharsArray) >= 0) {
            e.preventDefault();
        }
        if(inputLength == 0) {
            if (e.keyCode == 48 || e.keyCode == 96) {
                e.preventDefault();
            }
        }
    };
    $(".contact-input").each(function(index, object) {
        $(object).keydown(checkContactInput);
    });
});
