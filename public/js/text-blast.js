$(document).ready(function() {
    var token = $('meta[name="req"]').attr('content');
    var sessionId;
    var requestCountCheckTimer;
    // Get credentials
    $.ajax({
        url: '../res/textblast/getCredentials',
        method: 'get',
        data: {'_token': token},
        success: function(response, status) {
            var parsed = $.parseJSON(response);
            sessionId = parsed.session_Id;
            // console.log(parsed);
        },
        error: function(xhr, status, error) {
            //
        }
    });

    $("#recipients").tokenfield({
        delimiter: [',', ' '],
        inputType: 'text'
    });

    $("#message").keyup(function(e) {
        var messageLength = $(e.target).val().length;
        var sendButton = $("#sendTextBlast");
        if (messageLength > 0) {
            sendButton.prop("disabled", "");
            sendButton.removeClass("btn-default");
            sendButton.addClass("btn-success");
        } else {
            sendButton.prop("disabled", "disabled");
            sendButton.removeClass("btn-success");
            sendButton.addClass("btn-default");
        }
    });

    if($("#overrideRecipientsSwitch").is(":checked")) {
        $("#filtersPanel").hide();
    } else {
        var recipientsPanel = $("#recipientsPanel");
        recipientsPanel.hide(250);
        $("#recipients").prop("disabled", "disabled");

        if($("#filterRecipientsSwitch").is(":checked")) {
            $("#filtersPanel").show();
        }
    }

    $("#textBlastModal").on('hidden.bs.modal', function() {
        location.reload();
    });

    function checkRequestResponseCount(max) {
        var current = $("#sentSmsSpan").text();
        if (current == max) {
            $("#sendingMessaagesDiv").fadeOut(250);
            $("#messagesSentDiv").fadeIn(250);
            $("#close-textblast-modal").fadeIn(250);

            // $("#selected-promo-form")[0].reset();
            $(".token").each(function(index, token) {
                token.remove();
            });
            $("#mainForm")[0].reset();

            clearInterval(requestCountCheckTimer);
        }
        // console.log(max + ":" + current);
    }

    $("#sendTextBlast").click(function(e) {
        e.preventDefault();
        $("#close-textblast-modal").hide();
        $("#textBlastModal").modal('show');
        var formData = $(this).closest("form").serialize();
        var recipientOverrideDisabled = $("#recipients").is(":disabled");
        var message = $("#message").val();
        messageArray =  message.length <= 160 ? [message] : splitInto(message,153);
        // console.log(formData);

        // saveTextBlastActivity(message);

        $(".smsSuccess").text(0);
        $(".smsFailed").text(0);
        $("#sentSmsSpan").text(0);
        $("#sendingMessaagesDiv").fadeIn(150);
        $("#messagesSentDiv").fadeOut(150);

        if (recipientOverrideDisabled) {
            $.get('../res/textblast/prepare',formData,function(data) {
                var recipients = $.parseJSON(data);
                    if(recipients.length){
                        $("#loadingMsg").fadeOut(150);
                        $("#statusMsg").fadeIn(150);
                        $(".recipientCount").text(recipients.length);
                        // sendSmsAjax(recipients, message);
                    }
            }).fail(function(){
                alert("request fail");
            });
        } else {
            var recipients = $("#recipients").val().split(',');
            $("#loadingMsg").fadeOut(150);
            $("#statusMsg").fadeIn(150);
            $(".recipientCount").text(recipients.length);

            // sendSmsAjax(recipients, message);           
            sendSmsAjax(recipients, messageArray);
        }
    });

    function saveTextBlastActivity(message) {
        $.ajax({
            url: '../res/textblast/saveActivity',
            method: 'post',
            data: {'_token': token, 'message': message},
            success: function(response, status) {
                // console.log(response);
            },
            error: function(xhr, status, error) {
                //
            }
        });
    }

    function sendSmsAjax(recipients, message) {
        var requestsThatResponded = 0;
        var sentMessages = 0;
        var successMessages = 0;
        var failedMessages = 0;
 
        requestCountCheckTimer = setInterval(checkRequestResponseCount, 500, recipients.length);
        // console.log(recipients); return false;
        $(recipients).each(function(index, recipient) {
            setTimeout(function() {
                $.ajax({
                    url: '../res/textblast/send',
                    method: 'post',
                    data: {'sessionId': sessionId ,'recipient':recipient, 'message': message, '_token': token},
                    success: function(response, status) {
                        var parsed = $.parseJSON(response);
                        console.log(parsed);

                        if(parsed.success == true) {
                            successMessages = successMessages + 1;
                            $(".smsSuccess").text(successMessages);
                        } else {
                            failedMessages = failedMessages + 1;
                            $(".smsFailed").text(failedMessages);
                        }
                    },
                    error: function(xhr, status, error) {
                        failedMessages = failedMessages + 1;
                        $(".smsFailed").text(failedMessages);
                    },
                    complete: function() {
                        requestsThatResponded = requestsThatResponded + 1;
                        sentMessages = sentMessages + 1;
                        $("#sentSmsSpan").text(sentMessages);
                    }
                }, 100);
            });                        
        });
    }

    var toggleRecipientOverride = function(e) {
        var filtersFormGroup = $("#filtersFormGroup");
        var recipientsPanel = $("#recipientsPanel");
        var overrideRecipients = e.target.checked;
        if (overrideRecipients) {
            filtersFormGroup.slideUp(250);
            recipientsPanel.slideDown(250);
            $("#recipients").removeAttr("disabled");
            $(".tokenfield").removeClass("disabled");
        } else {
            filtersFormGroup.slideDown(250);
            recipientsPanel.slideUp(250);
            $("#recipients").prop("disabled", "disabled");
        }

        filtersFormGroup.find("input").each(function(index, input) {
            if (overrideRecipients) {
                $(input).prop("disabled", "disabled");
            } else {
                // $("#filterRecipientsSwitch").checked = false;
                console.log($("#filterRecipientsSwitch"));
                $(input).val(null);
                $(input).iCheck('uncheck');
                $(input).removeAttr("disabled");
            }
        });

        recipientsPanel.find("textarea").each(function(index, input) {
            if (overrideRecipients) {
                $(input).removeAttr("disabled");
            } else {
                $(input).val(null);
                $(input).prop("disabled", "disabled");
            }
        });
        recipientsPanel.find("input").each(function(index, input) {
            if (overrideRecipients) {
                $(input).removeAttr("disabled");
            } else {
                $(input).val(null);
                $(input).prop("disabled", "disabled");
            }
        });
    };
    $("#overrideRecipientsSwitch").change(toggleRecipientOverride);

    var toggleRecipientFilters = function(e) {
        var filtersPanel = $("#filtersPanel");
        var filterUsers = e.target.checked;
        if (filterUsers) {
            filtersPanel.slideDown(250);
        } else {
            filtersPanel.slideUp(250);
        }

        filtersPanel.find("input").each(function(index, input) {
            if (filterUsers) {
                $(input).removeAttr("disabled");
            } else {
                $(input).val(null);
                $(input).iCheck('uncheck');
                $(input).prop("disabled", "disabled");
            }
        });
    };
    // $("#filterRecipientsSwitch").change(toggleRecipientFilters);
    $("#filterRecipientsSwitch").bind('change', toggleRecipientFilters);

    var formHasChanges = false;
    $("#mainForm input").each(function(index, object) {
        $(object).bind('change', function(e) {
            formHasChanges = true;
        });
    });

    $("#mainForm textarea").each(function(index, object) {
        $(object).bind('change', function(e) {
            formHasChanges = true;
        });
    });


    var calculateCharacters = function(e) {
        var currentLength = this.value.length;
        var currentMessages = Math.ceil(currentLength / 160);

        var messageCharsLeft = $("#messageCharsLeft");
        var messageCount = $("#messageCount");
        var messageCountText = $("#messageCountText");

        if (currentLength == 0) {
            messageCharsLeft.text(160);
            messageCount.text(1);
        } else {
            messageCharsLeft.text((160 * currentMessages) - currentLength);
            messageCount.text(currentMessages);
            if (messageCount == 1) {
                messageCountText.text("message");
            } else {
                messageCountText.text("messages");
            }        

        }
    };

    var splitInto = function(str, l) {
        var strs = [];
        while(str.length > l){
            var pos = str.substring(0, l).lastIndexOf(' ');
            pos = pos <= 0 ? l : pos;
            strs.push(str.substring(0, pos));
            var i = str.indexOf(' ', pos)+1;
            if(i < pos || i > pos+l)
                i = pos;
            str = str.substring(i);
        }
        strs.push(str);
        return strs;
    };
    
    $("#message").keyup(calculateCharacters);
    $("#message").keydown(calculateCharacters);

    var previousSelectedPromo;

    $("#promoSelect").on("focus", function() {
            previousSelectedPromo = this.value;
    }).change(function(e) {
        var messageBox = $("#message");

        if (formHasChanges) {
            var confirmPageLeave = window.confirm('It looks like you have been editing something. '
                                + 'If you leave before saving, your changes will be lost.');

            (e || window.event).returnValue = confirmPageLeave; //Gecko + IE

            if(confirmPageLeave) {
                this.closest("form").submit();
            } else {
                e.preventDefault();
                this.value = previousSelectedPromo;
            }
        } else {
            this.closest("form").submit();
        }
    });
});
