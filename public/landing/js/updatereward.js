$(document).ready(function() {
	$(".img-upload").change(function(event) {
        	if (this.files && this.files[0]) {
	            var self = this;
	            var validImageTypes = ["image/jpeg", "image/png"];
	            var reader = new FileReader();
	            if ($.inArray(this.files[0].type, validImageTypes) < 0) {
	                 alert("Invalid File type. Please upload an image");
	                 $(this).val("");
	                 return false;
	            }
	            reader.onload = function (e) {
	              	$(self).siblings('img').attr('src', e.target.result).removeClass('hidden');
	            }
	            reader.readAsDataURL(this.files[0]);
        	}else{
            	alert("Preview not available.")
        	}
    	});
	$(".datetime").daterangepicker({
		singleDatePicker: true,
		timePicker: true,
		"timePicker24Hour": true,
		timePickerIncrement: 5,
		showDropdowns: true,
		minDate: moment(),
		startDate: moment(),
		buttonClasses: ['btn-sm-fix','btn-sm'],
		locale: { format: "YYYY-MM-DD h:mm:ss" },
  	}); 
	$(".ranges").css({
		margin: '0 auto',
		"text-align": 'center'
	});
	
	$("#adv-set-btn").click(function(event) {
	  	if($(this)[0].checked == true){
	  		$(".advance-settings").removeClass('hidden');
	  	}else{
	  		$(".advance-settings").addClass('hidden');
	  		$(":input[name=venue]").val("");
	  		$("textarea").val("");
	  		$("#editor").html("");
	  	}
	});

  	$('#details').keydown(function(event) {
  		var length = 100 - $(this).val().length;
  		if(length >= 0){
  			$("#char-count").html('Characters left:' + length);
  		} 
  	});
});