Entries = function() {
    this.rewardsClaimed = 0;
    this.rows = 1;

    this.init = function() {
        alert('asd');
        this.search();
        this.selectPromo();
        this.processPromo();
        this.addReceipt();
        this.processCoupons();
        this.processRewards();
    }

    this.search = function() {
        var self = this;
        $('#search').autocomplete({
            serviceUrl: '../search/autocomplete',
            showNoSuggestionNotice: true,
            minChars: 3,
            onSelect: function(suggestion) {
                $('.overlay').show();
                $("body").css("overflow", "hidden");
                $.get('../getUserInfo', { userid: suggestion.data }, function(data) {
                    $('.overlay').hide();
                    $("body").css("overflow", "auto");

                    $("#fname").val(data.first_name);
                    $("#lname").val(data.last_name);
                    $("#email").val(data.email);
                    $("#address").val(data.address);
                    $("#contact").val(data.contact);
                    $("#cusID").val(data.id);
                    $("#cusIDDisplay").val(data.display_id);
                    self.rewardsClaimed = data.rewardsClaimedToday;
                });
            }
        });
    }

    this.selectPromo = function() {
        var self = this;
        $('select#sel-promo').change(function(event) {
            self.promo = $(this).val();
            self.promoSettings = {
                promoTitle: $(this).find(':selected').text(),
                promoType: $(this).find(':selected').data('type'),
                rewardName: $(this).find(':selected').data('rewards'),
                rafflePremise: $(this).find(':selected').data('raffle_premise'),
                raffleAccumulationType: $(this).find(':selected').data('raffle_accumulation_type'),
                rafflePurchaseRequirement: $(this).find(':selected').data('raffle_purchase_requirement'),
                raffleMultiplier: $(this).find(':selected').data('raffle_multiplier'),
                premiumPremise: $(this).find(':selected').data('premium_premise'),
                premiumAccumulationType: $(this).find(':selected').data('premium_accumulation_type'),
                premiumPurchaseRequirement: $(this).find(':selected').data('premium_purchase_requirement'),
                premiumCeiling: $(this).find(':selected').data('premium_ceiling'),
                premiumMultiplier: $(this).find(':selected').data('premium_multiplier'),
            };
            $(".amount").val('');
            self.resetInputValues();
            if (self.promo) {
                console.log(self.promoSettings);
                $(".entry-info").removeClass('hidden');
                $("#promo-type").val(self.promoSettings.promoType);
                var premise = '(1 Raffle coupon for every ' + self.promoSettings.rafflePurchaseRequirement + ' Php amount of purchase)';
                if (self.promoSettings.rafflePremise == 1) {
                    premise = '(1 Raffle coupon for a minimum of ' + self.promoSettings.rafflePurchaseRequirement + ' Php amount of purchase)';
                }
                $(".entry-info .x_title h2").html(self.promoSettings.promoTitle + '<br /><small id="coupon-mechanics" style="margin-left:0px;">' + premise + '</small>')
                if (self.promoSettings.raffleAccumulationType != 0) {
                    $(".modes-cont").show();
                    if (self.promoSettings.raffleAccumulationType == 1) {
                        $(".mode").iCheck("disable");
                        $("#modeS").iCheck("check");
                    }
                    if (self.promoSettings.raffleAccumulationType == 2) {
                        $(".mode").iCheck("disable");
                        $("#modeA").iCheck("check");
                    }
                    if (self.promoSettings.raffleAccumulationType == 3) {
                        $(".mode").iCheck("enable");
                        $(".mode").iCheck("uncheck");
                    }
                }
                if (self.promoSettings.promoType == 2 || self.promoSettings.promoType == 3) {
                    $(".rewards-cont").removeClass('hidden');
                    $(".reward-label").html(self.promoSettings.rewardName + ":");
                    if (self.rewardsClaimed > 0 && self.rewardsClaimed < self.promoSettings.premiumCeiling) {
                        $("#rewardsText").text('claimed ' + self.rewardsClaimed + ' already');
                    }
                    if (self.rewardsClaimed <= 0) {
                        $("#rewardsText").text('0');
                    }
                    if (self.rewardsClaimed >= self.promoSettings.premiumCeiling) {
                        $("#rewardsText").text('claimed already');
                    }
                } else {
                    $(".rewards-cont").addClass('hidden');
                }
                if (self.promoSettings.raffleMultiplier == 1) {
                    $(".with-multiplier").removeClass('hidden');
                } else {
                    $(".with-multiplier").addClass('hidden');
                }
            } else {
                $(".entry-info").addClass('hidden');
            }
        });
    }

    this.resetInputValues = function() {
        $("#noCoupons").val('0');
        $("#totalCoupons").text('0');
        $("#totalAmount").text('0');
        $("#rewardsText").text('0');
        if (this.rewardsClaimed == 1) {
            $("#rewardsText").text('claimed 1 ' + self.promoSettings.rewardName + ' already');
        }
        $("#ticketsEarned").val('0');
    }

    this.processPromo = function() {
        var self = this;
        $(document).on('keyup', '.amount', function(event) {
            var amount = $(this).val();
            var coupons = 0;
            var totalAmount = 0;
            var totalCoupons = 0;
            var additionalCoupons = 0;
            var tickets;
            var mode = $("input[name=mode]:checked").val();
            var multiplier = $(this).parentsUntil('.row').siblings('.with-multiplier').find('.multiplier');
            var chosenPromo = $('#sel-promo').val();
            if (Math.floor(amount) == amount && $.isNumeric(amount)) {
                $('.amount').each(function() {
                    totalAmount += parseInt($(this).val());
                });
                if (chosenPromo == 1) {
                    coupons = Math.floor(amount / 500);
                    $(this).siblings('.coupons').val(coupons);
                    if (mode == "S") {
                        $('.coupons').each(function() {
                            totalCoupons += parseInt($(this).val());
                        });
                    } else if (mode == "A") {
                        totalCoupons = Math.floor(totalAmount / 500);
                    }
                    $(".multiplier").each(function() {
                        if ($(this).is(':checked')) {
                            var stockCoupon = $(this).parentsUntil('.row').siblings('.amt-grp').find('.coupons');
                            additionalCoupons += parseInt(stockCoupon.val());
                        }
                    });
                } else if (chosenPromo == 2) {
                    coupons = amount >= 1000 ? 1 : 0;
                    $(this).siblings('.coupons').val(coupons);
                    $('.coupons').each(function() {
                        totalCoupons += parseInt($(this).val());
                    });
                    if (rewardsClaimed != 'claimed') {
                        if (totalAmount >= 1000 && totalAmount < 2000) {
                            tickets = 1;
                        }
                        if (totalAmount >= 2000) {
                            if (rewardsClaimed == 1) {
                                tickets = 1;
                            } else {
                                tickets = 2;
                            }
                        }
                        if (totalAmount < 1000) {
                            tickets = 0;
                        }
                    }
                }
                totalCoupons += additionalCoupons;
                if (totalCoupons >= 1) {
                    if (mode == "S") {
                        $(multiplier).removeAttr('disabled');
                    }
                    $("#noCoupons").val(totalCoupons);
                    $("#totalCoupons").text(totalCoupons);
                    $("#totalAmount").text(totalAmount);
                    if (chosenPromo == 2 && rewardsClaimed != 'claimed') {
                        $("#rewardsText").text(tickets);
                        if (rewardsClaimed == 1) {
                            $("#rewardsText").text(tickets + ' - claimed 1 ticket already');
                        }
                        $("#ticketsEarned").val(tickets);
                        $("#noTickets").val(tickets);
                    } else {
                        $("#ticketsEarned").val('0');
                        $("#noTickets").val(tickets);
                    }
                } else {
                    if (chosenPromo == 2 && rewardsClaimed != 'claimed') {
                        resetVals();
                        $("#ticketsEarned").val('0');
                    }
                }
            }
        });
        $(document).on('change', '.store', function() {
            var category = $('option:selected', this).data('category');
            $(this).closest('div.entry-row').find('input.category').val(category);
        });
        $(document).on('click', '.multiplier', function(event) {
            $(".amount").first().trigger('keyup');
        });
        $("input[type=radio][name=mode]").on('ifChecked', function(event) {
            if (event.currentTarget.value == "A") {
                $(".multiplier").each(function() {
                    $(this).attr({
                        "disabled": true,
                        'checked': false
                    });;
                });
            } else if (event.currentTarget.value == "S") {
                $(".multiplier").each(function() {
                    $(this).removeAttr('disabled').removeAttr('checked');
                });
            }
            $(".amount").first().trigger('keyup');
        });
    }

    this.addReceipt = function() {
        var self = this;
        $(document).on('click', "#add-receipt", function(event) {
            event.preventDefault();
            self.rows++;
            var newRow = $("#coupon-form-container")
                .children('.row')
                .first()
                .clone(true)
                .find(".multiplier").attr({
                    'checked': false,
                    'disabled': true
                }).end()
                .find("input:text").val("").end()
                .find('select').attr('id', 'store-' + self.rows).end()
                .find(':input[type="number"]').val("").end()
            newRow.appendTo('#coupon-form-container');
            var noOfRows = parseInt($("#norows").val());;
            noOfRows += 1;
            $("#norows").val(noOfRows);
            // $('#store-'+ctr).chosen();
        });
    }

    this.processCoupons = function() {
        var self = this;
        $('#submit-coupons').click(function() {
            $('.overlay').css('height', $(document).height() + 'px');
            var chosenPromo = $('#sel-promo').val();
            var ticketsEarned = $("#noTickets").val();
            var ticketForm = '<div class="form-group tickets-form-group">' +
                '<label class="control-label col-md-4 col-sm-4 col-xs-4">Ticket no.</label>' +
                '<div class="col-md-8 col-sm-8 col-xs-8">' +
                '<input type="text" class="ticket-text form-control" required placeholder="Enter ticket number" name="tickets[]">' +
                '</div>' +
                '</div>';
            $('.tickets-info-modal .modal-body .row').html('');
            if (ticketsEarned != "" || ticketsEarned != 0) {
                for (var i = ticketsEarned - 1; i >= 0; i--) {
                    $('.tickets-info-modal .modal-body .row').append(ticketForm);
                }
            }
            self.validateEntryForms(chosenPromo, rewardsClaimed);
        });
    }
    this.processRewards = function() {
        $(".save-tickets").click(function(event) {
            // $('.tickets-info-modal').hide();
            $('.overlay').css('height', $(document).height() + 'px');
            $('.overlay').html("Saving tickets...").show();
            var valid = true;
            var requestCount = $('input.ticket-text').length;
            var ctr = 0;
            $('.ticket-text').filter('[required]:visible').each(function() {
                if ($(this).val() == "") {
                    valid = false;
                    $(this).css('border', '2px solid red');
                    // $('.tickets-info-modal').show();
                } else {
                    $(this).css('border', '1px solid #ccc');
                }
            });
            $('input.ticket-text').each(function() {
                var input = $(this);
                var serialno = $(this).val();
                if (valid === true) {
                    $.get('../validate-reward?serialno=' + serialno + '&promoid=2', function(data) {
                        if (data == "false") {
                            valid = false;
                            input.css('border', '2px solid orange');
                            input.closest('div.form-group').find('label.control-label').html('Ticket no.<small style="color:red"> *Already used.</small>');
                        } else {
                            input.css('border', '2px solid green');
                            input.closest('div.form-group').find('label.control-label').html('Ticket no.');
                        }
                        ctr++;
                        if (ctr == requestCount) {
                            $("body").css("overflow", "auto");
                            $('.overlay').html("Loading member's information, please wait...").hide();
                            if (valid === true) {
                                $("body").css("overflow", "hidden");
                                $('.overlay').html("Adding entry, please wait...").show();
                                $('.tickets-info-modal').hide();
                                $('#user-form').submit();
                            }
                        }

                    });
                }
            });
        });
    }
    this.validateEntryForms = function(chosenPromo, rewardClaimed) {
        var valid = true;
        $('input,textarea,select').filter('[required]:visible').each(function() {
            if ($(this).val() == "") {
                valid = false;
                $(this).css('border', '2px solid red');
            } else {
                $(this).css('border', '1px solid #ccc');
            }
        });

        var requestCount = $('input.invoice-text').length;
        var ctr = 0;
        if (valid === true) {
            $('.overlay').html("Validating entry, please wait...").show();
            $("body").css("overflow", "hidden");
        }

        $('input.invoice-text').each(function() {
            var input = $(this);
            var invoice = $(this).val();
            var store = $(this).closest('div.row').find('select.store').val();

            if (valid === true) {
                $.get('../validate-invoice?store=' + store + '&invoice=' + invoice, function(data) {
                    if (data == "false") {
                        valid = false;
                        input.css('border', '2px solid orange');
                        input.closest('div.form-group').find('label.control-label').html('Invoice Number<small style="color:red"> *Already used.</small>');
                    } else {
                        input.css('border', '2px solid green');
                        input.closest('div.form-group').find('label.control-label').html('Invoice Number');
                    }
                    ctr++;
                    if (ctr == requestCount) {
                        $("body").css("overflow", "auto");
                        $('.overlay').html("Loading member's information, please wait...").hide();
                        if (chosenPromo == 1 && valid === true) {
                            $("body").css("overflow", "hidden");
                            $('.overlay').html("Adding entry, please wait...").show();
                            $('#user-form').submit();
                        }
                        if (rewardClaimed != 'claimed') {
                            if (valid === true && chosenPromo == 2) {
                                $(".tickets-info-modal").modal('show');
                            }
                        } else {
                            $("body").css("overflow", "hidden");
                            $('.overlay').html("Adding entry, please wait...").show();
                            $('#user-form').submit();
                        }
                    }

                });
            }

        });
    }
}

jQuery(document).ready(function($) {
    var entriesFunctions = new Entries;
    entriesFunctions.init();
});
