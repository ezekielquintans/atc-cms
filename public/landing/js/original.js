jQuery(document).ready(function($) {
    var ticketsClaimed = 0; //get tickets claimed from database
    $('#search').autocomplete({
            serviceUrl: '../search/autocomplete',
            showNoSuggestionNotice:true,
          	minChars: 3,
          	onSelect: function (suggestion) {
                $('.overlay').show();
                $("body").css("overflow", "hidden");
		        $.get('../getUserInfo',{userid : suggestion.data}, function(data) {
                    $('.overlay').hide();
                    $("body").css("overflow", "auto");

		        	$("#fname").val(data.first_name);
                    $("#lname").val(data.last_name);
                    $("#email").val(data.email);
                    $("#address").val(data.address);
		        	$("#contact").val(data.contact);
                    $("#cusID").val(data.id);
		        	$("#cusIDDisplay").val(data.display_id);
                    ticketsClaimed = data.ticketsClaimedToday;
		        });
		    }
    });
    /*Settings*/
    $("input[type=radio][name=mode]").on('ifChecked', function(event) {

        if(event.currentTarget.value == "A"){
            $(".multiplier").each(function() {
               $(this).attr({
                   "disabled"   : true,
                   'checked'    : false
               });;
            });
        }else if(event.currentTarget.value == "S"){
            $(".multiplier").each(function() {
              $(this).removeAttr('disabled').removeAttr('checked');
            });
           }
        $(".amount").first().trigger('keyup');
    });

    $(document).on('click','.multiplier' ,function(event) {
        $(".amount").first().trigger('keyup');
    });

    /*Selecting Promo*/
    var promo;

    $('#sel-promo').change(function(event) {
        promo = $(this).val();
        $(".amount").val('');
        resetVals();
        if(promo != ""){
            $(".entry-info").removeClass('hidden');
        }else{
            $(".entry-info").addClass('hidden');
        }
        if(promo == 2){
            $(".modes-cont").hide();
            $(".mode").attr({
                "disabled": true,
                "checked": false
            });
            $("#cinemaTickets").text(ticketsClaimed);
            if(ticketsClaimed != 'claimed'){
                $("#ticketsEarned").val(ticketsClaimed);
                $("#noTickets").val(ticketsClaimed);
            }
            $("#coupon-mechanics").html('(1 Raffle coupon for every 1000 Php purchase)')
            $(".tickets-cont").show();
            if (ticketsClaimed == 1) {
                $("#cinemaTickets").text('claimed 1 ticket already');
            }
            /*Changable for future use*/
        }else if ( promo == 1) {
            $(".modes-cont").show();
            $(".mode").attr("disabled",false);
            $('#modeS').iCheck('check');
            $("#coupon-mechanics").html('(1 Raffle coupon for every 500 Php purchase)')
            $(".tickets-cont").hide();
        }
        $(".entry-info .x_title h2").html($(this).children('option:selected').text() +' - Entries<small id="coupon-mechanics"></small>')
        $(".tickets-info-modal .modal-header").html($(this).children('option:selected').text() + " - CINEMA TICKETS");
    });

    /*Receipts based on settings*/
    $(document).on('keyup', '.amount', function(event) {
        var amount = $(this).val();
        var coupons = 0;
        var totalAmount = 0;
        var totalCoupons = 0;
        var additionalCoupons = 0;
        var tickets;
        var mode = $("input[name=mode]:checked").val();
        var multiplier = $(this).parentsUntil('.row').siblings('.with-multiplier').find('.multiplier');
        var chosenPromo = $('#sel-promo').val();
        if(Math.floor(amount) == amount && $.isNumeric(amount)){
            $('.amount').each(function() {
                totalAmount += parseInt($(this).val());
            });
            if(chosenPromo == 1){
                coupons = Math.floor(amount/500);
                $(this).siblings('.coupons').val(coupons);
                if (mode == "S") {
                    $('.coupons').each(function() {
                        totalCoupons += parseInt($(this).val());
                    });
                } else if(mode == "A"){
                    totalCoupons =  Math.floor(totalAmount/500);
                }
                $(".multiplier").each(function() {
                    if ($(this).is(':checked')) {
                        var stockCoupon = $(this).parentsUntil('.row').siblings('.amt-grp').find('.coupons');
                        additionalCoupons += parseInt(stockCoupon.val());
                    }
                });
            }else if(chosenPromo == 2){
                coupons = amount >= 1000 ? 1 : 0;
                $(this).siblings('.coupons').val(coupons);
                $('.coupons').each(function() {
                    totalCoupons += parseInt($(this).val());
                });
                if(ticketsClaimed != 'claimed'){
                    if(totalAmount >= 1000 && totalAmount < 2000){
                        tickets = 1;
                    }
                    if(totalAmount >= 2000){
                        if (ticketsClaimed == 1) {
                            tickets = 1;
                        }else{
                            tickets = 2;
                        }
                    }
                    if(totalAmount < 1000){
                        tickets = 0;
                    }
                }
            }
            totalCoupons += additionalCoupons;
            if(totalCoupons >= 1){
                if(mode == "S"){
                    $(multiplier).removeAttr('disabled');
                }
                $("#noCoupons").val(totalCoupons);
                $("#totalCoupons").text(totalCoupons);
                $("#totalAmount").text(totalAmount);
                if(chosenPromo == 2 && ticketsClaimed != 'claimed'){
                    $("#cinemaTickets").text(tickets);
                    if (ticketsClaimed == 1) {
                        $("#cinemaTickets").text(tickets + ' - claimed 1 ticket already');
                    }
                    $("#ticketsEarned").val(tickets);
                    $("#noTickets").val(tickets);
                }else{
                    $("#ticketsEarned").val('0');
                    $("#noTickets").val(tickets);
                }
            }else{
                if(chosenPromo == 2 && ticketsClaimed != 'claimed'){
                    resetVals();
                    $("#ticketsEarned").val('0');
                }
            }
    	}
    });

    function resetVals(){
        $("#noCoupons").val('0');
        $("#totalCoupons").text('0');
        $("#totalAmount").text('0');
        $("#cinemaTickets").text('0');
        if (ticketsClaimed == 1) {
            $("#cinemaTickets").text('claimed 1 ticket already');
        }
        $("#ticketsEarned").val('0');

    }


    /*Add receipt*/
    var ctr = 1;
    $(document).on('click',"#add-receipt", function(event){
    	event.preventDefault();
        ctr++;
        var newRow = $("#coupon-form-container")
        .children('.row')
        .first()
        .clone(true)
        .find(".multiplier").attr({
            'checked'   : false,
            'disabled'  : true
        }).end()
        .find("input:text").val("").end()
        .find('select').attr('id', 'store-'+ctr).end()
        .find(':input[type="number"]').val("").end()
    	newRow.appendTo('#coupon-form-container');
    	var noOfRows = parseInt($("#norows").val());;
    	noOfRows += 1;
    	$("#norows").val(noOfRows);
        // $('#store-'+ctr).chosen();
    });
    $('#submit-coupons').click(function(){
        $('.overlay').css('height', $(document).height() + 'px');
        var chosenPromo   = $('#sel-promo').val();
        var ticketsEarned = $("#noTickets").val();
        var ticketForm    =    '<div class="form-group tickets-form-group">' +
                                '<label class="control-label col-md-4 col-sm-4 col-xs-4">Ticket no.</label>' +
                                '<div class="col-md-8 col-sm-8 col-xs-8">' +
                                    '<input type="text" class="ticket-text form-control" required placeholder="Enter ticket number" name="tickets[]">' +
                                '</div>' +
                            '</div>';
        $('.tickets-info-modal .modal-body .row').html('');
        if(ticketsEarned != "" || ticketsEarned != 0){
            for (var i = ticketsEarned - 1; i >= 0; i--) {
                $('.tickets-info-modal .modal-body .row').append(ticketForm);
            }
        }
        validateEntryForms(chosenPromo,ticketsClaimed);
    });

    /*Validate Movie Tickets*/
    $(".save-tickets").click(function(event) {
        // $('.tickets-info-modal').hide();
        $('.overlay').css('height', $(document).height() + 'px');
        $('.overlay').html("Saving tickets...").show();
        var valid = true;
        var requestCount = $('input.ticket-text').length;
        var ctr = 0;
        $('.ticket-text').filter('[required]:visible').each(function(){
            if($(this).val() == ""){
                valid = false;
                $(this).css('border','2px solid red');
                // $('.tickets-info-modal').show();
            }else{
                $(this).css('border','1px solid #ccc');
            }
        });
        $('input.ticket-text').each(function() {
            var input       = $(this);
            var serialno    = $(this).val();
            if(valid === true){
                $.get('../validate-reward?serialno='+serialno+'&promoid=2', function(data){
                    if(data == "false"){
                        valid = false;
                        input.css('border','2px solid orange');
                        input.closest('div.form-group').find('label.control-label').html('Ticket no.<small style="color:red"> *Already used.</small>');
                    }else{
                        input.css('border','2px solid green');
                        input.closest('div.form-group').find('label.control-label').html('Ticket no.');
                    }
                    ctr++;
                    if(ctr == requestCount){
                        $("body").css("overflow", "auto");
                        $('.overlay').html("Loading member's information, please wait...").hide();
                        if(valid === true){
                            $("body").css("overflow", "hidden");
                            $('.overlay').html("Adding entry, please wait...").show();
                            $('.tickets-info-modal').hide();
                            $('#user-form').submit();
                        }
                    }

                });
            }
        });
    });


    $(document).on('change', '.store', function(){
        var category = $('option:selected', this).data('category');
        $(this).closest('div.entry-row').find('input.category').val(category);
    });
});

function validateEntryForms(chosenPromo,rewardClaimed){
    var valid = true;
    $('input,textarea,select').filter('[required]:visible').each(function(){
        if($(this).val() == ""){
            valid = false;
            $(this).css('border','2px solid red');
        }else{
            $(this).css('border','1px solid #ccc');
        }
    });

    var requestCount = $('input.invoice-text').length;
    var ctr = 0;
    if(valid === true){
        $('.overlay').html("Validating entry, please wait...").show();
        $("body").css("overflow", "hidden");
    }

    $('input.invoice-text').each(function() {
        var input      = $(this);
        var invoice    = $(this).val();
        var store      = $(this).closest('div.row').find('select.store').val();

        if(valid === true){
            $.get('../validate-invoice?store='+store+'&invoice='+invoice, function(data){
                if(data == "false"){
                    valid = false;
                    input.css('border','2px solid orange');
                    input.closest('div.form-group').find('label.control-label').html('Invoice Number<small style="color:red"> *Already used.</small>');
                }else{
                    input.css('border','2px solid green');
                    input.closest('div.form-group').find('label.control-label').html('Invoice Number');
                }
                ctr++;
                if(ctr == requestCount){
                    $("body").css("overflow", "auto");
                    $('.overlay').html("Loading member's information, please wait...").hide();
                    if(chosenPromo == 1 && valid === true){
                        $("body").css("overflow", "hidden");
                        $('.overlay').html("Adding entry, please wait...").show();
                        $('#user-form').submit();
                    }
                    if(rewardClaimed != 'claimed'){
                        if(valid === true && chosenPromo == 2){
                            $(".tickets-info-modal").modal('show');
                        }
                    }else{
                        $("body").css("overflow", "hidden");
                        $('.overlay').html("Adding entry, please wait...").show();
                        $('#user-form').submit();
                    }
                }

            });
        }

    });
}