///////////////////////////////////////////////////
//JS of adding entries for Htech E-raffle system //
//Created by:                                    //
//    Ezekiel Quintans                           //
//    Kiel Lago                                  //
//Modified by:                                   //
//    Ezekiel Quintans                           //
//Version 2                                      //
///////////////////////////////////////////////////
var Entries = (function(){
    "use strict"

    var rewardsClaimed      = 0;
    var currentEntries      = 0;
    var entryrows           = 1;
    var vip                 = false;
    var mode                = "S";

    var ui                  = {};
    var urls                = {};


    /**
     * Bind html elements into javascript object
     * @return {object} html elements
     */
    var bindElements = function() {
        var el = {};
        /*Inputs*/
        el.search           = $('#search');
        el.amount           = $(".amount");
        el.mode             = $("input[name=mode]");

        /*Containers*/
        el.entryinfo        = $(".entry-info");
        el.entryrow         = $(".entry-row");
        el.table            = $("tbody");
        el.body             = $("body");
        el.form             = $('#entry-form')
        
        /*Hiddens*/
        el.customerId       = $("#customer-id")
        el.vip              = $("#vip")
        el.coupons          = $("#noCoupons");
        el.tickets          = $("#noTickets");
        el.rmv              = $("#raffle_multiplier_value");
        el.currententries   = $("#curEntries");
        
        /*Loading*/
        el.overlay          = $('.overlay');

        /*Buttons*/
        el.generate         = $(".submit-entries");
        el.addreceipt       = $("#add-receipt");
        el.newentry         = $("#new-entry");
        el.deleteEntry      = $(".btn-delete-entry");

        /*Indicators*/
        el.reward           = $("#rewards-label");
        el.rewardContainer  = $("#rewards-container");
        el.currentEntries   = $("#current-entries");
        el.totalAmount      = $("#totalAmount");
        el.totalCoupons     = $("#total-coupons");
        return el;
    };

    /**
     * URL used in this module
     * @return {object} url strings
     */
    var bindUrls = function() {
        var url = {};
        

        /*Url for autocomplete suggestions of user*/
        url.search          = baseUrl + 'search/autocomplete';

        /*Url for autocomplete for selected user*/
        url.userinfo        = baseUrl + 'getUserInfo';

        /*Url for checking user's current entries*/
        url.couponsearned   = baseUrl + 'getCouponsEarned';
        
        /*Url for validating the invoice*/
        url.validateinvoice = baseUrl + 'validate-invoice?store=';

        return url;
    };

    /**
     * Set data to be used in module upon searching 
     * @return None
     */
    var searchUser = function () {
        if(typeof $.fn.autocomplete == 'undefined'){
            alert("Please include chosen.jquery.js");
            return;
        }
        ui.search.autocomplete({
            serviceUrl: urls.search,
            showNoSuggestionNotice: true,
            minChars: 3,
            onSelect: function(suggestion) {
                $('.overlay').show();
                $("body").css("overflow", "hidden");
                $.get(urls.userinfo, { userid: suggestion.data }, bindUser);
            }
        });
    };


    /**
     * Function to bind results of ajax search 
     * @param  {object} data results from ajax
     */
    var bindUser = function(data) {
        $('.overlay').hide();
        $("body").css("overflow", "auto");
        ui.customerId.val(data.id);
        ui.search.attr('readonly', '');
        ui.entryinfo.removeClass('hidden');
        $("#fname").val(data.first_name);
        $("#mname").val(data.middle_name);
        $("#lname").val(data.last_name);
        $("#email").val(data.email);
        $("#address").val(data.address);
        $("#contact").val(data.contact);
        $("#dob").val(data.dob);
        $("#cusID").val(data.id);
        var cards = data.userCards;
        if(cards.length >= 1){
            $.each(cards, function(index, val) {
                var cbId = '#card-type-' + val;
                $(cbId).attr('checked','true');
            });
        }

    };
    /**
     * [Handle buttons and other functions]
     * @return {[type]} [description]
     */
    var bindFunctions = function(){
        ui.generate.on('click', generateResults);
        ui.addreceipt.on('click', addEntryRow);
        ui.deleteEntry.on('click', deleteEntryRow);

        $(document).on('change', '.store', function() {
            var category = $('option:selected', this).data('category');
            $(this).closest('tr.entry-row').find('input.category').val(category);
        });
        
        $(document).on('keyup', '.amount', processPromo);
        $(".card-type").change(function(event) {
            if ($(this).is(':checked') == true) {
                vip = true;
                $(".amount").first().trigger('keyup');
            }
            if ($(".card-type:checked").length <= 0) {
                vip = false;
            }
            console.log("VIP:" + vip);
        });
    };

    /**
     * [Promo Logic Computations]
     * @return None
     */
    var processPromo = function(event){
        event.preventDefault();
        var amount              = parseInt($(this).val());
        var totalAmount         = 0;
        if ($.isNumeric(amount)) {
            totalAmount     = computeTotalAmount();

            /*Separate this*/
            ui.totalAmount.text(totalAmount);
        }else{
            ui.totalAmount.text(0);
        }
    };

    /**
     * [compute the total amount of all amount inputs and set total amount indicator in ui]
     * @return {[int]}        [total amount computed]
     */
    var computeTotalAmount = function(){
        var totalAmount = 0;
        $('.amount').each(function() {
            var amount = $(this).val();
            totalAmount += parseInt(amount);
        });

        return totalAmount;
    };

    
    /**
     * [Generate results of computations]
     * @param  {[type]} event [description]
     * @return {[type]}       [description]
     */
    var generateResults = function(event){
        event.preventDefault();
        var validated = validateEntryForm();
    };

    var validateEntryForm = function(chosenPromo, rewardClaimed) {
        var valid = true;
        if(parseInt(ui.totalAmount.text())  < 1000){
            alert("Total Amount is less than 1000");
            valid = false;
        }
        $('input').filter('[required]:visible').each(function() {
            if ($(this).val() == "") {
                valid = false;
                $(this).css('border', '2px solid red');
            } else {
                $(this).css('border', '1px solid #ccc');
            }
        });

        var requestCount    = $('input.invoice-text').length;
        var ctr             = 0;
        if (valid === true) {
            ui.overlay.html("Validating entry, please wait...").show();
            ui.body.css("overflow", "hidden");
        }

        $('input.invoice-text').each(function() {
            var input   = $(this);
            var invoice = $(this).val();
            var store   = $(this).closest('div.row').find('select.store').val();
            if (valid === true) {
                $.get(urls.validateinvoice + store + '&invoice=' + invoice, function(data) {
                    if (data == "false") {
                        valid = false;
                        input.css('border', '2px solid orange');
                        input.closest('div.form-group').find('label.control-label').html('Invoice Number<small style="color:red"> *Already used.</small>');
                    }
                    ctr++;
                    if (ctr == requestCount) {
                        ui.body.css("overflow", "auto");
                        ui.overlay.html("Loading member's information, please wait...").hide();
                        if (valid === true) {
                            ui.body.css("overflow", "hidden");
                            ui.overlay.html("Adding entry, please wait...").show();
                            ui.form.submit();
                            return false;
                        }
                    }
                });
            }
        });
    }

    /**
     * [Add additional row in the table of entries]
     * @param {[type]} event [description]
     */
    var addEntryRow = function(event){
        var mode = $("input[name=mode]:checked").val();
        
        if($(".amount").last().val() < 1){
            alert("Please type a valid amount before adding a new receipt");
            return false;
        }
        entryrows++;
        var newRow = ui.entryrow
            .clone(true)
            .find("input:text").val("").end()
            .find('select').attr('id', 'store-' + entryrows).end()
            .find(':input[type="number"]').val("").end();
        newRow.appendTo(ui.table);
        $("#norows").val(entryrows);
            // If possible add more validation
    };

    /**
     * [Delete a row in the entries table]
     * @param  {[type]} event [description]
     * 
     */
    var deleteEntryRow = function(event){
        event.preventDefault();
        if($(".entry-row").length <= 1){
            alert("Ooops! Sorry you can't delete this");
            return false;
        }
        $(this).closest('.entry-row').remove();
        entryrows--;
    };

    /**
     * [Initialize the module]
     * @return None
     */
    var init = function(){
        ui              = bindElements();
        urls            = bindUrls();
        searchUser();
        bindFunctions();
        $(".datetime").daterangepicker({
            singleDatePicker: true,
            "timePicker24Hour": true,
            timePickerIncrement: 5,
            showDropdowns: true,
            autoUpdateInput : false,
            // startDate: moment(),
            buttonClasses: ['btn-sm-fix','btn-sm'],
            locale: { format: "YYYY-MM-DD" },
        }, function(chosen_date) {
            $('.datetime').val(chosen_date.format('YYYY-MM-DD'));
        } );

    };

    return {
        init : init,
    }
})();

$(document).ready(function() {
    Entries.init();
});