$(document).ready(function() {
	$(".img-upload").change(function(event) {
        if (this.files && this.files[0]) {
            var self = this;
            var validImageTypes = ["image/jpeg", "image/png"];
            var reader = new FileReader();
            if ($.inArray(this.files[0].type, validImageTypes) < 0) {
                 alert("Invalid File type. Please upload an image");
                 $(this).val("");
                 return false;
            }
            reader.onload = function (e) {
              $(self).siblings('img').attr('src', e.target.result).removeClass('hidden');
            }
            reader.readAsDataURL(this.files[0]);
        }else{
            alert("Preview not available.")
        }
    });
  /*Datepicker*/
  $(".duration").daterangepicker({
      // singleDatePicker: true,
      "timePicker": true,
      "timePicker24Hour": true,
      showDropdowns: true,
      opens:"left",
      // minDate: moment(),
      startDate: moment(),
      endDate: moment().add(1,'days'),
      buttonClasses: ['btn-sm-fix','btn-sm'],
      locale: { format: "YYYY-MM-DD h:mm A" },
  },function(start, end, label) {}); 
  // $(".daterangepicker").removeClass('opensright');

  /*Promo type switches*/
  $('#promo-type-raffle').click(function(event) {
    if($(this)[0].checked == true){
      $("#raffle-settings-container").removeClass('hidden')
      .find('input[type=radio],input[type=checkbox]')
      .iCheck('enable');
      $("#raffle_on").val("1");
      $("html, body").animate({ scrollTop: $(document).height() }, 700);
      $("#raffle-settings-container").find('input').removeAttr('disabled');
    }else{
      $("#raffle-settings-container")
        .addClass('hidden')
        .find('input[type=radio],input[type=checkbox]')
        .iCheck('uncheck');
        $("#raffle-settings-container")
        .find(':input[type=number]')
        .val("");
        var elems = $(".raffle-accumulation-type , .raffle-multiplier");
        elems.each(function(index, el) {
          if($(this)[0].checked == true){
            $(this).trigger('click');
          }
        });
      $("#raffle_on").val("0");
      $("#raffle-settings-container").find('input').attr('disabled', 'disabled');
      $(".right_col").css('min-height', 'auto');
    }
  });
  $('#promo-type-premium').click(function(event) {
    if($(this)[0].checked == true){
      $("#premium-settings-container").removeClass('hidden')
        .find('input[type=radio],input[type=checkbox]')
        .iCheck('enable');;
      $("#premium_on").val("1");
      $("html, body").animate({ scrollTop: $(document).height() }, 700);
      $("#premium-settings-container").find('input').removeAttr('disabled');
      $("#premium-settings-container").find('select').removeAttr('disabled');
    }else{
      $("#premium-settings-container")
        .addClass('hidden')
        .find('input[type=radio],input[type=checkbox]')
        .iCheck('uncheck');
      $("#premium-settings-container")
      .find(':input[name="premium_purchase_requirement"]')
      .val("");
      $("#premium-settings-container")
      .find(':input[name="premium_ceiling"]')
      .val("1");
      $("#premium-settings-container")
      .find(':input[name="reward_id"]')
      .val("");
        var elems = $(".premium-accumulation-type");
        elems.each(function(index, el) {
          if($(this)[0].checked == true){
            $(this).trigger('click');
          }
        });
      $("#premium-settings-container").find('input').attr('disabled','disabled');  
      $("#premium-settings-container").find('select').attr('disabled','disabled');  
      $("#premium_on").val("0");
      $(".right_col").css('min-height', 'auto');
    }
  });

  /*Bootsrap WYSIWYG*/ 
  tinymce.init({
    selector: "textarea#description",
    width: "100%",
    height: 300,
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    convert_urls: false,
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    statusbar:true,
    extended_valid_elements : "video[controls|preload|width|height|data-setup],source[src|type], iframe[src|width|height|name|align], embed[width|height|name|flashvars|src|bgcolor|align|play|loop|quality|allowscriptaccess|type|pluginspage]",
    media_strict: false,
    media_live_embeds: true,
    setup: function (editor) {
        editor.on('keyup', function(e) {;
          var cc = CountCharacters();
          $(".character-count").html("Character Count(Max: 500): " + cc);
        });
    }
  });

});

function CountCharacters(){
    var body = tinymce.get("description").getBody();
    var content = tinymce.trim(body.innerText || body.textContent);
    return content.length - 4;
};

function validateCharLength(){
    var max = 480;
    var count = CountCharacters();
    if(count > max){
       alert("Maximum " + max + " characters allowed.")
            return false;
        }
        return;
}