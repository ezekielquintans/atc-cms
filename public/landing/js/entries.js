Entries = function() {
    this.rewardsClaimed = 0;
    this.rows = 1;
    this.vip = false;

    this.init = function() {
        this.search();
        this.selectPromo();
        this.processPromo();
        this.addReceipt();
        this.processCoupons();
        this.processRewards();
        this.dob();
    }
    this.dob = function(){
         $(".datetime").daterangepicker({
            singleDatePicker: true,
            "timePicker24Hour": true,
            timePickerIncrement: 5,
            showDropdowns: true,
            minDate: '1920-01-01',
            // startDate: moment(),
            buttonClasses: ['btn-sm-fix','btn-sm'],
            locale: { format: "YYYY-MM-DD" },
        });
    }

    this.search = function() {
        var self = this;
        $('#search').autocomplete({
            serviceUrl: '../search/autocomplete',
            showNoSuggestionNotice: true,
            minChars: 3,
            maxHeight: 1000,
            onSelect: function(suggestion) {
                $('.overlay').show();
                $("body").css("overflow", "hidden");
                $.get('../getUserInfo', { userid: suggestion.data }, function(data) {
                    $('.overlay').hide();
                    $("body").css("overflow", "auto");

                    $("#fname").val(data.first_name);
                    $("#mname").val(data.middle_name);
                    $("#lname").val(data.last_name);
                    $("#email").val(data.email);
                    $("#address").val(data.address);
                    $("#contact").val(data.contact);
                    $("#dob").val(data.dob);
                    $("#cusID").val(data.id);
                    $("#cusIDDisplay").val(data.display_id);
                    $("#userCardDisplay").val(data.userCards);
                    self.rewardsClaimed = data.rewardsClaimedToday;
                    if (data.rewardsClaimedToday >= 1) {
                            $("#rewardsText").text('claimed ' + data.rewardsClaimedToday + ' already');
                    }else if(data.rewardsClaimedToday <=0){
                            $("#rewardsText").text('0');
                    }
                    var reward_id = $('#rewards-id').val();
                    if(reward_id){
                        self.handlePromoSelect(reward_id);
                    }
                    var cards = data.userCards;
                    if(cards.length >= 1){
                        self.vip = true;
                        $('#userCard').val('true');
                        $.each(cards, function(index, val) {
                            var cbId = '#card-type-' + val;
                            $(cbId).attr('checked','true');
                        });
                    }
                });
            }
        });
    }
    this.handlePromoSelect = function(reward_id){
        var custId = $('#cusID').val();
        var self = this;
        if(reward_id != 0 && custId){
            $.get('../rewardsgiven/validate-transaction',{rId:reward_id,cId:custId}, function(data) {
                self.rewardsClaimed = data;
                if (self.promoSettings.promoType == 2 || self.promoSettings.promoType == 3) {
                    if (self.rewardsClaimed > 0 && self.rewardsClaimed < self.promoSettings.premiumCeiling) {
                        $("#rewardsText").text('claimed ' + self.rewardsClaimed + ' already');
                    }
                    if (self.rewardsClaimed <= 0) {
                        $("#rewardsText").text('0');
                    }
                    if (self.rewardsClaimed >= self.promoSettings.premiumCeiling) {
                        $("#rewardsText").text('claimed already');
                        self.rewardsClaimed = "claimed";
                    }
                }

            });
        }
        
    }

    this.selectPromo = function() {
        var self = this;
        $('select#sel-promo').change(function(event) {
            self.promo = $(this).val();
            self.promoSettings = {
                promoTitle: $(this).find(':selected').text(),
                promoType: $(this).find(':selected').data('type'),
                rewardName: $(this).find(':selected').data('rewards'),
                rafflePremise: $(this).find(':selected').data('raffle_premise'),
                raffleAccumulationType: $(this).find(':selected').data('raffle_accumulation_type'),
                rafflePurchaseRequirement: $(this).find(':selected').data('raffle_purchase_requirement'),
                raffleMultiplier: $(this).find(':selected').data('raffle_multiplier'),
                premiumPremise: $(this).find(':selected').data('premium_premise'),
                premiumAccumulationType: $(this).find(':selected').data('premium_accumulation_type'),
                premiumPurchaseRequirement: $(this).find(':selected').data('premium_purchase_requirement'),
                premiumCeiling: $(this).find(':selected').data('premium_ceiling'),
                premiumMultiplier: $(this).find(':selected').data('premium_multiplier'),
                premium_rewards_id: $(this).find(':selected').data('rewards_id'),
                premiumIsEvent: $(this).find(':selected').data('premium_event'),
                
            };
            /*Check Tickets claimed*/
            self.handlePromoSelect(self.promoSettings.premium_rewards_id);
            $(".amount").val('');
            self.resetInputValues();
            if (self.promo) {
                console.log(self.promoSettings);
                $(".entry-info").removeClass('hidden');
                $("#promo-type").val(self.promoSettings.promoType);
                $("#rewards-id").val(self.promoSettings.premium_rewards_id != "" ? self.promoSettings.premium_rewards_id : "0" );
                var premise = "(";
                if(self.promoSettings.promoType == 1 || self.promoSettings.promoType == 3){
                    $(".generate-coupon").html("Generate Coupons");
                    if (self.promoSettings.rafflePremise == 1) {
                        premise += '1 Raffle coupon for every ' + self.promoSettings.rafflePurchaseRequirement + ' Php amount of purchase';
                    }else{
                        premise += '1 Raffle coupon for a minimum of ' + self.promoSettings.rafflePurchaseRequirement + ' Php amount of purchase';
                    }
                }
                if(self.promoSettings.promoType == 2 || self.promoSettings.promoType == 3){
                    if(self.promoSettings.premiumIsEvent != 1){
                        premise += ' and';
                    }else{
                        premise = "(";
                    }
                    if (self.promoSettings.rafflePremise == 1) {
                        premise += ' 1 '+(self.promoSettings.rewardName != "" ? self.promoSettings.rewardName : " item") + ' for every ' + self.promoSettings.premiumPurchaseRequirement + ' Php amount of purchase';
                    }else{
                        premise += ' 1 '+(self.promoSettings.rewardName != "" ? self.promoSettings.rewardName : " item") +' for a minimum '+ self.promoSettings.premiumPurchaseRequirement + ' Php amount of purchase';
                    }
                }
                premise += ' )'
                $(".entry-info .x_title h2").html(self.promoSettings.promoTitle + '<br /><small id="coupon-mechanics" style="margin-left:0px;">' + premise + '</small>')
                if (self.promoSettings.raffleAccumulationType != 0) {
                    $(".modes-cont").show();
                    if (self.promoSettings.raffleAccumulationType == 1) {
                        $(".mode").iCheck("disable");
                        $("#modeS").iCheck("check");
                        $("#if-mode-disabled").attr('name', 'mode').val('S');
                    }
                    if (self.promoSettings.raffleAccumulationType == 2) {
                        $(".mode").iCheck("disable");
                        $("#modeA").iCheck("check");
                        $("#if-mode-disabled").attr('name', 'mode').val('A');
                    }
                    if (self.promoSettings.raffleAccumulationType == 3) {
                        $(".mode").iCheck("enable");
                        $(".mode").iCheck("uncheck");
                        $("#if-mode-disabled").removeAttr('name').val('');
                    }
                }
                if (self.promoSettings.promoType == 2 || self.promoSettings.promoType == 3) {
                    $(".rewards-cont").removeClass('hidden');
                    if(self.promoSettings.promoType == 2){
                        $('.total-coupons-cont').addClass('hidden');
                        $(".modes-cont").hide();
                        if(self.promoSettings.premiumIsEvent == 1){
                            $('.total-amount-cont').addClass('hidden');
                            $(".generate-coupon").html("Generate Event Tickets");
                            $("#is-event").val(1);
                        }
                    }else{
                        $('.total-coupons-cont').removeClass('hidden');
                    }
                    $(".reward-label").html(self.promoSettings.rewardName);
                } else {
                    $(".rewards-cont").addClass('hidden');
                    $('.total-amount-cont').removeClass('hidden');
                    $('.total-coupons-cont').removeClass('hidden');
                }
                if(!self.vip){
                    if (self.promoSettings.raffleMultiplier == 1) {
                        if(self.promoSettings.raffleAccumulationType == 1){
                            $(".with-multiplier").removeClass('hidden');
                            $(".multiplier").each(function() {
                                $(this).removeAttr('disabled').removeAttr('checked');
                            });
                        }else{
                            $(".with-multiplier").addClass('hidden');
                            $(".multiplier").each(function() {
                                $(this).attr({
                                    "disabled": true,
                                    'checked': false
                                });;
                            });
                        }
                    } else {
                        $(".with-multiplier").addClass('hidden');
                    }
                }
            } else {
                $(".entry-info").addClass('hidden');
            }
            
        });
    }

    this.resetInputValues = function() {
        $("#noCoupons").val('0');
        $("#totalCoupons").text('0');
        $("#totalAmount").text('0');
        // $(".entry-row:not(:first)").remove();
    }

    this.processPromo = function() {
        var self = this;
        $(document).on('keyup', '.amount', function(event) {
            var amount = $(this).val();
            if(amount.length < 3) return;
            var coupons = 0;
            var totalAmount = 0;
            var totalCoupons = 0;
            var additionalCoupons = 0;
            var tickets = 0;
            var mode = $("input[name=mode]:checked").val();
            var multiplier = $(this).parentsUntil('.row').siblings('.with-multiplier').find('.multiplier');
            var chosenPromo = $('#sel-promo').val();
            if (Math.floor(amount) == amount && $.isNumeric(amount)) {
                $('.amount').each(function() {
                    totalAmount += parseInt($(this).val());
                });
                if(self.promoSettings.rafflePremise == 1){
                    coupons = Math.floor(amount / self.promoSettings.rafflePurchaseRequirement);
                } else if (self.promoSettings.rafflePremise == 2){
                    coupons = amount >= self.promoSettings.rafflePurchaseRequirement ? 1 : 0;
                }
                $(this).siblings('.coupons').val(coupons);
                if(!mode && self.promoSettings.promoType != 2){
                    alert('Please Select if "single" or "accumulated"');
                };
                if (mode == "S") {
                    $('.coupons').each(function() {
                        totalCoupons += parseInt($(this).val());
                    });
                } else if (mode == "A") {
                    totalCoupons = Math.floor(totalAmount / self.promoSettings.rafflePurchaseRequirement);
                }
                if(self.promoSettings.raffleMultiplier == 1){
                    if(self.promoSettings.raffleAccumulationType == 1 || mode == "S"){
                        $(".multiplier").each(function() {    
                            if ($(this).is(':checked') || self.vip == true) {
                                var stockCoupon = $(this).parentsUntil('.row').siblings('.amt-grp').find('.coupons');
                                additionalCoupons += parseInt(stockCoupon.val());
                            }
                        });    
                    } else if(self.promoSettings.raffleAccumulationType == 2 || mode == "A"){
                        additionalCoupons = Math.floor(totalAmount / self.promoSettings.rafflePurchaseRequirement);
                    }
                }
                if(self.rewardsClaimed != "claimed"){
                    /*if premium accumulation type is single*/
                    var maxAmt = self.promoSettings.premiumPurchaseRequirement * self.promoSettings.premiumCeiling;
                    var rwrdsTxt = ' - claimed '+self.rewardsClaimed+' already';
                    if(self.promoSettings.premiumAccumulationType == 1){
                        /*Per Amount*/
                        if(self.promoSettings.premiumPremise == 1){
                            $('.amount').each(function() {
                                if(parseInt($(this).val()) >= self.promoSettings.premiumPurchaseRequirement){
                                    tickets = tickets + Math.floor(parseInt($(this).val())/self.promoSettings.premiumPurchaseRequirement);
                                }else{
                                    tickets--;
                                }
                            });
                        }
                        /*Per Minimum Amount*/
                        else if(self.promoSettings.premiumPremise == 2){
                            $('.amount').each(function() {
                                if(parseInt($(this).val()) >= self.promoSettings.premiumPurchaseRequirement){
                                    tickets++;
                                }else{
                                    tickets--;
                                }
                            });
                        }
                        if(self.rewardsClaimed > 0 && tickets > 0){
                            $("#rewardsText").text(tickets + rwrdsTxt);
                        }else if(self.rewardsClaimed <= 0 && tickets > 0){
                            $("#rewardsText").text(tickets);
                        }  
                    }
                    /*If premium accumulation type is accumulated*/
                    else if(self.promoSettings.premiumAccumulationType == 2){
                        /*Per amount and per minimum amount premises would be the same because it's accumulated*/
                        if (totalAmount >= self.promoSettings.premiumPurchaseRequirement && totalAmount < maxAmt) {
                            tickets = Math.floor(totalAmount/self.promoSettings.premiumPurchaseRequirement);
                        }
                        if (totalAmount >= maxAmt) {
                            tickets = self.promoSettings.premiumCeiling - self.rewardsClaimed;
                        }
                        if (totalAmount < self.promoSettings.premiumPurchaseRequirement) {
                            tickets = 0;
                        }
                        if(self.rewardsClaimed > 0){
                            $("#rewardsText").text(tickets + rwrdsTxt);
                        }else{
                            $("#rewardsText").text(tickets);
                        }
                    }
                    // console.log(tickets);
                    $("#ticketsEarned").val(tickets);
                    $("#noTickets").val(tickets);
                }
                totalCoupons += additionalCoupons;
                if (totalCoupons >= 1) {
                    $("#noCoupons").val(totalCoupons);
                    $("#totalCoupons").text(totalCoupons);
                    $("#totalAmount").text(totalAmount);
                }else{
                    // self.resetInputValues();
                }                
            }
        });
        $(document).on('change', '.store', function() {
            var category = $('option:selected', this).data('category');
            $(this).closest('div.entry-row').find('input.category').val(category);
        });
        $(document).on('click', '.multiplier', function(event) {
            $(".amount").first().trigger('keyup');
        });
        $(".card-type").change(function(event) {
            if ($(this).is(':checked') == true) {
                self.vip = true;
                $(".amount").first().trigger('keyup');
            }
            if ($(".card-type:checked").length <= 0) {
                self.vip = false;
            }
            console.log("VIP:" + self.vip);
        });
        $("input[type=radio][name=mode]").on('ifChecked', function(event) {
            if (event.currentTarget.value == "A") {
                $(".multiplier").each(function() {
                    $(this).attr({
                        "disabled": true,
                        'checked': false
                    });;
                });
            } else if (event.currentTarget.value == "S") {
                $(".multiplier").each(function() {
                    $(this).removeAttr('disabled').removeAttr('checked');
                });
            }
            $(".amount").first().trigger('keyup');
        });
    }

    this.addReceipt = function() {
        var self = this;
        $(document).on('click', "#add-receipt", function(event) {
            event.preventDefault();
            self.rows++;
            var newRow = $("#coupon-form-container")
                .children('.row')
                .first()
                .clone(true)
                .find(".multiplier").attr({
                    'checked': false,
                    'disabled': true
                }).end()
                .find("input:text").val("").end()
                .find('select').attr('id', 'store-' + self.rows).end()
                .find(':input[type="number"]').val("").end()
            newRow.appendTo('#coupon-form-container');
            var noOfRows = parseInt($("#norows").val());;
            noOfRows += 1;
            $("#norows").val(noOfRows);
            // $('#store-'+ctr).chosen();
        });
    }

    this.processCoupons = function() {
        var self = this;
        $('.generate-coupon').click(function() {
            $('.overlay').css('height', $(document).height() + 'px');
            var chosenPromo = $('#sel-promo').val();
            var ticketsEarned = $("#noTickets").val();
            var ticketForm = '<div class="form-group tickets-form-group">' +
                '<label class="control-label col-md-4 col-sm-4 col-xs-4">Serial no.</label>' +
                '<div class="col-md-8 col-sm-8 col-xs-8">' +
                    '<input type="text" class="ticket-text form-control" required placeholder="Enter ticket number" name="tickets[]">' +
                '</div>' +
            '</div>';
            $('.tickets-info-modal .modal-body .row').html('');
            if (ticketsEarned != "" || ticketsEarned != 0) {
                for (var i = ticketsEarned - 1; i >= 0; i--) {
                    $('.tickets-info-modal .modal-body .row').append(ticketForm);
                }
            }
            self.validateEntryForms(chosenPromo, self.rewardsClaimed);
        });
    }
    this.processRewards = function() {
        $(".save-tickets").click(function(event) {
            // $('.tickets-info-modal').hide();
            $('.overlay').css('height', $(document).height() + 'px');
            $('.overlay').html("Saving tickets...").show();
            var valid = true;
            var requestCount = $('input.ticket-text').length;
            var chosenPromo = $('#sel-promo').val();
            var ctr = 0;
            $('.ticket-text').filter('[required]:visible').each(function() {
                if ($(this).val() == "") {
                    valid = false;
                    $(this).css('border', '2px solid red');
                    // $('.tickets-info-modal').show();
                } else {
                    $(this).css('border', '1px solid #ccc');
                }
            });
            $('input.ticket-text').each(function() {
                var input = $(this);
                var serialno = $(this).val();
                if (valid === true) {
                    $.get('../validate-reward?serialno=' + serialno + '&promoid=2', function(data) {
                        if (data == "false") {
                            valid = false;
                            input.css('border', '2px solid orange');
                            input.closest('div.form-group').find('label.control-label').html('Ticket no.<small style="color:red"> *Already used.</small>');
                        } else {
                            input.css('border', '2px solid green');
                            input.closest('div.form-group').find('label.control-label').html('Ticket no.');
                        }
                        ctr++;
                        if (ctr == requestCount) {
                            $("body").css("overflow", "auto");
                            $('.overlay').html("Loading member's information, please wait...").hide();
                            if (valid === true) {
                                $("body").css("overflow", "hidden");
                                $('.overlay').html("Adding entry, please wait...").show();
                                $('.tickets-info-modal').hide();
                                $('#user-form').submit();
                            }
                        }
                    });
                }
            });
        });
    }
    this.validateEntryForms = function(chosenPromo, rewardClaimed) {
        var valid = true;
        var self = this;
        $('input,textarea,select').filter('[required]:visible').each(function() {
            if ($(this).val() == "") {
                valid = false;
                $(this).css('border', '2px solid red');
            } else {
                $(this).css('border', '1px solid #ccc');
            }
        });

        var requestCount = $('input.invoice-text').length;
        var ctr = 0;
        if (valid === true) {
            $('.overlay').html("Validating entry, please wait...").show();
            $("body").css("overflow", "hidden");
        }

        $('input.invoice-text').each(function() {
            var input = $(this);
            var invoice = $(this).val();
            var store = $(this).closest('div.row').find('select.store').val();

            if (valid === true) {
                $.get('../validate-invoice?store=' + store + '&invoice=' + invoice, function(data) {
                    if (data == "false") {
                        valid = false;
                        input.css('border', '2px solid orange');
                        input.closest('div.form-group').find('label.control-label').html('Invoice Number<small style="color:red"> *Already used.</small>');
                    } 
                    ctr++;
                    if (ctr == requestCount) {
                        $("body").css("overflow", "auto");
                        $('.overlay').html("Loading member's information, please wait...").hide();
                        if (self.promoSettings.promoType == 1 && valid === true) {
                            $("body").css("overflow", "hidden");
                            $('.overlay').html("Adding entry, please wait...").show();
                            $('#user-form').submit();
                            return false;
                        }
                        if (self.rewardClaimed != 'claimed') {
                            if (valid === true && self.promoSettings.promoType != 1 && self.promoSettings.premiumIsEvent != 1) {
                                $(".tickets-info-modal").modal('show');
                            }else{
                                $('#user-form').submit();
                            }
                        } else {
                            $("body").css("overflow", "hidden");
                            $('.overlay').html("Adding entry, please wait...").show();
                            $('#user-form').submit();
                        }
                    }

                });
            }

        });
    }
}

jQuery(document).ready(function($) {
    var entriesFunctions = new Entries;
    entriesFunctions.init();
});
