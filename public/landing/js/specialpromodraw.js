    $('#get-highest-spender').click(function(){
        var highestSpender = $(this).data('spender');
        console.log(parseInt(highestSpender.total_amount));
        if($('select[name="promo"]').val() == ""){
            $('select[name="promo"]').css('border', '1px solid red');
            return false;
        }
        var winner = {};
        var button = $(this);
        button.attr('disabled', 'disabled');
        $('#myTargetElement').html('Loading...');
        var options = {
            useEasing : true,
            useGrouping : true,
            separator : ',',
            prefix : 'PHP ',
        };
        var demo = new CountUp("myTargetElement", 0, parseInt(highestSpender.total_amount), 2.5, options);
        demo.reset();
        demo.start(function(){
            $('#winner-name').html(highestSpender.name);
            $('#winner-id').html(highestSpender.custom_id);
            $('#winner-email').html(highestSpender.email);
            $('#winner-contact').html(highestSpender.contact);
            $('#winner-address').html(highestSpender.address);
            $('#winner-dob').html(highestSpender.dob);
            // ordinal();
            button.removeAttr('disabled');
        });
    });
