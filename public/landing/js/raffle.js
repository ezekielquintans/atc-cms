$("#btn-export").click(function (e) {
            $('div#winners-table table').removeClass('table table-striped').removeAttr('class');
            $('.ordinal').removeAttr('class');
            window.open('data:application/vnd.ms-excel,' + $('#winners-table').html());
            $('div#winners-table table').addClass('table table-striped');
            e.preventDefault();
        });

        var ctr = 1;
        $('#generate-winner').click(function(){
            if($('select[name="promo"]').val() == ""){
                $('select[name="promo"]').css('border', '1px solid red');
                return false;
            }
            var winner = {};
            var button = $(this);
            button.attr('disabled', 'disabled');
            $('#myTargetElement').html('Loading...');
            $.post('kashing',{"_token": $('meta[name="req"]').attr('content'), "promo_id": $('select[name="promo"]').val()  }, function(data){
                if(data == "null"){
                    alert("No Entries Found!");
                    return false;
                }
                var winner = JSON.parse(data);
                var options = {
                    useEasing : false,
                    useGrouping : false,
                    separator : '',
                    decimal : '',
                    prefix : '',
                    suffix : '',
                    coupon: winner.coupon_no
                };
                var demo = new CountUp("myTargetElement", 0, winner.entry_id, 0, 1.5, options);
                demo.reset();
                demo.start(function(){
                    $('#winner-name').html(winner.first_name + ' ' +winner.middle_name+ ' ' + winner.last_name);
                    $('#winner-id').html(winner.custom_id);
                    $('#winner-coupon').html(winner.coupon_no);
                    $('#winner-email').html(winner.email);
                    $('#winner-contact').html(winner.contact);
                    $('#winner-address').html(winner.address);
                    $('#winner-dob').html(winner.dob);

                    var html = "";
                        html += "<tr>";
                        html += "<td class='ordinal'>"+ctr+"</td>";
                        html += "<td>"+winner.coupon_no+"</td>";
                        html += "<td>"+winner.custom_id+"</td>";
                        html += "<td>"+winner.last_name+"</td>";
                        html += "<td>"+winner.first_name+"</td>";
                        html += "<td>"+winner.middle_name+"</td>";
                        html += "<td>"+winner.email+"</td>";
                        html += "<td>"+winner.contact+"</td>";
                        html += "</tr>";

                    $('#tbl-listing').append(html);
                    $('.winners-list').removeClass('hidden');
                    // ordinal();
                    button.removeAttr('disabled');
                    ctr++;
                });
            });
        });

        function ordinal(){
            $(".ordinal").text(function (i, t) {
                i++;
                var str = i.toString().slice(-1),
                    ord = '';
                switch (str) {
                    case '1':
                        ord = 'st';
                        break;
                    case '2':
                        ord = 'nd';
                        break;
                    case '3':
                        ord = 'rd';
                        break;
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                    case '0':
                        ord = 'th';
                        break;
                }
                return i + ord;
            });
        }